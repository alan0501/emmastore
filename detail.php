<?php
include_once("_config.php");

// $rows_banner = class_banner::getList();
// $rows_ad = class_ad::getList();
// $rows_ptype1 = class_product::getType1();
// $rows_top_product = class_product::getIndexTopProduct();
// $rows_index_product = class_product::getIndexProduct();
getCartInfo();

$rows_footer = class_footer::getList();

$type = get("type"); //1.網站使用條款 2.隱私權政策 3.聯絡我們
$content = isset($rows_footer[$colname_ft['use']])?$rows_footer[$colname_ft['use']]:"";
$label = "網站使用條款";
switch($type){
    case '1': //網站使用條款
        $label = "網站使用條款";
        $content = isset($rows_footer[$colname_ft['use']])?$rows_footer[$colname_ft['use']]:"";
        break;
    case '2': //隱私權政策
        $label = "隱私權政策";
        $content = isset($rows_footer[$colname_ft['privacy']])?$rows_footer[$colname_ft['privacy']]:"";
        break;
    case '3': //聯絡我們
        $label = "聯絡我們";
        $content = isset($rows_footer[$colname_ft['contact']])?$rows_footer[$colname_ft['contact']]:"";
        break;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta name="description" content="<?php echo $description; ?>" />
        <meta name="keywords" content="<?php echo $keywords; ?>" />
        <meta name="author" content="<?php echo $author; ?>" />
        <meta name="copyright" content="<?php echo $copyright; ?>" />
        <title><?php echo $web_name; ?></title>
        <link rel="shortcut icon" href="<?php echo $favicon; ?>" />
        <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
        <link href="css/reset.css" rel="stylesheet" type="text/css" />
        <link href="css/css.css" rel="stylesheet" type="text/css" />
        <link href="css/css_678.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.min.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
        <script type="text/javascript" src="js/slider.js"></script>
        <script src="js/site.js"></script>
        <script src="scripts/jquery.validate.js"></script>
        <script src="scripts/bootstrap.min.js"></script>
        <script src="scripts/coder_member.js"></script>
        <script src="scripts/member_comm.js"></script>
        <script src="scripts/shoppingcar.js"></script>
        <script src="scripts/order_comm.js"></script>
        <script src="scripts/jquery.infinitescroll.min.js"></script>
        <script src="scripts/jquery.masonry.min.js"></script>
        <script type="text/javascript">
        $(function() {
            var yan = new SellerScroll({lButton: "left_scroll",rButton: "right_scroll",oList: "slideshow_ul",showSum: 1, itemCount:1, showPage: 1});
        });
        </script>
    </head>
    <body>
        <div id="header" class="clearfix">
            <!--說是mobile用，但我不確定是否有在用-->
            <div id="m_nemu" >
                <?php include_once($inc_path."page/m_menu.php"); ?>
                <div class="mm_bg"></div>
            </div>
            <!--畫面最上方區塊-->
            <div id="top">
                <!--左上角的Q點logo，可連結至首頁-->
                <div id="top_left" class="clearfix">
                    <div  class="m_sub"></div>
                    <div class="wrap-logo"><a href="index.html"><img src="images/logo.jpg"/></a></div>
                    <div class="wrap-mame"><a href="index.html">Q點購物中心</a></div>
                    <div class="m_login">
                        <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                        <?php
                        if(!isLogin()){
                        ?>
                        <div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
                        <?php
                            }else{
                                $member_id = $_SESSION["session_925_id"];
                        ?>
                        <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
                        <?php
                            }
                        ?>
                    </div>
                </div>
                <!--中間的搜尋-->
                <div class="top-search">
                    <form name="search_form" id="search_form" action="search.html" method="post">
                        <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                        <input name="search" type="button" class="search_btn" id="search" value="送出"  />
                    </form>
                </div>
                <!--右邊的登入/登出/購物車-->
                <div id="top_right"> 
                    <!--top_cart star-->
                    <?php include_once($inc_path."page/top_cart.php"); ?>
                    <!--top_cart end-->
                    <?php
                        if(!isLogin()){
                    ?>
                    <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
                    <?php
                        }else{
                            $member_id = $_SESSION["session_925_id"];
                    ?>
                    <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
            
        <div id="wrap" >
            <div id="wrapper">
                <!--標題-->
                <div id="wrap-news">
                    <a href="index.html">首頁</a> &gt; 
                    <a href="#"><?php echo $label ?></a>
                </div>

                <!--主要內容-->
                <div id="wrap-main">
                    <div class="main_column1 clearfix">
                        <p>
                            <?php echo $content ?>
                        </p>
                    </div>                    
                </div>

                <div id="wrap-btm" style="text-align: center;">
                    <span>
                        <a href="javascript:history.back()">回上一頁</a>
                    </span>
                </div>
            </div>
            <?php include_once($inc_path."page/footer.php"); ?>
        </div>
    </body>
</html>
<?php
$db -> close();
?>
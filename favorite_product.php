<?php
include_once("_config.php");

if(!isLogin()){
	script("您已經登出系統，請重新登入!!", "index.html");
}else{
	$member_id = $_SESSION["session_925_id"];
}

$row_member = class_member::getInfo($member_id);
if($row_member){
	$acc = $row_member[coderDBConf::$col_member["account"]];
	$name = $row_member[coderDBConf::$col_member["name"]];
	$fb_id = $row_member[coderDBConf::$col_member["fb_id"]];
	$phone = $row_member[coderDBConf::$col_member["phone"]];
	$email = $row_member[coderDBConf::$col_member["email"]];
	$address = $row_member[coderDBConf::$col_member["address"]];
	$pwd = $row_member[coderDBConf::$col_member["password"]];
}

$ptype2_id = get("ptype2_id", 1);
$total = "";
$rows_fproduct = class_product::getFProduct($member_id, $ptype2_id);
if(count($rows_fproduct) > 0){
	$total = $rows_fproduct[0]["total"];
}

$rows_fptype2 = class_product::getFPtype2($member_id);

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix">
	<!--mobile menu star-->
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <!--mobile menu end-->
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">Q點購物中心</a></div>
            <div class="m_login">
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
				<div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}else{
				?>
				<div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
                
            </form>
        </div>
        <div id="top_right"> 
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
			?>
            <div class="top_login"><a href="member.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class="gray ">
    <div id="wrapper">
        <div id="member_menu">
            <dl class="clearfix" id="smbar">
                <dt><img src="images/member.jpg" align="absmiddle"/></dt>
                <dt class="email"><?php echo $acc; ?><br />
                    <span> <?php echo $email; ?> </span></dt>
                <dd><a href="cart.html">我的購物車</a></dd>
                <dd ><a href="bought.html">已買到的商品</a></dd>
                <dd class="selected"><a href="favorite_product.html">收藏的商品</a></dd>
                <dd><a href="favorite_store.html">收藏的店家</a></dd>
                <dd  ><a href="gold.html">購物金</a></dd>
                <dd><a href="member.html">會員管理</a></dd>
            </dl>
        </div>
        <div id="wrap-main" class="clearfix">
            <div id="column_left">
                <ul class="category_list">
                    <h3>收藏的商品</h3>
                    <li class="selected"><a href="favorite_product.html">全部商品(<?php echo $total; ?>)</a></li>
                    <?php
						foreach($rows_fptype2 as $row_fptype2){
					?>
                    <li id="<?php echo $row_fptype2[coderDBConf::$col_ptype2["id"]]; ?>"><a href="favorite_product_<?php echo $row_fptype2[coderDBConf::$col_ptype2["id"]]; ?>.html"><?php echo $row_fptype2[coderDBConf::$col_ptype2["name"]]; ?>(<?php echo $row_fptype2["t"]; ?>)</a></li>
                    <?php
						}
					?>
                    <!--<li><a href="#">花器/花藝/植栽(4)</a></li>
                    <li><a href="#"></a></li>-->
                </ul>
            </div>
            <div id="column_right">
                <div >
                    <select name="select" id="select" class="right_search">
                    	<option value="">全部商品(<?php echo $total; ?>)</option>
                        <?php
							foreach($rows_fptype2 as $row_fptype2){
						?>
                        <option value="<?php echo $row_fptype2[coderDBConf::$col_ptype2["id"]]; ?>"><?php echo $row_fptype2[coderDBConf::$col_ptype2["name"]]; ?>(<?php echo $row_fptype2["t"]; ?>)</option>
                        <?php
							}
						?>
                        <!--<option value="花器/花藝/植栽(4)">花器/花藝/植栽(4)(200)</option>
                        <option value="咖啡杯/馬克杯(1)">咖啡杯/馬克杯(1)</option>-->
                    </select>
                </div>
                <ul class="smail_p">
                	<?php
						if($total > 0){
							foreach($rows_fproduct as $row_fproduct){
					?>
                    <li><a href="prduct_info_<?php echo $row_fproduct[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_fproduct[coderDBConf::$col_ptype2["ptype1_id"]]; ?>.html"><img src="<?php echo $web_path_product."b".$row_fproduct[coderDBConf::$col_product["pic"]]; ?>" width="226" height="226" /></a>
                        <div class="prd_name"><a href="prduct_info_<?php echo $row_fproduct[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_fproduct[coderDBConf::$col_ptype2["ptype1_id"]]; ?>.html"><?php echo $row_fproduct[coderDBConf::$col_product["name"]]; ?></a></div>
                        <div class="price">NT$ <?php echo $row_fproduct[coderDBConf::$col_product["price"]]; ?><span class="fp_del_btn" fp_id="<?php echo $row_fproduct[coderDBConf::$col_fproduct["id"]]; ?>">刪除</span></div>
                    </li>
                    <?php
							}
						}else{
							echo("<li style='font-size:16px;'>您尚未收藏商品!</li>");
						}
					?>
                    <!--<li><a href="prduct_info.html"><img src="images/p226.jpg" width="226" height="226" /></a>
                        <div class="prd_name"><a href="prduct_info.html">Artificer  創意能量手環/項鍊</a></div>
                        <div class="price">NT$ 199</div>
                    </li>
                    <li><a href="prduct_info.html"><img src="images/p226.jpg" width="226" height="226" /></a>
                        <div class="prd_name"><a href="prduct_info.html">Artificer  創意能量手環/項鍊</a></div>
                        <div class="price">NT$ 199</div>
                    </li>
                    <li><a href="prduct_info.html"><img src="images/p226.jpg" width="226" height="226" /></a>
                        <div class="prd_name"><a href="prduct_info.html">Artificer  創意能量手環/項鍊</a></div>
                        <div class="price">NT$ 199</div>
                    </li>
                    <li><a href="prduct_info.html"><img src="images/p226.jpg" width="226" height="226" /></a>
                        <div class="prd_name"><a href="prduct_info.html">Artificer  創意能量手環/項鍊</a></div>
                        <div class="price">NT$ 199</div>
                    </li>-->
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end-->
<script>
carItemShow();
$(document).ready(function(e) {
    var type2_id = "<?php echo $ptype2_id; ?>";
	if(type2_id != ""){
		$("ul.category_list li").each(function(index, element) {
            var _type2_id = $(this).attr("id");
			if(type2_id == _type2_id){
				$(this).siblings().removeClass("selected");
				$(this).addClass("selected");
			}
        });
	}else{
		$("ul.category_list li:first").addClass("selected");
	}
});
</script>
</body>
</html>
<?php
$db -> close();
?>
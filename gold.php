<?php
include_once("_config.php");

if(!isLogin()){
	script("您已經登出系統，請重新登入!!", "index.html");
}else{
	$member_id = $_SESSION["session_925_id"];
}

$row_member = class_member::getInfo($member_id);
if($row_member){
	$acc = $row_member[coderDBConf::$col_member["account"]];
	$name = $row_member[coderDBConf::$col_member["name"]];
	$fb_id = $row_member[coderDBConf::$col_member["fb_id"]];
	$phone = $row_member[coderDBConf::$col_member["phone"]];
	$email = $row_member[coderDBConf::$col_member["email"]];
	$address = $row_member[coderDBConf::$col_member["address"]];
	$pwd = $row_member[coderDBConf::$col_member["password"]];
}

$rows_bonus = class_bonus::getAllBonusList($member_id);
$row_all = class_bonus::getTotalBonus($member_id);

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix">
    <!--mobile menu star-->
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <!--mobile menu end-->
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">Q點購物中心</a></div>
            <div class="m_login">
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
				<div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}else{
				?>
				<div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
                
            </form>
        </div>
        <div id="top_right">
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
					$member_id = $_SESSION["session_925_id"];
			?>
            <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class="gray ">
    <div id="wrapper">
        <div id="member_menu">
            <dl class="clearfix" id="smbar">
                <dt><img src="images/member.jpg" align="absmiddle"/></dt>
                <dt class="email"><?php echo $acc; ?><br />
                    <span> <?php echo $email; ?> </span></dt>
                <dd><a href="cart.html">我的購物車</a></dd>
                <dd ><a href="bought.html">已買到的商品</a></dd>
                <dd><a href="favorite_product.html">收藏的商品</a></dd>
                <dd><a href="favorite_store.html">收藏的店家</a></dd>
                <dd  class="selected"><a href="gold.html">購物金</a></dd>
                <dd><a href="member.html">會員管理</a></dd>
            </dl>
        </div>
        <div id="wrap-main" class="clearfix">
            <div id="column_left">
                <ul class="category_list">
                    <h3>購物金</h3>
                    <li>總共累積紅利</li>
                    <li class="gold_mum">$<?php echo ($row_all["c"] > 0) ? $row_all["c"] : 0; ?></li>
                    <li><a href="javascript:void(0);" class="change_bonus_btn" all_bonus="<?php echo ($row_all["c"] > 0) ? $row_all["c"] : 0; ?>"><img src="images/gold.png" /></a></li>
                </ul>
            </div>
            <div id="column_right">
                <dl class="gold_title clearfix">
                    <dt>總共累積紅利</dt>
                    <dd>$<?php echo ($row_all["c"] > 0) ? $row_all["c"] : 0; ?></dd>
                </dl>
                <div class="gold_btn"><a href="javascript:void(0);" class="change_bonus_btn" all_bonus="<?php echo ($row_all["c"] > 0) ? $row_all["c"] : 0; ?>"><img src="images/gold_btn.jpg" /></a></div>
                <div class="gold">
                    <ul class="clearfix">
                        <li class="grid1 tbhead">紅利計算</li>
                        <li class="grid2 tbhead">購物金兌換/使用記錄</li>
                        <li class="grid3 tbhead">日期</li>
                    </ul>
                    <?php
					foreach($rows_bonus as $row_bonus){
					?>
                    <ul class="clearfix">
                        <li class="grid1"><?php echo ($row_bonus["type"] == 1) ? "+" : "-"; ?><?php echo $row_bonus["bonus"]; ?></li>
                        <li class="grid2"><?php echo $row_bonus["comment"]; ?></li>
                        <li class="grid3"><?php echo date("Y/m/d", strtotime($row_bonus["create_time"])); ?></li>
                    </ul>
                    <?php
					}
					?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end-->
<script>
carItemShow();
</script>
</div>
</body>
</html>
<?php
$db -> close();
?>
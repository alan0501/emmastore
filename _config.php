<?php
$inc_path = "inc/";

include_once($inc_path."_config.php");
include_once($inc_path."_web_func.php");
include_once($inc_path."_cache.php");
$cache_path = $web_path_cache;
$ck_path = 'src="'.$web_path_ckeditor;

$db = Database::DB();
$pagefilename = request_basename();

$colname_order = coderDBConf::$col_order;
$colname_ft = coderDBConf::$col_footer;
/*****END PHP*****/
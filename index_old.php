<?php
include_once("_config.php");

$rows_banner = class_banner::getList();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="js/slider.js"></script>
<script src="js/site.js"></script>
<script type="text/javascript">
$(function() {
	var yan = new SellerScroll({lButton: "left_scroll", rButton: "right_scroll", oList: "slideshow_ul", showSum: 1, itemCount:1, showPage: 1});
});
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix">
	<!--mobile menu star-->
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <!--mobile menu end-->
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">購物中心</a></div>
            <div class="m_login">
                <div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
            </div>
        </div>
        <div class="top-search">
            <form action="search.html">
                <input type="text" value=""  autocomplete="off" placeholder="商品名稱" class="search-keywords"/>
                <input name="search" type="submit" class="search_btn" id="search" value="送出"  />
            </form>
        </div>
        <div id="top_right">
        	<!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
        </div>
    </div>
</div>
<div id="wrap" >
    <div id="wrapper">
        <div id="wrap-menu">
            <ul class="clearfix">
                <li class="menu1"><a href="index.html" class="current">&nbsp;</a></li>
                <li class="menu2"><a href="category.html">&nbsp;</a></li>
                <li class="menu3"><a href="category.html">&nbsp;</a></li>
                <li class="menu4"><a href="category.html">&nbsp;</a></li>
                <li class="menu5"><a href="category.html">&nbsp;</a></li>
                <li class="menu6"><a href="category.html">&nbsp;</a></li>
                <li class="menu7"><a href="category.html">&nbsp;</a></li>
                <li class="menu8"><a href="category.html">&nbsp;</a></li>
                <li class="menu9"><a href="category.html">&nbsp;</a></li>
            </ul>
        </div>
        <div id="wrap-news">新開幕！所有產品不分類別，全面優惠中。</div>
        <div id="wrap-main">
            <div class="main_column1 clearfix">
                <div id="banner">
                    <div class="prev" id="left_scroll"><img src="images/prev.png" width="28" height="51" /></div>
                    <div class="next" id="right_scroll"><img src="images/next.png" width="28" height="51" /></div>
                    <ul class="banner-circle clearfix" id="tab">
                    	<?php
							for($i = 0; $i < count($rows_banner); $i ++){
						?>
                        <li class='<?php echo ($i == 0) ? "selected" : ""; ?>'><a href='#'></a></li>
                        <?php
							}
						?>
                    </ul>
                    <ul id="slideshow_ul">
                    	<?php
							foreach($rows_banner as $row_banner){
						?>
                        <li><img src="<?php echo $web_path_banner."m".$row_banner[coderDBConf::$col_banner["pic"]]; ?>" /></li>
                        <?php
							}
						?>
                    </ul>
                </div>
                <div id="banner_small">
                    <ul class="clearfix">
                        <li><a href="#"><img src="images/b1.jpg"/></a> </li>
                        <li><a href="#"><img src="images/b2.jpg"/></a> </li>
                    </ul>
                    <ul class="clearfix">
                        <li><a href="#"><img src="images/b3.jpg"/></a> </li>
                        <li><a href="#"><img src="images/b4.jpg"/></a> </li>
                    </ul>
                </div>
            </div>
            <div class="main_column2">
                <div class="big_list">
                    <ul class="clearfix">
                        <li><a href="prduct_info.html"><img src="images/p1.jpg" border="0"  /></a>
                            <p><a href="prduct_info.html">Artificer FLORALS 創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                        <li><a href="prduct_info.html"><img src="images/p1.jpg" border="0"  /></a>
                            <p><a href="prduct_info.html">Artificer FLORALS 創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                        <li ><a href="prduct_info.html"><img src="images/p1.jpg" border="0"  /></a>
                            <p><a href="prduct_info.html">Artificer FLORALS 創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                    </ul>
                </div>
                <div class="smail_list">
                    <ul class="clearfix">
                        <li><a href="prduct_info.html"><img src="images/p2.jpg" border="0"  /></a>
                            <p><a href="prduct_info.html">Artificer  創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                        <li><a href="prduct_info.html"><img src="images/p3.jpg" border="0"  /></a>
                            <p><a href="#">Artificer  創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                        <li><a href="prduct_info.html"><img src="images/p4.jpg" border="0"  /></a>
                            <p><a href="#">Artificer  創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                        <li><a href="prduct_info.html"><img src="images/p2.jpg" border="0"  /></a>
                            <p><a href="#">Artificer  創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                        <li><a href="prduct_info.html"><img src="images/p2.jpg" border="0"  /></a>
                            <p><a href="#">Artificer  創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                        <li><a href="prduct_info.html"><img src="images/p2.jpg" border="0"  /></a>
                            <p><a href="prduct_info.html">Artificer  創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                        <li><a href="prduct_info.html"><img src="images/p2.jpg" border="0"  /></a>
                            <p><a href="#">Artificer  創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                        <li><a href="prduct_info.html"><img src="images/p2.jpg" border="0"  /></a>
                            <p><a href="#">Artificer  創意能量手環/項鍊</a></p>
                            <h3>NT$ 199</h3>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--pc footer star-->
    <?php include_once($inc_path."page/footer.php"); ?>
    <!--pc footer end-->
</div>
</body>
</html>

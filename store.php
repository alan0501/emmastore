<?php
include_once("_config.php");

$rows_ptype1 = class_product::getType1();

$shop_id = get("shop_id");
if($shop_id < 0){
	script("資料傳送錯誤!");
}

$rows_shop_product = class_product::getShopProduct($shop_id);
if($rows_shop_product){
	// $shop_id = $rows_shop_product[0][coderDBConf::$col_admin["id"]];
	// $shop_name = $rows_shop_product[0][coderDBConf::$col_admin["name"]];
	// $shop_logo = $rows_shop_product[0][coderDBConf::$col_admin["spic"]];
    $shop_id = $rows_shop_product[0][coderDBConf::$col_store["id"]];
    $shop_name = $rows_shop_product[0][coderDBConf::$col_store["name"]];
    $shop_logo = $rows_shop_product[0][coderDBConf::$col_store["spic"]];
}

//var_dump($rows_shop_product[0]);

$rows_hot = class_product::getHotProduct($shop_id);

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix">
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">購物中心</a></div>
            <div class="m_login">
                <div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
                
            </form>
        </div>
        <div id="top_right"> 
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
					$member_id = $_SESSION["session_925_id"];
			?>
            <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class=" white">
    <div id="wrapper">
        <div id="wrap-menu">
            <?php include_once($inc_path."page/menu.php"); ?>
        </div>
        <div id="wrap-main" class="clearfix">
            <div id="column_left">
                <div class="store_logo"><img src="<?php echo /*$web_path_admin*/$web_path_store."s".$shop_logo; ?>"/>
                    <div class="store_name"><?php echo $shop_name; ?></div>
                    <div class="favorite_sbtn" s_id="<?php echo $shop_id; ?>" m_id="<?php echo isset($_SESSION['session_925_id']) ? $_SESSION['session_925_id'] : ""; ?>"><a href="javascript:void(0);"><img src="images/storeup.png" /></a></div>
                </div>
                <div class="store_hot">
                    <h3>本店熱門商品</h3>
                    <?php
						foreach($rows_hot as $row_hot){
					?>
                    <dl class="clearfix">
                        <dt><a href="prduct_info_<?php echo $row_hot[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_hot[coderDBConf::$col_ptype1["id"]]; ?>.html"><img src="<?php echo $web_path_product."s".$row_hot[coderDBConf::$col_product["pic"]]; ?>" /></a></dt>
                        <dd><a href="prduct_info_<?php echo $row_hot[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_hot[coderDBConf::$col_ptype1["id"]]; ?>.html"> <?php echo $row_hot[coderDBConf::$col_product["name"]]; ?> </a>
                            <div class="price">NT$ <?php echo $row_hot[coderDBConf::$col_product["price"]]; ?></div>
                        </dd>
                    </dl>
                    <?php
						}
					?>
                </div>
            </div>
            <div id="column_right">
                <div id="m_store_logo" class="clearfix"> <img src="<?php echo $web_path_admin."s".$shop_logo; ?>" class="m_logo" />
                    <p><?php echo $shop_name; ?></p>
                </div>
                <ul class="big_p">
                    <?php
						$p = 1;
						foreach($rows_shop_product as $row_shop_product){
					?>
                    <li><a href="prduct_info_<?php echo $row_shop_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_shop_product[coderDBConf::$col_ptype1["id"]]; ?>.html"><img src="<?php echo $web_path_product."b".$row_shop_product[coderDBConf::$col_product["pic"]]; ?>" width="<?php echo ($p >= 3) ? 225 : ""; ?>" height="<?php echo ($p == 3) ? 225 : ""; ?>" /></a>
                        <div class="prd_name"><a href="prduct_info_<?php echo $row_shop_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_shop_product[coderDBConf::$col_ptype1["id"]]; ?>.html"><?php echo $row_shop_product[coderDBConf::$col_product["name"]]; ?></a></div>
                        <div class="price">NT$ <?php echo $row_shop_product[coderDBConf::$col_product["price"]]; ?></div>
                    </li>
                    <?php
							if($p == 2){
								echo('</ul><ul class="smail_p">');
							}
							$p ++;
						}
					?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end-->
<script>
carItemShow();
</script>
</body>
</html>
<?php
$db -> close();
?>
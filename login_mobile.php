<?php
include_once("_config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/fb.js"></script>
<script>
var api_id = '<?php echo $fb_appid; ?>';
var fb_uid = "";
var fb_session = null; 
var fb_me = null;
var fb_token = "";
var fblogin = false; //判斷FB是否登入
var req_perms = 'email, public_profile'; //要FB權限
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body style="background:#F1F3F2;">
<div id="site_login">
    <div class="login_form">
        <div class="login_logo"><a href="index.html"><img src="images/logo.jpg" border="0"  /></a></div>
        <div class="login_fb"><a href="javascript:void(0);" class="fb_login_btn"><img src="images/f_login.png"  /></a></div>
        <div class="login_acc">或使用9好買帳號登入</div>
        <div class="login_form">
            <form id="member_login_form">
            	<input name="path" type="hidden" value="p" id="path" />
                <input id="userName" type="text" name="userName" class="input-userName" autocomplete="off" placeholder="9好買帳號">
                <input id="password" type="password" name="password" class="input-password" autocomplete="off" placeholder="密碼">
                <div class="input-button login_btn"><img src="images/login.png" /></div>
                <dl class="pass-reglink clearfix">
                    <dt><a href="#">忘記密碼</a></dt>
                    <dd><a href="register_mobile.html">註冊新帳號</a></dd>
                </dl>
            </form>
        </div>
    </div>
</div>
</body>
</html>

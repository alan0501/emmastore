<?php
include_once("_config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix">
    <div id="m_nemu" >
        <div class="mm_txt">
            <div class="mbm">
                <h3><a href="member.html"><img src="images/m.jpg" border="0" align="absmiddle" /></a><span><a href="#" class="m_close"><img src="images/header_close.jpg" border="0" /></a></span></h3>
                <ul class="clearfix">
                    <li class="m_menu1"><a href="index.html">&nbsp;</a></li>
                    <li class="m_menu2"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu3"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu4"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu5"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu6"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu7"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu8"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu9"><a href="category.html">&nbsp;</a></li>
                </ul>
            </div>
            <div id="m_footer"><a href="#">網站使用條款</a><br />
                <a href="#">隱私權政策</a><br />
                <a href="#">免責條款</a><br />
                © 2014-2015 kagin. All Rights Reserved </div>
        </div>
        <div class="mm_bg"></div>
    </div>
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">購物中心</a></div>
            <div class="m_login">
                <div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
            </div>
        </div>
        <div class="top-search">
            <form action="search.html">
                <input type="text" value=""  autocomplete="off" placeholder="商品名稱" class="search-keywords"/>
                <input name="search" type="submit" class="search_btn" id="search" value="送出"  />
            </form>
        </div>
        <div id="top_right">
            <div class="top_cart">
                <div class="car_icon"><a href="cart.html"><img src="images/cart.png" width="27" height="27" border="0"  class="car_icon"/></a></div>
                <div class="car_date">
                    <h3>購物車</h3>
                    <h4>10個商品</h4>
                </div>
                <div class="top_cart_list" style="display: none" >
                    <ul >
                        <li class="clearfix">
                            <div class="cart_img"><a href="#"><img src="images/p55.jpg" width="56" height="56" /></a></div>
                            <div class="cart_product"><span class="pd_name"><a href="#">健康襪子</a></span><br />
                                <span class="cart_price">$99</span><br />
                                <span class="cart_del"><a href="#">刪除</a></span></div>
                        </li>
                        <li class="clearfix">
                            <div class="cart_img"><a href="#"><img src="images/p55.jpg" width="56" height="56" /></a></div>
                            <div class="cart_product"><span class="pd_name"><a href="#">健康襪子</a></span><br />
                                <span class="cart_price">$99</span><br />
                                <span class="cart_del"><a href="#">刪除</a></span></div>
                        </li>
                        <li class="clearfix">
                            <div class="cart_img"><a href="#"><img src="images/p55.jpg" width="56" height="56" /></a></div>
                            <div class="cart_product"><span class="pd_name"><a href="#">健康襪子</a></span><br />
                                <span class="cart_price">$99</span><br />
                                <span class="cart_del"><a href="#">刪除</a></span></div>
                        </li>
                        <li class="clearfix">
                            <div class="cart_img"><a href="#"><img src="images/p55.jpg" width="56" height="56" /></a></div>
                            <div class="cart_product"><span class="pd_name"><a href="#">健康襪子</a></span><br />
                                <span class="cart_price">$99</span><br />
                                <span class="cart_del"><a href="#">刪除</a></span></div>
                        </li>
                    </ul>
                    <div class="topcart_pcs">共有10件商品</div>
                </div>
            </div>
            <div class="top_login"><a href="#" class="login_lb">登入</a> | <a href="#" class="reg_lb">註冊</a></div>
        </div>
    </div>
</div>
<div id="wrap" >
    <div id="wrapper">
        <div id="wrap-menu">
            <ul class="clearfix">
                <li class="menu1"><a href="index.html" >&nbsp;</a></li>
                <li class="menu2"><a href="category.html">&nbsp;</a></li>
                <li class="menu3"><a href="category.html">&nbsp;</a></li>
                <li class="menu4"><a href="category.html">&nbsp;</a></li>
                <li class="menu5"><a href="category.html">&nbsp;</a></li>
                <li class="menu6"><a href="category.html">&nbsp;</a></li>
                <li class="menu7"><a href="category.html">&nbsp;</a></li>
                <li class="menu8"><a href="category.html">&nbsp;</a></li>
                <li class="menu9"><a href="category.html">&nbsp;</a></li>
            </ul>
        </div>
        <div id="wrap-main">
            <div class="merchants  clearfix">
                <div class="merchants_dsc">
                    <div class="merchants_t1">歡迎加入瘋狂賣客的服務行列！ </div>
                    <div class="merchants_t2" >我們尋找符合以下條件的的供應商 ：</div>
                    <div class="merchants_t2" >可以提供優質、酷、特別的商品，品質要好。 商品成本必須是市場上最低價（要比拍賣還低）。
                        有大量商品需要一次出清，保固較短 、整新品、庫存品出清不拘。
                        如果你有商品符合以上的條件，請在下方選擇類別後提供我們更詳細的資訊 :</div>
                    <div class="merchants_t2" ><img src="images/contact.png" /></div>
                </div>
                <div class="merchants_right">我是優質供應廠商，<br />
                    我想成為9好賣的合作夥伴，<br />
                    要怎麼做？</div>
            </div>
        </div>
    </div>
</div>
<div id="footer"><a href="#">網站使用條款</a> | <a href="#">隱私權政策</a> | <a href="#">免責條款</a><br />
    © 2014-2015 kagin. All Rights Reserved </div>
</body>
</html>

-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1:3306
-- 產生時間： 2019-06-17 10:09:53
-- 伺服器版本: 5.7.24
-- PHP 版本： 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- 資料庫： `love`
--

-- --------------------------------------------------------

--
-- 資料表結構 `925_footer`
--

DROP TABLE IF EXISTS `925_footer`;
CREATE TABLE IF NOT EXISTS `925_footer` (
  `ft_id` int(11) NOT NULL,
  `ft_use` text COLLATE utf8_unicode_ci NOT NULL COMMENT '網站使用條款',
  `ft_privacy` text COLLATE utf8_unicode_ci NOT NULL COMMENT '隱私權政策',
  `ft_contact` text COLLATE utf8_unicode_ci NOT NULL COMMENT '聯絡我們',
  `ft_admin` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ft_updatetime` datetime NOT NULL,
  `ft_createtime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `925_footer`
--

INSERT INTO `925_footer` (`ft_id`, `ft_use`, `ft_privacy`, `ft_contact`, `ft_admin`, `ft_updatetime`, `ft_createtime`) VALUES
(1, '', '', '', '', '2019-06-17 00:00:00', '2019-06-17 00:00:00');
COMMIT;

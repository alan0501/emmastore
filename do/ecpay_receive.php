<?php /*接收綠界回傳的訊息*/
include_once("_config.php");
require_once 'ECPay.Payment.Integration.php';

//測試站-------------
$merchant_id = "2000132"; //特店編號
$hash_key = "5294y06JbISpM5x9"; //ALL IN ONE 介接 HashKey
$hash_iv = "v77hoKGq4kWxNNIS"; //ALL IN ONE 介接 HashIV
//正式站-------------
//$merchant_id = "3117800"; //特店編號
//$hash_key = "P9TpcK6gtl1jhyZp"; //ALL IN ONE 介接 HashKey
//$hash_iv = "UA2bsb8vdckxn6K7"; //ALL IN ONE 介接 HashIV

define('ECPay_MerchantID', $merchant_id);
define('ECPay_HashKey', $hash_key);
define('ECPay_HashIV', $hash_iv);

//重新整理回傳參數。
$arParameters = $_POST;
foreach ($arParameters as $keys => $value) {
    if ($keys != 'CheckMacValue') {
        if ($keys == 'PaymentType') {
            $value = str_replace('_CVS', '', $value);
            $value = str_replace('_BARCODE', '', $value);
            $value = str_replace('_CreditCard', '', $value);
        }
        if ($keys == 'PeriodType') {
            $value = str_replace('Y', 'Year', $value);
            $value = str_replace('M', 'Month', $value);
            $value = str_replace('D', 'Day', $value);
        }
        $arFeedback[$keys] = $value;
    }
}

//計算出 CheckMacValue
$CheckMacValue = ECPay_CheckMacValue::generate( $arParameters, ECPay_HashKey, ECPay_HashIV );

//必須要支付成功並且驗證碼正確
if($_POST['RtnCode']=='1'){
    //要處理的程式放在這裡，例如將線上服務啟用、更新訂單資料庫付款資訊等
    $db = Database::DB();

    $order_sno = $_POST['MerchantTradeNo'];

    //更新訂單資料--------------
    $sql = "update $table_order 
               SET ".$colname_order["pay_time"]." = :paytime, 
                   ".$colname_order["payment_state"]." = :paymentstate, 
                   ".$colname_order["update_time"]." = :updatetime
             WHERE ".$colname_order["sno"]." = :ordersno";
    $val = array(':paytime'=>request_cd(), ':paymentstate'=>1, ':updatetime'=>request_cd(), ':ordersno'=>$order_sno);
    $db -> execute($sql, $val);

    //新增付費資料--------------
    $sql = "insert INTO $table_p_d(".
                $colname_p_d['sno'].", ".
                $colname_p_d['trade_no'].", ".
                $colname_p_d['payment_date'].", ".
                $colname_p_d['payment_type'].", ".
                $colname_p_d['trade_date'].")
            VALUES(:sno, :tradeno, :paymentdate, :paymenttype, :tradedate)";
    $val = array(
        ':sno'=>$arFeedback['MerchantTradeNo'], 
        ':tradeno'=>$arFeedback['TradeNo'], 
        ':paymentdate'=>$arFeedback['PaymentDate'], 
        ':paymenttype'=>$arFeedback['PaymentType'], 
        ':tradedate'=>$arFeedback['TradeDate']);
    $db -> execute($sql, $val);

    $db -> close();
}

//接收到資訊回應綠界
echo '1|OK';
?>
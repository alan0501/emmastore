<?php
include_once('_config.php');

$sResult = 0;

$m_id = post("m_id", 1);
$p_id = post("p_id", 1);

$msg = "";
$has = 0;

$sResult = isNull($m_id, "會員ID", 1, 255) ;
if($sResult){$sResult = isNull($p_id, "商品ID", 1, 255);}
if($sResult){
	
	$db = new Database($HS, $ID, $PW, $DB);
	$db -> connect();	
	
	$row = $db -> query_first("SELECT * FROM ".coderDBConf::$fproduct." WHERE ".coderDBConf::$col_fproduct["member_id"]." = '$m_id' AND ".coderDBConf::$col_fproduct["product_id"]." = '$p_id'");

	if($row){
		$sResult = 1;
		$msg = "此商品已在收藏列表!請去會員中心查看!";
		$has = 1;
	}else{
		$data[coderDBConf::$col_fproduct["member_id"]] = $m_id;
		$data[coderDBConf::$col_fproduct["product_id"]] = $p_id;
		$data[coderDBConf::$col_fproduct["create_time"]] = request_cd();
			
		$id = $db -> query_insert(coderDBConf::$fproduct, $data);
		if($id > 0){
			$sResult = 1;
			$msg = "已收藏此商品!請去會員中心查看!";
		}else{
			$msg = "資料傳輸錯誤!請再試一次!";
		}
	}
	
	$db -> close();
	
}else{
	//$msg = "資料傳輸錯誤!請再試一次!";
	$msg = $str_message;
}

$re["sResult"] = ($sResult == 1) ? true : false;
$re["msg"] = $msg;
$re["has"] = ($has == 1) ? true : false;

echo json_encode($re);

/*****END PHP*****/
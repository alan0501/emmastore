<?php
include_once('_config.php');
include_once($inc_path.'_cache.php');
include_once($inc_path.'lib/_shoppingcar.php');


$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();

$shop_id = post('shop_id', 1);
if($shop_id < 0){
	script("資料傳輸錯誤!");
}

if(isset($_SESSION["car"]) && is_array(unserialize($_SESSION["car"]))){
	$car = unserialize($_SESSION["car"]);
}else{
	$car = array();
}
if(!isset($car[$shop_id]) || !is_object($car[$shop_id])){
	$car[$shop_id] = new shoppingCar($shop_id);
}



//$_SESSION["car"] = serialize($car);
//print_r($car);

$method = post('method', 1);
$result = 0;
$msg = "";
$cart_item = 0;
$total = "";
//$all_total = "";
$all_amount = 0;
$re = array();

if($method == "add"){
	$product_id = post('product_id', 1);
	$product_sno = post('product_sno', 1);
	$product_name = post('product_name', 1);
	$sell_price = post('sell_price', 1);
	$amount = post('amount');
	$sub_total = post('sub_total');
	$product_bonus = post('product_bonus', 1);
	$pic = post('pic', 1);
	$shop_id = post('shop_id', 1);
	$member_id = (isset($_SESSION["session_id"])) ? $_SESSION["session_id"] : "";

	if($product_id > 0 && $product_sno != ""){
		
		$item = new shoppingItem($product_id, $product_sno, $product_name, $sell_price, $amount, $sub_total, $product_bonus, $pic, $shop_id);

		$car[$shop_id] -> add($item);//加入car
		
		$car[$shop_id] -> getCarFromDB();
		$car[$shop_id] -> calculate();//計算
		
		foreach($car as $item){
			$cart_item += $item -> num; //商品數
			$total += $item -> total; //總金額
		}
		 
		$result = 1;
	
		$_SESSION["car"] = serialize($car);
		//print_r($car);
				
	}else{
		$msg = '參數資料傳送錯誤!';
	}
}

if($method == "update"){
	$sid = post('cart_id', 1);
	$shop_id = post('shop_id', 1);
	$num = post('num');


	if($sid != ""){
		$car[$shop_id] -> modify($sid, $num);
		//$car[$shop_id] -> getCarFromDB();
		//$car->checkEvent();
		$car[$shop_id] -> calculate();
		
		$num1 = $car[$shop_id] -> getNum(); //計算商品是否為空
		if($num1 <= 0){
			unset($car[$shop_id]); //刪除店家
		}
		
		foreach($car as $item){
			$cart_item += $item -> num; //商品數
			$total += $item -> total; //總金額
		}
		
		$_SESSION["car"] = serialize($car);
    
		$result = 1;		
	}else{
		$msg = "參數資料傳送錯誤!";
	}
}

if($method == "clear"){	
	
	$car -> clear();
	$result = 1;
}

if($method == "carry"){	
	$shop_id = post("shop_id", 1);
	$val = post("val", 1);
	
	if($shop_id != ""){
		
		$car[$shop_id] -> setCarry($val);
		$car[$shop_id] -> calculate();
		$_SESSION["car"] = serialize($car);
		
		$result = 1;
	}else{
		$msg = "參數資料傳送錯誤!";
	}
		
}




if($method == "chkstock"){
	$product_id = post('product_id', 1);
	$num = post('num');

	if($product_id > 0 && $num > 0){
		$row = $db -> query_first("SELECT * FROM $table_product WHERE  product_id = '$product_id'");
		$stock = $row["product_stock"];
		if($stock >= $num){
			$result = 1;
			
		}else{
			$msg = $row["product_name_tw"]."庫存不足!庫存只剩".$row["product_stock"]."!";
		}
	}else{
		$msg = "參數資料傳送錯誤!";
	}
}
$db -> close();

//echo '[{"result":"'.($result===true ? 'true' : 'false') .'","message":'.$msg.',"total":'.$total.'}]';

$re["result"] = ($result == 1) ? true : false;
$re["message"] = $msg;
$re["total"] = $total;
$re["cart_item"] = $cart_item;
$re["all_amount"] = $all_amount;

echo json_encode($re);

/*****END PHP*****/
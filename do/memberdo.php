<?php
include_once("_config.php");
include_once($inc_path."_func_smtp.php");
include_once($inc_path."lib/codermember.php");

$actiontype = post("actiontype", 1);
$sResult = false;
$sMsg = "";

$member_id = 0;

$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();


//檢查帳號是否重覆
if($actiontype == "chkAcc"){ 
	try{
		$myMember = new CoderMember();
		$data = new CoderMemberItem();
		
		$data -> account = post("account", 1);

		$myMember -> chkAcc($data);
		
		$sResult = 1;
		$sMsg = 'OK';
	}catch(Exception $e){
		$sMsg = $e -> getMessage();
	}
	
	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}

//檢查帳號是否存在
if($actiontype == "isExistAcc"){ 
	try{
		$myMember = new CoderMember();
		//$data = new CoderMemberItem();
		
		//$data -> account = post("account", 1);
		$account = post("account", 1);

		$chk = $myMember -> isExistAcc($account);
		
		$sResult = 1;
		$sMsg = 'OK';
	}catch(Exception $e){
		$sMsg = $e -> getMessage();
	}
	

	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}

//檢查email是否正確
if($actiontype == "isTrueEmail"){ 
	try{
		$myMember = new CoderMember();
		//$data = new CoderMemberItem();
		
		//$data -> account = post("account", 1);
		$account = post("account", 1);
		$email = post("email", 1);

		$myMember -> isTrueEmail($account, $email);
		
		$sResult = 1;
		$sMsg = 'OK';
	}catch(Exception $e){
		$sMsg = $e -> getMessage();
	}
	

	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}

//檢查email是否重覆
if($actiontype == "chkEmail"){ 
	try{
		$myMember = new CoderMember();
		$data = new CoderMemberItem();
		
		$data -> email = post("email", 1);

		$myMember -> chkEmail($data); //無
		
		$sResult = 1;
		$sMsg = 'OK';
	}catch(Exception $e){
		$sMsg = $e -> getMessage();
	}
	
	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}
if($actiontype == "chkEmail_reg"){ 
	try{
		$myMember = new CoderMember();
		$data = new CoderMemberItem();
		
		$data -> email = post("email", 1);
		

		$myMember -> chkEmail_reg($data); //無
		
		$sResult = 1;
		$sMsg = 'OK';
	}catch(Exception $e){
		$sMsg = $e -> getMessage();
	}
	
	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}
if($actiontype == "chkEmail_self"){ 
	try{
		$myMember = new CoderMember();
		$data = new CoderMemberItem();
		
		$data -> email = post("email", 1);
		
		$myMember -> chkEmail_self($data); //無
		
		$sResult = 1;
		$sMsg = 'OK';
	}catch(Exception $e){
		$sMsg = $e -> getMessage();
	}
	
	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}
//會員新增 	
if($actiontype == "insert"){	
	$account = post("account", 1);
	$password = post("password", 1); 
	$email = post("email", 1);
	
	$sResult = isNull($account, "帳號", 1, 20);
	$sResult = isNull($password, "密碼", 1, 40);
	$sResult = isEmail2($email, "Email");
	
	if($sResult){	
		try{
			$myMember = new CoderMember();
			$data = new CoderMemberItem();
			
			$data -> account = $account;
			$data -> password = $password;
			$data -> email = $email;
			
			$myMember -> Insert($data);
			
			$sResult = 1;
			$sMsg = 'OK';
			
		}catch(Exception $e){
			$sResult = 0;
			$sMsg = $e -> getMessage();
		}
	}else{
		$sMsg = $str_message;
	}

	$re["result"] = ($sResult == 1) ? true : false;
	$re["member_id"] = $member_id;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}

//會員登入
if($actiontype == "memberLogin"){
	$member_id = "";
	$member_account = "";
	$member_fb_id = "";
	$member_name = "";
	
	$account = post("account", 1);
	$password = post("password", 1);
	
	$sResult = isNull($account, "帳號", 1, 20);
	if($sResult){$sResult = isNull($password, "密碼", 1, 40);}
	if($sResult){	
		try{
			$myMember = new CoderMember();
			$data = new CoderMemberItem();
			
			$data -> account = $account;
			$data -> password = $password;
			
			$myMember -> memberLogin($data);
			
			$member_id = $_SESSION["session_925_id"];
			$member_account = $_SESSION["session_925_acc"];
			//$member_name = $_SESSION["session_name"];
			//$member_fb = $_SESSION["session_fb"];
			
			$sResult = 1;
			$sMsg = 'OK';
		}catch(Exception $e){
			$sResult = 0;
			$sMsg = $e -> getMessage();
		}
	}else{
		$sMsg = $str_message;
	}
	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	//$re["member_name"] = $member_name;
	$re["member_id"] = $member_id;
	$re["member_acc"] = $member_account;
	//$re["member_fb"] = $member_fb;
	
	echo json_encode($re);
}

//會員登出
if($actiontype == "memberLogout"){
	unset($_SESSION["session_925_id"]);
	//unset($_SESSION["session_name"]);
	unset($_SESSION["session_925_acc"]);
	//unset($_SESSION["session_fb"]);
	//session_unset();
	//session_destroy();
	if(isset($_SESSION["car"]) && is_array(unserialize($_SESSION["car"]))){
		$car = unserialize($_SESSION["car"]);
		foreach($car as $key => $item){
			$car[$key] -> clear();
		}
		unset($_SESSION["car"]);
	}
	
	//$car = new shoppingCar();
	//$car -> clear();
	
	$sResult = 1;
	$sMsg = 'Logout';
	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}

//FB登入取資料
if($actiontype == "chkToken"){ 
	require_once("../inc/facebook.php");
	$sMsg = "系統發生錯誤!請聯絡系統管理員";
	//$name = "";
	//$email = "";
	//$mobile = "";
	$member_id = "";
	//$is_Member = false;
	//$success = false; //驗證是否有過
	$token = post("accesstoken", 1);
	//$isBonus = 0;
	
	$facebook = new Facebook(array(
	  'appId'  => $fb_appid,
	  'secret' => $fb_appsecrect,
	));
	
	try{
		$user_profile = $facebook -> api('/me', array('access_token' => $token));
		$sResult = true;
	}catch(FacebookApiException $e){
		$e = $e -> getResult();
		$sMsg = $e["error"]["message"];
	}
	
	if($sResult){
		//print_r($user_profile);
		$fb_id = $user_profile["id"];
		//$fb_email = isset($user_profile["email"]) ? $user_profile["email"] : "";
		//$fb_birthday = isset($user_profile["birthday"]) ? $user_profile["birthday"] : "";
		//$fb_gender = isset($user_profile["gender"]) ? $user_profile["gender"] : "";
		//$fb_locale = isset($user_profile["locale"]) ? $user_profile["locale"] : "";
		$fb_name = $user_profile["name"];
		
		$sResult = isNull($fb_id, "FBID", 1, 100);
		//if ($sResult){$sResult = isEmail2($uemail, "FB Email");}
		if ($sResult){
			//DB有否FB_id
			$row = getMemberByFBID($fb_id); 
			
			if(!$row){ //DB沒有,新增會員
				$data[coderDBConf::$col_member["account"]] = $fb_id;
				$data[coderDBConf::$col_member["fb_id"]] = $fb_id;
				$data[coderDBConf::$col_member["name"]] = $fb_name;
				$data[coderDBConf::$col_member["manager"]] = "web";
				//$data["member_is_show"] = 0;
				$data[coderDBConf::$col_member["update_time"]] = request_cd();
				$data[coderDBConf::$col_member["create_time"]] = request_cd();
				
				$member_id = $db -> query_insert(coderDBConf::$member, $data);
				
				if($member_id > 0){ //新增成功
					
					$sResult = 1;
					$sMsg = "會員已建立!";
 					//$isMember = true; 
					
					$_SESSION["session_925_id"] = $member_id;
					$_SESSION["session_925_fb"] = $fb_id;
					$_SESSION["session_925_acc"] = $fb_id;
					
				}else{ //新增失敗
					$sResult = 0;
					$sMsg = "會員新增失敗!請再試一次!";
				}		
				
			}else{
				$sMsg = "會員已登入!";
				//$isMember = true;
				//$success = true;
				$sResult = 1;
				
				$_SESSION["session_925_id"] = $row[coderDBConf::$col_member["id"]];
				$_SESSION["session_925_fb"] = $row[coderDBConf::$col_member["fb_id"]];
				$_SESSION["session_925_acc"] = $row[coderDBConf::$col_member["account"]];
				//$_SESSION["session_name"] = $row["member_name"];
			}
			
		}else{
			$sMsg = $str_message;
		}
	}

	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}

//忘記密碼
if($actiontype == "passwordMail"){
	$account = post('account', 1);
	$email = post('email', 1);
	
	$sResult = isNull($account, "帳號", 1, 255);
	$sResult = isNull($email, "密碼", 1, 255);
	if($sResult){
		try{
			$myMember = new CoderMember();
			$data = new CoderMemberItem();
			
			$new_pwd = generatorPassword();
			
			$data -> account = $account;
			$data -> email = $email;
			$data -> newpwd = $new_pwd;
			
			$myMember -> forget_pwd_Update($data);
			
						
			$sResult = 1;
			$sMsg = '新密碼已寄出,請去您的信箱接收!';
		}catch(Exception $e){
			$sResult = 0;
			$sMsg = $e -> getMessage();
		}
		/*$row = $db -> query_first("SELECT * FROM $table_member WHERE member_email = '$email'");
		if($row){	
			try{
				$newpwd = generatorPassword();
				$member_id = $row["member_id"];
				$member_name = $row["member_name"];
				$member_email = $row["member_email"];
				$subject = "新密碼";
				$content = "您的新密碼為:".$newpwd.",登入後,請至會員中心設定您慣用的密碼!";
				
				$md5_newpwd = md5($newpwd);
				
				//echo(md5($newpwd));
	
				//$db -> query_update($table_member, $data, "member_id='$member_id'");
				$db -> query("update $table_member set member_password = '$md5_newpwd' where member_id = '$member_id'");
				
				$fr_em = $sys_email;
				$fr_na = $sys_name;
				$to_em = $member_email;
				$to_na = $member_name;
				$subject = $subject;
				$msg = $content;
				
				sendMail($fr_em, $fr_na, $to_em, $to_na, $subject, $msg);
				
				$sResult = 1;
				$sMsg = '新密碼已寄出,請去您的信箱接收!';
				
			}catch(Exception $e){
				$sResult = 0;
				$sMsg = $e -> getMessage();
			}
		}else{
			$sResult = 0;
			$sMsg = '查無此email,請再輸入一次!';
		}*/
	}else{
		$sResult = 0;
		$sMsg = $str_message;
	}
	
	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}

//會員修改
if($actiontype == "update"){
	$name = post("name", 1);
	$email = post("email", 1);
	$phone = post("phone", 1);
	$address = post("address", 1);
	$old_pwd = post("old_pwd", 1);
	$new_pwd = post("new_pwd", 1);
	$chk_val = post("chk_val", 1);
	
	$sResult = isNull($name, "姓名", 1, 50);
	if($sResult){$sResult = isEmail2($email, "電子郵件");}
	if($sResult){$sResult = isNull($phone, "連絡電話", 1, 50);}
	if($sResult){$sResult = isNull($address, "地址", 1, 255);}
	if($sResult){
		if($chk_val == 1){
			$sResult = isNull($old_pwd, "舊密碼", 1, 20);
			if($sResult){$sResult = isNull($new_pwd, "新密碼", 1, 20);}
			if($sResult){
				
				if(isLogin()){
					$mid = $_SESSION["session_925_id"];
				
					$old_pwd2 = sha1($old_pwd);
					$row = $db -> query_first("SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["id"]." = '$mid' AND ".coderDBConf::$col_member["password"]." = '$old_pwd2'");
					if($row){
						$sResult = true;
					}else{
						$sResult = false;
						$str_message = '現有密碼不正確,請重新輸入!';
					}
				}else{
					$sResult = false;
					$str_message ="您已經登出系統，請重新登入!";
				}
			}
		}
	}
	if($sResult){
		if(isLogin()){
			try{
				$myMember = new CoderMember();
				$data = new CoderMemberItem();
				
				$data -> name = $name;
				$data -> email = $email;
				$data -> phone = $phone;
				$data -> address = $address;
				$data -> new_pwd = ($chk_val == 1) ? $new_pwd : "";
				
				$myMember -> Update($data);
				
				$sResult = 1;
				$sMsg = '修改完成!';
			}catch(Exception $e){
				$sResult = 0;
				$sMsg = $e -> getMessage();
			}
		}else{
			$sResult = 0;
			$str_message ="您已經登出系統，請重新登入!";
		}
	}else{
		$sMsg = $str_message;
	}
	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	
	echo json_encode($re);
}
//密碼修改
/*if($actiontype == "changePassword"){
	if(isLogin()){
		$mid = $_SESSION["session_id"];
		$old_password = post('old_password', 1);
		$new_password = post('new_password', 1);
		
		$sResult = isNull($old_password, "舊密碼", 6, 35);
		if($sResult){$sResult = isNull($new_password, "新密碼", 6, 35);}
		
		if($sResult){
			$old_password2 = md5($old_password);
			//echo('mid:'.$mid.'/old:'.$old_password.'/old2:'.$old_password2.'/new:'.$password);
			$row = $db -> query_first("SELECT * FROM $table_member WHERE member_id = '$mid' AND member_password = '$old_password2'");
			//print_r($row);
			if($row){	
				try{
					$myMember = new CoderMember();
							
					$myMember -> changePassword($mid, $old_password, $new_password);
					$sResult = 1;
					$sMsg = 'OK';
				}catch(Exception $e){
					$sResult = 0;
					$sMsg = $e -> getMessage();
				}
			}else{
				$sResult = 0;
				$sMsg = '現有密碼不正確,請重新輸入!';
			}
		}else{
			$sResult = 0;
			$sMsg = $str_message;
		}
		
	}else{
		$sMsg ="您已經登出系統，請重新登入!";
	}
	
	$re["result"] = ($sResult == 1) ? true : false;
	$re["msg"] = $sMsg;
	echo json_encode($re);
}*/

//CHK FB_ID
function getMemberByFBID($fb_id){
	global $db;
	return $db -> query_first("SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["fb_id"]." = '$fb_id'");
}


$db -> close();

/*****END PHP*****/

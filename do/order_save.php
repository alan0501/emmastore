<?php
include_once("_config.php");
include_once($inc_path.'lib/_shoppingcar.php');
include_once($inc_path."lib/codermember.php");
include_once($inc_path."lib/coderorder.php");
include_once($inc_path."lib/coderorderobj.php");
include_once($inc_path.'_func_smtp.php');

$sResult = 0;
$message = "";
$order_sno = "";

$shop_id = post("shop_id", 1);
$commend_email = post("commend_email", 1);

$recipient_name = post("recipient_name", 1);
$recipient_phone = post("recipient_phone", 1);
$recipient_email = post("recipient_email", 1);
$recipient_zcode = post("recipient_zcode", 1);
$recipient_address = post("recipient_address", 1);
$company_invoice = post("company_invoice", 1);
$payment_type = post("payment_type", 1);

$sResult = isNull($shop_id, "店家", 1, 300);
if($sResult){$sResult = isNull($recipient_name, "收件人姓名", 1, 20);}
if($sResult){$sResult = isNull($recipient_phone, "收件人聯絡電話", 1, 20);}
if($sResult){$sResult = isNull($recipient_email, "收件人email", 1, 255);}
if($sResult){$sResult = isNull($recipient_address, "收件人地址", 1, 255);}
if($sResult){$sResult = isNull($payment_type, "付款方式", 1, 2);}
//var_dump($sResult);

if($sResult){
	$db = new Database($HS, $ID, $PW, $DB);
	$db -> connect();
	
	if(isLogin()){
		$member_id = $_SESSION["session_925_id"];
		$row_mamber = class_member::getInfo($member_id);
		$purchaser_name = $row_mamber[coderDBConf::$col_member["name"]];
	}else{
		$member_id = 0;
		$purchaser_name = $recipient_name;
		/*$data["member_account"] = $payer_email;
		$data["member_password"] = md5($payer_password);
		$data["member_email"] = $payer_email;
		$data["member_name"] = $payer_name;
		$data["member_gender"] = $payer_gender;
		$data["member_mobile"] = $payer_mobile;
		$data["member_city"] = $payer_city;
		$data["member_area"] = $payer_area;
		$data["member_street"] = $payer_street;
		$data["member_zcode"] = $payer_zcode; 	
		$data["member_create_time"] = request_cd(); 
		$data["member_update_time"] = request_cd(); 
		
		$member_id = $db -> query_insert($table_member, $data); //新增會員
		
		//登入
		$_SESSION["session_id"] = $member_id;
		$_SESSION["session_acc"] = $payer_email;*/
	}
	
	if(isset($_SESSION["car"]) && is_array(unserialize($_SESSION["car"]))){
		$car = unserialize($_SESSION["car"]);
		foreach($car as $key => $item){
			$car[$key] -> getCarFromDB();
			$car[$key] -> calculate();
		}
		$_SESSION["car"] = serialize($car);
		$car = unserialize($_SESSION["car"]);
	}else{
		$car = array();
	}
	
	$u = count($car[$shop_id] -> car);
	if($u == 0){
		script("您目前未購買任何商品,請繼續購物!!", "index.html");
	}
	$total = $car[$shop_id] -> total;
	$freight = $car[$shop_id] -> freight;
	$bonus = $car[$shop_id] -> bonus;
	$carry = $car[$shop_id] -> carry; //運送方式
	$total_price = ($total+$freight);
	
	//print_r($car_bak);
	
	//推薦人
	if($commend_email != ""){
		//getCommendBonus($commend_email);
	}
	
	$coderorder = new CoderOrder();
	$myorder = new  coderOrderObj();
	
	$myorder -> order_sno = "";
	$myorder -> order_member_id = $member_id;
	$myorder -> order_shop_id = $shop_id;
	$myorder -> order_payment_type = $payment_type;
	$myorder -> order_payment_state = "0";
	$myorder -> order_state = "0";
	$myorder -> order_express_type = $carry;
	$myorder -> order_comment = "";
	
	//$myorder -> order_coupon_sno = $coupon_sno;
	$myorder -> order_total_price = $total;
	$myorder -> order_freight = $freight;
	$myorder -> order_bonus = $bonus;
	$myorder -> order_purchaser_name = $purchaser_name;
	$myorder -> order_recipient_name = $recipient_name;
	//$myorder -> order_recipient_gender = $recipient_gender;
	$myorder -> order_recipient_email = $recipient_email;
	$myorder -> order_recipient_phone = $recipient_phone;
	$myorder -> order_recipient_zcode = $recipient_zcode==""?"000":$recipient_zcode;
	$myorder -> order_recipient_address = $recipient_address;
	//$myorder -> order_recipient_comment = $recipient_comment;
	//$myorder -> order_recipient_time = $recipient_time;
	$myorder -> order_invoice = 1;
	$myorder -> order_company_invoice = $company_invoice==""?1:$company_invoice;
	//$myorder -> order_invoice_name = $invoice_name;
	//$myorder -> order_invoice_number = $invoice_number;
	$myorder -> order_create_time = request_cd();
	$myorder -> order_update_time = request_cd();
	//$myorder -> payment_return = "";
	
	foreach($car[$shop_id] -> car as $item){
	//for($i = 0; $i < count($carItem); $i++){
		$detail_item = new coderOrderObj();
		$detail_item -> orderdetail_product_id = $item -> product_id;
		$detail_item -> orderdetail_order_sno = "";
		$detail_item -> orderdetail_product_name = $item -> product_name;
		$detail_item -> orderdetail_product_sno = $item -> product_sno;
		$detail_item -> orderdetail_shop_id = $item -> shop_id;
		//$detailitem -> product_name_en = $carItem[$i] -> product_name_en;
		$detail_item -> orderdetail_price = $item -> sell_price;
		$detail_item -> orderdetail_amount = $item -> amount;
		$detail_item -> orderdetail_total_price = $item -> sub_total;
		$detail_item -> orderdetail_state = 0;
		$detail_item -> orderdetail_create_time = request_cd();
		$detail_item -> orderdetail_update_time = request_cd();
		$myorder -> insertDetail($detail_item);
	}
	
	try{
		//$result = $coderorder -> chkStock($myorder -> detail_ary);
		//if($result[0]){

		$coderorder -> orderInsert($myorder);//寫入資料庫
			
		//}else{
			//$message = $result[1];	
		//}
		
	
		$order_sno = $myorder -> order_sno;

		$order_sno_o = $order_sno; //先備份舊的訂單單號
		$id = substr($order_sno_o, 8); //取單號最後4位
		$order_sno = $order_sno_o.class_order::getsn($id); //產生隨機亂數，重組訂單單號
		class_order::update_sno($order_sno, $order_sno_o); //更新訂單單號
		
		//通知信
		$coderorder -> orderMail($myorder);

		//$car -> clear();
		$car[$shop_id] -> sno = $order_sno;
		$car_bak[$shop_id] = $car[$shop_id];
		$_SESSION["car_bak"] = serialize($car_bak);
		
		$car[$shop_id] -> clear();
		
		unset($car[$shop_id]);

		//reshoppingcar
		/*foreach($car as $key => $item){
			$car[$key] -> getCarFromDB();
			$car[$key] -> calculate();
		}
		$_SESSION["car"] = serialize($car);*/
		
		//print_r($car);
		//$_cart = count($car);

		if(count($car) <= 0){
			//unset($car);
			unset($_SESSION["car"]);
			$_SESSION["has_cart"] = "NO";
		}else{
			$_SESSION["car"] = serialize($car);
			$_SESSION["has_cart"] = "YES";
		}
		
		//print_r($car);
		//die();
		$sResule = 1;
		$message = "訂單已送出!感謝您的訂購!";
		
		$_SESSION["order_sno"] = $order_sno;
		//$_SESSION["total_price"] = $total_price;
		//$_SESSION["recipient_address"] = $recipient_city.$recipient_area.$recipient_street;
		
	}catch(Exception $ex){
		//$sResule = 0;
		$message = $e -> getMessage();
	}

	$db -> close();
}else{
	//$sResule = 0;
	$message = "資料傳輸錯誤!請再試一次!";
}

$res["result"] = ($sResult == 1) ? true : false;

$res["msg"] = $message;

echo json_encode($res);

/*****END PHP*****/
<?php
include_once('_config.php');

$sResult = 0;

$m_id = post("m_id", 1);
$s_id = post("s_id", 1);

$msg = "";

$sResult = isNull($m_id, "會員ID", 1, 255) ;
if($sResult){$sResult = isNull($s_id, "店家ID", 1, 255);}
if($sResult){
	
	$db = new Database($HS, $ID, $PW, $DB);
	$db -> connect();	
	
	$row = $db -> query_first("SELECT * FROM ".coderDBConf::$fshop." WHERE ".coderDBConf::$col_fshop["member_id"]." = '$m_id' AND ".coderDBConf::$col_fshop["shop_id"]." = '$s_id'");

	if($row){
		$sResult = 1;
		$msg = "此店家已在收藏列表!請去會員中心查看!";
	}else{
		$data[coderDBConf::$col_fshop["member_id"]] = $m_id;
		$data[coderDBConf::$col_fshop["shop_id"]] = $s_id;
		$data[coderDBConf::$col_fshop["create_time"]] = request_cd();
			
		$id = $db -> query_insert(coderDBConf::$fshop, $data);
		if($id > 0){
			$sResult = 1;
			$msg = "已收藏此店家!請去會員中心查看!";
		}else{
			$msg = "資料傳輸錯誤!請再試一次!";
		}
	}
	
	$db -> close();
	
}else{
	//$msg = "資料傳輸錯誤!請再試一次!";
	$msg = $str_message;
}

$re["sResult"] = ($sResult == 1) ? true : false;
$re["msg"] = $msg;

echo json_encode($re);

/*****END PHP*****/
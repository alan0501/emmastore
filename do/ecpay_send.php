<?php /*傳送金流資訊給綠界*/
include_once("_config.php");
require_once 'ECPay.Payment.Integration.php';
 
$obj = new \ECPay_AllInOne();
 
//服務參數---------------
//測試站
$service_url = "https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5"; //環境
$merchant_id = "2000132"; //特店編號
$hash_key = "5294y06JbISpM5x9"; //ALL IN ONE 介接 HashKey
$hash_iv = "v77hoKGq4kWxNNIS"; //ALL IN ONE 介接 HashIV
//正式站
//$service_url = "https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5"; //環境
//$merchant_id = "3117800"; //特店編號
//$hash_key = "P9TpcK6gtl1jhyZp"; //ALL IN ONE 介接 HashKey
//$hash_iv = "UA2bsb8vdckxn6K7"; //ALL IN ONE 介接 HashIV
$obj->ServiceURL  = $service_url;
$obj->HashKey     = $hash_key;
$obj->HashIV      = $hash_iv;
$obj->MerchantID  = $merchant_id;
$obj->EncryptType = '1';

//其他參數設定---------------
$obj->Send['ReturnURL']         = $_POST['ReturnURL'];
$obj->Send['ClientBackURL']     = $_POST['ClientBackURL'];
$obj->Send['EncryptType']       = $_POST['EncryptType'];
$obj->Send['PaymentType']       = $_POST['PaymentType'];
$obj->Send['ChoosePayment']     = $_POST['ChoosePayment'];
$obj->Send['NeedExtraPaidInfo'] = $_POST['NeedExtraPaidInfo'];
if($_POST['ChoosePayment']=="Credit"){
    $obj->Send['BindingCard']       = $_POST['BindingCard']; //信用卡-是否記住卡號
}
if($_POST['ChoosePayment']=="BARCODE" || $_POST['ChoosePayment']=="CVS"){
    $obj->SendExtend['Desc_1']          = $_POST['Desc_1'];          //超商繳費-交易描述1
    $obj->SendExtend['Desc_2']          = $_POST['Desc_2'];          //超商繳費-交易描述2
    $obj->SendExtend['Desc_3']          = $_POST['Desc_3'];          //超商繳費-交易描述3
    $obj->SendExtend['Desc_4']          = $_POST['Desc_4'];          //超商繳費-交易描述4
    $obj->SendExtend['StoreExpireDate'] = $_POST['StoreExpireDate']; //超商繳費期限
}

//訂單資訊---------------
$obj->Send['MerchantTradeNo']   = $_POST['MerchantTradeNo'];
$obj->Send['MerchantTradeDate'] = $_POST['MerchantTradeDate'];
$obj->Send['TradeDesc']         = $_POST['TradeDesc'];

$shipping_fee = (int)$_POST['shipping_fee']; //運費
$obj->Send['TotalAmount'] = $shipping_fee;

$order_sno = $_POST['order_sno'];
$rows_item = array();
if(!empty($order_sno)){
    $db = new Database($HS, $ID, $PW, $DB);
    $db -> connect();

    $sql = "select ".$colname_order_detail['price'].",".$colname_order_detail['amount'].",".$colname_product['name']." 
              FROM $table_order_detail 
              LEFT JOIN $table_product ON ".$colname_product['id']."=".$colname_order_detail['product_id']."
             WHERE ".$colname_order_detail['sno']." = :sno";
    $rows_item = $db -> fetch_all_array($sql, array(':sno' => $order_sno));

    $db -> close();
}
//var_dump($rows_item);exit;

for($i=0; $i<count($rows_item); $i++){
    array_push(
        $obj->Send['Items'],
        array(
            'Name' => $rows_item[$i]['product_name'],
            'Price' => $rows_item[$i]['orderdetail_price'],
            'Currency' => "元",
            'Quantity' => (int)$rows_item[$i]['orderdetail_amount']
        )
    );
    $obj->Send['TotalAmount'] += ($rows_item[$i]['orderdetail_amount']*$rows_item[$i]['orderdetail_price']);
}
if(!empty($shipping_fee)){
    array_push(
        $obj->Send['Items'],
        array(
            'Name' => "運費",
            'Price' => $shipping_fee,
            'Currency' => "元",
            'Quantity' => (int)"1"
        )
    );
}
    
 
//產生訂單(auto submit至ECPay)---------------
$Response = (string)$obj->CheckOutString();
echo $Response;

//自動將表單送出---------------
echo '<script>document.getElementById("__ecpayForm").submit();</script>';
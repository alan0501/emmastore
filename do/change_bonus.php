<?php
include_once('_config.php');

$sResult = 0;

$all_bonus = post("all_bonus", 1);
$msg = "";
$member_id = "";

$sResult = isNull($all_bonus, "紅利", 1, 255) ;
if($sResult){
	if(!(((isset($_SESSION["session_925_acc"]) && trim($_SESSION["session_925_acc"]) != "") || (isset($_SESSION["session_925_fb"]) && trim($_SESSION["session_925_fb"]) != "")) && isset($_SESSION["session_925_id"]) && trim($_SESSION["session_925_id"] != ""))){
			throw new Exception('您已經登出系統，請重新登入!');
	}else{
		$member_id = $_SESSION["session_925_id"];
	}
	
	
	$db = new Database($HS, $ID, $PW, $DB);
	$db -> connect();	

	$data[coderDBConf::$col_used_bonus["member_id"]] = $member_id;
	$data[coderDBConf::$col_used_bonus["money"]] = $all_bonus;
	$data[coderDBConf::$col_used_bonus["bonus"]] = $all_bonus;
	$data[coderDBConf::$col_used_bonus["comment"]] = "轉換購物金";
	$data[coderDBConf::$col_used_bonus["type"]] = 2;
	$data[coderDBConf::$col_used_bonus["create_time"]] = request_cd();
		
	$db -> query_insert(coderDBConf::$used_bonus, $data);
	
	$db -> close();
	
	$sResult = 1;
	$msg = "購物金轉換完成!";
}else{
	//$msg = "資料傳輸錯誤!請再試一次!";
	$msg = $str_message;
}

$re["sResult"] = ($sResult == 1) ? true : false;
$re["msg"] = $msg;

echo json_encode($re);

/*****END PHP*****/
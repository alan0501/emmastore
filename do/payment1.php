<?php /*綠界金流-信用卡*/

include_once("_config.php");
include_once($inc_path.'lib/_shoppingcar.php');

$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();

$cart = unserialize($_SESSION["car_bak"]);

$total = 0;
$freight = 0;
$shopping_item = array();

foreach($cart as $key => $val){
	$total = $cart[$key]->total;
	$freight = $cart[$key]->freight;
    $shopping_item = $cart[$key]->car;
}

$u = count($cart);

if($u <= 0){
	script("您目前未購買任何商品,請繼續購物!!", "index.html");
}

$order_sno = isset($_SESSION["order_sno"]) ? $_SESSION["order_sno"] : "";

if($order_sno != ""){

	//金流資料(S)----------------------
    $merchant_trade_no = $order_sno; //訂單單號
	$merchant_trade_date = date("Y/m/d H:i:s"); //交易日期(格式=yyyy/MM/dd HH:mm:ss)
    $trade_desc = urlencode("Q點購物中心"); //交易描述
    $choose_payment = "Credit"; //付費方式

    $client_back_url = $http."index.php"; //付款完成後，在綠界平台上的「返回商店」按鈕
    $return_url = $http."do/ecpay_receive.php"; //付款完成後，綠界回傳的訊息
    //金流資料(E)----------------------
}else{
	script("資料傳送錯誤!請再試一次!");
}

$db -> close();
?>
<!DOCTYPE html>
<html lang="zh_tw">
<head>
<meta charset="UTF-8" />
<meta name="keywords" content="<?php echo $keywords; ?>">
<meta name="description" content="<?php echo $description; ?>">
<meta name="author" content="<?php echo $author; ?>">
<meta name="copyright" content="<?php echo $copyright; ?>">
<title><?php echo $web_name; ?></title>

<style>
#loading_pic {
	height: 32px;
	width: 32px;
	margin-top: 250px;
	margin-right: auto;
	margin-left: auto;
}
</style>
</head>
<body>
    <div id="loading_pic"><img src="../images/bx_loader.gif"></div>
    <form id="pay_form"  name="pay_form" action="ecpay_send.php" method="post" >
        <input type="hidden" name="order_sno" value="<?php echo $order_sno; ?>">
        <input type="hidden" name="shipping_fee" value="<?php echo $freight; ?>">

        <input type="hidden" name="MerchantTradeNo" value="<?php echo $merchant_trade_no; ?>">
        <input type="hidden" name="MerchantTradeDate" value="<?php echo $merchant_trade_date; ?>">
        <input type="hidden" name="TradeDesc" value="<?php echo $trade_desc; ?>">
        <input type="hidden" name="ReturnURL" value="<?php echo $return_url; ?>">
        <input type="hidden" name="CheckMacValue" value="<?php //echo $; ?>">

        <input type="hidden" name="ChoosePayment" value="<?php echo $choose_payment; ?>">
        <input type="hidden" name="ClientBackURL" value="<?php echo $client_back_url; ?>">

        <input type="hidden" name="EncryptType" value="1">
        <input type="hidden" name="BindingCard" value="0"> <!--是否記憶信用卡-->
        <input type="hidden" name="PaymentType" value="aio">
        <input type="hidden" name="NeedExtraPaidInfo" value="N">
        
        <input name="" type="submit">
    </form>

    <!--自動將表單送出-->
    <script>//document.getElementById("pay_form").submit();</script>
</body>
</html>
<!--style="display:none;"-->
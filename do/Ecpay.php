<?php
include_once("_config.php");
require_once 'ECPay.Payment.Integration.php';
 
$obj = new \ECPay_AllInOne();
 
//服務參數
$obj->ServiceURL  = $_POST['ServiceURL'];
$obj->HashKey     = $_POST['HashKey'];
$obj->HashIV      = $_POST['HashIV'];
$obj->MerchantID  = $_POST['MerchantID'];

$obj->Send['TotalAmount'] = 0;
$obj->Send['MerchantTradeNo'] = $_POST['MerchantTradeNo'];
$obj->Send['MerchantTradeDate'] = $_POST['MerchantTradeDate'];
$obj->Send['PaymentType'] = $_POST['PaymentType'];
$obj->Send['TradeDesc'] = $_POST['TradeDesc'];
$obj->Send['ReturnURL'] = $_POST['ReturnURL'];
$obj->Send['ChoosePayment'] = $_POST['ChoosePayment'];

$shipping_fee = (int)$_POST['shipping_fee']; //運費
$obj->Send['TotalAmount'] += $shipping_fee;

$order_sno = $_POST['order_sno'];
$rows_item = array();
if(!empty($order_sno)){
    $db = new Database($HS, $ID, $PW, $DB);
    $db -> connect();

    $sql = "select ".$colname_order_detail['price'].",".$colname_order_detail['amount'].",".$colname_product['name']." 
              FROM $table_order_detail 
              LEFT JOIN $table_product ON ".$colname_product['id']."=".$colname_order_detail['product_id']."
             WHERE ".$colname_order_detail['sno']." = :sno";
    $rows_item = $db -> fetch_all_array($sql, array(':sno' => $order_sno));

    $db -> close();
}
//var_dump($rows_item);exit;
 
//訂單的商品資料
for($i=0; $i<count($rows_item); $i++){
    array_push(
        $obj->Send['Items'],
        array(
            'Name' => $rows_item[$i]['product_name'],
            'Price' => $rows_item[$i]['orderdetail_price'],
            'Currency' => "元",
            'Quantity' => (int)$rows_item[$i]['orderdetail_amount']
        )
    );
    $obj->Send['TotalAmount'] += ($rows_item[$i]['orderdetail_amount']*$rows_item[$i]['orderdetail_price']);
}
if(!empty($shipping_fee)){
    array_push(
        $obj->Send['Items'],
        array(
            'Name' => "運費",
            'Price' => $shipping_fee,
            'Currency' => "元",
            'Quantity' => (int)"1"
        )
    );
}
    
 
//產生訂單(auto submit至ECPay)
//$obj->CheckOut();
$Response = (string)$obj->CheckOutString();
echo $Response;

//自動將表單送出
echo '<script>document.getElementById("__ecpayForm").submit();</script>';
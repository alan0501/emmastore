<?php
include_once("_config.php");
include_once("../inc/_func_smtp.php");

if(!isLogin()){
	script("您已經登出系統，請重新登入!!", "index.html");
}else{
	$member_id = $_SESSION["session_925_id"];
}

$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();

$order_sno = post("order_sno", 1 );
$product_id = post("product_id", 1 );
$state = post("state", 1 );

$sResult = false;
$sMsg = "";

if($order_sno != "" && $product_id != ""){
	$colname = coderDBConf::$col_order_detail;
	$colname_order = coderDBConf::$col_order;
	$colname_member = coderDBConf::$col_member;

	$sql = "SELECT * 
			FROM ".coderDBConf::$order_detail." 
			LEFT JOIN ".coderDBConf::$order." ON {$colname_order['sno']} = {$colname['sno']}
			LEFT JOIN ".coderDBConf::$member." ON {$colname_member['id']} = {$colname_order['member_id']}
			WHERE {$colname['sno']} = '$order_sno' AND {$colname['product_id']} = '$product_id' AND {$colname_order['member_id']} = '$member_id'";
			
	$row = $db -> query_first($sql);
	
	if($row){
		
		$db -> query("UPDATE ".coderDBConf::$order_detail." SET {$colname['state']} = '$state' WHERE {$colname['sno']} = '$order_sno' AND {$colname['product_id']} = '$product_id'");
		
		$member_name = $row[$colname_member['name']];
		$member_email = $row[$colname_member['email']];
		//$bcc = "";
		$bcc =  "bill@coder.com.tw";
		//$fr_em = $sys_email;
		$fr_em = $sys_email;
		$fr_na = $sys_name;
		$to_em = $member_email;
		$to_na = $member_name;
		
		$subject = "您的退貨通知";
		
		$msg = '';
		$msg .= '親愛的'.$member_name.'&nbsp;您好:';
		$msg .= '<br>';
		$msg .= '925已經收到您的退貨申請!';
		$msg .= '<br>';
		$msg .= '客服人員將會盡快幫您處理!';
		

		//sendMail($fr_em, $fr_na, $to_em, $to_na, $subject, $msg, $bcc);
		
		//$sMsg = $ary_order_state[$state]."完成!";
		$sMsg = "退貨完成!";
		$sResult = true;
	}else{
		$sMsg = "查無此訂單!!";
	}
	
	
}else{
	$sMsg = "資料傳輸錯誤!請再試一次!";
}
$db -> close();


$re["sResult"] = $sResult;
$re["msg"] = $sMsg;

echo json_encode($re);


/*****END PHP*****/
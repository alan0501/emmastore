<?php
include_once("_config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="<?php echo $keywords; ?>" />
<meta name="author" content="<?php echo $author; ?>" />
<meta name="copyright" content="<?php echo $copyright; ?>" />
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script src="js/jquery.min.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/fb.js"></script>
<script>
var api_id = '<?php echo $fb_appid; ?>';
var fb_uid = "";
var fb_session = null; 
var fb_me = null;
var fb_token = "";
var fblogin = false; //判斷FB是否登入
var req_perms = 'email,public_profile'; //要FB權限
</script>
</head>
<body>
<div id="site_login">
    <div class="login_form">
        <div class="login_logo"><img src="images/logo.png" /></div>
        <!--<div class="login_fb"><a href="javascript:void(0);" class="fb_login_btn"><img src="images/f_login.png" width="300"  /></a></div>-->
        <div class="login_acc">忘記密碼</div>
        <div class="login_form">
            <form id="member_forget_form">
            	<input name="path" type="hidden" value="i" id="path" />
                <input id="forget_acc" type="text" name="forget_acc" class="input-userName" autocomplete="off" placeholder="帳號">
                <input id="forget_email" type="text" name="forget_email" class="input-email" autocomplete="off" placeholder="登記的Email信箱">
            </form>
            <div class="input-button forget_send_btn"><img src="images/send.jpg" width="300"  /></div>
            <dl class="pass-reglink clearfix">
                <!--<dt><a href="javascript:void(0);" class="forget_btn">忘記密碼</a></dt>
                <dd><a href="javascript:void(0);" class="reg_new_btn">註冊新帳號</a></dd>-->
            </dl>
        </div>
    </div>
</div>
</body>

<?php
include_once("_config.php");
include_once($inc_path.'lib/_shoppingcar.php');

if(isLogin()){
	header('Location: cart.html');
}

if(isset($_SESSION["car"]) && is_array(unserialize($_SESSION["car"]))){
	$car = unserialize($_SESSION["car"]);
	foreach($car as $key => $item){
		$car[$key] -> getCarFromDB();
		$car[$key] -> calculate();
	}
	$_SESSION["car"] = serialize($car);
	$car = unserialize($_SESSION["car"]);
}else{
	$car = array();
}
if(count($car) <= 0){
	//script("您的購物車已無購買商品!", "index.html");
}

//print_r($car);

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix">
    <!--mobile menu star-->
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <!--mobile menu end-->
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">Q點購物中心</a></div>
            <div class="m_login">
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
				<div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}else{
				?>
				<div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
                
            </form>
        </div>
        <div id="top_right">
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
			?>
            <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class="gray ">
    <div id="wrapper">
        <div id="member_menu">
            <dl class="clearfix">
                <dt><img src="images/member.jpg" align="absmiddle"/></dt>
                <dt class="email">親愛的訪客您好:<br />
                    <span>  </span></dt>
                <!--<dd class="selected"><a href="cart.html">我的購物車</a></dd>
                <dd ><a href="bought.html">已買到的商品</a></dd>
                <dd><a href="favorite_product.html">收藏的商品</a></dd>
                <dd><a href="favorite_store.html">收藏的店家</a></dd>
                <dd><a href="gold.html">購物金</a></dd>
                <dd><a href="member.html">會員管理</a></dd>-->
            </dl>
        </div>
        <div id="wrap-main" class="clearfix">
            <div id="bought_left">
            	<?php
					if(count($car) <= 0){
				?>
                	<p style="font-size:24px;">您尚未選購商品!</p>
                <?php
					}
				?>
            	<?php
					foreach($car as $key => $item){
						$shop = $key;
						$list = $item -> car;
				?>
                <div class="bought_table1">
                    <div class="tb_title">
                        <dl>
                            <dt>店家 : <?php echo class_shop::getName($shop); ?></dt>
                        </dl>
                    </div>
                    <?php
						foreach($list as $l){
					?>
                    <ul class="bought_list clearfix shop_box item_list<?php //echo $i; ?>" cart_id="<?php echo $l -> product_id.$l -> product_sno; ?>" shop_id="<?php echo $shop; ?>" list="<?php //echo $i; ?>" p_id="<?php echo $l -> product_id; ?>" m_id="<?php echo $member_id; ?>">
                        <li class="bought_pic"><a href="#"><img src="<?php echo $web_path_product."s".$l -> pic; ?>"  /></a></li>
                        <li class="product_name"><a href="#"><?php echo $l -> product_name; ?></a><br />
                            <div class="move"> <img src="images/del.png" width="12" height="12" />&nbsp;<span class="del shop_box_del"><a href="#">移除本商品</a></span>&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/favorite.png" width="16" height="15"  class="favor_pic"/>&nbsp;<span class="favorite shop_box_favorite"><a href="#">移動至收藏夾</a></span></div>
                        </li>
                        <li class="cart_unit">單價：NT$ <?php echo $l -> sell_price; ?><br />
                            數量&nbsp;&nbsp;
                            <input name="cart_unit_amount" type="text" class="cart_pcs" id="amount[]" value="<?php echo $l -> amount; ?>" />
                        </li>
                    </ul>
                    <?php
						}
					?>
                    <!--<ul class="bought_list clearfix">
                        <li class="bought_pic"><a href="#"><img src="images/p86.jpg"  /></a></li>
                        <li class="product_name"><a href="#">Artificer FLORALS 創意能量手環創意能量</a><br />
                            <div class="move"> <img src="images/del.png" width="12" height="12" />&nbsp;<span class="del"><a href="#">移除本商品</a></span>&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/favorite.png" width="16" height="15"  class="favor_pic"/>&nbsp;<span class="favorite"><a href="#">移動至收藏夾</a></span></div>
                        </li>
                        <li class="cart_unit">單價：NT$ 299<br />
                            數量&nbsp;&nbsp;
                            <input name="" type="text" class="cart_pcs" value="1" />
                        </li>
                    </ul>-->
                    <ul class="carry clearfix" >
                        <li class="carry_t"><img src="images/carry.png" />
                            <div class="carry_list">
                            	<?php
									foreach($ary_carry as $val => $name){
								?>
                                <P><a href="javascript:void(0);" val="<?php echo $val; ?>" shop_id="<?php echo $shop; ?>"><?php echo $name; ?></a> </P>
                                <?php
									}
								?>
                                <!--<P><a href="#" val="2">宅急便 NT80</a></P>
                                <P><a href="#">郵局 NT80</a></P>
                                <p><a href="#">貨到付款 NT100</a></p>-->
                            </div>
                        </li><span class="carry_show<?php echo $shop; ?>" style="float:left; width:20%; line-height:30px; padding-left:10px; font-size:14px"><?php echo show_carry($item -> carry); ?></span>
                        <li class="cost"> NT$ <?php echo $item -> freight; ?> </li>
                    </ul>
                    <ul class="check clearfix" >
                        <?php /*?><li ><a href="post_copy_<?php echo $shop; ?>.html" id="cart_check_btn"><img src="images/check.png" /></a></li><?php */?>
                        <li ><a href="javascript:void(0);" id="cart_check_btn" shop_id="<?php echo $shop; ?>"><img src="images/check.png" /></a></li>
                        <li class="cost"> NT$ <?php echo ($item -> total + $item -> freight); ?> </li>
                    </ul>
                </div>
                <?php
					}
				?>
                <!--<div class="bought_table1">
                    <div class="tb_title">
                        <dl>
                            <dt>店家: 9好買</dt>
                        </dl>
                    </div>
                    <ul class="bought_list clearfix">
                        <li class="bought_pic"><a href="#"><img src="images/p86.jpg"  /></a></li>
                        <li class="product_name"><a href="#">Artificer FLORALS 創意能量手環創意能量</a><br />
                            <div class="move"> <img src="images/del.png" width="12" height="12" />&nbsp;<span class="del"><a href="#">移除本商品</a></span>&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/favorite.png" width="16" height="15"  class="favor_pic"/>&nbsp;<span class="favorite"><a href="#">移動至收藏夾</a></span></div>
                        </li>
                        <li class="cart_unit">單價：NT$ 299<br />
                            數量&nbsp;&nbsp;
                            <input name="" type="text" class="cart_pcs" value="1" />
                        </li>
                    </ul>
                    <ul class="carry clearfix" >
                        <li class="carry_t"><img src="images/carry.png" />
                            <div class="carry_list" style="display:none">
                                <P><a href="#">7-11 取貨 NT60</a> </P>
                                <P><a href="#">宅急便 NT80</a></P>
                                <P><a href="#">郵局 NT80</a></P>
                                <p><a href="#">貨到付款NT100</a></p>
                            </div>
                        </li>
                        <li class="cost"> NT$ 998 </li>
                    </ul>
                    <ul class="check clearfix" >
                        <li ><a href="post_copy.html"><img src="images/check.png" /></a></li>
                        <li class="cost"> NT$ 998 </li>
                    </ul>
                </div>-->
            </div>
            <div id="bought_right">
                <div class="pay1 pay_type"> 購物安全、有保障，付款超方便
                    <h3> <img src="images/pay.png" width="237" height="95" /></h3>
                    我們採用最安全的線上刷卡服務，讓您付款快速又有保障。沒有手續費，還可以累積 Pinkoi 紅利點數喔！ </div>
                <div class="pay2 pay_type"> 找不到 ATM 嗎？超商繳費更便利
                    <h3> <img src="images/ibon.png" width="222" height="25" /> </h3>
                    只要在超商輸入代碼，就可以列印帳單直接繳費。我們還提供「7-ELEVEN 交貨便」及「全家店到店」物流服務，今天寄件後天取件，安心又放心。了解怎麼使用 </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end-->
<script>
carItemShow();
</script>
</body>
</html>

<?php
$fhelp = new coderFormHelp();
$fobj = array();

$fobj[$colname['keyword']] = array('type' => 'textarea', 'name' => 'keyword', 'column' => $colname['keyword'], 'validate' => array('required' => 'yes'));
$fobj[$colname['description']] = array('type' => 'textarea', 'name' => 'description', 'column' => $colname['description'], 'validate' => array('required' => 'yes'));
$fobj[$colname['ga']] = array('type' => 'textarea', 'name' => 'Google Analytics', 'column' => $colname['ga'], 'validate' => array('required' => 'yes'));

$fhelp -> Bind($fobj);

/*****END PHP*****/
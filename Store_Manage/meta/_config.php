<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'index';
$logkey = 'meta';
include_once('../_config.php');
coderAdmin::vaild($authkey);

$auth = coderAdmin::Auth($authkey);

$table = coderDBConf::$meta;
$colname = coderDBConf::$col_meta;

$idColumn = $colname['id'];


$page_title = $auth['name'].' - META';
$page_name = '';
$page_desc = "{$page_title}-您可將內容修改為希望呈現的內容。";
$mtitle = '<li class="active">'.$page_title.'</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");

/*****END PHP*****/
<?php
include_once('_config.php');
$id = get('id');
$manageinfo = "";
$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();
include_once('formconfig.php');

$def_prod_val = '';
$def_prod_name = '';

if($id > 0){
	$row = $db -> query_prepare_first("select * from $table where {$colname['id']} = $id");
	$row[$colname['sdate']] = coderHelp::getDate($row[$colname['sdate']]);
	$row[$colname['edate']] = coderHelp::getDate($row[$colname['edate']]);
	//$row['s_prod_cid'] = explode(',', $row['s_prod_cid']);
	//$row['s_m_type'] = coderHelp::getMatchKeyAry(coderMember::$member_level, $row['s_m_type']);
	
	/*if($row['s_type']==5){
	$prod = Product::getItem('id, title, capacity, sdate, edate' ,$row['s_val']);
	if($prod){
		$def_prod_val=$prod['id'];
		$def_prod_name=$prod['title'];
	}
	$row['gift_val'] = $row['s_val'];
	
	}else{
		$row['event_val'] = $row['s_val'];
	}*/
	
	$fhelp -> bindData($row);
	
	$method = 'edit';
	$active = '編輯';	
	$manageinfo = '  管理者 : '.$row[$colname['admin']].' | 上次修改時間 : '.$row[$colname['updatetime']];
	
}else{
	$method = 'add';
	$active = '新增';

}
$db -> close();

?>
<!DOCTYPE html>
<html>
<head>
<?php  include_once('../head.php'); ?>
</head>
<body>

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
    
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon?>"></i> <?php echo $page_title?>管理</h1>
                <h4><?php echo $page_desc?></h4>
            </div>
        </div>
        <!-- END Page Title -->
        <?php if ($manageinfo!='') {?>
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>系統資訊 : </strong> <?php echo $manageinfo?> </div>
        <?php }?>
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3><i class="<?php echo getIconClass($method); ?>"></i> <?php echo $page_title.$active; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                    </div>
                    <div class="box-content">
                        <form  class="form-horizontal" action="save.php" id="myform" name="myform" method="post">
                            <?php echo $fhelp -> drawForm($colname['id']); ?>
                            <div class="row"> 
                                <!--left start-->
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel($colname['sdate'])?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm($colname['sdate'])?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel($colname['edate'])?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm($colname['edate'])?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel($colname['title'])?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm($colname['title'])?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel($colname['comment'])?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm($colname['comment'])?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel($colname['member_type'])?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm($colname['member_type'])?> </div>
                                    </div>
                                    <?php /*?><div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel('m_is_vip')?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm('m_is_vip')?> </div>
                                    </div><?php */?>
                                </div>
                                <!--left end--> 
                                <!--left start-->
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel($colname['type'])?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm($colname['type'])?> </div>
                                    </div>
                                    <div class="form-group" id="teacher_box" style="display:none;">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel($colname['teacher_id'])?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm($colname['teacher_id'])?> </div>
                                    </div>
                                    <div class="form-group" id="rule_box" style="display:none;">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel($colname['rule'])?> </label>
                                        <div class="col-sm-3 controls"> <?php echo $fhelp->drawForm($colname['rule'])?> </div>
                                        <div id="rule_note" class="blue"><i class="icon-exclamation-sign"></i> 請輸入滿額條件,ex:購物滿3000可享8折優惠,請輸入3000</div>
                                    </div>
                                    <?php /*?><div class="form-group" id="div_scope">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel('scope')?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm('scope')?> </div>
                                    </div>
                                    <div class="form-group" id="div_prod_cid">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel('prod_cid')?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm('prod_cid')?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel('event_type')?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp->drawForm('event_type')?> </div>
                                    </div><?php */?>
                                    <div class="form-group" id="rule_value_box" style="display:none;">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel($colname['rule_value'])?> </label>
                                        <div class="col-sm-3 controls"> <?php echo $fhelp->drawForm($colname['rule_value'])?> </div>
                                        <div id="event_note" class="blue"><i class="icon-exclamation-sign"></i> 請輸入要折扣的%。ex:打8折,請輸入80</div>
                                    </div>
                                    <?php /*?><div class="form-group" id="div_gift_val">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp->drawLabel('gift_val')?> </label>
                                        <div class="col-sm-8 controls">
                                            <div id="product_info"> </div>
                                        </div>
                                    </div><?php */?>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                            <button type="submit" class="btn btn-primary" ><i class="icon-ok"></i>完成<?php echo $active?></button>
                                            <button type="button" class="btn" onclick="if(confirm('確定要取消<?php echo $active?>?')){parent.closeBox();}"><i class="icon-remove"></i>取消<?php echo $active?></button>
                                        </div>
                                    </div>
                                </div>
                                <!--left end--> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- END Main Content -->
        
        <?php include_once('../footer.php')?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php')?>
<script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script> 
<script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.js"></script> 
<script type="text/javascript" src="../assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="../js/codersingleselect.js"></script> 
<script type="text/javascript">
<?php echo coderFormHelp::drawVaildScript();?>  
     <?php /*?> var p1=new CODER.Util.SingleSelect('product_info','gift_val','<?php echo $def_prod_val?>','<?php echo $def_prod_name?>');
      p1.title='贈品編號';
      p1.src='../product_single_selection/index.php?parentID=p1';
      p1.viewlink='../product/manage.php?id={:val}';
      p1.required=true;
      p1.init();<?php */?>
		
      $(document).ready(function(){
        $('input[name="promotion_type"]').click(function(){
           //chkType();
        });
		
		function chkType(){
        var v = $("input[name=promotion_type]:checked").val();
		console.log(v);
        if(v == 2){
           $('#teacher_box').show();
           $('#rule_box').hide();
		   $('#rule_value_box').hide();
		   
			$('#promotion_teacher_id').rules('add', {
               required: true
            });
		    $('#promotion_rule').rules('remove'); 
			$('#promotion_rule_value').rules('remove'); 
			/*$('#promotion_rule_value').rules('add', {
               required: true,
			   digits:true
            });*/ 
            
        }else if(v == 1){
			$('#teacher_box').hide();
			$('#rule_box').show();
			$('#rule_value_box').show();
			
			$('#promotion_teacher_id').rules('remove');
			$('#promotion_rule').rules('add', {
               required: true,
			   digits:true
            }); 
			$('#promotion_rule_value').rules('add', {
               required: true,
			   digits:true
            });
        }
        //chkScope();
      }
	  
	  chkType();
	  
        /*$('input[name="event_type"]').click(function(){
           chkEventType();
        });
        $('input[name="scope"]').click(function(){
           chkScope();
        });
        chkScope();
        chkRule();
        chkEventType();
        
      });

      function chkScope(){
        var v=$("input[name=scope]:checked").val();
        var v2=$("input[name=rule]:checked").val();
        if(v==0 && v2==0){
           $('#div_prod_cid').show();
        }
        else{
          $('#div_prod_cid').hide();
        }        

      }

      

      function chkEventType(){
        var v=$("input[name=event_type]:checked").val();
        var show=false;
        var note='';
        switch (v){
          case '0' :
            show=true;
            note='請輸入要折價的金額。ex:滿3000元折價200,請輸入200';
          break;
          case '1' :
            show=true;
            note='請輸入要折扣的%。ex:滿3000元打8折,請輸入80';
          break;
          case '2' :
            show=true;
            note='請輸入要贈送的紅利。ex:滿3000元贈送紅利300點,請輸入300';
          break;  
          case '3' :
            show=true;
            note='請輸入要贈送的購物金。ex:滿3000元贈送紅利購物金50,請輸入50';
          break;  
          case '4' :
            show=true;
            note='請輸入要加的紅利倍數。ex:滿3000元,紅利x2,請輸入2';
          break;       
        }
        if(show==true){
           $('#event_note').html('<i class="icon-exclamation-sign"></i>'+note);
           $('#div_event_val').show();
           $('#event_val').rules('add', {
               required: true,
               digits:true
            }); 
           $('#gift_val').val('0');
           $('#div_gift_val').hide();
        }
        else{
          $('#div_event_val').hide();
          $('#event_val').rules('remove');
          $('#div_gift_val').show();

        }
      }*/
});
</script>
</body>
</html>

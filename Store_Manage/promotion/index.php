<?php 
include_once('_config.php');
include_once('filterconfig.php');

$listHelp = new coderListHelp('table1','促銷優惠');
$listHelp -> mutileSelect = true;
$listHelp -> editLink = "manage.php";
$listHelp -> addLink = "manage.php";
$listHelp -> ajaxSrc = "service.php";
$listHelp -> delSrc = "delservice.php";
//$listHelp -> orderSrc = "orderservice.php";
//$listHelp -> orderColumn = $orderColumn;
//$listHelp -> orderDesc = $orderDesc;


$col = array();
$col[] = array('column' => $colname['title'], 'name' => '活動名稱',);
$col[] = array('column' => $colname['comment'], 'name' => '活動說明',);
//$col[] = array('column'=>'s_content', 'name'=>'優惠內容',);

$col[] = array('column' => 's_time', 'name' => '上架資訊', 'order' => false);
$col[] = array('column' => $colname['admin'], 'name' => '管理者', 'width' => '100');
$col[] = array('column' => $colname['updatetime'], 'name' => '最後更新時間', 'order' => true, 'width' => '150');

$listHelp -> Bind($col);

$listHelp -> bindFilter($filterhelp);

$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();

coderAdminLog::insert($adminuser['name'], $logkey, 'view');

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
</head>
<body >
<?php include_once('../navbar.php'); ?>
<!-- BEGIN Container -->
<div class="container" id="main-container">
    <?php include_once('../left.php'); ?>
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title --> 
        
        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li> <i class="icon-home"></i> <a href="../home/index.php">Home</a> <span class="divider"><i class="icon-angle-right"></i></span> </li>
                <?php echo $mtitle; ?>
            </ul>
        </div>
        <!-- END Breadcrumb --> 
        
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3 style="float:left"><i class="icon-table"></i> <?php echo $page_title; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-content"> <?php echo $listHelp -> drawTable(); ?> </div>
                </div>
            </div>
        </div>
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../js/coderlisthelp.js"></script> 
<script type="text/javascript">
	$( document ).ready(function() {
		$('#table1').coderlisthelp({debug:true,callback:function(obj,rows){
			obj.html('');
			var count=rows.length;
			for(var i=0;i<count;i++){
				var row=rows[i];
				var $tr=$('<tr></tr>');

				$tr.attr("orderlink","id="+row["<?php echo $colname['id'];?>"]);
				$tr.attr("editlink","id="+row["<?php echo $colname['id'];?>"]);
				$tr.attr("delkey",row["<?php echo $colname['id'];?>"]);
				$tr.attr("title",row["<?php echo $colname['title'];?>"]);
										
				$tr.append('<td>'+row["<?php echo $colname['title'];?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['comment'];?>"]+'</td>');
				//$tr.append('<td>'+row["s_content"]+'</td>');   
				$tr.append('<td>'+row["s_time"]+'</td>');	
				$tr.append('<td>'+row["<?php echo $colname['admin'];?>"]+'</td>');																
				$tr.append('<td>'+row["<?php echo $colname['updatetime'];?>"]+'</td>');								
				obj.append($tr);
			}
		}});			
	});
</script>
</body>
</html>

<?php
include_once($inc_path."lib/codermember.php");

$fhelp = new coderFormHelp();

$fobj = array();
$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);
$fobj[$colname['title']] = array('type' => 'text', 'name' => '活動名稱', 'column' => $colname['title'], 'placeholder' => '請輸入活動名稱', 'validate' => array('required' => 'yes', 'maxlength' => '20', 'minlength' => '3'));
$fobj[$colname['comment']] = array('type' => 'textarea', 'name' => '活動說明', 'column' => $colname['comment'], 'placeholder' => '請輸入活動內容');
$fobj[$colname['sdate']] = array('type' => 'text', 'name' => '上架日期', 'column' => $colname['sdate'], 'placeholder' => 'yyyy-mm-dd', 'help' => '設定活動的上架日期與下架日期,若只設定上架日期,活動會在上架日期之後永遠顯示,反之,若只設定下架日期,活動在下架日期之前也會一直顯示', 'validate' => array('date' => 'yes'));
$fobj[$colname['edate']] = array('type' => 'text', 'name' => '下架日期', 'column' => $colname['edate'], 'placeholder' => 'yyyy-mm-dd', 'help' => '設定活動的上架日期與下架日期,若只設定上架日期,活動會在上架日期之後永遠顯示,反之,若只設定下架日期,活動在下架日期之前也會一直顯示', 'validate' => array('date' => 'yes'));
$fobj[$colname['member_type']] = array('type' => 'radio', 'name' => '會員限制', 'column' => $colname['member_type'], 'ary' => coderHelp::makeAryKeyToAryElement(coderMember::$member_type, 'key', 'name'), 'help' => '設定能參加此活動的會員等級', 'mode' => 'no_default');

$fobj[$colname['type']] = array('type' => 'radio', 'name' => '活動類型', 'column' => $colname['type'], 'ary' => coderHelp::makeAryKeyToAryElement(coderPromotion::$promotion_type, 'key', 'name'), 'help' => '設定活動類型', 'mode' => 'no_default', 'validate' => array('required' => 'yes'));

$fobj[$colname['teacher_id']] = array('type' => 'select', 'name' => '講師', 'column' => $colname['teacher_id'], 'ary' => $teacher_ary);

$fobj[$colname['rule']] = array('type' => 'text', 'name' => '活動滿足條件', 'column' => $colname['rule'], 'help' => '如生日當月購物滿3000元有折扣,活動類型就為生日當月;活動滿足條件填寫3000');

$fobj[$colname['rule_value']] = array('type' => 'text', 'name' => '優惠內容', 'column' => $colname['rule_value'], 'help' => '此欄填寫內容優惠,ex:購物滿3000打8折,此欄填寫80');

//$fobj['gift_val']=array('type'=>'text','name'=>'贈品編號','column'=>'gift_val','placeholder'=>'請選擇贈品','sql'=>false);

$fhelp -> Bind($fobj);

/*****END PHP*****/
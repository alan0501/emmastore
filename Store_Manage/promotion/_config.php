<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'promotion'; 
$logkey = 'promotion';
include_once('../_config.php');
coderAdmin::vaild($authkey);

$table = coderDBConf::$promotion;
$colname = coderDBConf::$col_promotion;
$table_teacher = coderDBConf::$teacher;

$orderColumn = $colname['id'];
$orderDesc = "desc";

$auth = coderAdmin::Auth($authkey);

$page = request_pag("page");
$page_title = $auth['name'];
$page_desc = "{$page_title}管理-此區用來管理{$page_title}的項目,您可以進行列表檢視/查詢,或使用新增/修改/刪除/排序等操作。";
$mtitle = '<li class="active">'.$page_title.'</li>';
$mainicon = $auth['icon'];


$teacher_ary = class_teacher::getAllList_name();

/*****END PHP*****/
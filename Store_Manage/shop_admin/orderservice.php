<?php
include_once('_config.php');

$success = false;
$count = 0;
$msg = "未知錯誤,請聯絡系統管理員";
$order_id = post('order_id',1);
$order_key = post('order_key',1);
$method = post('method',1);
$where = '';

$sqlstr = $help -> getSQLStr();
$where .= $sqlstr -> SQL != '' ? ' AND '.$sqlstr -> SQL : '';

if($order_id > 0 && $order_id != ""){
	$db = Database::DB();
	try{
		if($orderDesc == "desc"){
			coderlistorderhelp::changeDescOrder($method, $table, $orderColumn, $order_id, $order_key, $where);
		}else{
			coderlistorderhelp::changeAscOrder($method, $table, $orderColumn, $order_id, $order_key, $where);
		}
		$success = true;
		//class_index_pic::clearCache();
		$db -> close();
	}catch(Execption $e){
		$msg = $e -> getMessage();
	}
}else{
	$msg = "未設定排序資料";
}

$result['result'] = $success;
$result['count'] = $count;
$result['msg'] = $msg;
echo json_encode($result);

/*****END PHP*****/
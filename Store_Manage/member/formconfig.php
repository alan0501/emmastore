<?php
include_once('_config.php');

$fhelp = new coderFormHelp();

$fobj = array();
$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);


$fobj[$colname['is_show']] = array('type' => 'checkbox', 'name' => '停權', 'column' => $colname['is_show'], 'value' => '1', 'default' => '1');
$fobj[$colname['name']] = array('type' => 'text', 'name' => '會員姓名', 'column' => $colname['name'], 'placeholder' => '請填寫會員姓名', 'validate' => array('required' => 'yes', 'maxlength' => '20', 'minlength' => '1'));
$fobj[$colname['phone']] = array('type' => 'text', 'name' => '連絡電話', 'column' => $colname['phone'], 'placeholder' => '請填寫連絡電話', 'validate' => array('required' => 'yes', 'maxlength' => '20', 'minlength' => '1'));
$fobj[$colname['email']] = array('type' => 'text', 'name' => '電子信箱', 'column' => $colname['email'], 'placeholder' => '請填寫電子信箱', 'validate' => array('required' => 'yes', 'maxlength' => '255', 'minlength' => '1'));
$fobj[$colname['address']] = array('type' => 'text', 'name' => '地址', 'column' => $colname['address'], 'placeholder' => '請填寫地址', 'sql'=>true, 'validate' => array('required' => 'yes', 'maxlength' => '255', 'minlength' => '1'));

$fhelp -> Bind($fobj);

/*****END PHP*****/
<?php
include_once('_config.php');
$id = get('id');
$manageinfo = "";
$db = Database::DB();
include_once('formconfig.php');

if($id != ""){
    $row = $db -> query_prepare_first("SELECT * FROM $table WHERE {$colname['id']} = $id");
    $fhelp -> bindData($row);
    $method = 'edit';
    $active = '編輯';
    $manageinfo = '管理者 : '.$row[$colname['manager']].' | 建立時間 : '.$row[$colname['create_time']].' | 上次修改時間 : '.$row[$colname['update_time']];
	
	include_once('../bonus/filterconfig.php');
	$listHelp = new coderListHelp('table_bonus', '紅利管理');
	$listHelp -> mutileSelect = false;
	$listHelp -> ajaxSrc = "../bonus/service.php?m_id=".$id;
	
	$col = array();
	$col[] = array('column' => 'bonus', 'name' => '紅利計算', 'order' => false, 'width' => '100');
	$col[] = array('column' => 'comment', 'name' => '購物金兌換/使用記錄', 'order' => false);
	$col[] = array('column' => 'time', 'name' => '日期', 'order' => true, 'width' => '150');
	
	$listHelp -> Bind($col);
	//$listHelp -> bindFilter($filterhelp);
	
	
}else{
	$method = 'add';
	$active = '新增';
}

$db -> close();

?>
<!DOCTYPE html>
<html>
<head>
<?php  include_once('../head.php') ?>
<link href="../css/codertextgroup.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
</script>
</head>
<body>

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
    
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title -->
        <?php if($manageinfo != ''){?>
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>系統資訊 : </strong> <?php echo $manageinfo; ?> </div>
        <?php }?>
        <!-- BEGIN Main Content -->
        <div class="row">
            <form  class="form-horizontal" action="save.php" id="myform" name="myform" method="post">
                <?php echo $fhelp -> drawForm($colname['id']); ?>
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-title">
                            <h3><i class="<?php echo getIconClass($method); ?>"></i> <?php echo $page_title.$active; ?></h3>
                            <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        </div>
                        <div class="box-content">
                            <div class="row"> 
                                <!--left start-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['is_show']); ?> </label>
                                        <div class="col-sm-4 controls"> <?php echo $fhelp -> drawForm($colname['is_show']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > FB_ID </label>
                                        <div class="col-sm-2 control-label" style="text-align:left;"> <?php echo $row[$colname['fb_id']]; ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > 會員帳號 </label>
                                        <div class="col-sm-4 control-label" style="text-align:left;"> <?php echo $row[$colname['account']]; ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['name']); ?> </label>
                                        <div class="col-sm-4 controls"> <?php echo $fhelp -> drawForm($colname['name']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['phone']); ?> </label>
                                        <div class="col-sm-4 controls"> <?php echo $fhelp -> drawForm($colname['phone']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['email']); ?> </label>
                                        <div class="col-sm-4 controls"> <?php echo $fhelp -> drawForm($colname['email']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['address']); ?> </label>
                                        <div class="col-sm-4 controls"> <?php echo $fhelp -> drawForm($colname['address']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > 註冊時間 </label>
                                        <div class="col-sm-4 control-label" style="text-align:left;"> <?php echo $row[$colname['create_time']]; ?> </div>
                                    </div>
                                    <div class="form-group text-center">
                                        <div class="">
                                            <button type="submit" class="btn btn-primary" ><i class="icon-ok"></i>完成<?php echo $active; ?></button>
                                            &nbsp;&nbsp;&nbsp;
                                            <button type="button" class="btn" onClick="if(confirm('確定要取消<?php echo $active; ?>?')){parent.closeBox();}"><i class="icon-remove"></i>取消<?php echo $active; ?></button>
                                        </div>
                                    </div>
                                </div>
                                <!--left end--> 
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
		if($method == 'edit'){
		?>
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-table"></i> 購物金列表 > 總共累積紅利 : <span class="total_money"></span></h3>
                    <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                </div>
                <div class="box-content">
                    <div class="row"> 
                        <!--left start-->
                        <div class="col-md-12 ">
                            <div class="form-group"> <?php echo $listHelp -> drawTable(); ?> </div>
                        </div>
                        <!--left end--> 
                    </div>
                </div>
            </div>
        </div>
        <?php
		}
		?>
        <!-- END Main Content -->
        
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script> 
<script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.js"></script> 
<script type="text/javascript" src="../js/jquery.twzipcode.js"></script> 
<script type="text/javascript">
<?php echo coderFormHelp::drawVaildScript('myform', $fobj); ?>
</script>
<script type="text/javascript" src="../js/coderlisthelp.js"></script> 
<script>
var total = 0;
$( document ).ready(function() {
	$('#table_bonus').coderlisthelp({
		debug : true,
		callback : function(obj,rows){
		obj.html('');
		var count = rows.length;
		total = rows[0]["total"];
		for(var i = 0; i < count; i ++){
			var row = rows[i];
			var $tr = $('<tr></tr>');

			<?php /*?>$tr.attr("sendemail",row["<?php echo $colname_record['id']?>"]);
			$tr.attr("delkey",row["<?php echo $colname_record['id']?>"]);
			$tr.attr("title",row["lc_btn"]+"的課程紀錄");<?php */?>

			$tr.append('<td>'+row["bonus"]+'</td>');
			$tr.append('<td>'+row["comment"]+'</td>');
			$tr.append('<td>'+row["time"]+'</td>');
			obj.append($tr);
			$("span.total_money").text(total);
		}}, 
		
	});
	
});
</script>
</body>
</html>

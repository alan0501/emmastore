<?php
include_once('_config.php');
include_once('filterconfig.php');

$listHelp = new coderListHelp('table1','會員');
$listHelp -> mutileSelect = true;
$listHelp -> editLink = "manage.php";
//$listHelp -> addLink = "manage.php";
$listHelp -> ajaxSrc = "service.php";
$listHelp -> delSrc = "delservice.php";
//$listHelp -> orderSrc = "orderservice.php";
//$listHelp -> orderColumn = $orderColumn;
//$listHelp -> orderDesc = $orderDesc;


$col = array();
$col[] = array('column' => $colname['id'], 'name'=>'ID', 'order' => true, 'width' => '60');
$col[] = array('column' => $colname['fb_id'], 'name'=>'FB_ID', 'order' => true, 'width' => '80');
$col[] = array('column' => $colname['account'], 'name' => '帳號', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['name'], 'name' => '會員姓名', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['phone'], 'name' => '連絡電話', 'order' => true, 'width' => '110');
$col[] = array('column' => $colname['email'], 'name' => '電子信箱', 'order' => true, 'width' => '120');
$col[] = array('column' => $colname['address'], 'name' => '地址', 'order'=>true);
$col[] = array('column' => $colname['is_show'], 'name' => '停權', 'order' => true, 'width' => '60');
$col[] = array('column' => $colname['manager'], 'name' =>' 管理者', 'width' => '80', 'order' => true);
$col[] = array('column' => $colname['update_time'], 'name' => '最後更新', 'order' => true, 'width' => '100');

$listHelp -> Bind($col);

$listHelp -> bindFilter($filterhelp);

$db = Database::DB();

coderAdminLog::insert($adminuser['username'], $logkey, 'view');

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
</head>
<body >
<?php include_once('../navbar.php'); ?>
<!-- BEGIN Container -->
<div class="container" id="main-container">
    <?php include_once('../left.php'); ?>
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title --> 
        
        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li> <i class="icon-home"></i> <a href="../home/index.php">Home</a> <span class="divider"><i class="icon-angle-right"></i></span> </li>
                <?php echo $mtitle; ?>
            </ul>
        </div>
        <!-- END Breadcrumb --> 
        
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3 style="float:left"><i class="icon-table"></i> <?php echo $page_title; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-content"> <?php echo $listHelp -> drawTable(); ?> </div>
                </div>
            </div>
        </div>
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../js/coderlisthelp.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	$('#table1').coderlisthelp({
		debug : true,
		callback : function(obj, rows){
			obj.html('');
			var count = rows.length;
			for(var i = 0; i < count; i ++){
				var row = rows[i];
				var $tr = $('<tr></tr>');
				$tr.attr("orderlink", "order_id="+row["<?php echo $colname['id']; ?>"]+"&order_key=<?php echo $colname['id']; ?>");
				$tr.attr("editlink", "id="+row["<?php echo $colname['id']; ?>"]);
				$tr.attr("delkey", row["<?php echo $colname['id']; ?>"]);
				$tr.attr("title", "ID:"+row["<?php echo $colname['id']; ?>"]+"之會員");
				
				$tr.append('<td>'+row["<?php echo $colname['id']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['fb_id']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['account']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['name']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['phone']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['email']; ?>"]+'</td>');						
				$tr.append('<td>'+row["<?php echo $colname['address']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['is_show']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['manager']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['update_time']; ?>"]+'</td>');
				obj.append($tr);
			}
		},
		listComplete:function(){
			$('#table1').find('img').click(function(){
				$.colorbox({href:$(this).attr('src'), initialWidth:'50px', initialHeight:'50px', maxHeight:'80%'});
			});
		}
	});
});
</script>
</body>
</html>

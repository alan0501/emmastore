<?php 
include_once('_config.php');

$listHelp = new coderListHelp('table1', '使用記錄');
$listHelp -> ajaxSrc = "service.php";

$col = array();
$col[] = array('column' => $colname['id'], 'name' => 'ID', 'order' => true, 'width' => '60', 'def_desc' => 'desc');
$col[] = array('column' => $colname['name'], 'name' => '帳號', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['type'], 'name' => '模組', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['action'], 'name' => '操作', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['comment'], 'name' => '資訊', 'order' => true);
$col[] = array('column' => $colname['ip'], 'name' => 'IP', 'width' => '100');
$col[] = array('column' => $colname['create_time'], 'name' => '操作時間', 'order' => true, 'width' => '150');
$listHelp -> Bind($col);

$listHelp -> bindFilter($help);

$db = Database::DB();

coderAdminStoreLog::insert($adminuser['username'], 'admin', 'view', '操作記錄');

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
</head>
<body >
<?php include_once('../navbar.php'); ?>
<!-- BEGIN Container -->
<div class="container" id="main-container">
    <?php include_once('../left.php'); ?>
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title --> 
        
        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li> <i class="icon-home"></i> <a href="../home/index.php">Home</a> <span class="divider"><i class="icon-angle-right"></i></span> </li>
                <?php echo $mtitle; ?>
            </ul>
        </div>
        <!-- END Breadcrumb --> 
        
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3 style="float:left"><i class="icon-table"></i> <?php echo $page_title; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-content"> <?php echo $listHelp->drawTable(); ?> </div>
                </div>
            </div>
        </div>
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../js/coderlisthelp.js"></script> 
<script type="text/javascript">
$( document ).ready(function() {
	$('#table1').coderlisthelp({
		debug : true, 
		callback : function(obj, rows){
			obj.html('');
			var count = rows.length;
			for(var i = 0; i < count; i ++){
				var row = rows[i];
				var $tr = $('<tr></tr>');
				
				$tr.append('<td>'+row["<?php echo $colname['id']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['name']; ?>"]+'</td>');	
				$tr.append('<td>'+row["<?php echo $colname['type']; ?>"]+'</td>');		
				$tr.append('<td>'+row["<?php echo $colname['action']; ?>"]+'</td>');							
				$tr.append('<td>'+row["<?php echo $colname['comment']; ?>"]+'</td>');										
				$tr.append('<td>'+row["<?php echo $colname['ip']; ?>"]+'</td>');								
				$tr.append('<td>'+row["<?php echo $colname['create_time']; ?>"]+'</td>');
				obj.append($tr);
			}
		}
	});			
});
</script>
</body>
</html>

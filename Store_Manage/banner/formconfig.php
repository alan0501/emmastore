<?php
$fhelp = new coderFormHelp();

$fobj = array();
$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);
$fobj['pic_o'] = array('type' => 'hidden', 'name' => '圖', 'column' => $colname['pic'], 'sql' => false);

$fobj[$colname['is_show']] = array('type' => 'checkbox', 'name' => '啟用', 'column' => $colname['is_show'], 'value' => '1', 'default' => '1');
$fobj[$colname['pic']] = array('type' => 'pic', 'name' => '圖', 'column' => $colname['pic'], 'validate' => array('required' => 'yes'));
$fobj[$colname['link']] = array('type' => 'text', 'name' => '連結', 'column' => $colname['link'], 'placeholder' => '請填寫連結',
 'validate' => array('maxlength' => '255', 'minlength' => '1'));

$fhelp -> Bind($fobj);

/*****END PHP*****/
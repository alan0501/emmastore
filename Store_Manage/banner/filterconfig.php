<?php
$filterhelp = new coderFilterHelp();

$ary = array();
$ary[] = array('column' => $colname['manager'], 'name' => '管理員');
$obj[] = array('type' => 'keyword', 'name' => '關鍵字', 'sql' => true, 'ary' => $ary);

$_is_show = array('type' => 'select', 'name' => '公開', 'column' => $colname['is_show'], 'sql' => true, 'ary' => array(array('value' => '1', 'name' => '是'), array('value' => '0', 'name' => '否')));
$obj[] = $_is_show;

$obj[] = array('type' => 'dategroup', 'sql' => true, 'column' => 'dategroup', 'ary' => array(array('name' => '建立日期', 'column' => $colname['create_time']), array('name' => '最後修改日期', 'column' => $colname['update_time'])));

$filterhelp -> Bind($obj);

/*****END PHP*****/
<?php
$fhelp = new coderFormHelp();
$fobj = array();
$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);
$fobj[$colname['is_show']] = array('type' => 'checkbox', 'name' => '啟用', 'column' => $colname['is_show'], 'value' => '1', 'default' => '1');


$fobj[$colname['ptype1_id']] = array('type' => 'select', 'name' => '所屬一階分類', 'column' => $colname['ptype1_id'], 'ary' => $ptype1_ary, 'validate' => array('required' => 'yes',));

$fobj[$colname['name']] = array('type' => 'text', 'name' => '二階分類名稱', 'column' => $colname['name'], 'placeholder' => '請填寫二階分類名稱', 'validate' => array('required' => 'yes', 'maxlength' => '30', 'minlength' => '1'));
	
$fhelp -> Bind($fobj);

/*****END PHP*****/
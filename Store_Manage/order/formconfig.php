<?php
$fhelp = new coderFormHelp();

$fobj = array();
$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);
$fobj[$colname['sno']] = array('type' => 'text', 'name' => '訂單編號', 'column' => $colname['sno'], 'sql' => false, 'readonly' => true);
$fobj[$colname['express_sno']] = array('type' => 'text', 'name' => '物流單號', 'column' => $colname['express_sno'], 'sql' => true);
$fobj[$colname['order_state']] = array('type' => 'radio', 'name' => '訂單狀態', 'column' => $colname['order_state'], 'ary' => coderHelp::makeAryKeyToAryElement(coderOrder::$order_state, 'key', 'name'), 'help' => '設定訂單狀態,若將訂單狀態修改為交易完成或交易取消,則訂單狀態就無法再修改', 'equal' => '=',  'mode' => 'no_default');

$fobj[$colname['payment_state']] = array('type' => 'radio', 'name' => '付款狀態', 'column' => $colname['payment_state'], 'ary' => coderHelp::makeAryKeyToAryElement(coderOrder::$payment_state, 'key', 'name'), 'help' => '設定訂單的付款狀態', 'equal' => '=', 'mode' => 'no_default');

//$fobj[$colname['express_type']] = array('type' => 'radio', 'name' => '取貨方式', 'column' => $colname['express_type'], 'ary' => coderHelp::makeAryKeyToAryElement(coderOrder::$express_type, 'key', 'name'), 'equal' => '=', 'mode' => 'no_default');

$fobj[$colname['comment']] = array('type' => 'textarea', 'name' => '訂單備註', 'column' => $colname['comment']);

$fhelp -> Bind($fobj);

/*****END PHP*****/
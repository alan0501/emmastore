<?php 
include_once('_config.php');
include_once('filterconfig.php');

$listHelp = new coderListHelp('table1','訂單');
$listHelp -> mutileSelect = false;
$listHelp -> editLink = "manage.php";
$listHelp -> ajaxSrc = "service.php";


$col = array();
$col[] = array('column' => $colname["sno"], 'name' => '訂單編號', 'order' => true, 'width' => '90', 'def_desc' => 'desc');
$col[] = array('column' => $colname["order_state"], 'name' => '狀態', 'order' => true, 'width' => '90');
$col[] = array('column' => $colname["payment_state"], 'name' => '付款', 'order' => true, 'width' => '90');
$col[] = array('column' => $colname["total_price"], 'name' => '金額', 'order' => true);
//$col[]=array('column'=>'o_discount','name'=>'折扣','order'=>true);
$col[] = array('column' => $colname["freight"], 'name' => '運費', 'order' => true);
//$col[]=array('column'=>'o_disbonus','name'=>'紅利折抵','order'=>true);
//$col[]=array('column'=>'o_dismoney','name'=>'購物金折抵','order'=>true);
$col[] = array('column' => $colname["total_price"]+$colname["freight"], 'name' => '應付金額', 'order' => true);
$col[] = array('column' => $colname["shop_id"], 'name' => '店家', 'order' => true);
$col[] = array('column' => $colname["purchaser_name"], 'name' => '訂購人', 'order' => true);
$col[] = array('column' => $colname["recipient_name"], 'name' => '收件人', 'order' => true);
$col[] = array('column' => $colname["recipient_address"], 'name' => '收件地址', 'order' => true);

$col[] = array('column' => $colname["manager"], 'name' => '管理者', 'width' => '100', 'visible-lg' => false);
$col[] = array('column' => $colname["update_time"], 'name' => '最後更新時間', 'order' => true, 'width' => '150', 'visible-lg' => false);

$listHelp -> Bind($col);

$listHelp -> bindFilter($filterhelp);

$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();

coderAdminLog::insert($adminuser['name'], $logkey, 'view');

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
</head>
<body >
<?php include_once('../navbar.php'); ?>
<!-- BEGIN Container -->
<div class="container" id="main-container">
    <?php include_once('../left.php'); ?>
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title --> 
        
        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li> <i class="icon-home"></i> <a href="../home/index.php">Home</a> <span class="divider"><i class="icon-angle-right"></i></span> </li>
                <?php echo $mtitle; ?>
            </ul>
        </div>
        <!-- END Breadcrumb --> 
        
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3 style="float:left"><i class="icon-table"></i> <?php echo $page_title; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-content"> <?php echo $listHelp -> drawTable(); ?> </div>
                </div>
            </div>
        </div>
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../js/coderlisthelp.js"></script> 
<script type="text/javascript">
$( document ).ready(function() {
	$('#table1').coderlisthelp({
		debug : true,
		callback : function(obj, rows){
			obj.html('');
			var count = rows.length;
			for(var i = 0; i < count; i ++){
				var row = rows[i];
				var $tr = $('<tr></tr>');
	
				$tr.attr("orderlink", "id="+row["<?php echo $colname['id']; ?>"]);
				$tr.attr("editlink", "no="+row["<?php echo $colname['sno']; ?>"]);
				<!--$tr.attr("delkey",row["id"]);-->
				$tr.attr("title", row["<?php echo $colname['sno']; ?>"]);
										
				$tr.append('<td>'+row["<?php echo $colname['sno']; ?>"]+'</td>');
				$tr.append('<td>'+row["o_state"]+'</td>');							
				$tr.append('<td>'+row["o_pay_state"]+'</td>');	
				$tr.append('<td>'+row["<?php echo $colname['total_price']; ?>"]+'</td>');   
				<!--$tr.append('<td>'+row["o_discount"]+'</td>'); -->  
				$tr.append('<td>'+row["<?php echo $colname['freight']; ?>"]+'</td>');   
				<!--$tr.append('<td>'+row["o_disbonus"]+'</td>');-->      
				$tr.append('<td ><span class="badge badge-important">NT$'+row["pay_price"]+'</span></td>');	
				<?php /*?>$tr.append('<td>'+chkMember(row["<?php echo $colname['member_id']; ?>"])+row["md_name"]+'</td>');<?php */?>  
				$tr.append('<td>'+row["shop_name"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['purchaser_name']; ?>"]+'</td>'); 
				$tr.append('<td>'+row["<?php echo $colname['recipient_name']; ?>"]+'</td>');  
				$tr.append('<td>'+row["<?php echo $colname['recipient_address']; ?>"]+'</td>');  
				$tr.append('<td class="visible-lg">'+row["<?php echo $colname['manager']; ?>"]+'</td>');																
				$tr.append('<td class="visible-lg">'+row["o_updatetime"]+'</td>');								
				obj.append($tr);
			}
		}
	});			
});

function chkMember(mid){
	if(mid > 0){
		return '<i class="icon-user lime"></i>';
	}
	return '';
}
</script>
</body>
</html>

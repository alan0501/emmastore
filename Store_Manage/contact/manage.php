<?php
include('_config.php');
$id=get('id');
$manageinfo="";
$db = Database::DB();
include('formconfig.php');

if($id>0)
{
  $row=$db->query_prepare_first("select * from $table where $idColumn='$id'");

	$fhelp->bindData($row);

	$method='edit';
	$active='瀏覽';
  $manageinfo=' 發訊時間 : '.$row[$colname['createtime']];
}else{
  die();
}
$db->close();

?>
<!DOCTYPE html>
<html>
    <head>
		<?php  include('../head.php') ?>
        <script language="javascript" type="text/javascript">
		</script>
    </head>
    <body>



        <!-- BEGIN Container -->
        <div class="container" id="main-container">

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="<?php echo $mainicon?>"></i> <?php echo $page_title?>管理</h1>
                        <h4><?php echo $page_desc?></h4>
                    </div>
                </div>
                <!-- END Page Title -->
                <div class="alert alert-info">
                       <button class="close" data-dismiss="alert">&times;</button>
                      <strong>系統資訊 : </strong> <?php echo $manageinfo?>
                 </div>
                <!-- BEGIN Main Content -->
                <div class="row">
<form  class="form-horizontal" action="save.php" id="myform" name="myform" method="post">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="<?php echo getIconClass($method)?>"></i> <?php echo $page_title.$active?></h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
      	          <div class="row">
                      <!--left start-->
                      <div class="col-md-12 ">
                          <div class="col-md-12">
                              <div class="well">
                                <dl class="dl-horizontal">
                                  <dt><p class="text-success">發訊時間</p></dt>
                                  <dd><p><?php echo $row[$colname['createtime']];?></p></dd>

                                  <dt><p class="text-success">姓名</p></dt>
                                  <dd><p><?php echo $row[$colname['name']];?></p></dd>

                                  <dt><p class="text-success">稱呼</p></dt>
                                  <dd><p><?php echo $row[$colname['appname']];?></p></dd>

                                  <dt><p class="text-success">電話</p></dt>
                                  <dd><p><?php echo $row[$colname['phone']];?></p></dd>

                                  <dt><p class="text-success">E-mail</p></dt>
                                  <dd><p><?php echo $row[$colname['email']];?></p></dd>

                                  <dt><p class="text-success">訊息內容</p></dt>
                                  <dd><p><?php echo $row[$colname['content']];?></p></dd>
                                </dl>
                                <div class="form-group">
                                    <div class="col-sm-3 col-sm-offset-2 col-lg-3 col-lg-offset-2">
                                      <button type="button" class="btn btn-primary" onClick="parent.closeBox();"><i class="icon-ok"></i>關閉<?php echo $active?></button>
                                    </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      <!--left end-->
           				</div>
            </div>
        </div>
    </div>
</form>

</div>
<?php include('../footer.php')?>
                <!-- END Main Content -->
                <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a>
            </div>
            <!-- END Content -->
        </div>
        <!-- END Container -->


		<?php include('../js.php')?>
        <link href="../css/codertextgroup.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
        <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.js"></script>

		<script type="text/javascript">
			<?php echo coderFormHelp::drawVaildScript();?>
      $('.sendmailbtn').click(function(){
          var $this=$(this);
          $.ajax({
              url:'sendemail.php',
              cache: false,
              type:"POST",
              data:{id:'<?php echo $id;?>'},
              dataType:"json",
              success:function(data){
                  if(data['result']==true){
                    showNotice('ok','轉寄完成','您己成功轉寄此篇聯絡訊息!。');
                  }
                  else{
                    showNotice('alert','轉寄失敗','轉寄失敗。'.data['msg']);
                  }
              },
              error:function(xhr, ajaxOptions, thrownError){
                  showNotice('alert','網站頁面管理','系統發生錯誤,請聯絡系統管理員。');
              }
          });
      });
		</script>
    </body>
</html>

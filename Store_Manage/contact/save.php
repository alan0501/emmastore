<?php
include('_config.php');
include('formconfig.php');


$errorhandle=new coderErrorHandle();
try{
  if(post('id')>0){
	  $method='edit';
	  $active='編輯';
  }
  else{
	  $method='add';
	  $active='新增';
  }

  $data=$fhelp->getSendData();
  $error=$fhelp->vaild($data);

  if(count($error)>0){
	 $msg=implode('\r\n',$error);
	  throw new Exception($msg);
  }
  $data['admin']=$adminuser['username'];
  $data['updatetime']=datetime();



	  $db = Database::DB();

	  if($method=='edit'){
		  $id=post('id');
		  $db->query_update($table,$data," id={$id}");
	  }
	  else{
		  $data['createtime']=datetime();
		  $data['ind']=coderListOrderHelp::getMaxInd($table,'ind');
		  $id=$db->query_insert($table,$data);
	  }

	  

	  coderAdminLog::insert($adminuser['username'],$logkey,$method,$data['title']);
	  echo showParentSaveNote($page_title,$active,$data['title'],"manage.php?id=".$id);
	  $db->close();

}
catch(Exception $e){
	$errorhandle->setException($e); // 收集例外
}

if ($errorhandle->isException()) {
    $errorhandle->showError();
}

?>
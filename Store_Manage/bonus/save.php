<?php
include_once('_config.php');
$id = post($colname['id']);
include_once('formconfig.php');


$errorhandle = new coderErrorHandle();
try{  
  if($id > 0){
	  $method = 'edit';
	  $active = '編輯';
  }else{
	  $method = 'add';
	  $active = '新增';
  }
  $data = $fhelp -> getSendData();
  $error = $fhelp -> vaild($data);
  
  if(count($error) > 0){
	 $msg = implode('\r\n', $error);
	  throw new Exception($msg);
  }

  $data[$colname['admin']] = $adminuser['name'];

	  $db = new Database($HS, $ID, $PW, $DB);
	  $db -> connect();
	  if($method == 'edit'){
		  unset($data[$colname['order_sno']]);
		  $data[$colname['sno']] = strtoupper($data[$colname['sno']]);
		  $data[$colname['updatetime']] = datetime();
		  $db -> query_update($table,$data," {$colname['id']}={$id}");			
	  }else{
		  $n = 4; //一次隨機字數
		  $data[$colname['state']] = 1; //未使用
		  $num = $data['num'];
		  unset($data['num']);
		  for($i = 0; $i < $num; $i ++){			  
			  $data[$colname['sno']] = strtoupper(generatorPassword($n).'-'.generatorPassword($n).'-'.generatorPassword($n));
			  $data[$colname['createtime']] = datetime();
		  	  $db -> query_insert($table, $data);
		  }
	  }
	  coderAdminLog::insert($adminuser['name'], $logkey, $method, 'coupon');
	  
	  if($id > 0){
	   	 echo showParentSaveNote($page_title,$active, 'Coupon', "manage.php?id=".$id);
	  }else{
		 echo showParentSaveNote($page_title,$active, 'Coupon', "index.php"); 
	  }
	  $db -> close();

}
catch(Exception $e){
	$errorhandle -> setException($e); // 收集例外
}

if ($errorhandle -> isException()) {
    $errorhandle -> showError();
}

/*****END PHP*****/
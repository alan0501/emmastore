<?php
include_once('_config.php');
ob_start();
include_once('filterconfig.php');

//$errorhandle=new coderErrorHandle();
//try{  
	$db = new Database($HS, $ID, $PW, $DB);
	$db->connect();
	$sHelp=new coderSelectHelp($db);
	$sHelp->select="*";
	$sHelp->table=$table;
	$sHelp->page_size=-1;
	//$sHelp->page=get("page");
//
	$sHelp->orderby=get("orderkey",1);
	$sHelp->orderdesc=get("orderdesc",1);
//	
	$sqlstr=$filterhelp->getSQLStr();
	$sHelp->where=$sqlstr->SQL;	

	$rows=$sHelp->getList();
		
	for($i=0;$i<count($rows);$i++){
		$rows[$i]['c_updatetime'] = !is_null($rows[$i]['c_updatetime']) ? datetime("Y/m/d H:i:s",$rows[$i]['c_updatetime']) : '';
		$rows[$i]['c_state'] = coderSales::$coupon_state[$rows[$i]['c_state']];
	}
	
	//print_r($rows);
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>excel</title>
</head>
<body>
<table width="100%" border="1" cellspacing="0" class="list-table">
  <thead>
    <tr>
      <th width="50" align="center" >ID</th>
      <th width="150" align="center" >Coupon序號</th>
      <th width="100" align="center" >折扣金額</th>
      <th width="100" align="center" >使用狀態</th>
      <th width="100" align="center" >訂單編號</th>
      <th width="100" align="center" >管理者</th>
      <th width="100" align="center" >建立時間</th>
      <th width="100" align="center" >最後更新時間</th>
    </tr>
  </thead>
  <tbody>
  <?php
			foreach($rows as $row){
		?>
    <tr>
      <th align="center"><?php echo $row["c_id"];?></th>
      <td align="center"><?php echo $row["c_prefix"].'-'.$row["c_coupon"];?></td>
      <td align="center" ><?php echo $row["c_discount"]; ?></td>
      <td align="center" ><?php echo $row["c_state"]; ?></td>
      <td align="center" ><?php echo $row["c_order_no"] ? $row["c_order_no"] : ''; ?></td>
      <td align="center" ><?php echo $row["c_admin"];?></td>
      <td align="center" ><?php echo $row["c_createtime"]; ?></td>
      <td align="center" ><?php echo $row["c_updatetime"]?></td>
    <?php
			}
			$db->close();
		?>
  </tbody>
  <tfoot>
  </tfoot>
</table>
</body>
</html>	
<?php 
$outStr=ob_get_contents(); 
ob_end_clean(); 
	
header("Content-type:application/vnd.ms-excel");
header("Accept-Ranges: bytes"); 
header("Accept-Length: ".strlen($outStr)); 
		
header("Content-Disposition: attachment; filename=coupon.xls"); 
// 輸出文件內容          
echo $outStr;
?>	

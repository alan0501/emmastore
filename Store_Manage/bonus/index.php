<?php 
include_once('_config.php');
include_once('filterconfig.php');

$listHelp = new coderListHelp('table_bonus', '紅利管理');
//$listHelp -> mutileSelect = true;
//$listHelp -> editLink = "manage.php";
//$listHelp -> addLink = "manage.php";
$listHelp -> ajaxSrc = "service.php";
//$listHelp -> delSrc = "delservice.php";
//$listHelp -> excelLink = "savetoexcel.php";
//$listHelp->orderSrc="orderservice.php";
//$listHelp->orderColumn=$orderColumn;
//$listHelp->orderDesc=$orderDesc;


$col = array();
$col[] = array('column' => $colname['id'], 'name' => 'ID', 'order' => true, 'width' => '60');
$col[] = array('column' => $colname['sno'], 'name' => 'Coupon序號', 'order' => true);
$col[] = array('column' => $colname['discount'], 'name' => '折扣金額', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['state'], 'name' => '使用狀態', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['order_sno'], 'name' => '訂單編號', 'order' => true, 'width' => '150');
$col[] = array('column' => $colname['admin'], 'name' => '管理者', 'width' => '80');
$col[] = array('column' => $colname['createtime'], 'name' => '建立時間', 'order' => true, 'width' => '150');
$col[] = array('column' => $colname['updatetime'], 'name' => '最後更新時間', 'order' => true, 'width' => '150');

$listHelp -> Bind($col);

$listHelp -> bindFilter($filterhelp);

$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();

coderAdminLog::insert($adminuser['name'], $logkey, 'view');

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php')?>
</head>
<body >
<?php include_once('../navbar.php')?>
<!-- BEGIN Container -->
<div class="container" id="main-container">
    <?php include_once('../left.php')?>
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title --> 
        
        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li> <i class="icon-home"></i> <a href="../home/index.php">Home</a> <span class="divider"><i class="icon-angle-right"></i></span> </li>
                <?php echo $mtitle; ?>
            </ul>
        </div>
        <!-- END Breadcrumb --> 
        
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3 style="float:left"><i class="icon-table"></i> <?php echo $page_title?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-content"> <?php echo $listHelp -> drawTable()?> </div>
                </div>
            </div>
        </div>
        <?php include_once('../footer.php')?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php')?>
<script type="text/javascript" src="../js/coderlisthelp.js"></script> 
<script type="text/javascript">
$( document ).ready(function() {
	$('#table1').coderlisthelp({debug:true,callback:function(obj,rows){
		obj.html('');
		var count=rows.length;
		for(var i=0;i<count;i++){
			var row=rows[i];
			var $tr=$('<tr></tr>');

			$tr.attr("orderlink","id="+row["<?php echo $colname['id'];?>"]);
			$tr.attr("editlink","id="+row["<?php echo $colname['id'];?>"]);
			$tr.attr("delkey",row["<?php echo $colname['id'];?>"]);
			$tr.attr("title",row["<?php echo $colname['sno'];?>"]);
			$tr.append('<td>'+row["<?php echo $colname['id'];?>"]+'</td>');
			$tr.append('<td>'+row["<?php echo $colname['sno'];?>"]+'</td>');
			$tr.append('<td>'+row["<?php echo $colname['discount'];?>"]+'</td>');							
			$tr.append('<td>'+row["<?php echo $colname['state'];?>"]+'</td>');	
			$tr.append('<td>'+(row["<?php echo $colname['order_sno'];?>"] ? row["<?php echo $colname['order_sno'];?>"] : '')+'</td>');
			$tr.append('<td>'+row["<?php echo $colname['admin'];?>"]+'</td>');	
			$tr.append('<td>'+row["<?php echo $colname['createtime'];?>"]+'</td>');										
			$tr.append('<td>'+row["<?php echo $colname['updatetime'];?>"]+'</td>');								
			obj.append($tr);
		}
	}});
});
</script>
</body>
</html>

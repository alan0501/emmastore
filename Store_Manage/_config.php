<?php
include_once($inc_path."_config.php");
include_once($inc_path."_web_func.php");
//$weburl = $weburl;
$path_cache = $admin_path_cache;

include_once($inc_path.'_cache.php');


//取得登入USER順便檢查是否登入
$db = Database::DB();
coderAdminStore::getUser_cookie();
$adminuser = coderAdminStore::getUser();

function showParentSaveNote($auth_name, $active, $title, $link = ""){
	$str = '<script>parent.closeBox();parent.showNotice("ok","'.$auth_name.$active.'完成。",\''.$title.'己'.$active.'完成。';
	if($link != ""){
		$str .= '<br><a href="#" onclick="openBox(\\\''.$link.'\\\')"><i class="icon-check"></i>您可以按這裡檢視'.$active.'資料</a>';
	}
	$str .= '<br>\');</script>';
	return $str;
}

function showCompleteIcon(){
	$numargs = func_num_args();
	if($numargs < 1){
		return '';
	}
	$arg_array  = func_get_args();
	$has_value = 0;
	for($i = 0; $i < $numargs; $i ++){
		if(isset($arg_array[$i]) && trim($arg_array[$i]) != ''){
			$has_value ++;
		}
	}	
	return ($numargs == $has_value) ? '' : '<i class="red icon-exclamation-sign" title="該語系資料輸入不完全"></i>';
}

function getIconClass($type){
	switch($type){
		case 'add':
			return 'icon-plus-sign-alt';
			break;
		case 'edit':
			return 'icon-edit-sign';
			break;	
		case 'pic':
			return 'icon-picture';
			break;		
		case 'q':
			return 'icon-question-sign';
			break;					
		default :
			return 'icon-info-sign';
			break;
	}
}


/*****END PHP*****/
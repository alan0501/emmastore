<?php
$ary_submenu = array(
    'admin' => array(
        //'店家列表' => $manage_path . 'admin/index.php?level=2',
		'管理者列表' => $manage_path . 'store/index.php',
        '操作歷程記錄列表' => $manage_path . 'storelog/index.php'
    ),
	'product' => array(
        //'一階分類列表' => $manage_path.'ptype1/index.php',
        //'二階分類列表' => $manage_path.'ptype2/index.php',
        '商品列表' => $manage_path.'shop_product/index.php'
    ),
	'order' => array(
        '訂單列表' => $manage_path.'shop_order/index.php'
    )/* ,
	'shop' => array(
        '訂單列表' => $manage_path.'shop_order/index.php',
		'商品列表' =>  $manage_path.'shop_product/index.php',
        '基本資料' => $manage_path.'shop_admin/manage.php'
    ) */
	
	
	
);
?>
<!-- BEGIN Sidebar -->
<div id="sidebar" class="navbar-collapse collapse"> 
    <!-- BEGIN Navlist -->
    <ul class="nav nav-list">
        <li> <a href="../home/index.php"> <i class="icon-home"></i> <span>首頁</span> </a> </li>
        <?php 
			
			coderAdminStore::drawMenu($ary_submenu); 
		?>
    </ul>
    <!-- END Navlist --> 
    
    <!-- BEGIN Sidebar Collapse Button -->
    <div id="sidebar-collapse" class="visible-lg"> <i class="icon-double-angle-left"></i> </div>
    <!-- END Sidebar Collapse Button --> 
</div>
<!-- END Sidebar --> 

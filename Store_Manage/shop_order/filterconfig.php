<?php
$filterhelp = new coderFilterHelp();

$ary = array();
$ary[] = array('column' => $colname["purchaser_name"], 'name' => '訂購人');
$ary[] = array('column' => $colname["recipient_name"], 'name' => '收件人');
//$ary[] = array('column' => $colname["recipient_mobile"], 'name' => '收件手機');
$ary[] = array('column' => $colname["recipient_phone"], 'name' => '收件電話');
$ary[] = array('column' => $colname["recipient_address"], 'name' => '收件地址');
//$ary[] = array('column' => $colname["invoice_number"], 'name' => '統編');
//$ary[] = array('column' => $colname["invoice_name"], 'name' => '統編抬頭');
$ary[] = array('column' => $colname["manager"], 'name' => '管理員');

$obj = array();
$obj[] = array('type' => 'keyword', 'name' => '關鍵字', 'sql' => true, 'ary' => $ary);

//$obj[] = array('type' => 'select', 'name' => '店家', 'column'=> $colname['shop_id'], 'sql' => false, 'ary' => $shop_ary, 'default' => '');

$obj[] = array('type' => 'select', 'name' => '訂單狀態', 'column' => $colname["order_state"], 'sql' => true, 'ary' => coderHelp::makeAryKeyToAryElement(coderOrder::$order_state, 'value', 'name'));

$obj[] = array('type' => 'select', 'name' => '付款狀態', 'column' => $colname["payment_state"], 'sql' => true, 'ary' => coderHelp::makeAryKeyToAryElement(coderOrder::$payment_state, 'value', 'name'));


$obj[] = array('type' => 'dategroup', 'sql' => true, 'column' => 'dategroup', 'ary' => array(array('name' => '訂單日期', 'column' => $colname["create_time"]), array('name' => '最後修改日期', 'column' => $colname["update_time"])));


/*$obj[] = array('type' => 'select', 'name' => '會員', 'column'=>'is_member', 'sql' => false, 'ary' => array(array('name' => '是', 'value' => '1'), array('name' => '否', 'value' => '0')));*/


/*$obj[] = array('type'=>'numgroup','sql'=>false,'column'=>'numgroup',
'ary'=>array(array('name'=>'應付金額','column'=>'o_total'),array('name'=>'折扣','column'=>'o_discount'),array('name'=>'運費','column'=>'o_freight'),array('name'=>'訂單紅利','column'=>'o_bonus'),array('name'=>'訂單購物金','column'=>'o_money'),array('name'=>'扣抵紅利','column'=>'o_dismoney'),array('name'=>'扣抵購物金','column'=>'o_dismoney'))
);*/




$filterhelp -> Bind($obj);

/*****END PHP*****/
<?php
//被勾選的訂單
$list_id = get('list_id', 1);

$db = Database::DB();
$sHelp=new coderSelectHelp($db);
$sHelp->select="*";
$sHelp->table=$table;

$sHelp->join="LEFT JOIN";
$sHelp->join_table=$table_member;
$sHelp->on="member_id = order_member_id";

$sqlstr=$filterhelp->getSQLStr();
$where=$sqlstr->SQL;
$where .= ($where==""?" ":" and ")."order_shop_id=".$adminuser["id"];

if(!empty($list_id)){
    $list_id = "\"".str_replace(",", "\",\"", $list_id)."\"";
    $where .= ($where==NULL ? "" : " AND ")." order_sno IN ( $list_id )";
}

$sHelp->where=$where;

$sHelp->page_size=-1;
$sHelp->page=get("page");
$sHelp->orderby=get("orderkey",1);
$sHelp->orderdesc=get("orderdesc",1);

$rows=$sHelp->getList();

for($i=0;$i<count($rows);$i++){
	/* ## coder [modify] --> ## */
    $rows[$i]['order_payment_type'] = coderOrder::$payment_type[$rows[$i]["order_payment_type"]];
    $rows[$i]['order_detail'] = coderOrder::getDetails($rows[$i]["order_sno"]);
	/* ## coder [modify] <-- ## */
}

?>
<?php
include_once('_config.php');
include_once($inc_path.'lib/_shoppingcar.php');
$no = get('no', 1);
$manageinfo = "";

$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();
include_once('formconfig.php');

$hasitem = false;
if($no != ''){
	$item = coderOrder::getItem($no);
	$row = array();
	$row[$colname['sno']] = $item -> order_no;
	$row[$colname['id']] = $item -> order_id;
	$row[$colname["order_state"]] = $item -> order_state;
	$row[$colname["payment_state"]] = $item -> order_payment_state;
	$row[$colname["comment"]] = $item -> order_comment;
	$row[$colname["express_type"]] = $item -> order_express_type;
	$row[$colname["payment_type"]] = $item -> order_payment_type;
	$row[$colname["express_sno"]] = $item -> order_express_sno;
	
	if($item -> order_payment_state == 0){
		$fobj[$colname["order_state"]] = array('type' => 'radio', 'name' => '訂單狀態', 'column' => $colname["order_state"], 'ary' => coderHelp::makeAryKeyToAryElement(coderOrder::$order_state_type2, 'key', 'name'), 'help' => '設定訂單狀態,若將訂單狀態修改為交易完成或交易取消,則訂單狀態就無法再修改','equal'=>'=', 'mode'=>'no_default');
		$fhelp -> Bind($fobj);
	}
	
	if($item -> order_payment_state == 1){
		$fobj[$colname["payment_state"]] = array('type' => 'radio', 'name' => '付款狀態', 'column' => $colname["payment_state"], 'ary' => coderHelp::makeAryKeyToAryElement(coderOrder::$payment_state2, 'key', 'name'), 'help' => '設定訂單的付款狀態', 'equal' => '=', 'mode' => 'no_default');
		$fhelp -> Bind($fobj);
	}
	
	if($item -> order_state == 3 || $item -> order_state == 11){
		$fhelp -> setAttr($colname["order_state"], 'readonly', true);
	}
	

	$fhelp -> bindData($row);

	/*$ary_pay = coderOrder::getPayInfo($no);
	
	$item -> pay_type=$ary_pay['pay_type'];
	$item -> pay_info=$ary_pay['pay_info'];*/

	$method = 'edit';
	$active = '編輯';	
	$manageinfo = '管理者 : '.$colname["manager"].' | 訂單建立時間 : '.$item -> order_create_time.' | 上次修改時間 : '.$item -> order_update_time;
	$hasitem = true;
}


if(!$hasitem){
  die('<script>parent.closeBox();parent.showNotice("alert","訂單瀏覽","操作錯誤")</script>');
}

$db -> close();

?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
</head>
<body>

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
    
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title -->
        <?php if ($manageinfo!='') {?>
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>系統資訊 : </strong> <?php echo $manageinfo; ?> </div>
        <?php }?>
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3><i class="<?php echo getIconClass($method); ?>"></i> <?php echo $page_title.$active; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                    </div>
                    <form  class="form-horizontal" action="save.php" id="myform" name="myform" method="post">
                        <div class="box-content"> <?php echo $fhelp -> drawForm($colname['id']); ?>
                            <div class="row"> 
                                <!--left start-->
                                <div class="col-md-10 ">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['sno']); ?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp -> drawForm($colname['sno']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > 店家</label>
                                        <div class="col-sm-8 controls"> <span class="font-size-17"><?php echo class_shop::getName($item -> order_shop_id); ?></span> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > 應付金額</label>
                                        <div class="col-sm-8 controls"> <span class="red font-size-17">NT$<?php echo ($item -> order_total_price+$item -> order_freight); ?></span> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > 付款方式 </label>
                                        <div class="col-sm-8 controls" style="padding-top:6px;"> <?php echo coderOrder::$payment_type[$row[$colname["payment_type"]]]; ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname["order_state"])?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp -> drawForm($colname["order_state"]); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname["payment_state"]); ?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp -> drawForm($colname["payment_state"]); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > 取貨方式 </label>
                                        <div class="col-sm-8 controls"> <?php echo ($row[$colname["express_type"]] > 0) ? coderOrder::$express_type[$row[$colname["express_type"]]] : ""; ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['express_sno']); ?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp -> drawForm($colname['express_sno']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname["comment"]); ?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp -> drawForm($colname["comment"]); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                            <button type="submit" name="submit" class="btn btn-primary" value="submite"><i class="icon-ok"></i>完成<?php echo $active?></button>
                                            <?php /*?><button type="submit" name="submit" class="btn btn-pink" value="mail"><i class="icon-envelope"></i>完成<?php echo $active?>並寄信通知</button><?php */?>
                                            <button type="button" class="btn" onclick="if(confirm('確定要取消<?php echo $active?>?')){parent.closeBox();}"><i class="icon-remove"></i>取消<?php echo $active?></button>
                                        </div>
                                    </div>
                                </div>
                                <!--left end--> 
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <!--訂購明細start-->
            <div class="col-md-12">
                <div class="box box-orange">
                    <div class="box-title">
                        <h3><i class="icon-shopping-cart"></i> 訂購明細</h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-md-6 col-sm-offset-1">
                                <table class="table" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>品項</th>
                                            <th>價格</th>
                                            <th>數量</th>
                                            <th>小計</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
										$detail = $item -> order_detail_ary;
                                        $i = 0;
                                        foreach($detail as $ditem){
                                        	$i ++;
										echo '
										<tr>
											<td>'.$i.'</td>
											<td>'.$ditem["product_name"].'</td>
											<td>'.$ditem["sell_price"].'</td>
											<td>'.$ditem["amount"].'</td>
											<td>'.$ditem["sub_total"].'</td>                                             
										</tr>      
                                         ';
										}
										?>
                                    </tbody>
                                </table>
                                <hr/>
                                <div class="invoice-amount col-md-12">
                                    <p><strong>購買金額:</strong><span><?php echo $item -> order_total_price; ?></span></p>
                                    <?php /*?><p><strong>coupon折扣:</strong><span>-<?php echo $item -> coupon_discount?></span></p>
                                    <?php
									if($item -> discount != 0 && $item -> discount_desc != ""){
									?>
                                    <p><strong><?php echo $item -> discount_desc?></strong><span>-<?php echo $item -> discount?></span></p>
                                    <?php
									}
									?><?php */?>
                                    <p><strong>運費:</strong><span>+<?php echo $item -> order_freight; ?></span></p>
                                    <p><strong>應付金額:</strong><span class="red font-size-17"><?php echo ($item -> order_total_price+$item -> order_freight); ?></span></p>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <?php /*?><div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">折扣資訊</h4>
                                    </div>
                                    <div class="panel-body"> <?php echo str_replace(';','<br>',$item->discount_desc) ?> </div>
                                </div><?php */?>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">寄件資料</h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p><strong>訂購資訊</strong></p>
                                                <p><strong><i class="icon-user"></i> : </strong><?php echo $item -> order_purchaser_name; ?></p>
                                                <p><strong><i class="icon-phone"></i> : </strong><?php echo $item -> order_purchaser_phone; ?></p>
                                                <?php /*?><p><strong><i class="icon-mobile-phone"></i> : </strong><?php echo $item->mobile?></p><?php */?>
                                                <?php /*?><p><strong><i class="icon-envelope-alt"></i> : </strong><?php echo $item->email?></p><?php */?>
                                                <p><strong><i class="icon-map-marker"></i> : </strong><?php echo $item -> order_purchaser_address; ?></p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><strong>寄件資訊</strong></p>
                                                <p><strong><i class="icon-user"></i> : </strong><?php echo $item -> order_recipient_name; ?></p>
                                                <p><strong><i class="icon-phone"></i> : </strong><?php echo $item -> order_recipient_phone; ?></p>
                                                <?php /*?><p><strong><i class="icon-mobile-phone"></i> : </strong><?php echo $item->to_mobile?></p>
                                                <p><strong><i class="icon-envelope-alt"></i> : </strong><?php echo $item->to_email?></p><?php */?>
                                                <p><strong><i class="icon-map-marker"></i> : </strong><?php echo ($item -> order_recipient_zcode != 0 ? $item -> order_recipient_zcode : "").$item -> order_recipient_address; ?></p>
                                            </div>
                                            <hr/>
                                            <?php /*?><div class="col-md-6">
                                                <p><strong>訂單紅利:</strong><span class="green font-size-17"><?php echo $item->bonus?></span></p>
                                                <p><strong>訂單購物金:</strong><span class="green font-size-17"><?php echo $item->money?></span></p>
                                            </div><?php */?>
                                            <div class="col-md-6">
                                            	<p><strong><?php echo ($item -> order_company_invoice == 1 ? "需要三聯式發票" : ""); ?></strong></p>
                                                <?php /*?><p><strong>發票類型:</strong><span ><?php echo coderOrder::$invoice_type[$item->invoice]?></span></p><?php */?>
                                                <?php /*?><p><strong>統編:</strong><span ><?php echo $item->invoice_no?></span></p>
                                                <p><strong>抬頭:</strong><span ><?php echo $item->invoice_name?></span></p><?php */?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--訂購明細end--> 
            
        </div>
        
        <!-- END Main Content -->
        
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script> 
<script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.js"></script> 
<script type="text/javascript" src="../assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript">
<?php echo coderFormHelp::drawVaildScript(); ?>  
</script>
</body>
</html>

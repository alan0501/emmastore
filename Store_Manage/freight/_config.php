<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'freight';
$logkey = 'freight';
include_once('../_config.php');

coderAdmin::vaild($authkey);
$auth = coderAdmin::Auth($authkey);

$table = coderDBConf::$freight;
$colname = coderDBConf::$col_freight;

$page_title = $auth['name'].' - 運費設定';
$page_desc = "{$page_title}-您可將內容修改為希望呈現的內容。";
$mtitle = '<li class="active">'.$page_title.'</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");

/*****END PHP*****/
<?php
include_once('_config.php');
include_once('filterconfig.php');

$listHelp = new coderListHelp('table1', '運費');
$listHelp -> mutileSelect = false;
$listHelp -> editLink = "manage.php";
//$listHelp -> addLink = "manage.php";
$listHelp -> ajaxSrc = "service.php";
/*$listHelp->delSrc="delservice.php";
$listHelp->orderSrc="orderservice.php";
$listHelp->orderColumn=$orderColumn;
$listHelp->orderDesc=$orderDesc;*/

$col = array();
$col[] = array('column' => $colname['keyword'], 'name' => '名稱', 'width' => '150', 'order' => false);
$col[] = array('column' => $colname['value'], 'name' => '金額', 'order' => false);
$col[] = array('column' => $colname['manager'], 'name' => '管理者', 'width' => '80', 'order' => true);
$col[] = array('column' => $colname['update_time'], 'name' => '最後更新', 'order' => true, 'width' => '100');

$listHelp->Bind($col);

$listHelp->bindFilter($filterhelp);

$db = Database::DB();

coderAdminLog::insert($adminuser['username'], $logkey, 'view');

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
</head>
<body >
<?php include_once('../navbar.php'); ?>
<!-- BEGIN Container -->
<div class="container" id="main-container">
    <?php include_once('../left.php'); ?>
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title --> 
        
        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li> <i class="icon-home"></i> <a href="../home/index.php">Home</a> <span class="divider"><i class="icon-angle-right"></i></span> </li>
                <?php echo $mtitle; ?>
            </ul>
        </div>
        <!-- END Breadcrumb --> 
        
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3 style="float:left"><i class="icon-table"></i> <?php echo $page_title; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-content"> <?php echo $listHelp -> drawTable(); ?> </div>
                </div>
            </div>
        </div>
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../js/coderlisthelp.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	$('#table1').coderlisthelp({
		debug : true,
		callback : function(obj, rows){
			obj.html('');
			var count = rows.length;
			for(var i = 0; i < count; i ++){
				var row = rows[i];
				var $tr = $('<tr></tr>');
				$tr.attr("orderlink", "order_id="+row["<?php echo $colname['id']; ?>"]+"&order_key=<?php echo $colname['id']; ?>");
				$tr.attr("editlink", "id="+row["<?php echo $colname['id']; ?>"]);
				<?php /*?>$tr.attr("delkey",row["<?php echo $colname['id'];?>"]);<?php */?>
				$tr.attr("title", "ID:"+row["<?php echo $colname['id']; ?>"]+"之運費");
	
				<?php /*?>$tr.append('<td>'+row["<?php echo $colname['ispublic'];?>"]+'</td>');<?php */?>
				$tr.append('<td>'+row["<?php echo $colname['keyword']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['value']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['manager']; ?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['update_time']; ?>"]+'</td>');
				obj.append($tr);
			}
		},
		listComplete:function(){
			$('#table1').find('img').click(function(){
				$.colorbox({href:$(this).attr('src'), initialWidth:'50px', initialHeight:'50px', maxHeight:'80%'});
			});
		}
	});
});
</script>
</body>
</html>

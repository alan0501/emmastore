<?php
include_once('_config.php');
include_once('formconfig.php');

$errorhandle = new coderErrorHandle();
try{
	$db = Database::DB();
	$id = post($colname['id']);
	if($id != ""){
		$method = 'edit';
		$active = '編輯';
	}else{
		$method = 'add';
		$active = '新增';
	}
	$data = $fhelp -> getSendData();
	$error = $fhelp -> vaild($data);
	if(count($error) > 0){
		$msg = implode('<br/>', $error);
		throw new Exception($msg);
	}
	
	$nowtime = datetime();
	$data[$colname['manager']] = $adminuser['username'];
	$data[$colname['update_time']] = $nowtime;
	if($method == 'edit'){
		$db -> query_update($table, $data, "{$colname['id']} = {$id}");
	}else{
	$data[$colname['ind']] = coderListOrderHelp::getMaxInd($table, $colname['ind']);
		$data[$colname['create_time']] = $nowtime;
		$id = $db -> query_insert($table, $data);
	}
	
	coderFormHelp::moveCopyPic($data[$colname['pic']], $admin_path_temp, $file_path, 'm');
	coderAdminLog::insert($adminuser['username'], $logkey, $method, $page_title." - {$data[$colname['name']]},ID_$id");
	class_ptype1::clearCache(); // 消除cache
	$db -> close();
	echo showParentSaveNote($page_title, $active, $data[$colname['name']], "manage.php?id=".$id);
}catch(Exception $e){
	$errorhandle -> setException($e); 
}

if($errorhandle -> isException()){
    $errorhandle -> showError();
}

/*****END PHP*****/
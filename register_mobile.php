<?php
include_once("_config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body style="background:#F1F3F2;">
<div id="site_reg">
    <div class="login_form">
        <div class="login_logo"><a href="index.html"><img src="images/logo.png" border="0"  /></a></div>
        <div class="login_acc">註冊個人帳號</div>
        <div class="login_form">
            <form id="member_reg_form">
            	<input name="path" type="hidden" value="p" id="path" />
                <input id="reg_userName" type="text" name="reg_userName" class="input-userName" autocomplete="off" placeholder="輸入你喜歡的帳號">
                <input id="reg_password" type="password" name="reg_password" class="input-password" autocomplete="off" placeholder="輸入密碼">
                <input id="reg_password2" type="password" name="reg_password2" class="input-password" autocomplete="off" placeholder="再次輸入密碼">
                <input id="input_email" type="text" name="input_email" class="input-email" autocomplete="off" placeholder="Email信箱">
                <div class="reg_txt">點擊註冊的同時，表示您已詳閱我們的資料使用政策與使用條款</div>
                <div class="input-button reg_btn"><img src="images/register.png" /></div>
                <div class="tologin"><a href="login_mobile.html">已經有帳號了？馬上登入</a></div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
<?php
$db -> close();
?>
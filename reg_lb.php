<?php
include_once("_config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="<?php echo $keywords; ?>" />
<meta name="author" content="<?php echo $author; ?>" />
<meta name="copyright" content="<?php echo $copyright; ?>" />
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script src="js/jquery.min.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
</head>
<body>
<div id="site_reg">
    <div class="login_form">
        <div class="login_logo"><img src="images/logo.png" /></div>
        <div class="login_acc">註冊個人帳號</div>
        <div class="login_form">
            <form id="member_reg_form">
            	<input name="path" type="hidden" value="i" id="path" />
                <input id="reg_userName" type="text" name="reg_userName" class="input-userName" autocomplete="off" placeholder="輸入你喜歡的帳號">
                <input id="reg_password" type="password" name="reg_password" class="input-password" autocomplete="off" placeholder="輸入密碼">
                <input id="reg_password2" type="password" name="reg_password2" class="input-password" autocomplete="off" placeholder="再次輸入密碼">
                <input id="input_email" type="text" name="input_email" class="input-email" autocomplete="off" placeholder="Email信箱">
                <div class="reg_txt">點擊註冊的同時，表示您已詳閱我們的資料使用政策與使用條款</div>
                <div class="input-button reg_btn"><img src="images/register.png" width="300"  /></div>
                <div class="tologin"><a href="javascript:void(0);" class="go_login_btn">已經有帳號了？馬上登入</a></div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
<?php
$db -> close();
?>
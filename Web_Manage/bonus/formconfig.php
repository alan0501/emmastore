<?php
$fhelp = new coderFormHelp();

$fobj = array();
$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);
$fobj[$colname['discount']] = array('type' => 'text', 'name' => '折扣金額', 'column' => $colname['discount'], 'placeholder' => '折扣金額', 'validate' => array('required' => 'yes', 'digits' => 'yes'), 'icon' => 'NT$', 'help' => '折抵的訂單金額');
//$fobj['prefix'] = array('type'=>'text','name'=>'序號前綴','column'=>'c_prefix','placeholder'=>'請輸入前綴','validate'=>array('required'=>'yes','maxlength'=>'10','minlength'=>'1'));
if($id > 0){
	$fobj[$colname['sno']] = array('type' => 'text', 'name' => '序號', 'column' => $colname['sno'], 'placeholder' => '請輸入序號', 'validate' => array('required' => 'yes', 'maxlength' => '50', 'minlength' => '8'));
	$fobj[$colname['state']] = array('type' => 'radio', 'name' => '使用狀態', 'column' => $colname['state'], 'ary'=>coderHelp::makeAryKeyToAryElement(coderPromotion::$coupon_state, 'key', 'name'), 'mode'=>'no_default');
	$fobj[$colname['order_sno']] = array('type' => 'text', 'name' => '訂單編號', 'column' => $colname['order_sno'], 'readonly' => 'true');
}else{
	$fobj['num'] = array('type' => 'text', 'name' => '序號組數', 'column' => 'num', 'validate' => array('required' => 'yes', 'digits' => 'yes'));
}

$fhelp -> Bind($fobj);

/*****END PHP*****/
<?php
include_once('_config.php');
$id = get('id');
$manageinfo = "";
$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();
include_once('formconfig.php');

$pic = '';

if($id > 0){
	$row = $db -> query_first("select * from $table where {$colname['id']}=$id");
	$fhelp -> bindData($row);

	$method = 'edit';
	$active = '編輯';	
	$manageinfo = '  管理者 : '.$row[$colname['admin']].' | 建立時間 : '.$row[$colname['createtime']].' | 上次修改時間 : '.$row[$colname['updatetime']];
	
}else{
	$method = 'add';
	$active = '新增';

}

//print_r($colname);

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php  include_once('../head.php') ?>
</head>
<body>
<!-- BEGIN Container -->
<div class="container" id="main-container"> 
    
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon?>"></i> <?php echo $page_title?>管理</h1>
                <h4><?php echo $page_desc?></h4>
            </div>
        </div>
        <!-- END Page Title -->
        <?php if ($manageinfo!='') {?>
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>系統資訊 : </strong> <?php echo $manageinfo?> </div>
        <?php }?>
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3><i class="<?php echo getIconClass($method)?>"></i> <?php echo $page_title.$active?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                    </div>
                    <div class="box-content">
                        <form  class="form-horizontal" action="save.php" id="myform" name="myform" method="post">
                            <?php echo $fhelp->drawForm($colname['id']); ?>
                            <div class="row"> 
                                <!--left start-->
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" > <?php echo $fhelp->drawLabel($colname['discount'])?> </label>
                                        <div class="col-sm-10 controls"> <?php echo $fhelp->drawForm($colname['discount'])?> </div>
                                    </div>
                                    <?php /*?><div class="form-group">
                                      <label class="col-sm-2 control-label" >
                                          <?php echo $fhelp->drawLabel('prefix')?>                                      </label> 
                                          <div class="col-sm-10 controls">
                                              <?php echo $fhelp->drawForm('prefix')?>
                                          </div>
                                      </div>  <?php */?>
                                    <?php 
									 if($id > 0){
									 ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" > <?php echo $fhelp->drawLabel($colname['sno'])?> </label>
                                        <div class="col-sm-10 controls"> <?php echo $fhelp->drawForm($colname['sno'])?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" > <?php echo $fhelp->drawLabel($colname['state'])?> </label>
                                        <div class="col-sm-10 controls"> <?php echo $fhelp->drawForm($colname['state'])?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" > <?php echo $fhelp->drawLabel($colname['order_sno'])?> </label>
                                        <div class="col-sm-10 controls"> <?php echo $fhelp->drawForm($colname['order_sno'])?> </div>
                                    </div>
                                    <?php
									 }else{
									  ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" > <?php echo $fhelp->drawLabel('num')?> </label>
                                        <div class="col-sm-10 controls"> <?php echo $fhelp->drawForm('num')?> </div>
                                    </div>
                                    <?php
									 }
									 ?>
                                </div>
                                <!--left end--> 
                                <!--right start--> 
                                
                                <!--right end--> 
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <div class="myform-formgroup-button">
                                            <button type="submit" class="btn btn-primary" ><i class="icon-ok"></i>完成<?php echo $active?></button>
                                            <button type="button" class="btn" onclick="if(confirm('確定要取消<?php echo $active?>?')){parent.closeBox();}"><i class="icon-remove"></i>取消<?php echo $active?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- END Main Content -->
        
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script> 
<script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.js"></script> 
<script type="text/javascript" src="../assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="../js/coderpicupload.js"></script> 
<script type="text/javascript">
	<?php echo coderFormHelp::drawVaildScript('myform', $fobj); ?>  
</script>
</body>
</html>

<?php
include_once('_config_bonus.php');
include_once('filterconfig.php');

$m_id = get("m_id", 1);

$errorhandle = new coderErrorHandle();
try{  
	$db = new Database($HS, $ID, $PW, $DB);
	$db -> connect();
	$sHelp = new coderSelectHelp($db);
	$sHelp -> select = "t.*";
	$sHelp -> table = "(SELECT {$colname_bonus['member_id']} AS member_id, {$colname_bonus['bonus']} AS bonus, {$colname_bonus['type']} AS type, {$colname_bonus['comment']} AS comment, {$colname_bonus['create_time']} AS create_time 
				FROM ".coderDBConf::$bonus." 
				WHERE {$colname_bonus['member_id']} ='$m_id' AND {$colname_bonus['is_use']} = 1
				UNION ALL
				SELECT {$colname_used_bonus['member_id']} AS member_id, {$colname_used_bonus['bonus']} AS bonus, {$colname_used_bonus['type']} AS type, {$colname_used_bonus['comment']} AS comment, {$colname_used_bonus['create_time']} AS create_time 
				FROM ".coderDBConf::$used_bonus." 
				WHERE {$colname_used_bonus['member_id']} ='$m_id') AS t";
				
	$sHelp -> page_size = get("pagenum");
	$sHelp -> page = get("page");

	//$sHelp -> orderby = get("orderkey", 1);
	//$sHelp -> orderdesc = get("orderdesc", 1);
	$sHelp -> orderby = "t.create_time";
	$sHelp -> orderdesc = "desc";
	
	$sqlstr = $filterhelp -> getSQLStr();
	$where = $sqlstr -> SQL;
	$sHelp -> where = $where;

	$rows = $sHelp -> getList();
	
	$row_total = $db -> query_first("SELECT (SELECT SUM({$colname_bonus['bonus']}) FROM ".coderDBConf::$bonus." WHERE {$colname_bonus['member_id']} ='$m_id' AND {$colname_bonus['is_use']} = 1) - IFNULL((SELECT SUM({$colname_used_bonus['bonus']}) FROM ".coderDBConf::$used_bonus." WHERE {$colname_used_bonus['member_id']} ='$m_id'), 0) AS c FROM ".coderDBConf::$member." WHERE {$colname_member['id']} ='$m_id'");
	if($row_total){
		$total = $row_total["c"]; 
	}
		
	for($i = 0; $i < count($rows); $i ++){
		$rows[$i]['bonus'] = ($rows[$i]['type']=1?'+':'-').$rows[$i]['bonus'];
		$rows[$i]['comment'] = $rows[$i]['comment'];
		$rows[$i]['time'] = $rows[$i]['create_time'];
		$rows[$i]['total'] = ($total > 0) ? $total : 0;
	}
	
	$result['result'] = true;
	$result['data'] = $rows;
	$result['page'] = $sHelp -> page_info;
	echo json_encode($result);
}catch(Exception $e){
	$errorhandle -> setException($e); 
}

if($errorhandle -> isException()){
	$result['result'] = false;
    $result['data'] = $errorhandle -> getErrorMessage();
	echo json_encode($result);
}

/*****END PHP*****/
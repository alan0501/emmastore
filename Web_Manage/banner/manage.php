<?php
include_once('_config.php');
$id = get('id');
$manageinfo = "";
$db = Database::DB();
include_once('formconfig.php');

if($id != ""){
	$row = $db -> query_prepare_first("SELECT * FROM $table  WHERE {$colname['id']} = $id");
	$fhelp -> bindData($row);
	$pic = $row[$colname['pic']];
	$method = 'edit';
	$active = '編輯';
	$manageinfo = ' 管理者 : '.$row[$colname['manager']].' | 建立時間 : '.$row[$colname['create_time']].' | 上次修改時間 : '.$row[$colname['update_time']];
}else{
	$pic = '';
	$method = 'add';
	$active = '新增';
}
$db -> close();

?>
<!DOCTYPE html>
<html>
<head>
<?php  include_once('../head.php'); ?>
<link href="../css/codertextgroup.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
<?php
  coderFormHelp::drawPicScript($method, $file_path, $pic, 'org_pic');
?>
</script>
</head>
<body>

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
    
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title -->
        <?php if($manageinfo != ''){?>
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>系統資訊 : </strong> <?php echo $manageinfo; ?> </div>
        <?php }?>
        <!-- BEGIN Main Content -->
        <div class="row">
            <form  class="form-horizontal" action="save.php" id="myform" name="myform" method="post">
                <?php echo $fhelp -> drawForm($colname['id']); ?> 
				<?php echo $fhelp -> drawForm('pic_o'); ?>
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-title">
                            <h3><i class="<?php echo getIconClass($method); ?>"></i> <?php echo $page_title.$active; ?></h3>
                            <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        </div>
                        <div class="box-content">
                            <div class="row"> 
                                <!--left start-->
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-3 control-label" > <?php echo $fhelp->drawLabel($colname['is_show']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp->drawForm($colname['is_show']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-3 control-label" > <?php echo $fhelp->drawLabel($colname['pic']); ?> </label>
                                        <div class="col-sm-9 controls">
                                            <div id="picupload"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-3 control-label" > <?php echo $fhelp -> drawLabel($colname['link']); ?></label>
                                        <div class="col-sm-1 controls">
                                            <input type="text" id="banner_link2" name="banner_link2" class=" form-control" value="http://" style="text-align: center; font-size: 14px; font-weight: bold;" disabled>
                                        </div>
                                        <div class="col-sm-5 controls">
                                            <?php echo $fhelp -> drawForm($colname['link']); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-3">
                                            <button type="submit" class="btn btn-primary submit" ><i class="icon-ok"></i>完成<?php echo $active; ?></button>
                                            <button type="button" class="btn" onClick="if(confirm('確定要取消<?php echo $active; ?>?')){parent.closeBox();}"><i class="icon-remove"></i>取消<?php echo $active; ?></button>
                                        </div>
                                    </div>
                                </div>
                                <!--left end--> 
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        <!-- END Main Content -->
        
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script> 
<script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.js"></script> 
<script type="text/javascript" src="../js/coderpicupload.js"></script> 
<script type="text/javascript">

$('#picupload').coderpicupload({
	pics : [{
		name : '縮圖',
		type : 6,
		tag : 'm',
		width : <?php echo $banner_pic_w; ?>,
		height : <?php echo $banner_pic_h; ?>
	}],
	width : <?php echo $banner_pic_w; ?>,
	height : <?php echo $banner_pic_h; ?>,
	s_width : '60px',
	s_height : '60px',
	id : '<?php echo $colname["pic"]; ?>',
	required : true,
	org_pic : org_pic
});

<?php echo coderFormHelp::drawVaildScript(); ?>
</script>
</body>
</html>

<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'index';
$logkey = 'banner';
include_once('../_config.php');
coderAdmin::vaild($authkey);

$auth = coderAdmin::Auth($authkey);

$file_path = $admin_path_banner;

$table = coderDBConf::$banner;
$colname = coderDBConf::$col_banner;

$orderColumn = $colname['ind'];
$orderDesc = "desc";

$page_title = $auth['name'].' - BANNER';
$page_desc = "{$page_title}-您可將內容修改為希望呈現的內容。";
$mtitle = '<li class="active">'.$page_title.'</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");

function deletefile($pic_old, $pic_new, $file){
    if($pic_old != $pic_new){
        if(is_file($file.$pic_old)){unlink($file.$pic_old);}
        if(is_file($file.'m'.$pic_old)){unlink($file.'m'.$pic_old);}
    }
}

/*****END PHP*****/
<?php
$fhelp = new coderFormHelp();

$fobj = array();
$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);


$fobj[$colname['update_time']] = array('type' => 'text', 'name' => '最後更新時間', 'column' => $colname['update_time']);
$fobj[$colname['keyword']] = array('type' => 'text', 'name' => '名稱', 'column' => $colname['keyword'], 'placeholder' => '請填寫名稱');

$fobj[$colname['value']] = array('type' => 'text', 'name' => '金額', 'column' => $colname['value'], 'placeholder' => '請填寫金額', 'validate' => array('required' => 'yes', 'digits' => 'number'));


$fhelp -> Bind($fobj);

/*****END PHP*****/
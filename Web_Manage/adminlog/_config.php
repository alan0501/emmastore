<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'admin';
include_once('../_config.php');
coderAdmin::vaild($authkey);

$file_path = $admin_path_admin;

$auth = coderAdmin::Auth($authkey);

$table = coderDBConf::$log;
$colname = coderDBConf::$col_log;

$page_title = '操作歷程記錄';
$page_desc = "後台使用者操作記錄列表,此區不能進行新增/修改/刪除的動作。";
$mtitle = '<li class="active">'.$auth['name'].'<span class="divider"><i class="icon-angle-right"></i></span>操作記錄瀏覽</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");

$help = new coderFilterHelp();

$obj = array();
$ary = array();
$ary[] = array('column' => $colname['name'], 'name' => '登入帳號');
$ary[] = array('column' => $colname['ip'], 'name' => 'ip');
$obj[] = array('type' => 'keyword', 'name' => '關鍵字', 'sql' => true, 'ary' => $ary);
$obj[] = array('type' => 'select', 'name' => '功能', 'column' => $colname['type'], 'sql' => true, 'ary' => coderHelp::parseAryKeys(coderAdminLog::$type, array('key' => 'value')));
$obj[] = array('type' => 'select', 'name' => '操作', 'column' => $colname['action'], 'sql' => true, 'ary' => coderHelp::parseAryKeys(coderAdminLog::$action, array('key' => 'value')));
$obj[] = array('type' => 'datearea', 'sql' => true, 'column' => $colname['create_time'], 'name' => '操作日期');
$obj[] = array('type' => 'hidden', 'sql' => true, 'column' => $colname['name'], 'name' => '');

$help -> Bind($obj);


/*****END PHP*****/
<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'product';
$logkey = 'ptype2';
include_once('../_config.php');

coderAdmin::vaild($authkey);

$auth = coderAdmin::Auth($authkey);

$table = coderDBConf::$ptype2;
$colname = coderDBConf::$col_ptype2;

$table_product = coderDBConf::$product;
$colname_product = coderDBConf::$col_product;

$table_ptype1 = coderDBConf::$ptype1;
$colname_ptype1 = coderDBConf::$col_ptype1;

$filter_ptype1_id = get($colname_ptype1['name'], 1);

$idColumn = $colname['id'];
$orderColumn = $colname['ind'];
$orderDesc = "desc";

$page_name = '二階分類';
$page_title = $auth['name'].' - '.$page_name;
$page_desc = "{$page_title}-您可將內容修改為希望呈現的內容。";
$mtitle = '<li class="active">'.$page_title.'</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");

$ptype1_ary = class_ptype1::getList();


/*****END PHP*****/
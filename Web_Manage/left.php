<?php
$ary_submenu = array(
    'admin' => array(
        //'店家列表' => $manage_path . 'store/index.php',
		'管理者列表' => $manage_path . 'admin/index.php?level=1',
        '操作歷程記錄列表' => $manage_path . 'adminlog/index.php'
    ),
    'index' => array(
        'BANNER列表' => $manage_path.'banner/index.php',
		'小廣告列表' => $manage_path.'ad/index.php',
		'META修改' => $manage_path.'meta/manage.php',
        'FOOTER修改' => $manage_path.'footer/manage.php'
    ),
	'product' => array(
        '一階分類列表' => $manage_path.'ptype1/index.php',
        '二階分類列表' => $manage_path.'ptype2/index.php',
        '商品列表' => $manage_path.'product/index.php'
    ),
	'member' => array(
        '會員列表' => $manage_path.'member/index.php',
    ),
	'order' => array(
        '訂單列表' => $manage_path.'order/index.php'
    ),
	'freight' => array(
        '運費設定列表' => $manage_path.'freight/index.php'
    ),
    'store' => array(
        '店家列表' => $manage_path . 'store/index.php',
    )
	/* 'shop' => array(
        '訂單列表' => $manage_path.'shop_order/index.php',
		'商品列表' =>  $manage_path.'shop_product/index.php',
        '基本資料' => $manage_path.'shop_admin/manage.php'
    ) */
	
	
	
);
?>
<!-- BEGIN Sidebar -->
<div id="sidebar" class="navbar-collapse collapse"> 
    <!-- BEGIN Navlist -->
    <ul class="nav nav-list">
        <li> <a href="../home/index.php"> <i class="icon-home"></i> <span>首頁</span> </a> </li>
        <?php 
			
			coderAdmin::drawMenu($ary_submenu); 
		?>
    </ul>
    <!-- END Navlist --> 
    
    <!-- BEGIN Sidebar Collapse Button -->
    <div id="sidebar-collapse" class="visible-lg"> <i class="icon-double-angle-left"></i> </div>
    <!-- END Sidebar Collapse Button --> 
</div>
<!-- END Sidebar --> 

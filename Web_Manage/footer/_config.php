<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'index';
$logkey = 'footer';
include_once('../_config.php');
coderAdmin::vaild($authkey);

$auth = coderAdmin::Auth($authkey);

$table = coderDBConf::$footer;
$colname = coderDBConf::$col_footer;

$page_title = $auth['name'].' - FOOTER';
$page_name = '';
$page_desc = "{$page_title}-您可將內容修改為希望呈現的內容。";
$mtitle = '<li class="active">'.$page_title.'</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");

/*****END PHP*****/
<?php
include_once('_config.php');
include_once('formconfig.php');

$errorhandle = new coderErrorHandle();
try{
	$id = '1';
	$db = Database::DB();

	$method = 'edit';
	$active = '編輯';

	$data = $fhelp -> getSendData();
	$error = $fhelp -> vaild($data);

	if(count($error) > 0){
		$msg = implode('<br/>', $error);
		throw new Exception($msg);
	}

  	$data[$colname['admin']] = $adminuser['username'];
  	$data[$colname['updatetime']] = datetime();

  	$db -> query_update($table, $data, " {$colname['id']} = {$id}");

	coderAdminLog::insert($adminuser['username'], $logkey, $method, $page_title);
	$db -> close();


    $result['result'] = true;
    $result['data'] = $errorhandle -> getErrorMessage();
    echo json_encode($result);

}catch(Exception $e){
	$errorhandle -> setException($e); 
}

if($errorhandle -> isException()){
    $result['result'] = false;
    $result['data'] = $errorhandle -> getErrorMessage();
    echo json_encode($result);
}

/*****END PHP*****/
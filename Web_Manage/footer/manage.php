<?php
include_once('_config.php');
$id = get('id');
$manageinfo = "";
$db = Database::DB();
include_once('formconfig.php');

$row = $db -> query_prepare_first("SELECT * FROM $table WHERE {$colname['id']} = 1");

$method = 'edit';
$active = '編輯';
$fhelp -> bindData($row);
$manageinfo = $row != null ? (' 管理者 : '.$row[$colname['admin']].' | 上次修改時間 : '.$row[$colname['updatetime']]) : "";

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
</head>
<body>
<?php include_once('../navbar.php'); ?>
<!-- BEGIN Container -->
<div class="container" id="main-container">
    <?php include_once('../left.php'); ?>
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title.' 管理'; ?></h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title -->
        <?php if($manageinfo != ''){?>
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>系統資訊 : </strong> <?php echo $manageinfo?> </div>
        <?php }?>
        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li> <i class="icon-home"></i> <a href="../home/index.php">Home</a> <span class="divider"><i class="icon-angle-right"></i></span> </li>
                <?php echo $mtitle; ?>
            </ul>
        </div>
        <!-- END Breadcrumb --> 
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3><i class="<?php echo getIconClass($method); ?>"></i> <?php echo $page_title.' '.$page_name; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                    </div>
                    <div class="box-content">
                        <form  class="form-horizontal" action="save.php" id="myform" name="myform" method="post">
                            <div class="row"> 
                                <!--left start-->
                                <div class="col-md-12">
                                	<div class="row">
                                        <label class="col-md-1 col-lg-2 control-label"><?php echo $fhelp -> drawLabel($colname['use']); ?></label>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <div class="col-sm-10 controls"> <?php echo $fhelp -> drawForm($colname['use']); ?> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-1 col-lg-2 control-label"><?php echo $fhelp -> drawLabel($colname['privacy']); ?></label>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <div class="col-sm-10 controls"> <?php echo $fhelp -> drawForm($colname['privacy']); ?> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-1 col-lg-2 control-label"><?php echo $fhelp -> drawLabel($colname['contact']); ?></label>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <div class="col-sm-10 controls"> <?php echo $fhelp -> drawForm($colname['contact']); ?> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-md-1 col-lg-2 control-label"><?php echo $fhelp -> drawLabel($colname['right']); ?></label>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <div class="col-sm-10 controls"> <?php echo $fhelp -> drawForm($colname['right']); ?> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-7 col-lg-7 text-right">
                                            <button type="submit" class="btn btn-primary" ><i class="icon-ok"></i>完成<?php echo $active; ?></button>
                                        </div>
                                    </div>
                                </div>
                                <!--left end--> 
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- END Main Content -->
        
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script> 
<script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.js"></script> 
<script type="text/javascript" src="../assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="../assets/jquery-form/jquery.form.js"></script> 
<script type="text/javascript">
<?php echo coderFormHelp::drawVaildScript('myform', $fobj); ?>
$(document).ready(function(){
	var options = {
		dataType : 'json',
		success : function(data){
			if(data){
				if(data['result'] == true){
					showNotice('ok', '修改完成', '您己成功修改了<?php echo $page_title; ?>的內容。');
				}else{
					showNotice('alert', '修改失敗', '修改<?php echo $page_title; ?>內容失敗。<br>'+data['data']);
				}
			}
		},
		error : function(){
			showNotice('alert', '網站頁面管理', '系統發生錯誤,請聯絡系統管理員。');
		}
	};
	$('#myform').ajaxForm(options);
});

</script>
</body>
</html>

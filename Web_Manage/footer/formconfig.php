<?php
$fhelp = new coderFormHelp();
$fobj = array();

$fobj[$colname['use']] = array('type' => 'html', 'name' => '網站使用條款', 'column' => $colname['use'], 'validate' => array('required' => 'yes'));
$fobj[$colname['privacy']] = array('type' => 'html', 'name' => '隱私權政策', 'column' => $colname['privacy'], 'validate' => array('required' => 'yes'));
$fobj[$colname['contact']] = array('type' => 'html', 'name' => '聯絡我們', 'column' => $colname['contact'], 'validate' => array('required' => 'yes'));
$fobj[$colname['right']] = array('type' => 'textarea', 'name' => '版權聲明', 'column' => $colname['right'], 'validate' => array('required' => 'yes'));

$fhelp -> Bind($fobj);

/*****END PHP*****/
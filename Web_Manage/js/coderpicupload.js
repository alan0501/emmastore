(function($) {
$.fn.coderpicupload = function(settings) {
    var _defaultSettings = {
		id:'pic',
		callback:null,
		pics:null,
		org_pic:null,
		filepath:'../../upload/temp/',
		nophotopic:'../images/nophoto2.gif',
		ajaxsrc:"../comm/upload.php",
		org_width:'130px',
		org_height:'130px',
		s_width:'100px',
		s_height:'100px',
		width:'100px',
		height:'100px',
		debug:true,
		required:false
    };
    var _settings = $.extend(_defaultSettings, settings);	
	return this.each(function() {
		var parent = $(this);
		var $content = null;
		var $origin = null;
		var $file = null;
		var $del = null;
		var $process = null;
		var $hidden = null;
		function init(){
			
			//圖片區
			var $table = $('<table cellpadding="5" ></table>');
			$content = $('<tr></tr>');
			$table.append($content);
			var sizetxt = '';
			if(_settings.width != '' && +settings.height != ''){
				sizetxt = '建議圖片大小為:'+_settings.width+'x'+_settings.height;
			}
			$origin = $('<td valign="bottom" align="left"><img src="'+_settings.nophotopic+'" style="max-width:'+_settings.org_width+';max-height:'+_settings.org_height+'" class="myform_pic show-popover"  data-placement="top" data-trigger="hover"  data-content="此圖片為原圖的預覽,點擊圖片可看原始圖檔。'+sizetxt+'" data-original-title="原始圖片"><p><span class="label label-gray label-font">原始圖片</span></p></td>');
			$content.append($origin);
			if(_settings.pics != null){
				for(var i = 0; i < _settings.pics.length; i ++){
					$content.append('<td valign="bottom" align="left"><img src="'+_settings.nophotopic+'" id="spic'+i+'" style="max-width:'+(Object.prototype.toString.call(_settings.s_height) === '[object Array]'?_settings.s_width[i]:_settings.s_width)+';max-height:'+(Object.prototype.toString.call(_settings.s_height) === '[object Array]'?_settings.s_height[i]:_settings.s_height)+';"  class="myform_pic"><p><span class="label label-gray label-font">'+_settings.pics[i].name+_settings.pics[i].tag+':'+_settings.pics[i].width+'x'+_settings.pics[i].height+'</span></p>');
				}
			}
			parent.append($table);
			
			//進度條
			$process=$('<div class="progress progress-striped active"><div style="width: 0%;" class="progress-bar"></div> </div>');
			parent.append($process);
			$process.hide();
			

			//按鈕區
			var $buttoncontent = $('<div style="width:220px;"></div>');

			$file = $('<input type="file" class="show-popover " data-trigger="hover" data-placement="left" data-content="點我重新上傳或修改圖片,上傳後圖片會自動裁切為右邊各縮圖的大小" data-original-title="要修改圖片嗎?" style="float:left; line-height:30px; width:180px;">');
			$del = $('<button type="button" class="btn btn-danger show-popover"  data-trigger="hover" data-placement="right" data-content="點我移除圖片" data-original-title="要移除圖片嗎?" style="float:;left"><i class="icon-remove"></i></button>');
			
			$del.click(function(){
				clearPics();
			});
			
			$buttoncontent.append($file);
			$buttoncontent.append($del);
			$buttoncontent.append('<div style="clear:both"></div>');
			
			$hidden = $('<input type="hidden" thisid="'+_settings.id+'" name="'+_settings.id+'" id="'+_settings.id+'" value="" '+(_settings.required==true ? ' required="yes" ' : ' ')+'>');
			$buttoncontent.append($hidden);
			parent.append($buttoncontent);
			parent.find('img').click(function(){
				$.colorbox({href:$(this).attr('src'), initialWidth:'50px', initialHeight:'50px'});
			})
			initButton();
			if(_settings.org_pic != null){
				showPics(_settings.org_pic);
			}
		}
		
		function initButton(){
			$file.change(function() {
				var pic = this.files[0];
				var pics = _settings.pics;
				if(pic){
					var $processbar = $process.find('.progress-bar');
					$process.show();
					$processbar.css('width', '0px');
					var fd = new FormData();
					var xhr = new XMLHttpRequest();
					xhr.open('POST', _settings.ajaxsrc);
					xhr.onload = function() {
						var r = $.parseJSON(this.responseText);
						$processbar.css('width', '100%');
						$process.fadeOut(1500);
						if(r['result'] == true){
							showPics(r);
							showNotice('ok', '上傳作業完成', '您己成功上傳圖片。');							
						}else{
							oops('上傳失敗:'+r['msg'],r);
						}
					};
					xhr.upload.onprogress = function(evt){
					//上傳進度
						if(evt.lengthComputable){
							var complete = (evt.loaded / evt.total * 100 | 0);
							if(100==complete){
								complete=99.9;
							}
							$processbar.css('width', complete+'%');
						}
					}
					fd.append('pic', pic);
					
					if(pics != null){
						for(var i = 0; i < pics.length; i ++){
							fd.append('arys[]', pics[i].tag+','+pics[i].type+','+pics[i].width+','+pics[i].height);
						}
					}
					xhr.send(fd);//開始上傳
				}
            });
		}
		
		function showPics(pic){
			var _pic = $origin.find('img');
			_pic.attr('src',pic['filepath']+pic['filename']);
			$origin.find('span').html('原始圖片'+pic['width']+'x'+pic['height']);
			$hidden.val(pic['filename']);
			var pics=_settings.pics;
			if(pics!=null){
				for(var i=0;i<pics.length;i++){
					parent.find('#spic'+i).attr('src',pic['filepath']+pics[i].tag+pic['filename']);
				}
			}
		}
		
		function clearPics(){
			var _pic=$origin.find('img');
			_pic.attr('src',_settings.nophotopic);
			$origin.find('span').html('您尚未選擇圖片');
			$hidden.val('');
			var pics=_settings.pics;
			if(pics!=null){
				for(var i=0;i<pics.length;i++){
					parent.find('#spic'+i).attr('src',_settings.nophotopic);
				}
			}
		}		
		function oops(msg,data){
			showNotice('alert','作業失敗',msg);
			if(_settings.debug==true){				
				console.log(data);
			}
		}	
		init();
	});
}
})(jQuery);

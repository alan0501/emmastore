<?php
include_once('_config.php');
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
</head>
<body >
<?php include_once('../navbar.php'); ?>
<!-- BEGIN Container -->
<div class="container" id="main-container">
    <?php include_once('../left.php'); ?>
    
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="icon-home"></i> 首頁資訊</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title --> 
        
        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li class="active"><i class="icon-home"></i> Home</li>
            </ul>
        </div>
        <!-- END Breadcrumb --> 
        
        <!-- BEGIN Tiles -->
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tile">
                                    <p class="title"><?php echo $adminuser['name']; ?> - 歡迎使用本系統</p>
                                    <p style="margin-top:10px"><img src="<?php echo $adminuser['pic']; ?>" style="float:left;" onerror="javascript:this.src='../images/nophoto.jpg'" width="50" >
                                    <div style="float:left;margin:5px"><?php echo '您本次登入時間為:'.$adminuser['time'].'<br>登入IP:'.request_ip().'<br><li class="icon-smile"> '.coderAdmin::sayHello().'</li>'?></div>
                                    <div class="clearfix"></div>
                                    </p>
                                    <div class="img img-bottom"> <i class="icon-desktop"></i> </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--<div class="row">
                                    <div class="col-md-6 tile-active">
                                        <a class="tile tile-pink" data-stop="3000" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <div class="img img-center">
                                                <img src="../images/demo/coder-logo.png" />
                                            </div>
                                            <p class="title text-center">Visit CODER wp</p>
                                        </a>

                                        <a class="tile tile-orange" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <p>CODER wp is new custom theme designed for the Wordpress admin.</p>
                                        </a>
                                    </div>

                                    <div class="col-md-6">
                                        <a class="tile tile-red" href="http://themeforest.net/item/flaty-premium-responsive-admin-template/5247864">
                                            <div class="img img-center">
                                                <i class="icon-shopping-cart"></i>
                                            </div>
                                            <p class="title text-center">Buy CODER</p>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-12 tile-active">
                                        <div class="tile tile-magenta">
                                            <div class="img img-center">
                                                <i class="icon-desktop"></i>
                                            </div>
                                            <p class="title text-center">CODER Admin</p>
                                        </div>

                                        <div class="tile tile-blue">
                                            <p class="title">CODER Admin</p>
                                            <p>CODER is the new premium and fully responsive admin dashboard template.</p>
                                            <div class="img img-bottom">
                                                <i class="icon-desktop"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tile tile-green">
                                            <div class="img">
                                                <i class="icon-copy"></i>
                                            </div>
                                            <div class="content">
                                                <p class="big">+30</p>
                                                <p class="title">Ready Page</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>--> 
                    </div>
                </div>
            </div>
            <!--<div class="col-md-5">
                <div class="box box-black">
                    <div class="box-title">
                        <h3><i class="icon-retweet"></i> 系統資訊</h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                    </div>
                    <div class="box-content">
                        <ul class="things-to-do">
                            <li>
                                <p> <i class="icon-user"></i> 本月新會員 : <span class="value">20</span> 本日新會員 : <span class="value">1</span> </p>
                            </li>
                            <li>
                                <p> <i class="icon-shopping-cart blue"></i> 新訂單 : <span class="value">7</span> 己揀貨訂單  : <span class="value">1</span> 未付款訂單  : <span class="value">1</span> 退貨中訂單  : <span class="value">1</span> </p>
                            </li>
                            <li>
                                <p> <i class="icon-comments"></i> 未回覆訊息 : <span class="value">14</span> </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>--> 
        </div>
        
        <!-- END Tiles --> 
        
        <!-- BEGIN Main Content --> 
        <!--<div class="row">
                    <div class="col-md-7">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-bar-chart"></i> 流量統計</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div id="visitors-chart" style="margin-top:20px; position:relative; height: 290px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-bar-chart"></i> 週報</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <ul class="weekly-stats">
                                    <li>
                                        <span class="inline-sparkline">134,178,264,196,307,259,287</span>
                                        Visits: <span class="value">376</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">89,124,197,138,235,169,186</span>
                                        Unique Visitors: <span class="value">238</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">625,517,586,638,669,698,763</span>
                                        Page Views: <span class="value">514</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">1.34,2.98,0.76,1.29,1.86,1.68,1.92</span>
                                        Pages / Visit: <span class="value">1.43</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">2.34,2.67,1.47,1.97,2.25,2.47,1.27</span>
                                        Avg. Visit Time: <span class="value">00:02:34</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">70.34,67.41,59.45,65.43,78.42,75.92,74.29</span>
                                        Bounce Rate: <span class="value">73.56%</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">78.12,74.52,81.25,89.23,86.15,91.82,85.18</span>
                                        % New Visits: <span class="value">82.65%</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>--> 
        
        <!--<div class="row">
                    <div class="col-md-7">
                        <div class="box box-magenta">
                            <div class="box-title">
                                <h3><i class="icon-comment"></i> 聯絡訊息</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <ul class="messages nice-scroll" style="height: 300px">
                                    <li>
                                        <img src="../images/demo/avatar/avatar2.jpg" alt="">
                                        <div>
                                            <div>
                                                <h5>David</h5>
                                                <span class="time"><i class="icon-time"></i> 26 minutes ago</span>
                                            </div>
                                            <p>Lorem ipsum commodo quis dolor voluptate et in Excepteur. Lorem ipsum amet dolor qui cupidatat in anim reprehenderit quis id culpa consequat non culpa. Lorem ipsum in culpa aliquip incididunt cupidatat dolore irure ...</p>
                                            <div class="messages-actions">
                                                <a class="show-tooltip" href="#" title="Approve"><i class="icon-ok green"></i></a>
                                                <a class="show-tooltip" href="#" title="Disapprove"><i class="icon-remove orange"></i></a>
                                                <a class="show-tooltip" href="#" title="Remove"><i class="icon-trash red"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="../images/demo/avatar/avatar3.jpg" alt="">
                                        <div>
                                            <div>
                                                <h5>Sarah</h5>
                                                <span class="time"><i class="icon-time"></i> 1 days ago</span>
                                            </div>
                                            <p>Lorem ipsum commodo quis dolor voluptate et in Excepteur. Lorem ipsum amet dolor qui cupidatat in anim reprehenderit quis id culpa consequat non culpa.</p>
                                            <div class="messages-actions">
                                                <a class="show-tooltip" href="#" title="Approve"><i class="icon-ok green"></i></a>
                                                <a class="show-tooltip" href="#" title="Disapprove"><i class="icon-remove orange"></i></a>
                                                <a class="show-tooltip" href="#" title="Remove"><i class="icon-trash red"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="../images/demo/avatar/avatar4.jpg" alt="">
                                        <div>
                                            <div>
                                                <h5>Emma</h5>
                                                <span class="time"><i class="icon-time"></i> 4 days ago</span>
                                            </div>
                                            <p>Lorem ipsum commodo quis dolor voluptate et in Excepteur. Lorem ipsum amet dolor qui cupidatat in anim reprehenderit quis id culpa consequat non culpa. Lorem ipsum in culpa aliquip incididunt cupidatat dolore irure cupidatat aute cupidatat quis nulla.</p>
                                            <div class="messages-actions">
                                                <a class="show-tooltip" href="#" title="Approve"><i class="icon-ok green"></i></a>
                                                <a class="show-tooltip" href="#" title="Disapprove"><i class="icon-remove orange"></i></a>
                                                <a class="show-tooltip" href="#" title="Remove"><i class="icon-trash red"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="../images/demo/avatar/avatar5.jpg" alt="">
                                        <div>
                                            <div>
                                                <h5>John</h5>
                                                <span class="time"><i class="icon-time"></i> 2 weeks ago</span>
                                            </div>
                                            <p>Lorem ipsum commodo quis dolor voluptate et in Excepteur. Lorem ipsum amet dolor qui cupidatat in anim reprehenderit quis id culpa consequat non culpa. Lorem...</p>
                                            <div class="messages-actions">
                                                <a class="show-tooltip" href="#" title="Approve"><i class="icon-ok green"></i></a>
                                                <a class="show-tooltip" href="#" title="Disapprove"><i class="icon-remove orange"></i></a>
                                                <a class="show-tooltip" href="#" title="Remove"><i class="icon-trash red"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="../images/demo/avatar/avatar1.jpg" alt="">
                                        <div>
                                            <div>
                                                <h5>Penny <span class="label label-info">Admin</span></h5>
                                                <span class="time"><i class="icon-time"></i> 14 July</span>
                                            </div>
                                            <p>Lorem ipsum commodo quis dolor voluptate et in Excepteur. Lorem ipsum amet dolor qui cupidatat in anim reprehenderit quis id culpa consequat non culpa. Lorem ipsum in culpa aliquip incididunt cupidatat dolore irure cupidatat aute cupidatat quis nulla.</p>
                                            <div class="messages-actions">
                                                <a class="show-tooltip" href="#" title="Approve"><i class="icon-ok green"></i></a>
                                                <a class="show-tooltip" href="#" title="Disapprove"><i class="icon-remove orange"></i></a>
                                                <a class="show-tooltip" href="#" title="Remove"><i class="icon-trash red"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> 
        
    </div>--> 
        
        <!-- END Main Content -->
        
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
    
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script src="../assets/flot/jquery.flot.js"></script> 
<script src="../assets/flot/jquery.flot.resize.js"></script> 
<script src="../assets/flot/jquery.flot.pie.js"></script> 
<script src="../assets/flot/jquery.flot.stack.js"></script> 
<script src="../assets/flot/jquery.flot.crosshair.js"></script> 
<script src="../assets/flot/jquery.flot.tooltip.min.js"></script> 
<script src="../assets/sparkline/jquery.sparkline.min.js"></script> 
<script>
function showplot(){
	//define placeholder class
	var placeholder = $("#visitors-chart");
	
	if($(placeholder).size() == 0){
		return;
	}
	//some data
	var d1 = [
		[1, 35],
		[2, 48],
		[3, 34],
		[4, 54],
		[5, 46],
		[6, 37],
		[7, 40],
		[8, 55],
		[9, 43],
		[10, 61],
		[11, 52],
		[12, 57],
		[13, 64],
		[14, 56],
		[15, 48],
		[16, 53],
		[17, 50],
		[18, 59],
		[19, 66],
		[20, 73],
		[21, 81],
		[22, 75],
		[23, 86],
		[24, 77],
		[25, 86],
		[26, 85],
		[27, 79],
		[28, 83],
		[29, 95],
		[30, 92]
	];
	var d2 = [
		[1, 9],
		[2, 15],
		[3, 16],
		[4, 21],
		[5, 19],
		[6, 15],
		[7, 22],
		[8, 29],
		[9, 20],
		[10, 27],
		[11, 32],
		[12, 37],
		[13, 34],
		[14, 30],
		[15, 28],
		[16, 23],
		[17, 28],
		[18, 35],
		[19, 31],
		[20, 28],
		[21, 33],
		[22, 25],
		[23, 27],
		[24, 24],
		[25, 36],
		[26, 25],
		[27, 39],
		[28, 28],
		[29, 35],
		[30, 42]
	];
	var chartColours = ['#88bbc8', '#ed7a53', '#9FC569', '#bbdce3', '#9a3b1b', '#5a8022', '#2c7282'];
	//graph options
	var options = {
			grid: {
				show: true,
				aboveData: true,
				color: "#3f3f3f" ,
				labelMargin: 5,
				axisMargin: 0, 
				borderWidth: 0,
				borderColor:null,
				minBorderMargin: 5 ,
				clickable: true, 
				hoverable: true,
				autoHighlight: true,
				mouseActiveRadius: 20
			},
			series: {
				grow: {
					active: false,
					stepMode: "linear",
					steps: 50,
					stepDelay: true
				},
				lines: {
					show: true,
					fill: true,
					lineWidth: 3,
					steps: false
					},
				points: {
					show:true,
					radius: 4,
					symbol: "circle",
					fill: true,
					borderColor: "#fff"
				}
			},
			legend: { 
				position: "ne", 
				margin: [0,-25], 
				noColumns: 0,
				labelBoxBorderColor: null,
				labelFormatter: function(label, series) {
					// just add some space to labes
					return label+'&nbsp;&nbsp;';
				 }
			},
			yaxis: { min: 0 },
			xaxis: {ticks:11, tickDecimals: 0},
			colors: chartColours,
			shadowSize:1,
			tooltip: true, //activate tooltip
			tooltipOpts: {
				content: "%s : %y.0",
				defaultTheme: false,
				shifts: {
					x: -30,
					y: -50
				}
			}
		};
		$.plot(placeholder, [
		{
			label: "Visits", 
			data: d1,
			lines: {fillColor: "#f2f7f9"},
			points: {fillColor: "#88bbc8"}
		}, 
		{
			label: "Unique Visits", 
			data: d2,
			lines: {fillColor: "#fff8f2"},
			points: {fillColor: "#ed7a53"}
		} 

	], options);
		
}
if (jQuery.plot) {
	showplot();
}	
if (jQuery().sparkline) {
	$('.inline-sparkline').sparkline(
		'html',
		{
			width: '70px',
			height: '26px',
			lineWidth: 2,
			spotRadius: 3,
			lineColor: '#88bbc8',
			fillColor: '#f2f7f9',
			spotColor: '#14ae48',
			maxSpotColor: '#e72828',
			minSpotColor: '#f7941d'
		}
	);
}	
</script>
</body>
</html>

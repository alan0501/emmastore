<?php
include_once('_config.php');

$username = get('username', 1);
$level = get("level", 1);
$manageinfo = "";
$spic = "";
//$bpic = "";

if($username != ""){
	$db = Database::DB();
	$row = $db -> query_prepare_first("SELECT * FROM $table WHERE {$colname['account']} = :username", array(':username' => $username));
	$row[$colname['auth']] = coderAdmin::getAuthListAryByInt($row[$colname['auth']]);
	//編輯時,password預設為空白
	unset($row[$colname['password']]);
	$fhelp -> bindData($row);
	if($level == 2){
		$spic = $row[$colname['spic']];
	}
	//$bpic = $row[$colname['bpic']];
	
	$fhelp -> setAttr($colname['account'], 'readonly', true);

	$method = 'edit';
	$active = '編輯';
	$manageinfo = ' 管理者 : '.$row[$colname['manager']].' | 建立時間 : '.$row[$colname['create_time']].' | 上次修改時間 : '.$row[$colname['update_time']] .' | 最後登入時間 : '.$row[$colname['login_time']].' | 最後登入IP : '.$row[$colname['ip']];

	//$row_history = coderAdminLog::getLogByUser($row[$colname['account']], 5);

	$db -> close();
}else{
	$method = 'add';
	$active = '新增';
	$fhelp -> setAttr($colname['password'], 'validate', array('required' => 'yes', 'maxlength' => '20', 'minlength' => 6));
	$fhelp -> setAttr('repassword', 'validate', array('required' => 'yes', 'maxlength' => '20', 'minlength' => 6, 'equalto' => '#'.$colname['password'], 'data-msg-equalto' => '請重新輸入管理員密碼'));
}

?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php')?>
<script language="javascript" type="text/javascript">

<?php
//if($spic != ""){
coderFormHelp::drawPicScript($method, $file_path, $spic, 'org_pic_s');
//}
//coderFormHelp::drawPicScript($method, $file_path, $bpic, 'org_pic_b');

?>
</script>
</head>
<body>

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
    
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title -->
        <?php if($manageinfo != ''){?>
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>系統資訊 : </strong> <?php echo $manageinfo; ?> </div>
        <?php }?>
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-10">
                <div class="box">
                    <div class="box-title">
                        <h3><i class="<?php echo getIconClass($method); ?>"></i> <?php echo $page_title.$active; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                    </div>
                    <div class="box-content">
                        <form  class="form-horizontal" action="save.php?level=<?php echo $level; ?>" id="myform" name="myform" method="post">
                            <?php echo $fhelp -> drawForm($colname['id']); ?> <?php echo $fhelp -> drawForm($colname['level']); ?>
                            <div class="row"> 
                                <!--right start-->
                                <div class="col-md-7 ">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['is_show']); ?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp -> drawForm($colname['is_show']); ?> </div>
                                    </div>
                                    <?php /*<div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['is_admin']); ?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp -> drawForm($colname['is_admin']);?> </div>
                                    </div>*/ ?>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['account']); ?> </label>
                                        <div class="col-sm-8  controls"> <?php echo $fhelp -> drawForm($colname['account']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['password']); ?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp -> drawForm($colname['password']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel('repassword'); ?> </label>
                                        <div class="col-sm-8 controls"> <?php echo $fhelp -> drawForm('repassword'); ?> </div>
                                    </div>
                                    <?php
                                    /*
										if(coderAdmin::isAuth('admin')){
									?>
                                    <div id="authgroup" class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['auth']); ?> </label>
                                        <div class="col-sm-8  controls"> <?php echo $fhelp -> drawForm($colname['auth']); ?> </div>
                                    </div>
                                    <?php 
                                        } 
                                        */
									?>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['name']); ?> </label>
                                        <div class="col-sm-8  controls" > <?php echo $fhelp -> drawForm($colname['name']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['email']); ?> </label>
                                        <div class="col-sm-8  controls" > <?php echo $fhelp -> drawForm($colname['email']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['phone']); ?> </label>
                                        <div class="col-sm-8  controls" > <?php echo $fhelp -> drawForm($colname['phone']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['mobile']); ?> </label>
                                        <div class="col-sm-8  controls" > <?php echo $fhelp -> drawForm($colname['mobile']); ?> </div>
                                    </div>
                                </div>
                                <!--left end--> 
                                <!--right start-->
                                <div class="col-md-5 ">
                                    <?php
									if($level == 2){
									?>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['spic']); ?> </label>
                                        <div class="col-sm-8  controls" >
                                            <div id="picupload_s"></div>
                                        </div>
                                    </div>
                                    <?php
									}
									?>
                                    <!-- <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['bpic']); ?> </label>
                                        <div class="col-sm-8  controls" >
                                            <div id="picupload_b"></div>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                            <button type="submit" class="btn btn-primary" ><i class="icon-ok"></i>完成<?php echo $active; ?></button>
                                            <button type="button" class="btn" onclick="if(confirm('確定要取消<?php echo $active; ?>?')){parent.closeBox();}"><i class="icon-remove"></i>取消<?php echo $active; ?></button>
                                        </div>
                                    </div>
                                </div>
                                <!--right end--> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php 
				if(isset($row_history)){
			?>
            <div class="col-md-2">
                <div class="box">
                    <div class="box-title">
                        <h3><i class="<?php echo getIconClass('info'); ?>"></i> 操作記錄</h3>
                    </div>
                    <div class="box-content">
                        <?php
						$len = count($row_history);
						$note = $len < 1 ? "{$row[$colname['account']]}目前沒有操作記錄。" : '以下為最近5筆操作記錄';
						echo ' <p> '.$note.' <button type="button" class="btn btn-primary pull-right" onclick="openBox(\'../adminlog/index.php?username='.$row[$colname['account']].'\')">more</button></p><hr>';
						for($i = 0; $i < count($row_history); $i ++){
							echo '<p>['.$row_history[$i][$log_colname['create_time']].']<br>'.$row_history[$i][$log_colname['type']].$row_history[$i][$log_colname['action']].'-'.$row_history[$i][$log_colname['comment']].'</p>';
						}
					?>
                    </div>
                </div>
            </div>
            <?php 
				} 
			?>
        </div>
        
        <!-- END Main Content -->
        
        <?php include_once('../footer.php')?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php')?>
<script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script> 
<script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.js"></script> 
<script type="text/javascript" src="../js/coderpicupload.js"></script> 
<script type="text/javascript">
<?php
	if($level == 2){
?>
$('#picupload_s').coderpicupload({
	pics : [{
		name : '縮圖', 
		type : 5,
		tag : 's',
		width : <?php echo $admin_spic_w; ?>,
		height : <?php echo $admin_spic_h; ?>
	}], 
	id : '<?php echo $colname["spic"]; ?>',
	width : '<?php echo $admin_spic_w; ?>', 
	height : '<?php echo $admin_spic_h; ?>', 
	s_width : '60px', 
	s_height : '60px',
	required : true, 
	org_pic : org_pic_s
});
<?php
	}
?>
// $('#picupload_b').coderpicupload({
// 	pics : [{
// 		name : '縮圖', 
// 		type : 5,
// 		tag : 'b',
// 		width : <?php echo $admin_bpic_w; ?>,
// 		height : <?php echo $admin_bpic_h; ?>
// 	}],
// 	id : '<?php echo $colname["bpic"]; ?>', 
// 	width : '<?php echo $admin_bpic_w; ?>', 
// 	height : '<?php echo $admin_bpic_h; ?>', 
// 	s_width : '60px', 
// 	s_height : '60px',
// 	required : true, 
// 	org_pic : org_pic_b
// });

<?php echo coderFormHelp::drawVaildScript(); ?>
<?php 
	if($method == 'add'){
?>
$("#<?php echo $colname['account']; ?>").rules("add", {
	messages : {
		remote :  "此帳號己被使用,請重新輸入!"
	},
	remote : {
		url : "checkisexisit.php",
		type : "post",
		data : {
			data : function(){return $('#<?php echo $colname['account']; ?>').val()},
			type : "<?php echo $colname['account']; ?>"
		}
	}
});

$("#<?php echo $colname['email']; ?>").rules("add", {
	messages:{
  		remote:  "此E-mail己被使用,請重新輸入!"
	},
	remote : {
		url : "checkisexisit.php",
		type : "post",
		data : { 
			data : function(){return $('#<?php echo $colname['email']; ?>').val()},
			type : '<?php echo $colname["email"]; ?>'
		}
	}
});
<?php
	}
?>
<?php
	if($level == "2"){
?>
$("#<?php echo $colname["spic"]; ?>").rules("add", {
	required : true,
	messages : {
		required:  "請上傳圖片!"
	}
});
<?php
	}
?>
// $("#<?php echo $colname["bpic"]; ?>").rules("add", {
// 	required : true,
// 	messages : {
// 		required:  "請上傳圖片!"
// 	}
// });


$('#<?php echo $colname['is_admin']; ?>').click(function(){
	disableAuth();
})

function disableAuth(){
	$('#authgroup').css('display', $('#<?php echo $colname['is_admin']; ?>').prop('checked') ? 'none' : 'block');
}

disableAuth();
</script>
</body>
</html>

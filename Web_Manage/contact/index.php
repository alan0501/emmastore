<?php
include('_config.php');
include('filterconfig.php');
$listHelp=new coderListHelp('table1','聯絡我們');
$listHelp->mutileSelect=true;
//$listHelp->editLink="manage.php";
//$listHelp->addLink="manage.php";
$listHelp->ajaxSrc="service.php";
$listHelp->delSrc="delservice.php";

$col=array();
$col[]=array('column'=>$colname['id'],'name'=>'ID','order'=>true,'width'=>'60','def_desc'=>'desc');
$col[]=array('column'=>$colname['createtime'],'name'=>'發訊時間','order'=>true,'width'=>'160');
$col[]=array('column'=>$colname['name'],'name'=>'姓名','order'=>true);
$col[]=array('column'=>$colname['appname'],'name'=>'稱呼','order'=>true);
$col[]=array('column'=>$colname['phone'],'name'=>'手機','order'=>true,'width'=>'120');
$col[]=array('column'=>$colname['email'],'name'=>'E-mail','order'=>true,'width'=>'300');
$col[]=array('column'=>$colname['id'],'name'=>'瀏覽','order'=>false,'width'=>'55','classname'=>'text-center');

$listHelp->Bind($col);
$listHelp->bindFilter($filterhelp);

$db = Database::DB();

coderAdminLog::insert($adminuser['username'],$logkey,'view','列表');

$db->close();
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include('../head.php')?>

    </head>
    <body >
		<?php include('../navbar.php')?>
        <!-- BEGIN Container -->
        <div class="container" id="main-container">
			<?php include('../left.php')?>
            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="<?php echo $mainicon?>"></i> <?php echo $page_title?>管理</h1>
                        <h4><?php echo $page_desc?></h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="../home/index.php">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <?php echo $mtitle?>

                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3 style="float:left"><i class="icon-table"></i> <?php echo $page_title?></h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div class="box-content">

	<?php echo $listHelp->drawTable()?>

                            </div>
                        </div>
                    </div>
                </div>


				<?php include('../footer.php')?>

                <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a>
            </div>
            <!-- END Content -->
        </div>
        <!-- END Container -->


		<?php include('../js.php')?>



        <script type="text/javascript" src="../js/coderlisthelp.js"></script>
		<script type="text/javascript">
			$( document ).ready(function() {
				$('#table1').coderlisthelp({debug:true,callback:function(obj,rows){
					obj.html('');
					var count=rows.length;
					for(var i=0;i<count;i++){
						var row=rows[i];
						var $tr=$('<tr></tr>');

                        $tr.attr("sendemail",row["<?php echo $colname['id']?>"]);
						$tr.attr("delkey",row["<?php echo $colname['id']?>"]);
						$tr.attr("title",row["<?php echo $colname['name']?>"]+"的聯絡訊息");

						$tr.append('<td>'+row["<?php echo $colname['id']?>"]+'</td>');
                        $tr.append('<td>'+row["<?php echo $colname['createtime']?>"]+'</td>');
                        $tr.append('<td>'+row["<?php echo $colname['name']?>"]+'</td>');
                        $tr.append('<td>'+row["<?php echo $colname['appname']?>"]+'</td>');
						$tr.append('<td>'+row["<?php echo $colname['phone']?>"]+'</td>');
                        $tr.append('<td>'+row["<?php echo $colname['email']?>"]+'</td>');
                        $tr.append('<td class="text-center"><button class="btn btn-sm btn-lime" onclick="openBox(\'manage.php?id=' + row["<?php echo $colname['id']?>"] + '\')"><span class="glyphicon glyphicon-search"></span></button></td>');
						obj.append($tr);
					}}, 
                });
			});
		</script>
    </body>
</html>

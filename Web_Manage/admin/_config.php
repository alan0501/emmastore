<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'admin';
$logkey = 'admin';
include_once('../_config.php');

$pagename = request_basename();

//自己要能編輯自己的資料
if(($pagename != 'manage.php' && $pagename != 'save.php') || request_str('username') != $adminuser['username']){
	coderAdmin::vaild($authkey);
}
$auth = coderAdmin::Auth($authkey);

$table = coderDBConf::$admin;
$colname = coderDBConf::$col_admin;
$log_colname = coderDBConf::$col_log;

$file_path = $admin_path_admin;

$idColumn = $colname['id'];
$orderColumn = $colname['ind'];
$orderDesc = "desc";

$level = get("level");
/* switch($level){
    case '1':
        $title_name = "管理者";
        break;
    case '2':
        $title_name = "店家";
        break;
    default:
        $title_name = '';
        break;
} */
$title_name = "管理者";
$page_title = $auth['name']."-".$title_name;
$page_desc = "後台管理者帳號管理區,您可以在這裡檢視所有帳號,或對帳號進行新增、修改、刪除等操作。";
$mtitle = '<li class="active">'.$auth['name'].'管理</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");

/*****搜尋*****/
$help = new coderFilterHelp();
$obj = array();
$obj[] = array('type' => 'select', 'name' => '啟用', 'column' => $colname['is_show'], 'sql' => true, 'ary' => array(array('name' => '是', 'value' => '1'), array('name' => '否', 'value' => '0'))
);

$help -> Bind($obj);

/*****表單*****/
$fhelp = new coderFormHelp();
$fobj = array();

$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);
$fobj[$colname['level']] = array('type' => 'hidden', 'name' => 'level', 'column' => $colname['level'], 'default' => $level);
$fobj[$colname['is_show']] = array('type' => 'checkbox', 'name' => '啟用', 'column' => $colname['is_show'], 'value' => '1', 'default' => '1');
$fobj[$colname['account']] = array('type' => 'text', 'name' => '帳號', 'column' => $colname['account'], 'autocomplete' => 'off', 'placeholder' => '請輸入管理員帳號', 'help' => '此帳號為登入系統之帳號,不能重覆。', 'validate' => array('required' => 'yes', 'maxlength' => '15', 'minlength' => '3'), 'icon' => '<i class="icon-user"></i>');
$fobj[$colname['password']] = array('type' => 'password', 'name' => '密碼', 'column' => $colname['password'], 'autocomplete' => 'off', 'placeholder' => '請輸入管理員密碼', 'help' => '登入系統之密碼。', 'icon' => '<i class="icon-key"></i>');
$fobj['repassword'] = array('type' => 'password', 'name' => '密碼確認', 'column' => $colname['password'], 'autocomplete' => 'off', 'placeholder' => '請重新輸入管理員密碼', 'help' => '為了確認密碼是否確,麻煩您再輸入一次', 'sql' => false, 'icon' => '<i class="icon-check-sign"></i>');
$fobj[$colname['name']] = array('type' => 'text', 'name' => '名稱', 'column' => $colname['name'], 'placeholder' => '請輸入名稱', 'validate' => array('required' => 'yes'));
$fobj[$colname['email']] = array('type' => 'text', 'name' => 'Email', 'column' => $colname['email'], 'placeholder' => '請輸入Email', 'validate' => array('required' => 'yes', 'email' => 'yes'));
$fobj[$colname['phone']] = array('type' => 'text', 'name' => '連絡電話', 'column' => $colname['phone'], 'placeholder' => '請填寫連絡電話', 'validate' => array('maxlength' => '20', 'minlength' => '1'));
$fobj[$colname['mobile']] = array('type' => 'text', 'name' => '行動電話', 'column' => $colname['mobile'], 'placeholder' => '請填寫行動電話', 'validate' => array('maxlength' => '20', 'minlength' => '1'));
if($level == 2){
$fobj[$colname['spic']] = array('type' => 'pic', 'name' => 'LOGO圖片', 'column' => $colname['spic'], 'validate' => array('required' => 'yes'));
}
$fobj[$colname['bpic']] = array('type' => 'pic', 'name' => '大圖片', 'column' => $colname['bpic'], 'validate' => array('required' => 'yes'));
$fobj[$colname['is_admin']] = array('type' => 'checkbox', 'name' => '最高權限', 'column' => $colname['is_admin'], 'value' => '1', 'default' => '0', 'help' => '最高權限,可以使用所有功能');

if(coderAdmin::isAuth('admin')){
	$fobj[$colname['auth']] = array('type' => 'checkgroup', 'name' => '使用權限', 'column' => $colname['auth'], 'ary' => coderAdmin::getAuthAry($level));
}

$fhelp -> Bind($fobj);


function isNotExisit($data, $type){
	global $db, $table, $colname;
	if(strlen($data) > 2 && !$db -> query_first('SELECT '.$colname['id'].' FROM '.$table.' WHERE '.$type.'=\''.hc($data).'\'')){
		return true;
	}else {
		return false;
	}
}
//echo 	realpath('index.php');
//echo 	realpath('../admin/index.php');

/*****END PHP*****/
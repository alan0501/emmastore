<?php
include_once('_config.php');

$id = get('id');
$manageinfo = "";
$db = Database::DB();
include_once('formconfig.php');

$pic = "";
$product_type_ary = "";

if($id != ""){
	$row = $db -> query_prepare_first("SELECT *
									   FROM $table
									   LEFT JOIN $table_ptype2 ON {$colname_ptype2['id']} = {$colname['ptype2_id']} 
									   LEFT JOIN $table_ptype1 ON {$colname_ptype1['id']} = {$colname_ptype2['ptype1_id']} 
									   LEFT JOIN $table_shop ON {$colname_shop['id']} = {$colname['shop_id']} 
									   WHERE {$colname['id']} = $id");
									   
									   
    $product_type_ary = array($row[$colname_ptype2['ptype1_id']], $row[$colname['ptype2_id']]);
	
    //$spic = $row[$colname['spic']];
	//$bpic = $row[$colname['bpic']];
	$pic = $row[$colname['pic']];
	$sno = $row[$colname['sno']];
	
    $fhelp -> bindData($row);
	
	$fhelp -> setAttr($colname['sno'], 'readonly', true);

    $method = 'edit';
    $active = '編輯';
    $manageinfo = ' 管理者 : '.$row[$colname['manager']].' | 建立時間 : '.$row[$colname['create_time']].' | 上次修改時間 : '.$row[$colname['update_time']];
}else{
    $method = 'add';
	$active = '新增';
    //$p_ptid_val = array();
}

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
<link href="../css/codertextgroup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../assets/bootstrap-fileupload/bootstrap-fileupload.css" />
<script language="javascript" type="text/javascript">
<?php
//coderFormHelp::drawPicScript($method, $file_path, $spic, 'org_pic_s');
//coderFormHelp::drawPicScript($method, $file_path, $bpic, 'org_pic_b');
coderFormHelp::drawPicScript($method, $file_path, $pic, 'org_pic');
?>
</script>
</head>
<body>

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
    
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title -->
        <?php if($manageinfo!=''){?>
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>系統資訊 : </strong> <?php echo $manageinfo; ?> </div>
        <?php }?>
        <!-- BEGIN Main Content -->
        <div class="row">
            <form  class="form-horizontal" enctype="multipart/form-data" action="save.php" id="myform" name="myform" method="post">
                <?php echo $fhelp -> drawForm($colname['id']); ?>
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-title">
                            <h3><i class="<?php echo getIconClass($method); ?>"></i> <?php echo $page_title.$active; ?></h3>
                            <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        </div>
                        <div class="box-content">
                            <div class="row"> 
                                <!--left start-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['is_show']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['is_show']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['is_index_top']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['is_index_top']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['is_index']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['is_index']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['is_top']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['is_top']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['is_hot']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['is_hot']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel('shop_id'); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm('shop_id'); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel('ptype1_id'); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm('ptype1_id'); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['ptype2_id']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['ptype2_id']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['name']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['name']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['sno']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['sno']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['price']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['price']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['bonus']); ?> </label>
                                        <div class="col-sm-9 controls"> <?php echo $fhelp -> drawForm($colname['bonus']); ?> </div>
                                    </div>
                                </div>
                                <!--left end--> 
                                <!--right start-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['pic']); ?> </label>
                                        <div class="col-sm-9 controls">
                                            <div id="picupload"></div>
                                        </div>
                                    </div>
                                    <?php /*?><div class="form-group">
                                        <label class="col-sm-2 col-lg-2 control-label" > <?php echo $fhelp -> drawLabel($colname['bpic']); ?> </label>
                                        <div class="col-sm-9 controls">
                                            <div id="picupload_b"></div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <!--right end-->
                            <div class="row">
                                <div class="col-md-12">
                                	<div class="form-group">
                                        <label class="col-sm-2 col-lg-1 control-label" > <?php echo $fhelp -> drawLabel($colname['information']); ?> </label>
                                        <div class="col-sm-10 controls"> <?php echo $fhelp -> drawForm($colname['information']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-1 control-label" > <?php echo $fhelp -> drawLabel($colname['specification']); ?> </label>
                                        <div class="col-sm-10 controls"> <?php echo $fhelp -> drawForm($colname['specification']); ?> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-lg-1 control-label" > <?php echo $fhelp -> drawLabel($colname['notice']); ?> </label>
                                        <div class="col-sm-10 controls"> <?php echo $fhelp -> drawForm($colname['notice']); ?> </div>
                                    </div>
                                    <div class="form-group text-center">
                                        <div class="">
                                            <button type="submit" class="btn btn-primary" ><i class="icon-ok"></i>完成<?php echo $active?></button>
                                            &nbsp;&nbsp;&nbsp;
                                            <button type="button" class="btn" onClick="if(confirm('確定要取消<?php echo $active?>?')){parent.closeBox();}"><i class="icon-remove"></i>取消<?php echo $active?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        <!-- END Main Content -->
        
        <?php include_once('../footer.php'); ?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script> 
<script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.js"></script> 
<script type="text/javascript" src="../assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="../js/coderpicupload.js"></script> 
<script type="text/javascript" src="../ui/selectboxes/jquery.selectboxes.js"></script> 
<script type="text/javascript" src="../js/coderselectBoxManipulation.js"></script> 
<script type="text/javascript" src="../js/codertextgroup.js"></script><!--動態增加欄位--> 
<script type="text/javascript" src="../assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<?php
$ary_selectbox = array(0 => 'ptype1_id', 1 => $colname['ptype2_id']);
echo coderFormHelp::selectBoxManipulation($ary_selectbox, $ptype2_ary, $product_type_ary);
?>
<script type="text/javascript">
$('#picupload').coderpicupload({
	pics : [
		{
			name : '大圖', 
			type : 6, 
			tag : 'b', 
			width : <?php echo $product_bpic_w; ?>, 
			height : <?php echo $product_bpic_h; ?>
		},
		{
			name : '小圖', 
			type : 6, 
			tag : 's', 
			width : <?php echo $product_spic_w; ?>, 
			height : <?php echo $product_spic_h; ?>
		}
	], 
	width : <?php echo $product_bpic_w; ?>, 
	height : <?php echo $product_bpic_h; ?>, 
	s_width : ['110px', '80px'], 
	s_height : ['110px', '80px'], 
	id : '<?php echo $colname["pic"]; ?>', 
	required : true, 
	org_pic : org_pic
});
<?php /*?>$('#picupload_b').coderpicupload({
	pics : [{
		name : '縮圖', 
		type : 6, 
		tag : 'b', 
		width : <?php echo $product_bpic_w; ?>, 
		height : <?php echo $product_bpic_h; ?>
	}], 
	width : <?php echo $product_bpic_w; ?>, 
	height : <?php echo $product_bpic_h; ?>, 
	s_width : '80px', 
	s_height : '80px', 
	id : '<?php echo $colname["bpic"]; ?>', 
	required : true, 
	org_pic : org_pic_b
});<?php */?>

<?php echo coderFormHelp::drawVaildScript(); ?>
<?php 
	if($method == 'add'){
?>
$("#<?php echo $colname['sno']; ?>").rules("add", {
	messages : {
		remote :  "此序號己被使用,請重新輸入!"
	},
	remote : {
		url : "checkisexisit.php",
		type : "post",
		data : {
			data : function(){return $('#<?php echo $colname['sno']; ?>').val()},
			type : "<?php echo $colname['sno']; ?>"
		}
	}
});
<?php
	}
?>

</script>
</body>
</html>

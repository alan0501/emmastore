<?php
$filterhelp = new coderFilterHelp();

$ary = array();
$ary[] = array('column' => $colname['manager'], 'name' => '管理員');
$ary[] = array('column' => $colname['name'], 'name' => '商品名');
$obj[] = array('type' => 'keyword', 'name' => '關鍵字', 'sql' => true, 'ary' => $ary);

$_is_show = array('type' => 'select', 'name' => '公開', 'column' => $colname['is_show'], 'sql' => true, 'ary' => array(array('value' => '1', 'name' => '是'), array('value' => '0', 'name' => '否')));
$obj[] = $_is_show;

$_is_index_top = array('type' => 'select', 'name' => '首頁大圖顯示', 'column' => $colname['is_index_top'], 'sql' => true, 'ary' => array(array('value' => '1', 'name' => '是'), array('value' => '0', 'name' => '否')));
$obj[] = $_is_index_top;

$_is_index = array('type' => 'select', 'name' => '首頁小圖顯示', 'column' => $colname['is_index'], 'sql' => true, 'ary' => array(array('value' => '1', 'name' => '是'), array('value' => '0', 'name' => '否')));
$obj[] = $_is_index;

$_is_top = array('type' => 'select', 'name' => '分類頁置頂', 'column' => $colname['is_top'], 'sql' => true, 'ary' => array(array('value' => '1', 'name' => '是'), array('value' => '0', 'name' => '否')));
$obj[] = $_is_top;

/*$_is_like = array('type' => 'select', 'name' => '推薦', 'column' => $colname['is_like'], 'sql' => true, 'ary' => array(array('value' => '1', 'name' => '是'), array('value' => '0', 'name' => '否')));
$obj[] = $_is_like;*/

$_shop = array('type' => 'select', 'name' => '店家', 'column'=> $colname['shop_id'], 'sql' => false, 'ary' => $shop_ary, 'default' => '');
$obj[]=$_shop;

$_ptype1 = array('type' => 'select', 'name' => '一階分類', 'column' => $colname_ptype2['ptype1_id'], 'sql' => false, 'ary' => $ptype2_ary, 'default' => $filter_ptype1_id);
$obj[] = $_ptype1;

$_ptype2 = array('type' => 'select', 'name' => '二階分類', 'column' => $colname_ptype2['id'], 'sql' => false, 'ary' => $ptype2_ary, 'default' => $filter_ptype2_id);
$obj[] = $_ptype2;

$obj[] = array('type' => 'dategroup', 'sql' => true, 'column' => 'dategroup', 'ary' => array(array('name' => '建立日期', 'column' => $colname['create_time']), array('name' => '最後修改日期', 'column' => $colname['update_time'])));

$filterhelp -> Bind($obj);


/*****END PHP*****/
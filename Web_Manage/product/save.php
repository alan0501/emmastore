<?php
include_once('_config.php');
include_once('formconfig.php');

$errorhandle = new coderErrorHandle();
try{
	$db = Database::DB();
    $id = post($colname['id']);
    //$ps_id = '';
    if($id != ""){
        $method = 'edit';
        $active = '編輯';
    }else{
        $method = 'add';
        $active = '新增';
    }
    $data = $fhelp -> getSendData();
    $error = $fhelp -> vaild($data);
    // var_dump($data);
    if(count($error) > 0){
        $msg = implode('<br/>', $error);
        throw new Exception($msg);
    }
	
    $data[$colname['manager']] = $adminuser['username'];
	$data[$colname['update_time']] = datetime();

    $data[$colname['is_show']] = $data[$colname['is_show']]?1:0;
    $data[$colname['is_index_top']] = $data[$colname['is_index_top']]?1:0;
    $data[$colname['is_index']] = $data[$colname['is_index']]?1:0;
    $data[$colname['is_top']] = $data[$colname['is_top']]?1:0;
    $data[$colname['is_hot']] = $data[$colname['is_hot']]?1:0;
    $data[$colname['bonus']] = $data[$colname['bonus']]?1:0;

    $data[$colname['is_like']] = 0;
    $data['product_spic'] = "";
    $data['product_bpic'] = "";
	
	
	if($method == 'edit'){
        $db -> query_update($table, $data, "{$colname['id']} = {$id}");
	}else{
		$sno = $data[$colname['sno']];
		
		if($db -> isExisit($table, "{$colname['sno']}", $sno)){
			throw new Exception('商品序號'.$sno.'重覆,請重新輸入一組序號!');
		}
		
        $data[$colname['ind']] = coderListOrderHelp::getMaxInd($table, $colname['ind']);
		$data[$colname['create_time']] = datetime();        
		
        $id = $db -> query_insert($table, $data);
	}


    //coderFormHelp::moveCopyPic($data[$colname['spic']], $admin_path_temp, $file_path, 's');
	//coderFormHelp::moveCopyPic($data[$colname['bpic']], $admin_path_temp, $file_path, 'b');
	coderFormHelp::moveCopyPic($data[$colname['pic']], $admin_path_temp, $file_path, 'b,s');
	
    coderAdminLog::insert($adminuser['username'], $logkey, $method, $page_title.',ID_'.$id);
    $db -> close();
    //echo showParentSaveNote($page_title, $active, $data[$colname['name']], "manage.php?id=$id&psid=$ps_id");
	echo showParentSaveNote($page_title, $active, $data[$colname['name']], "manage.php?id=$id");
	
}catch(Exception $e){
	$errorhandle -> setException($e);
}

if($errorhandle -> isException()){
    $errorhandle -> showError();
}

/*****END PHP*****/
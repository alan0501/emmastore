<?php
$fhelp = new coderFormHelp();

$fobj = array();
$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);
$fobj[$colname['is_show']] = array('type' => 'checkbox', 'name' => '公開', 'column' => $colname['is_show'], 'value' => '1', 'default' => '1');
$fobj[$colname['is_index_top']] = array('type' => 'checkbox', 'name' => '首頁大圖', 'column' => $colname['is_index_top'], 'value' => '1', 'default' => '0');
$fobj[$colname['is_index']] = array('type' => 'checkbox', 'name' => '首頁小圖', 'column' => $colname['is_index'], 'value' => '1', 'default' => '0');
$fobj[$colname['is_top']] = array('type' => 'checkbox', 'name' => '分類頁置頂', 'column' => $colname['is_top'], 'value' => '1', 'default' => '0');
$fobj[$colname['is_hot']] = array('type' => 'checkbox', 'name' => '本店熱門商品', 'column' => $colname['is_hot'], 'value' => '1', 'default' => '0');
//$fobj[$colname['spic']] = array('type' => 'pic', 'name' => '列表圖', 'column' => $colname['spic'], 'validate' => array('required' => 'yes'), 'help' => '用於商品列表頁');
//$fobj[$colname['bpic']] = array('type' => 'pic', 'name' => '商品圖', 'column' => $colname['bpic'], 'validate' => array('required' => 'yes'), 'help' => '用於商品說明頁上方大圖');
$fobj[$colname['pic']] = array('type' => 'pic', 'name' => '商品圖', 'column' => $colname['pic'], 'validate' => array('required' => 'yes'), 'help' => '商品圖');
$fobj['shop_id'] = array('type' => 'select', 'name' => '店家', 'column' => $colname['shop_id'], 'ary' => $shop_ary, 'validate' => array('required' => 'yes'));
$fobj['ptype1_id'] = array('type' => 'select','name' => '一階分類', 'column'  => 'ptype1_id', 'validate' => array('required' => 'yes'), 'sql' => false);
$fobj[$colname['ptype2_id']] = array('type' => 'select', 'name' => '二階分類', 'column' => $colname['ptype2_id'], 'validate' => array('required' => 'yes'));
$fobj[$colname['sno']] = array('type' => 'text', 'name' => '商品序號', 'column' => $colname['sno'], 'placeholder' => '請填寫商品序號', 'validate' => array('required' => 'yes', 'maxlength' => '20', 'minlength' => '1'));
$fobj[$colname['name']] = array('type' => 'text', 'name' => '商品名稱', 'column' => $colname['name'], 'placeholder' => '請填寫商品名稱', 'validate' => array('required' => 'yes', 'maxlength' => '50', 'minlength' => '1'));
$fobj[$colname['price']] = array('type' => 'text', 'name' => '定價', 'column' => $colname['price'], 'placeholder' => '請填寫商品定價', 'validate' => array('required' => 'yes', 'number' => 'yes'));
$fobj[$colname['bonus']] = array('type' => 'text', 'name' => '購物金', 'column' => $colname['bonus'], 'placeholder' => '請填寫商品購物金', 'validate' => array('number' => 'yes'));
$fobj[$colname['specification']] = array('type' => 'html', 'name' => '商品規格', 'column' => $colname['specification'], 'placeholder' => '請輸入說明', 'validate' => array('required' => 'yes'), 'htmlencode' => false);
$fobj[$colname['information']] = array('type' => 'html', 'name' => '商品描敘', 'column' => $colname['information'], 'placeholder' => '請輸入說明', 'validate' => array('required' => 'yes'), 'htmlencode' => false);
$fobj[$colname['notice']] = array('type' => 'html', 'name' => '購物須知', 'column' => $colname['notice'], 'placeholder' => '請輸入說明', 'validate' => array('required' => 'yes'), 'htmlencode' => false);


$fhelp -> Bind($fobj);


/*****END PHP*****/
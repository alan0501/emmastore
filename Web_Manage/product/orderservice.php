<?php
include_once('_config.php');
include_once('filterconfig.php');
$success = false;
$count = 0;
$msg = "未知錯誤,請聯絡系統管理員";
$order_id = post('order_id', 1);
$order_key = post('order_key', 1);
$method = post('method', 1);
$where = '';

$sqlstr = $filterhelp -> getSQLStr();
$where .= $sqlstr -> SQL != '' ? ' AND '.$sqlstr -> SQL : '';

$ptype1_id = get($colname_ptype2['ptype1_id']);
if(!empty($ptype1_id)){
	$where .= " AND ".$colname_ptype2['ptype1_id'].'='.$ptype1_id;
}
$ptype2_id = get($colname_ptype2['id']);
if(!empty($ptype2_id)){
	$where .= " AND ".$colname_ptype2['id'].'='.$ptype2_id;
}
$shop_id = get($colname['shop_id']);
if(!empty($shop_id)){
	$where .= " AND ".$colname['shop_id'].'='.$shop_id;
}

if($order_id > 0 && $order_id != ""){
	$db = Database::DB();
	try{
		$table = "$table 
				  LEFT JOIN $table_ptype2 ON {$colname['ptype2_id']} = {$colname_ptype2['id']}
				  LEFT JOIN $table_ptype1 ON {$colname_ptype2['ptype1_id']} = {$colname_ptype1['id']}";
		if($orderDesc == "desc"){
			coderlistorderhelp::changeDescOrder($method, $table, $orderColumn, $order_id, $order_key, $where);
		}else{
			coderlistorderhelp::changeAscOrder($method, $table, $orderColumn, $order_id, $order_key, $where);
		}
		$success = true;
		$db -> close();
		
	}catch(Execption $e){
		$msg = $e -> getMessage();
	}
}else{
	$msg = "未設定排序資料";
}

$result['result'] = $success;
$result['count'] = $count;
$result['msg'] = $msg;
echo json_encode($result);

/*****END PHP*****/
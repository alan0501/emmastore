<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'shop';
$logkey = 'shop_product';
include_once('../_config.php');

coderAdmin::vaild($authkey);
$auth = coderAdmin::Auth($authkey);

$table = coderDBConf::$product;
$colname = coderDBConf::$col_product;

$table_ptype2 = coderDBConf::$ptype2;
$colname_ptype2 = coderDBConf::$col_ptype2;
$table_ptype1 = coderDBConf::$ptype1;
$colname_ptype1 = coderDBConf::$col_ptype1;
$table_shop = coderDBConf::$admin;
$colname_shop = coderDBConf::$col_admin;

$idColumn = $colname['id'];
$orderColumn = $colname['ind'];
$orderDesc = "desc";

$file_path = $admin_path_product;

$page_name = '商品';
$page_title = $auth['name'].' - '.$page_name;
$page_desc = "{$page_title}-您可將內容修改為希望呈現的內容。";
$mtitle = '<li class="active">'.$page_title.'</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");

$filter_ptype1_id = get($colname_ptype2['ptype1_id'], 1);
$filter_ptype2_id = get($colname_ptype2['id']);
$filter_shop_id = get($colname['shop_id']);


$ptype1_ary = class_ptype1::getList();
$ptype2_ary = class_ptype2::getList();
$shop_ary = class_shop::getList();

foreach($ptype1_ary as $type1){
	 $ptype2_ary[] = array('name' => $type1['name'], 'value' => $type1['value'], 'pid' => '0');
}


function isNotExisit($data, $type){
	global $db, $table, $colname;
	if(strlen($data) > 2 && !$db -> query_first('SELECT '.$colname['id'].' FROM '.$table.' WHERE '.$type.'=\''.hc($data).'\'')){
		return true;
	}else {
		return false;
	}
}

/*****END PHP*****/
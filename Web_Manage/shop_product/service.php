<?php
include_once('_config.php');
include_once('filterconfig.php');

$errorhandle = new coderErrorHandle();
try{
	$db = Database::DB();
	$sHelp = new coderSelectHelp($db);
	$sHelp -> select = "*";
	$sHelp -> table = "$table  
					   LEFT JOIN $table_ptype2 ON {$colname['ptype2_id']} = {$colname_ptype2['id']}
					   LEFT JOIN $table_ptype1 ON {$colname_ptype2['ptype1_id']} = {$colname_ptype1['id']}";
	$sHelp -> page_size = get("pagenum");
	$sHelp -> page = get("page");
	$sHelp -> orderby = get("orderkey", 1);
	$sHelp -> orderdesc = get("orderdesc", 1);
	// $sHelp->orderby=$orderColumn;
	// $sHelp->orderdesc='DESC';

	$sqlstr = $filterhelp -> getSQLStr();
	$where = $sqlstr -> SQL;
	

	$ptype1_id = get($colname_ptype2['ptype1_id']);
	if(!empty($ptype1_id)){
		$where .= ($where == '' ? '' : ' AND ').$colname_ptype2['ptype1_id'].'='.$ptype1_id;
	}
	$ptype2_id = get($colname_ptype2['id']);
	if(!empty($ptype2_id)){
		$where .= ($where == '' ? '' : ' AND ').$colname_ptype2['id'].'='.$ptype2_id;
	}
	$shop_id = $adminuser["id"];
	if(!empty($shop_id)){
		$where .= ($where == '' ? '' : ' AND ').$colname['shop_id'].'='.$shop_id;
	}


	$sHelp -> where = $where;

	$rows = $sHelp -> getList();
	//print_r($rows);exit;
	for($i = 0; $i < count($rows); $i ++){
		$rows[$i][$colname['is_show']] = $ary_yn[$rows[$i][$colname['is_show']]];
		//$rows[$i][$colname['is_index']] = $ary_yn[$rows[$i][$colname['is_index']]];
		//$rows[$i][$colname['is_top']] = $ary_yn[$rows[$i][$colname['is_top']]];
		$rows[$i][$colname['is_hot']] = $ary_yn[$rows[$i][$colname['is_hot']]];
		$rows[$i]['ptype1_id'] = $rows[$i]['ptype1_name'];
		$rows[$i][$colname['ptype2_id']] = $rows[$i]['ptype2_name'];
		$rows[$i][$colname['shop_id']] = class_shop::getName($rows[$i][$colname['shop_id']]);
		$rows[$i][$colname['update_time']] = coderHelp::getDateTime($rows[$i][$colname['update_time']]);
	}

	$result['result'] = true;
	$result['data'] = $rows;
	$result['page'] = $sHelp -> page_info;
	echo json_encode($result);
	
}catch(Exception $e){
	$errorhandle -> setException($e); 
}

if($errorhandle -> isException()){
	$result['result'] = false;
    $result['data'] = $errorhandle -> getErrorMessage();
	echo json_encode($result);
}

/*****END PHP*****/
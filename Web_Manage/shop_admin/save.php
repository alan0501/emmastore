<?php
include_once('_config.php');
$errorhandle = new coderErrorHandle();
try{  
	if(post($colname['id']) > 0){
		$method = 'edit';
		$active = '編輯';
	}else{
		$method = 'add';
		$active = '新增';
		$fhelp -> setAttr($colname['password'], 'validate', array('required' => 'yes', 'maxlength' => '20', 'minlength' => '6'));
	}
	
	$data = $fhelp -> getSendData();
	//print_r($data);
	$error = $fhelp -> vaild($data);
	
	if(count($error) > 0){
		$msg = implode('\r\n', $error);
		throw new Exception($msg);
	}
	
	if(isset($data[$colname['auth']])){
		$_auth = array_sum(explode(',', $data[$colname['auth']]));
		$data[$colname['auth']] = $_auth;
	}
  
	$data[$colname['manager']] = $adminuser['username'];
	$data[$colname['update_time']] = datetime();
	
	if(isset($data[$colname['spic']])){
		coderFormHelp::moveCopyPic($data[$colname['spic']], $admin_path_temp, $file_path, 's');
	}
	coderFormHelp::moveCopyPic($data[$colname['bpic']], $admin_path_temp, $file_path, 'b');

	$db = Database::DB();
	if($method == 'edit'){
		
		$s_username = $adminuser['username'];
		$username = post($colname['account'], 1);
		unset($data[$colname['account']]);
		
		if($data[$colname['password']] == ''){
			unset($data[$colname['password']]);
		}else{
			$data[$colname['password']] = sha1($data[$colname['password']]);
		}
		
		$id = $db -> query_update($table, $data, " {$colname['account']} = :username ", array(':username' => $username));
		
					
		if($s_username === $username){
			coderAdmin::change_admin_data($username);
		}
	}else{
		$username = $data[$colname['account']];
		$email = $data[$colname['email']];
		$data[$colname['password']] = sha1($data[$colname['password']]);
		
		if($db -> isExisit($table, "{$colname['account']}", $username)){
			throw new Exception('帳號'.$username.'重覆,請重新輸入一組帳號');
		}
		if($db -> isExisit($table, "{$colname['email']}", $email)){
			throw new Exception('E-mail'.$email.'重覆,請重新輸入一組E-mail');
		}
		$data[$colname['ind']] = coderListOrderHelp::getMaxInd($table, $colname['ind']);
		$data[$colname['token_id']] = getmid();
		$data[$colname['create_time']] = datetime();
		
		$id = $db -> query_insert($table, $data);
	  
	}
	
	coderAdminLog::insert($adminuser['username'], $logkey, $method, $username);
	class_shop::clearCache();
	//echo showParentSaveNote($auth['name'], $active, $username, "manage.php?username=".$username);
	$db -> close();
	
	$result['result'] = true;
    $result['data'] = $errorhandle -> getErrorMessage();
    echo json_encode($result);
	 
}catch(Exception $e){
	$errorhandle -> setException($e); 
}

if($errorhandle -> isException()){
	$result['result'] = false;
    $result['data'] = $errorhandle -> showError();
	echo json_encode($result);
}

function getmid(){
    global $db, $table, $colname;
    $mid = md5(uniqid(rand()));
    if($db -> isExisit($table, "{$colname['token_id']}", $mid)){
        return getmid();
    }else{
        return $mid;
    }
}

/*****END PHP*****/
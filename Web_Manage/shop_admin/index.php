<?php 
include_once('_config.php');

$listHelp = new coderListHelp('table1', '帳號');
$listHelp -> mutileSelect = true;
$listHelp -> editLink = "manage.php";
$listHelp -> addLink = "manage.php?level=$level";
$listHelp -> ajaxSrc = "service.php?level=$level";
$listHelp -> delSrc = "delservice.php";
$listHelp -> orderSrc = "orderservice.php?level=$level";
$listHelp -> orderColumn = $orderColumn;
$listHelp -> orderDesc = $orderDesc;

$col = array();
$col[] = array('column' => $colname['id'], 'name' => 'ID', 'order' => true, 'width' => '60');
if($level == 2){
	$col[] = array('column' => $colname['spic'], 'name' => '圖片', 'width' => '50');
}else{
	$col[] = array('column' => $colname['bpic'], 'name' => '圖片', 'width' => '50');	
}
$col[] = array('column' => $colname['account'], 'name' => '登入帳號', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['name'], 'name' => '名稱', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['is_show'], 'name' => '啟用', 'order' => true, 'width' => '60');
$col[] = array('column' => $colname['auth'], 'name' => '操作權限');
$col[] = array('column' => $colname['ip'], 'name' => '登入IP', 'width' => '100');
$col[] = array('column' => $colname['manager'], 'name' => '管理員', 'order' => true, 'width' => '100');
$col[] = array('column' => $colname['update_time'], 'name' => '最後更新時間','order' => true, 'width' => '150');
$listHelp -> Bind($col);

$listHelp -> bindFilter($help);

$db = Database::DB();

coderAdminLog::insert($adminuser['username'], $logkey, 'view', '列表');

$db -> close();
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('../head.php'); ?>
</head>
<body >
<?php include_once('../navbar.php'); ?>
<!-- BEGIN Container -->
<div class="container" id="main-container">
    <?php include_once('../left.php'); ?>
    <!-- BEGIN Content -->
    <div id="main-content"> 
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="<?php echo $mainicon; ?>"></i> <?php echo $page_title; ?>管理</h1>
                <h4><?php echo $page_desc; ?></h4>
            </div>
        </div>
        <!-- END Page Title --> 
        
        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li> <i class="icon-home"></i> <a href="../home/index.php">Home</a> <span class="divider"><i class="icon-angle-right"></i></span> </li>
                <?php echo $mtitle; ?>
            </ul>
        </div>
        <!-- END Breadcrumb --> 
        
        <!-- BEGIN Main Content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3 style="float:left"><i class="icon-table"></i> <?php echo $page_title; ?></h3>
                        <div class="box-tool"> <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a> <a data-action="close" href="#"><i class="icon-remove"></i></a> </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-content"> <?php echo $listHelp -> drawTable(); ?> </div>
                </div>
            </div>
        </div>
        <?php include('../footer.php')?>
        <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a> </div>
    <!-- END Content --> 
</div>
<!-- END Container -->

<?php include_once('../js.php'); ?>
<script src="../js/coderlisthelp.js"></script> 
<script>
$(document).ready(function() {
	$('#table1').coderlisthelp({
		debug : true, 
		callback : function(obj, rows){
			obj.html('');
			var count = rows.length;
			for(var i = 0; i < count; i ++){
				var row = rows[i];
				var $tr = $('<tr></tr>');
				$tr.attr("orderlink", "order_id="+row["<?php echo $colname['id'];?>"]+"&order_key=<?php echo $colname['id'];?>");
				
				$tr.attr("editlink", "username="+row["<?php echo $colname['account'];?>"]+"&level=<?php echo $level;?>");
				$tr.attr("delkey", row["<?php echo $colname['id'];?>"]);
				$tr.attr("title", row["<?php echo $colname['account'];?>"]);
				
				$tr.append('<td>'+row["<?php echo $colname['id'];?>"]+'</td>');
				$tr.append('<td><img src="../../upload/admin/'+row["pic"]+'" width="50"></img></td>');
				$tr.append('<td>'+row["<?php echo $colname['account'];?>"]+'</td>');	
				$tr.append('<td>'+row["<?php echo $colname['name'];?>"]+'</td>');	
				$tr.append('<td>'+row["<?php echo $colname['is_show'];?>"]+'</td>');		
				$tr.append('<td>'+row["<?php echo $colname['auth'];?>"]+'</td>');											
				$tr.append('<td>'+row["<?php echo $colname['ip'];?>"]+'</td>');								
				$tr.append('<td>'+row["<?php echo $colname['manager'];?>"]+'</td>');
				$tr.append('<td>'+row["<?php echo $colname['update_time'];?>"]+'</td>');
				obj.append($tr);
			}
		}
	});			
});
</script>
</body>
</html>

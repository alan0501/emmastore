<?php
include('_config.php');
include('filterconfig.php');
$errorhandle=new coderErrorHandle();
try{
	$db = Database::DB();
	$sHelp=new coderSelectHelp($db);
	$sHelp->select="*";
	$sHelp->table=$table;
	$sHelp->page_size=get("pagenum");
	$sHelp->page=get("page");
	$sHelp->orderby=get("orderkey",1);
	$sHelp->orderdesc=get("orderdesc",1);
	// $sHelp->orderby=$orderColumn;
	// $sHelp->orderdesc='DESC';

	$sqlstr=$filterhelp->getSQLStr();
	$where = $sqlstr->SQL;
	if($type!=''){$where .= ($where!=''?' AND ':'').$colname['type'].'='.$type;}
	$sHelp->where=$where;

	$rows=$sHelp->getList();
	//print_r($rows);exit;
	for($i=0;$i<count($rows);$i++){
		$rows[$i][$colname['ispublic']]=$ary_yn[$rows[$i][$colname['ispublic']]];
		$rows[$i][$colname['updatetime']]=coderHelp::getDateTime($rows[$i][$colname['updatetime']]);
		$rows[$i][$colname['type']]='<span class="label '.($rows[$i][$colname['type']]==1?'label-important':'label-yellow').'">'.$ary_aboutnew_title[$rows[$i][$colname['type']]]['tag'].'</span>';
	}

	$result['result']=true;
	$result['data']=$rows;
	$result['page']=$sHelp->page_info;
	echo json_encode($result);
}
catch(Exception $e){
	$errorhandle->setException($e); // 收集例外
}

if ($errorhandle->isException()) {
	$result['result']=false;
    $result['data']=$errorhandle->getErrorMessage();
	echo json_encode($result);
}

?>
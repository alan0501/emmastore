<?php
include('_config.php');
include('formconfig.php');

$errorhandle=new coderErrorHandle();
try{
$db = Database::DB();
  $id = post($colname['id']);
  if($id!=""){
    $method='edit';
    $active='編輯';
  }
  else{
    $method='add';
    $active='新增';
  }
  $data=$fhelp->getSendData();
  $error=$fhelp->vaild($data);
  if(count($error)>0){
    $msg=implode('<br/>',$error);
    throw new Exception($msg);
  }

  $nowtime = datetime();
  $data[$colname['admin']]=$adminuser['username'];
  $data[$colname['updatetime']]= $nowtime;
  if($method=='edit'){
    $db->query_update($table,$data," {$colname['id']}={$id}");
	}else{
    $data[$colname['ind']]=coderListOrderHelp::getMaxInd($table,$colname['ind']);
		$data[$colname['createtime']]= $nowtime;
		$id=$db->query_insert($table,$data);
	}


  coderAdminLog::insert($adminuser['username'],$logkey,$method,$page_title.',ID_'.$id);
  class_index_news::clearCache();
  $db->close();
  echo showParentSaveNote($page_title,$active,$data[$colname['title']],"manage.php?id=".$id);
}
catch(Exception $e){
	$errorhandle->setException($e); // 收集例外
}

if ($errorhandle->isException()) {
    $errorhandle->showError();
}

?>
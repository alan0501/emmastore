<?php
$fhelp=new coderFormHelp();
$fobj=array();
$fobj[$colname['id']]=array('type'=>'hidden','name'=>'ID','column'=>$colname['id'],'sql'=>false);


$fobj[$colname['ispublic']]=array('type'=>'checkbox','name'=>'啟用','column'=>$colname['ispublic'],'value'=>'1','default'=>'1');
$fobj[$colname['type']] = array(
    'type' => 'radio','name' => '類型','column' =>$colname['type'] ,'validate' => array('required' => 'yes'),'ary' => coderHelp::makeAryKeyToAryElement($aboutnew_type_ary,'key','name'),'mode'=>'no_default','default'=>$type
);
$fobj[$colname['title']]=array('type'=>'text','name'=>'標題','column'=>$colname['title'],'placeholder'=>'請填寫標題','validate' => array(
        'required' => 'yes',
        'maxlength' => '150',
        'minlength' => '1'
    ));
$fobj[$colname['content']] = array(
    'type' => 'html','name' => '內容','column' => $colname['content'],'htmlencode' => false,
);


$fhelp->Bind($fobj);
?>
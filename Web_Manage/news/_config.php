<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'news';
$logkey = 'news';
include('../_config.php');
coderAdmin::vaild($authkey);

$auth=coderAdmin::Auth($authkey);

$table=coderDBConf::$about_new;
$colname=coderDBConf::$col_about_new;

$idColumn = $colname['id'];
$orderColumn=$colname['ind'];
$orderDesc="desc";

$type = get("type")==0?'':get("type");

$page=request_pag("page");
switch ($type) {
    case '1':
        $page_name=$ary_aboutnew_title[$type]['tag2'];
        break;
    case '2':
        $page_name=$ary_aboutnew_title[$type]['tag2'];
        break;
    default:
        $page_name='總列表';
        break;
}
$page_title=$auth['name'].' - '.$page_name;


$page_desc="{$page_title}-您可將內容修改為希望呈現的內容。";
$mtitle='<li class="active">'.$page_title.'</li>';
$mainicon=$auth['icon'];

$aboutnew_type_ary=array();
foreach ($ary_aboutnew_title as $key => $row) {
    $aboutnew_type_ary[$key] = $row['tag2'];
}
?>
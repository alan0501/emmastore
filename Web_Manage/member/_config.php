<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'member';
$logkey = 'member';
include_once('../_config.php');

coderAdmin::vaild($authkey);
$auth = coderAdmin::Auth($authkey);

$table = coderDBConf::$member;
$colname = coderDBConf::$col_member;

$orderColumn = $colname['id'];
$orderDesc = "desc";

$page_title = $auth['name'].' - 會員列表';
$page_desc = "{$page_title}-您可將內容修改為希望呈現的內容。";
$mtitle = '<li class="active">'.$page_title.'</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");


/*****END PHP*****/
<?php
include('_config.php');
include('formconfig.php');

$errorhandle = new coderErrorHandle();
try{
	$db = Database::DB();
	$id = post($colname['id']);
	
	if($id != ""){
		$method = 'edit';
		$active = '編輯';
	}else{
		$method = 'add';
		$active = '新增';
	}
	
	$data = $fhelp -> getSendData();
	$error = $fhelp -> vaild($data);
	if(count($error) > 0){
		$msg = implode('<br/>',$error);
		throw new Exception($msg);
	}
	

	$nowtime = datetime();
	$data[$colname['manager']] = $adminuser['username'];
	$data[$colname['update_time']] = $nowtime;
	
	if($method == 'edit'){
    	$db -> query_update($table, $data, " {$colname['id']} = {$id}");
	}else{
    	//$data[$colname['ind']] = coderListOrderHelp::getMaxInd($table, $colname['ind']);
		$data[$colname['create_time']] = $nowtime;
		$id = $db -> query_insert($table, $data);
	}

	coderAdminLog::insert($adminuser['username'], $logkey, $method, $page_title.',ID_'.$id);
  
	$db -> close();
	//class_index_pic::clearCache();
	echo showParentSaveNote($page_title, $active, '會員列表', "manage.php?id=".$id);
	
}catch(Exception $e){
	$errorhandle -> setException($e);
}

if($errorhandle -> isException()){
    $errorhandle -> showError();
}

/*****END PHP*****/
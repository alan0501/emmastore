<?php
$fhelp = new coderFormHelp();

$fobj = array();
$fobj[$colname['id']] = array('type' => 'hidden', 'name' => 'ID', 'column' => $colname['id'], 'sql' => false);
$fobj[$colname['is_show']] = array('type' => 'checkbox', 'name' => '啟用', 'column' => $colname['is_show'], 'value' => '1', 'default' => '1');

/*$fobj[$colname['ppt_id']] = array(
    'type' => 'select','name' => '所屬大分類','column' =>$colname['ppt_id'],'ary' => coderHelp::makeAryKeyToAryElement($ary_ppt_onlyname,'value','name'),'validate' => array(
        'required' => 'yes',
    )
);*/

$fobj[$colname['name']] = array('type' => 'text', 'name' => '一階分類名稱', 'column' => $colname['name'], 'placeholder' => '請填寫分類名稱', 'validate' => array('required' => 'yes', 'maxlength' => '30', 'minlength' => '1'));
$fobj[$colname['pic']] = array('type' => 'pic', 'name' => '圖', 'column' => $colname['pic'], 'validate' => array('required' => 'yes'));

$fhelp -> Bind($fobj);

/*****END PHP*****/
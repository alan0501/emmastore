<?php
include_once('_config.php');
include_once('filterconfig.php');
$errorhandle = new coderErrorHandle();
try{
	$db = Database::DB();
	$sHelp = new coderSelectHelp($db);
	$sHelp -> select = "*, (SELECT COUNT($table_ptype2.{$colname_ptype2['id']}) FROM $table_ptype2 WHERE $table.{$colname['id']} = $table_ptype2.{$colname_ptype2['ptype1_id']}) AS countnum";
	$sHelp -> table = $table;
	$sHelp -> page_size = get("pagenum");
	$sHelp -> page = get("page");
	$sHelp -> orderby = get("orderkey", 1);
	$sHelp -> orderdesc = get("orderdesc", 1);
	// $sHelp->orderby=$orderColumn;
	// $sHelp->orderdesc='DESC';

	$sqlstr = $filterhelp -> getSQLStr();
	$where = $sqlstr -> SQL;
	$sHelp -> where = $where;

	$rows = $sHelp -> getList();
	//print_r($rows);exit;
	for($i = 0; $i < count($rows); $i ++){
		$rows[$i][$colname['is_show']] = $ary_yn[$rows[$i][$colname['is_show']]];
		//$rows[$i]['pptid_btn'] = '<span class="label '.$ary_ppt[$rows[$i][$colname['ppt_id']]]['color'].'">'.$ary_ppt[$rows[$i][$colname['ppt_id']]]['name'].'</span>';
		$rows[$i][$colname['update_time']] = coderHelp::getDateTime($rows[$i][$colname['update_time']]);
	}

	$result['result'] = true;
	$result['data'] = $rows;
	$result['page'] = $sHelp -> page_info;
	echo json_encode($result);
}catch(Exception $e){
	$errorhandle -> setException($e); 
}

if($errorhandle -> isException()){
	$result['result'] = false;
    $result['data'] = $errorhandle -> getErrorMessage();
	echo json_encode($result);
}

/*****END PHP*****/
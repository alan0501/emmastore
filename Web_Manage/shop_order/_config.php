<?php
$inc_path = "../../inc/";
$manage_path = "../";
$authkey = 'shop'; 
$logkey = 'shop_order';
include_once('../_config.php');
$root_path = '../../';

coderAdmin::vaild($authkey);
$auth = coderAdmin::Auth($authkey);

$table = coderDBConf::$order;
$colname = coderDBConf::$col_order;

$table_shop = coderDBConf::$admin;
$colname_shop = coderDBConf::$col_admin;

$idColumn = $colname['id'];
$orderColumn = $colname['id'];
$orderDesc = "desc";

$filter_shop_id = get($colname['shop_id']);

$shop_ary = class_shop::getList();


$page_name = '訂單';
$page_title = $auth['name'].' - '.$page_name;
$page_desc = "{$page_title}管理-您可以進行列表檢視/查詢,或使用修改來改變訂單的狀態。";
$mtitle = '<li class="active">'.$page_title.'</li>';
$mainicon = $auth['icon'];
$page = request_pag("page");


function getOrderState($state){
	$txt = coderOrder::$order_state[$state];
	$labclass = ' label ';
	if($state < 4){
		$labclass .= 'label-info';
	}else if($state == 4){
		$labclass .= 'label-success';
	}else if($state == 10){
		$labclass .= 'label-warning';
	}else if($state == 11){
		$labclass .= 'label-gray';
	}
	return '<span class="'.$labclass.'">'.$txt.'</span>';
}

function getPayState($state){
	$txt = coderOrder::$payment_state[$state];
	$labclass = ' label ';	
	if($state == 1){
		$labclass .= 'label-important';
	}
	return '<span class="'.$labclass.'">'.$txt.'</span>';
}

/*****END PHP*****/
<?php
include_once($inc_path."lib/codermember.php");
//搜尋欄位
$filterhelp = new coderFilterHelp();

$ary = array();
$ary[] = array('column' => $colname['title'], 'name' => '活動名稱');
$ary[] = array('column' => $colname['comment'], 'name' => '活動說明');
$ary[] = array('column' => $colname['admin'], 'name' => '管理員');

$obj = array();
$obj[] = array('type' => 'keyword', 'name' => '關鍵字', 'sql' => true, 'ary' => $ary);


/*$obj[]=array('type'=>'select','name'=>'活動範圍','column'=>'s_scope','sql'=>true,
'ary'=>coderHelp::makeAryKeyToAryElement(coderPromotion::$scope_type,'value','name')
);*/

$obj[] = array('type' => 'select', 'name' => '活動類型', 'column' => $colname['type'], 'sql' => true, 
'ary' => coderHelp::makeAryKeyToAryElement(coderPromotion::$promotion_type, 'value', 'name'));

/*$obj[]=array('type'=>'select','name'=>'活動方式','column'=>'s_type','sql'=>true,
'ary'=>coderHelp::makeAryKeyToAryElement(coderPromotion::$event_type,'value','name')
);*/

$obj[] = array('type' => 'select', 'name' => '會員限制', 'column' => $colname['member_type'], 'sql' => true, 
'ary' => coderHelp::makeAryKeyToAryElement(coderMember::$member_type, 'value', 'name'));

/*$obj[]=array('type'=>'select','name'=>'VIP限定','column'=>'s_m_is_vip','sql'=>true,
'ary'=>array(array('name'=>'是','value'=>'1'),array('name'=>'否','value'=>'0'))
);*/

$obj[] = array('type' => 'dategroup', 'sql' => true, 'column' => 'dategroup', 
'ary' => array(array('name' => '開始日期', 'column' => $colname['sdate']), array('name' => '結束日期', 'column' => $colname['edate']), array('name' => '最後修改日期', 'column' => $colname['updatetime'])));


$filterhelp -> Bind($obj);

/*****END PHP*****/
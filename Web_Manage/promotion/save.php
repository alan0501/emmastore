<?php
include_once('_config.php');
include_once('formconfig.php');


$errorhandle = new coderErrorHandle();
try{  
  if(post($colname['id']) > 0){
	  $method = 'edit';
	  $active = '編輯';
  }
  else{
	  $method = 'add';
	  $active = '新增';
  }
  
  $data = $fhelp -> getSendData();
  $error = $fhelp -> vaild($data);
  
  if(count($error)>0){
	 $msg = implode('\r\n', $error);
	  throw new Exception($msg);
  }
 
  /*if(isset($data['s_m_type'])){
  	$_auth = array_sum(explode(',',$data['s_m_type']));
  	$data['s_m_type']=$_auth;
  }

  if($data['s_type']==5){
  	 $data['s_val']=post('gift_val');
  }
  else{
  	$data['s_val']=post('event_val');
  }*/
  //全館,首購和生日優惠不需要選優惠館別
  /*if($data['s_scope']==1 || $data['s_rule']>10){
  	$data['s_prod_cid']='';
  }*/
  $data[$colname['admin']] = $adminuser['name'];
  $data[$colname['updatetime']] = datetime();
  
  $db = new Database($HS, $ID, $PW, $DB);
  $db -> connect();
  if($method == 'edit'){
	  $id = post($colname['id']);
	  $db -> query_update($table, $data, " {$colname['id']}={$id}");			
  }else{
	  //$data['ind']=coderListOrderHelp::getMaxInd($table,'ind');
	  $id = $db -> query_insert($table, $data);		  
  }
  coderAdminLog::insert($adminuser['name'], $logkey, $method, $data[$colname['title']]);
  echo showParentSaveNote($page_title, $active, $data[$colname['title']], "manage.php?id=".$id);
  
  $db -> close();
  coderPromotion::clearCache();
  
}catch(Exception $e){
	$errorhandle -> setException($e); // 收集例外
}

if($errorhandle -> isException()){
    $errorhandle -> showError();
}

/*****END PHP*****/
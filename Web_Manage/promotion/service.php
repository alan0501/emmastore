<?php
include_once('_config.php');
include_once('filterconfig.php');
$errorhandle = new coderErrorHandle();
try{  
	$db = new Database($HS, $ID, $PW, $DB);
	$db -> connect();
	
	$sHelp = new coderSelectHelp($db);
	$sHelp -> select = "*";
	$sHelp -> table = $table;
	$sHelp -> page_size = get("pagenum");
	$sHelp -> page = get("page");
	$sHelp -> orderby = get("orderkey", 1);
	$sHelp -> orderdesc = get("orderdesc", 1);
	
	$sqlstr = $filterhelp -> getSQLStr();
	$sHelp -> where = $sqlstr -> SQL;	

	$rows = $sHelp -> getList();
	
	//$row_newstype = QAType::getList();
	
	for($i = 0; $i < count($rows); $i++){
		$rows[$i][$colname['updatetime']] = coderHelp::getDateTime($rows[$i][$colname['updatetime']]);
		$rows[$i]['s_time'] = coderHelp::getDateInfo($rows[$i][$colname['sdate']], $rows[$i][$colname['edate']]);		
		//$rows[$i]['s_content'] = coderPromotion::getEventContentTxt($rows[$i]);
	}
	
	$result['result'] = true;
	$result['data'] = $rows;
	$result['page'] = $sHelp -> page_info;
	echo json_encode($result);
}
catch(Exception $e){
	$errorhandle -> setException($e); 
}

if ($errorhandle -> isException()) {
	$result['result'] = false;
    $result['data'] = $errorhandle -> getErrorMessage();
	echo json_encode($result);
}

/*****END PHP*****/
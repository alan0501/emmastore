<!--basic scripts-->
<script src="../assets/jquery/jquery-2.0.3.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.js"></script>
<script src="../assets/nicescroll/jquery.nicescroll.js"></script>
<script src="../assets/jquery-cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../assets/gritter/js/jquery.gritter.js"></script>
<!--page specific plugin scripts-->

<!--flaty scripts-->
<script src="../js/flaty.js"></script>
<!--colorbox scripts-->
<script type="text/javascript" src="../assets/colorbox/jquery.colorbox.js"></script>

<script src="../js/public.js"></script>
         

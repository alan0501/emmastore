<?php
include_once('_config.php');
include_once('filterconfig.php');

$errorhandle = new coderErrorHandle();
try{  
	$db = new Database($HS, $ID, $PW, $DB);
	$db -> connect();
	
	$sHelp = new coderSelectHelp($db);
	$sHelp -> select = "*";
	$sHelp -> table = "$table";
	$sHelp -> page_size = get("pagenum");
	$sHelp -> page = get("page");
	$sHelp -> orderby = get("orderkey", 1);
	$sHelp -> orderdesc = get("orderdesc", 1);
	
	$sqlstr = $filterhelp -> getSQLStr();
	$where = $sqlstr -> SQL;
	
	$sHelp -> where = $where;
	//var_dump($where);

	$rows = $sHelp -> getList();
		
	for($i = 0; $i < count($rows); $i ++){
		$rows[$i]['o_state'] = getOrderState($rows[$i][$colname['order_state']]);
		$rows[$i]['o_pay_state'] = getPayState($rows[$i][$colname['payment_state']]);
		$rows[$i]['shop_name'] = class_shop::getName($rows[$i][$colname['shop_id']]);
		$rows[$i]['o_updatetime'] = coderHelp::getDateTime($rows[$i][$colname['update_time']]);
		$rows[$i]['pay_price'] = $rows[$i][$colname['total_price']]+$rows[$i][$colname['freight']];
	}
	
	$result['result'] = true;
	$result['data'] = $rows;
	$result['page'] = $sHelp -> page_info;
	echo json_encode($result);
	
}catch(Exception $e){
	$errorhandle -> setException($e); 
}

if($errorhandle -> isException()){
	$result['result'] = false;
    $result['data'] = $errorhandle -> getErrorMessage();
	echo json_encode($result);
}

/*****END PHP*****/
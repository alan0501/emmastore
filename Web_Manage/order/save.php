<?php
include_once('_config.php');
include_once('formconfig.php');

//$template_path = $root_path . $template_path;
include_once($inc_path.'_func_smtp.php');

$errorhandle = new coderErrorHandle();
try{
    if(post($colname['sno'], 1) != ''){
        $method = 'edit';
        $active = '編輯';
    }else{
        $method = 'add';
        $active = '新增';
    }

    $data = $fhelp -> getSendData();
    $error = $fhelp -> vaild($data);

    if(count($error) > 0){
        $msg = implode('\r\n', $error);
        throw new Exception($msg);
    }
  
    $data[$colname['manager']] = $adminuser['username'];
    $data[$colname["update_time"]] = datetime();

    //var_dump($data);exit;
 
    $db = new Database($HS, $ID, $PW, $DB);
    $db -> connect();
    if($method == 'edit'){
        $no = post($colname['sno'], 1);
        $id = post($colname['id']);
        //var_dump($id);exit;
        
        $express_sno = post($colname['express_sno'], 1);
        $data2[$colname["express_sno"]] = $express_sno;

        $ismail = post('submit', 1) == 'mail' ? true : false;

        $order = new coderOrder();
        $order -> changeState($no, $data[$colname["order_state"]], $data[$colname["payment_state"]], null, $data[$colname["comment"]], $ismail);
        if($express_sno != ""){
            $db -> query_update(coderDBConf::$order, $data2, "order_sno='$no'"); 
        }
        
        $db -> query_update(coderDBConf::$order, $data, "order_id='$id'");
    }else{}
    
    coderAdminLog::insert($adminuser['username'], $logkey, $method, $no);
    echo showParentSaveNote($page_title, $active, $no, "manage.php?no=".$no);
    $db -> close();
	 
}catch(Exception $e){
	$errorhandle -> setException($e); 
}

if($errorhandle -> isException()){
    $errorhandle -> showError();
}

/*****END PHP*****/
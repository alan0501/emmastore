<?php
include_once("_config.php");
include_once('filterconfig.php');
$errorhandle = new coderErrorHandle();
try{
	$method='export';
	$active='匯出';
	
	include('getservicedata.php');

	//繪製excel  start ------------------------------------
	$cd_kn = ['A','B','C','D','E','F','G','H','I','J'];

	//使用phpexcel匯出excel檔
	ob_end_clean();
	require_once('../../inc/PHPExcelClasses/PHPExcel.php');
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->setActiveSheetIndex(0);//指定目前要編輯的工作表 0表示第一個

	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->setTitle('order');

	//設定寬度
	$cd_ki = 0;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(10);$cd_ki++;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(17);$cd_ki++;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(30);$cd_ki++;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(10);$cd_ki++;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(17);$cd_ki++;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(30);$cd_ki++;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(60);$cd_ki++;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(10);$cd_ki++;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(10);$cd_ki++;
	$sheet->getColumnDimension($cd_kn[$cd_ki])->setWidth(50);$cd_ki++;

	//設定合併儲存格
	$sheet->mergeCells('A1:C1');
	$sheet->mergeCells('D1:F1');
	$sheet->mergeCells('G1:G2');
	$sheet->mergeCells('H1:H2');
	$sheet->mergeCells('I1:I2');
	$sheet->mergeCells('J1:J2');

	//設定置中
	$sheet->getStyle('A1:J2')->getAlignment()->applyFromArray(
		array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);
	$sheet->getStyle('A1:J2')->getAlignment()->applyFromArray(
		array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
	);
	$sheet->getStyle('H3:I3')->getAlignment()->applyFromArray(
		array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,)
	);

	//設定背景顏色
	$sheet->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FCF3CF');
	$sheet->getStyle('A2:F2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FEF9E7');

	//設定第一行標題
	$cd_ki = 0;
	$index = 1;
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'訂購人資訊');$cd_ki+=3;
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'收件人資訊');$cd_ki+=3;
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'商品名稱');$cd_ki++;
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'數量');$cd_ki++;
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'總金額');$cd_ki++;
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'付款方式');$cd_ki++;

	//設定第二行標題
	$cd_ki = 0;
	$index = 2;
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'姓名');$cd_ki++;	
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'電話');$cd_ki++;	
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'地址');$cd_ki++;
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'姓名');$cd_ki++;	
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'電話');$cd_ki++;	
	$sheet->setCellValue($cd_kn[$cd_ki].$index,'地址');$cd_ki++;
	$sheet->getStyle('A1:J2')->getFont()->setBold( true );

	$styleArray = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	$sheet->getStyle('A1:J2')->applyFromArray($styleArray);


	//下方內容
	if($rows){
		$index = 3;

		foreach ($rows as $row) {
			$cd_ki = 0;
			$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,$row['order_purchaser_name'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;
			$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,$row['member_phone'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;
			$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,$row['member_address'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;

			$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,$row['order_recipient_name'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;
			$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,$row['order_recipient_phone'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;
			$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,$row['order_recipient_address'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;

			$order_detail_ary = coderOrder::getDetails($row["order_sno"]);
			if(isset($order_detail_ary[0])){
				$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,$order_detail_ary[0]['product_name'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;
				$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,$order_detail_ary[0]['amount'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;
			}
			
			$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,($row["order_total_price"]+$row["order_freight"]),PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;
			$sheet->setCellValueExplicit($cd_kn[$cd_ki].$index,$row['order_payment_type'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;

			$sheet->getStyle('H'.$index.':I'.$index)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,)
			);
			$sheet->getStyle('A'.$index.':J'.$index)->applyFromArray($styleArray);

			for($i=1; $i<count($order_detail_ary); $i++){
				++$index;
				$sheet->setCellValueExplicit($cd_kn[6].$index,$order_detail_ary[$i]['product_name'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;
				$sheet->setCellValueExplicit($cd_kn[7].$index,$order_detail_ary[$i]['amount'],PHPExcel_Cell_DataType::TYPE_STRING);$cd_ki++;
				$sheet->getStyle('H'.$index)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,)
				);

				$sheet->getStyle('A'.$index.':J'.$index)->applyFromArray($styleArray);
			}

			$index++;
		}
	}
}catch(Exception $e){
	$db->close();
	$errorhandle->setException($e);
}
if ($errorhandle->isException()) {
	$errorhandle->showError();exit;
}


$objPHPExcel->setActiveSheetIndex(0);
while (@ob_end_clean());
//下載
$_file = "order_".date('Y.m.d').'.xlsx';
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type:application/force-download');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$_file);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save("php://output");exit;
?>
//***cln used {
document.oncontextmenu = new Function("return false");
$(document).ready(function(){
	//登出
	$("#mlogout").click(function(e) {
        var parent = this,
		success = false;
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { actiontype : "memberLogout" },
			dataType  :"json",
			success : function(data){
				if(data){					
					if(data.result == true){
						success = true;
						location.reload();
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			}
			,error : function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});	
		return success;
    });
}); 
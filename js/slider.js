var SellerScroll = function(options){
	this.SetOptions(options);
	this.lButton = this.options.lButton;
	this.rButton = this.options.rButton;
	this.oList = this.options.oList;
	this.showSum = this.options.showSum;
	this.showPage = this.options.showPage;//當前頁
	this.itemCount = this.options.itemCount;//顯示格數
	var _this=this;
	_this.relwidth=$("#banner").width();
	
	this.iList = $("#" + this.options.oList + " > li");
	this.iListSum = this.iList.length;
	this.iListWidth = _this.relwidth;//this.iList.outerWidth(true);
	this.moveWidth = this.iListWidth * this.showSum;
	this.defaultMoveWidth = this.iListWidth * parseInt(this.showPage-this.itemCount/2);
	
	this.dividers = Math.ceil(this.iListSum / this.showSum);	//共分为多少块
	this.moveMaxOffset = (this.dividers - 1) * this.moveWidth;
	this.ListMaxOffset=(this.itemCount-1)*this.iListWidth;
	this.currentPage=0;
	this.transition_interval=5000;

	this.pause_on_hover=true;
	this.LeftScroll();
	this.RightScroll();

	_this.iList.css("width",_this.relwidth);
	
		 $(window).resize(function(){
			_this.relwidth=$("#banner").width();
	       _this.iList.css("width",_this.relwidth);
		   _this.resetList();
		 });
 
};
SellerScroll.prototype = {
	SetOptions: function(options){
		this.options = options;
		$.extend(this.options, options || {});
	},
	resetList: function(lwidth){
		var _this = this
		_this.iListWidth = _this.relwidth;
		_this.moveWidth = _this.iListWidth * _this.showSum;
		 _this.moveMaxOffset = (_this.dividers - 1) * _this.moveWidth;
	    _this.ListMaxOffset=(_this.itemCount-1)*_this.iListWidth;
		_this.defaultMoveWidth = _this.iListWidth * parseInt(_this.showPage-_this.itemCount/2);
		_this.currentPage=0;
		$("#" + _this.oList ).css("left","0");
	},
	ReturnLeft: function(){
		return isNaN(parseInt($("#" + this.oList).css("left"))) ? 0 : parseInt($("#" + this.oList).css("left"));
	},
	LeftScroll: function(){
		if(this.dividers == 1) return;
		var _this = this, currentOffset;
		$("#tab li").click(function(){
			_this.currentPage=$(this).index();
				$("#" + _this.oList + ":not(:animated)").animate( { left:- _this.moveWidth*(_this.currentPage) }, 500 ,function(){
				 $("#tab li").removeClass("selected");
				 $("#tab li").eq(_this.currentPage).addClass("selected");					  
																												  });
		});
		if (_this.transition_interval > 0) {
			var timer = setInterval(function () {
				$("#right_scroll").click();
			}, _this.transition_interval);

			if (_this.pause_on_hover) {
				_this.iList.mouseenter(function() {
					clearInterval( timer );
				}).mouseleave(function() {
					clearInterval( timer );
					timer = setInterval(function () {
						$("#right_scroll").click();
					}, _this.transition_interval);
				});
			}
		}
		if($.support.leadingWhitespace){
		var startX_1 = 0;
		var n=0;
		var _this=this;
		$("#slideshow_ul")[0].addEventListener('touchmove', function(e){
			var touch = e.touches[0];			
			var cX = touch.pageX;
			if(cX < startX_1-10){
			  e.preventDefault();
			   n=1;
			}
			else if(cX > startX_1+10){
			  e.preventDefault();
			    n=2;
			}
		});
		$("#slideshow_ul")[0].addEventListener('touchstart', function(e){
			//e.preventDefault();
			var touch = e.touches[0];
			startX_1 = touch.pageX;
		});
		$("#slideshow_ul")[0].addEventListener('touchend', function(e){
			//e.preventDefault();
			if(n==1){
			   n++;
			 $("#right_scroll").click();
			}else if(n==2){
			 $("#left_scroll").click();
			}
			n=0;
		});
		}
		$("#" + this.lButton).click(function(){
			currentOffset = _this.ReturnLeft();
			if(currentOffset == 0){
/*			for(var i = 1; i <= _this.showSum; i++){
					$(_this.iList[_this.iListSum - i]).prependTo($("#" + _this.oList));
				}
				$("#" + _this.oList).css({ left: -_this.moveWidth });
				$("#" + _this.oList + ":not(:animated)").animate( { left: "+=" + _this.moveWidth }, { duration: "slow", complete: function(){
					for(var j = _this.showSum + 1; j <= _this.iListSum; j++){
						$(_this.iList[_this.iListSum - j]).prependTo($("#" + _this.oList));
					}
					$("#" + _this.oList).css({ left: -_this.moveWidth * (_this.dividers - 1) });
				} } );*/
				$("#" + _this.oList + ":not(:animated)").animate( { left:- _this.moveWidth*(_this.iListSum-1) }, 200 ,function(){
				 _this.currentPage=_this.iListSum-1;
				 $("#tab li").removeClass("selected");
				 $("#tab li").eq(_this.currentPage).addClass("selected");					  
																												  });
			}else{
				$("#" + _this.oList + ":not(:animated)").animate( { left: "+=" + _this.moveWidth }, 500,function(){
				 _this.currentPage--;
				 $("#tab li").removeClass("selected");
				 $("#tab li").eq(_this.currentPage).addClass("selected");
																												 });

			}

		});
	},
	RightScroll: function(){
		
		if(this.dividers == 1) return;
		var _this = this, currentOffset;
		$("#" + this.rButton).click(function(){
			currentOffset = _this.ReturnLeft();
			
			if(Math.abs(currentOffset) >= _this.moveMaxOffset-_this.ListMaxOffset){
/*			   for(var i = 0; i < _this.showSum; i++){
					$(_this.iList[i]).appendTo($("#" + _this.oList));
				}
				$("#" + _this.oList).css({ left: -_this.moveWidth * (_this.dividers - 2) });
				
				$("#" + _this.oList + ":not(:animated)").animate( { left: "-=" + _this.moveWidth }, { duration: "slow", complete: function(){
					for(var j = _this.showSum; j < _this.iListSum; j++){
						$(_this.iList[j]).appendTo($("#" + _this.oList));
					}
					
				} } );*/
				//$("#" + _this.oList).css({ left: 0 });
				$("#" + _this.oList + ":not(:animated)").animate( { left: 0 }, 200 ,function(){
				 _this.currentPage=0;
				 $("#tab li").removeClass("selected");
				 $("#tab li").eq(_this.currentPage).addClass("selected");					  
																												  });
			}else{
				$("#" + _this.oList + ":not(:animated)").animate( { left: "-=" + _this.moveWidth }, 500 ,function(){
				 _this.currentPage++;
				 $("#tab li").removeClass("selected");
				 $("#tab li").eq(_this.currentPage).addClass("selected");					  
																												  });

			}
		});
		_this.defaultMoveWidth=parseInt(_this.defaultMoveWidth)<=0?0:parseInt(_this.defaultMoveWidth);
		if(_this.defaultMoveWidth >= _this.moveMaxOffset-_this.ListMaxOffset){
			_this.defaultMoveWidth=_this.moveMaxOffset-_this.ListMaxOffset;
		}
		//$("#" + _this.oList + ":not(:animated)").animate( { left: "-=" + _this.defaultMoveWidth}, "slow" );
		$("#" + _this.oList ).css("left","-" + _this.defaultMoveWidth +"px");
		//$("#a").html(_this.moveMaxOffset);
	}
};


$(function(){
    $("#form_contact").validate({
        //debug:true,
        onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
            "name":"required",
            "appname":"required",
            "phone":"phonecheck required",
            "email":{
                required:true,
                email:true
            },
            "content":"required"
        },
        messages:{
            "name":"請輸入姓名",
            "appname":"請輸入稱呼",
            "phone":"請輸入正確的Phone",
            "email":{
				required: "請輸入email",
				email: "email格式錯誤"
			},
            "content":"請輸入意見"
        }
    });
});
jQuery.validator.addMethod("phonecheck",
    function(cellphone, element) {
        cellphone = cellphone.replace(/\s+/g, "");
        return(this.optional(element) || /[1-9]/.test(cellphone));
    },
"請輸入正確手機號碼");

// 填寫完聯絡我們表單 ------------------------------------------------------------------------------------------------
function finishContactForm(){
	
  var contact_submit_msg='表單送出中';

  var options = { 
        url:'do/contact_save.php',
        type:"post",
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_contact").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.state === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_contact :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                alert("送出成功"); 
                location.reload();
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_contact').ajaxSubmit(options);
}
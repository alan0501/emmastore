function clock(targettime)
{
    var DifferenceDay = -1,
        DifferenceHour = -1,
        DifferenceMinute = -1,
        DifferenceSecs = 0,
        DifferenceMSecs = 0;

    var Tday = new Date(targettime) ;//設定目標日期
    var daysms = 24 * 60 * 60 * 1000,
        hoursms = 60 * 60 * 1000,
        Minutems = 60 * 1000,
        Secondms = 1000,
        MSecondms = 1000/1;

    var time = new Date();
    var hour = time.getHours();
    var minute = time.getMinutes();
    var second = time.getSeconds();
    var timevalue = ""+((hour > 12) ? hour-12:hour);
    timevalue +=((minute < 10) ? ":0":":")+minute;
    timevalue +=((second < 10) ? ":0":":")+second;
    timevalue +=((hour >12 ) ? " PM":" AM");

    // document.formnow.now.value = timevalue
    var convertDay = DifferenceDay;
    var convertHour = DifferenceHour;
    var convertMinute = DifferenceMinute;
    var Diffms = Tday.getTime() - time.getTime();
    if(Diffms>0){
        DifferenceDay = Math.floor(Diffms / daysms);
        Diffms -= DifferenceDay * daysms;
        DifferenceHour = Math.floor(Diffms / hoursms);
        Diffms -= DifferenceHour * hoursms;
        DifferenceMinute = Math.floor(Diffms / Minutems);
        Diffms -= DifferenceMinute * Minutems;
        DifferenceSecs = Math.floor(Diffms / Secondms);
        Diffms -= DifferenceSecs * Secondms;

        DifferenceMSecs = Math.floor(Diffms / MSecondms);
        if(convertDay != DifferenceDay) $("#dd").text(DifferenceDay);
        if(convertHour != DifferenceHour) $("#hh").text(DifferenceHour);
        if(convertMinute != DifferenceMinute) $("#mm").text(DifferenceMinute);
        $("#ss").text(DifferenceSecs);
        if(DifferenceMSecs != 0)$("#mss").text(DifferenceMSecs);
        setTimeout("clock('"+targettime+"')",MSecondms);
    }
}
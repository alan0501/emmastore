$(document).ready(function(){
	
	function calendar_text(){
		//清涼音日程
		$("td").on("mouseenter",".calendar_day", function(e) {
			  $(this).find(".calendar_text").show();
		}).on("mouseleave",".calendar_day", function(e) {
			  $(this).find(".calendar_text").hide();
		});
	};
	calendar_text();
	
	//上一頁按鈕
	$("div#calendar").on("click",".pre",(function() {	
		var year = $(".pre").attr("year");
		var month = $(".pre").attr("month");
		change_calendar(year, month);		
	}));
	
	//下一頁按鈕
	$("div#calendar").on("click",".next",(function() {	
		var year = $(".next").attr("year");
		var month = $(".next").attr("month");
		change_calendar(year, month);		
	}));
	function change_calendar(year, month){
		$.ajax({
			url : "do/change_calendar.php",
			type : "GET",
			data: { 
				year : year,
				month : month,

			},
			success : function(data){				 
				//console.log(data);		
				$("div#calendar").html(data);
				//alert(response.list);	
				
				calendar_text();	
			},
			error : function(){
				alert('資料傳送錯誤!請在試一次!');
			}
		});
	};
	
	
	
});	
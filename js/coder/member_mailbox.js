//驗證碼
function changeCaptcha(path){
	var _path = path ? '../' : '';
	$('#captcha').attr('src', _path + 'img.php?time='+ new Date().getTime());
}

$(function(){
    $("#form_mailbox").validate({
        //debug:true,
        onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
            "name":"required",
			"telphone":{
				required: true,
				telphone: true
			},
            "email":{
                required:true,
                email:true
            },
			"content":"required",
			"verify_code":"required"        
			
        },
        messages:{
            "name":"請輸入姓名",
			"telphone":{
				required: " 請輸入電話號碼",
				telphone: " 請輸入XX-XXXXXXXX"
			},
            "email":{
				required: " 請輸入email",
				email: " email格式錯誤"
			},
			"content":"請輸入您的意見",
			"verify_code":"請輸入驗證碼"      
			
        }

    });
});
function reset_mailbox(){
	$("#form_mailbox")[0].reset();
	};
	
jQuery.validator.addMethod("telphone",
          function(cellphone, element) {

              cellphone = cellphone.replace(/\s+/g, "");

              return(
                  this.optional(element) || /^([0-9]{2}-[0-9]{6,8})|([0-9]{3}-[0-9]{6,8})$/.test(cellphone));

      });

// 填寫完會員登入表單 ------------------------------------------------------------------------------------------------
function finishContactForm(){
	
  var contact_submit_msg='表單送出中';
  var options = { 
        url:'do/memberdo.php',
        type:"post",
		data : { actiontype : "mailbox" },
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_mailbox").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.result === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_mailbox :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                alert("您的意見已送出成功，在此感謝您的寶貴意見"); 
                //location.reload();
				//window.location.href='index.html';
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_mailbox').ajaxSubmit(options);
}
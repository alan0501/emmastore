$(document).ready(function(){
	//會員取消訂單
	$("tr").on('click', '.order_cancel_btn', function() {
		$(this).hide();
		var sno = $(this).parents("tr.tbdoy").attr("sno");
		var state = $(this).attr("state");	
			
		var success = false;
		$.ajax({
			url: 'do/cancel_order.php',
			type: 'POST',
			data : { 
				order_sno : sno,
				state : state
				},
			dataType: "json",
			async:false,
			success: function(data){
				if(data){					
					alert("訂單已取消!");
					location.reload();
				}else{
					alert("資料傳送錯誤!請稍候再試一次!");
				}
				success = data.sResult;			
			},
			error: function(){
				alert('發生錯誤');
			}
		});		
		return success;
	});
}); 
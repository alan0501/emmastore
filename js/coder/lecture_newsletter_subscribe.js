$(function(){
	//取消訂閱
    $("#form_news_cancel").validate({
        //debug:true,
        onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
            "email_cancel":{
                required:true,
                email:true
            }
            
			
        },
        messages:{
            "email_cancel":{
				required: "請輸入email",
				email: "email格式錯誤"
			}    
			
        }

    });
	
	//訂閱
	$("#form_news").validate({
        //debug:true,
        onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
            "email":{
                required:true,
                email:true
            }
            			
        },
        messages:{
            "email":{
				required: "請輸入email",
				email: "email格式錯誤"
			}    
			
        }

    });
});


// 填寫完忘記密碼表單 ------------------------------------------------------------------------------------------------
//取消訂閱
function finishContactForm_cancel(){
	
  var contact_submit_msg='表單送出中';
  var options = { 
        url:'do/lecture_newsdo.php',
        type:"post",
		data : { actiontype : "newscancel"},
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_news_cancel").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.result === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_news_cancel :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                alert("取消訂閱已成功!!"); 
                location.reload();
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_news_cancel').ajaxSubmit(options);
}

//訂閱
function finishContactForm(){
	
  var contact_submit_msg='表單送出中';
  var options = { 
        url:'do/lecture_newsdo.php',
        type:"post",
		data : { actiontype : "news"},
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_news").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.result === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_news :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                alert("訂閱已成功!!"); 
                location.reload();
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_news').ajaxSubmit(options);
}
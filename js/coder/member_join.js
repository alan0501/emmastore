//驗證碼
function changeCaptcha(path){
	var _path = path ? '../' : '';
	$('#captcha').attr('src', _path + 'img.php?time='+ new Date().getTime());
}
//檢查帳號是否重覆
function checkAcc(acc){ 
	var parent = this,
	success = false;  
	$.ajax({
		url : "do/memberdo.php",
		async : false,
		type : "POST",
		data : { acc : acc, actiontype : "chkAcc" },
		dataType : "json",
		success : function(data){
			if(data){
				if(data.result == true){
					success = true;
				}else{
					parent.message = data.msg;
				}
			}else{
				parent.message = "回傳資料錯誤!";
			}
		},
		error:function(xhr, ajaxOptions, thrownError){
			parent.message = "讀取資料時發生錯誤,請梢候再試!"+thrownError;
		}
	});	
	return success;	 
}
$(function(){
	jQuery.validator.addMethod("chkAcc", function(value, element){
		var acc = $('#acc').val();
		return this.optional(element) || checkAcc(acc);
	}, "會員帳號重覆,請重新輸入");
    $("#form_join").validate({
        //debug:true,
        //onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
            "acc":{ 
				required: true,
                email:true, 
				chkAcc : true
			},			
            "pwd": { 
				required: true, 
				rangelength: [6, 8]
			},
			"c_pwd":{ 
				required: true, 
				equalTo: "#pwd" 
			},
			"name":"required",
			"gender": "required",
			"ymd_year":"required",
			"ymd_month":"required",
			"ymd_day":"required",
			"verify_code":"required",
			/*"telphone":{
				required: true,
				telphone: true
			},*/
            "phone":{
				required: true,
				phonecheck: true
			},
            /*"email":{
                required:true,
                email:true
            },
			"addr":"required",
			"sub":"required",*/
			"agree":"required"
            
			
        },
        messages:{
            "acc":{ 
				required : " 請輸入會員帳號<br>",
				email: " email格式錯誤<br>"
			},
            "pwd":{ 
				required: " 請輸入會員密碼", 
				rangelength: " 長度必須介於{0}和{1}之間的字符"
			},
			"c_pwd":{ 
				required: " 請輸入確認密碼", 
				equalTo: " 兩次輸入密碼不一致不一致" 
			},
			"name":" 請輸入真實姓名<br>",
			"gender": " 請輸入性別",
			"ymd_year":" 請輸入年",
			"ymd_month":" 請輸入月",
			"ymd_day":" 請輸入日",
			"verify_code":" 請輸入驗證碼",
			/*"telphone":{
				required: " 請輸入電話號碼",
				telphone: " 請輸入XX-XXXXXXXX"
			},*/
            "phone":{
				required: " 請輸入手機號碼",
				phonecheck: " 請輸入正確手機號碼"
			},
            /*"email":{
				required: " 請輸入email",
				email: " email格式錯誤"
			},*/
			/*"addr":" 請輸入通訊地址", 
			"sub":" 請勾選訂閱",*/
			"agree":" 請勾選同意會員權益"        
			
        }

    });
});

function reset_join(){
	$("#form_join")[0].reset();
	};
jQuery.validator.addMethod("phonecheck",
    function(cellphone, element) {
        cellphone = cellphone.replace(/\s+/g, "");
        return(this.optional(element) || /^09[0-9]{8}/.test(cellphone));
    });

jQuery.validator.addMethod("telphone",
          function(cellphone, element) {

              cellphone = cellphone.replace(/\s+/g, "");

              return(
                  this.optional(element) || /^([0-9]{2}-[0-9]{6,8})|([0-9]{3}-[0-9]{6,8})$/.test(cellphone));

      });

// 填寫完加入會員表單 ------------------------------------------------------------------------------------------------
function finishContactForm(){
	
  var contact_submit_msg='表單送出中';
  var acc = $('#acc').val();
  var options = { 
        url:'do/memberdo.php',
        type:"post",
		data : { acc : acc, actiontype : "insert" },
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_join").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.result === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_join :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                alert("加入會員成功"); 
                //location.reload();
				window.location.href='member.html';
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_join').ajaxSubmit(options);
}
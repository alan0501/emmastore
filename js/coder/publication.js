function get_teacherlist(page){
    page = page || 1;
    $.ajax({
        url: 'do/getTeacherList.php',
        data: {page:page},
        type:"GET",
        dataType:'json',

        success: function(data){
            if(data.result == true){
                draw_teacherList(data.data,data.page);
            }else{
                alert(data.msg);
            }
        },

         error:function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
            alert(thrownError);
         }
    });
}
function draw_teacherList(data,page){
    var list = '';
    for(var i=0,num = data.length; i<num;i++){
        list += '<li><a href="publication_teachers_info_'+data[i]['id']+'.html">'+data[i]['name']+data[i]['position']+'</a></li>';
    }
    $(".teacher_list ul#teacherlist_box").html(list);
    $(".teacher_list .pager ul").html(page);
}

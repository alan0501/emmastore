//驗證碼
function changeCaptcha(path){
	var _path = path ? '../' : '';
	$('#captcha').attr('src', _path + 'img.php?time='+ new Date().getTime());
}

$(function(){
    $("#form_forget").validate({
        //debug:true,
        onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
            "acc":{
                required:true,
                email:true
            },
            /*"email":{
                required:true,
                email:true
            }*/
            
			
        },
        messages:{
            "acc":{
				required: "請輸入會員帳號",
				email: "email格式錯誤"
			},
            /*"email":{
				required: "請輸入email",
				email: "email格式錯誤"
			}   */ 
			
        }

    });
});

function reset_join(){
	$("#form_forget")[0].reset();
	};
jQuery.validator.addMethod("phonecheck",
    function(cellphone, element) {
        cellphone = cellphone.replace(/\s+/g, "");
        return(this.optional(element) || /[0][1-9]{3}[0-9]{6}/.test(cellphone));
    });

jQuery.validator.addMethod("telphone",
          function(cellphone, element) {

              cellphone = cellphone.replace(/\s+/g, "");

              return(
                  this.optional(element) || /[0-9]{2}\-[0-9]{7}/.test(cellphone));

      });

// 填寫完忘記密碼表單 ------------------------------------------------------------------------------------------------
function finishContactForm(){
	
  var contact_submit_msg='表單送出中';
  var acc = $('#acc').val();
  var email = $('#email').val();
  var options = { 
        url:'do/memberdo.php',
        type:"post",
		data : { acc : acc, email : email, actiontype : "memberforget" },
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_forget").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.result === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_forget :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                alert("您的新密碼已寄出，請至信箱確認!!"); 
                //location.reload();
				window.location.href='member.html';
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_forget').ajaxSubmit(options);
}
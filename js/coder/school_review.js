$(document).ready(function(){
	//進入網頁時先隱藏
	//$(".sidebar_list ul li").siblings("li").find("ul").hide().first().show();
	$(".sub_sidebar_title a").eq($("#type").val()).addClass("active");
	
	$(".sidebar_list ul li").siblings("li").find("ul").hide().eq($("#type").val()).show();
	
	$(".sidebar_list").find("li:has(ul)").children(".sub_sidebar_title").click(function(){
	  if($(this).next("ul").is(":hidden")){      
	   $(this).next("ul").slideDown("slow");
	   
	   $(this).parent("li").find("div a").addClass("active");
	   $(this).parent("li").siblings("li").find("div a").removeClass("active");
	   
	   if($(this).parent("li").siblings("li").children("ul").is(":visible")){
		
		$(this).parent("li").siblings("li").find("ul").slideUp("1000");
		$(this).parent("li").siblings("li:has(ul)").children("a").end().find("li:has(ul)").children("a");
	   }
			return false;
	  }else{		  	   
	   $(this).next("ul").slideUp("normal");  
	   $(this).next("ul").children("li").find("ul").fadeOut("normal");
	   $(this).next("ul").find("li:has(ul)").children("a");
	   return false;
	  }
	  
	 });
	 
	 
	 // hide all but the first of our paragraphs
	$('.some-container:not(:first)').hide();
	
	$('.pagination').jqPagination({
		max_page    : $('.some-container').length, 
		paged        : function(page) {
			
			// a new 'page' has been requested
			
			// hide all paragraphs
			$('.some-container').hide();
			
			// but show the one we want
			$($('.some-container')[page - 1]).show();
			
		}
	});
    
	 
});
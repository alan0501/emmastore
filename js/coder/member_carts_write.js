$(document).ready(function(){
	$("#checkbox").click(function() {	
		//同訂購人 checkbox
		if($("#checkbox").prop("checked"))
		{
			//打勾
			$("#namechk").val($("#name").val());
			$('#namechk-error').remove();
			$("#phonechk").val($("#phone").val());
			$('#phonechk-error').remove();
			$("#telphonechk").val($("#telphone").val());
			//$('#telphonechk-error').remove();
			$("#emailchk").val($("#email").val());
			$('#emailchk-error').remove();
			
			if($('#addr_old').val() != "")
			{
				$("#addr").val($("#addr_old").val());
			}
			else
			{
				var tempaddr = $("#zipcode").val()+$("#county").val()+$("#district").val()+$("#addr_tw").val()
				$("#addr").val(tempaddr);
			}
			$('#addr-error').remove();
			//$("#member_carts_write").valid();
		}
		else
		{
			$("#namechk").val("");
			$("#phonechk").val("");
			$("#telphonechk").val("");
			$("#emailchk").val("");
			$("#addr").val("");

		}
		
	});
	
});

$(document).ready(function() {
	//台灣地址
	$('#twzipcode').twzipcode({
		'zipcodeSel'  : zipcode,
		'countySel'   : county,
		'districtSel' : district
	});
	ckaddr();
	//訂郵遞區號 text 寬
	$('#zipcode').width(80);
	function ckaddr()
	{
		if($("input[name='addrtype']:checked").val() != '1')
		{
			$('#addr_old').val("");
			$('#addr_old').attr('disabled', true);
			$('#addr_tw').attr('disabled', false);
			$('#county').attr('disabled', false);
			$('#district').attr('disabled', false);
			$('#zipcode').attr('disabled', false);
			$('#addr_old').removeAttr('class');
			$('#addr_old-error').remove();
		}
		else
		{
			$('#addr_tw').val("");	
			$('#addr_tw').attr('disabled', true);
			$('#county').attr('disabled', true);
			$('#district').attr('disabled', true);
			$('#zipcode').attr('disabled', true);
			$('#addr_old').attr('disabled', false);
			
			$('#addr_tw').removeAttr('class');
			
			$('#addr_tw-error').remove();
			$('#county-error').remove();
			//重製台灣地址
			$('#twzipcode').twzipcode('reset');	
		}
	}
	
	//地址選擇按鈕
	$("input[name='addrtype']").click(function(){	
		ckaddr();				
	});
	
    $("#member_carts_write").validate({
        //debug:true,
        //onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
			"name":"required",
			/*"telphone":{
				required: true,
				telphone: true
			},
            "phone":{
				required: true,
				phonecheck: true
			},
            "email":{
                required:true,
                email:true
            },*/			
			"addr":"required",
			"namechk":"required",
			"phonechk":{
				required: true,
				phonecheck: true
			},			
			"telphonechk":{
				/*required: true,*/
				telphone: true
			},
            "emailchk":{
                required:true,
                email:true
            },
			"time":"required",
			"receipt":"required",
			"invoice_number": { 
				required: '#receipt:checked',
				invoice_number: true
			},
			"invoice_head": { 
				required: '#receipt:checked'
			},
			"addr_old": { 
				required: '#addrtype:checked',
			},
			"county": { 
				required: '#addrtype:checked',
			},
			"addr_tw": { 
				required: '#addrtype:checked',
			}
        },
        messages:{
			/*"name":" 請輸入收件人",
			"telphone":{
				required: " 請輸入電話號碼",
				telphone: " 請輸入XX-XXXXXXXX"
			},
            "phone":{
				required: " 請輸入手機號碼",
				phonecheck: " 請輸入正確手機號碼"
			},
            "email":{
				required: " 請輸入email",
				email: " email格式錯誤"
			},*/
			"addr":" 請輸入地址",
			
			"namechk":" 請輸入訂購人",
			"phonechk":{
				required: " 請輸入手機號碼",
				phonecheck: " 請輸入正確手機號碼"
			},
			"telphonechk":{
				/*required: " 請輸入電話號碼",*/
				telphone: " 請輸入XX-XXXXXXXX"
			},            
      		"emailchk":{
				required: " 請輸入email",
				email: " email格式錯誤"
			},
			"time":"請選擇收件時間",
			"receipt":"請選擇發票資訊",
			"invoice_number":{
				required: " 請輸入統一編號",
				invoice_number:" 請填寫正確的統一編號"
				
			},
			"invoice_head":" 請輸入發票抬頭",
			"addr_old":{
				required: " 請輸入通訊地址",			
			},
			"county": { 
				required: " 請輸入縣市",
			},
			"addr_tw": { 
				required: " 請輸入台灣地區地址",
			} 
        }

    });
	$(".btn_write").on('click',function() {
		if($("#member_carts_write").valid())
		{
			$(".btn_write").hide();
			orderSave();
			
		}
		
	});
});

//手機驗證
jQuery.validator.addMethod("phonecheck",
    function(cellphone, element) {
        cellphone = cellphone.replace(/\s+/g, "");
        return(this.optional(element) || /^09[0-9]{8}/.test(cellphone));
    });

//連絡電話驗證
jQuery.validator.addMethod("telphone",
          function(cellphone, element) {

              cellphone = cellphone.replace(/\s+/g, "");

              return(
                  this.optional(element) || /^([0-9]{2}-[0-9]{6,8})|([0-9]{3}-[0-9]{6,8})$/.test(cellphone));

      });				

//統一編號驗證
jQuery.validator.addMethod("invoice_number", 
		function(identifier, element) {
            var invalidList = "00000000,11111111";
            
            if(identifier) {
                  if (/^\d{8}$/.test(identifier) == false || invalidList.indexOf(identifier) != -1) {
                      return false;
                  }

                  var validateOperator = [1, 2, 1, 2, 1, 2, 4, 1],
                      sum = 0,
                      calculate = function(product) { // 個位數 + 十位數

                          var ones = product % 10,
                              tens = (product - ones) / 10;
                          return ones + tens;
                      };

                  for (var i = 0; i < validateOperator.length; i++) {
                      sum += calculate(identifier[i] * validateOperator[i]);
                  }

                  return sum % 10 == 0 || (identifier[6] == "7" && (sum + 1) % 10 == 0);
              }
              else {
                  return true;
              }
        });
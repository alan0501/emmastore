//驗證碼
function changeCaptcha(path){
	var _path = path ? '../' : '';
	$('#captcha').attr('src', _path + 'img.php?time='+ new Date().getTime());
}

$(function(){
    $("#form_member").validate({
        //debug:true,
        onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
            "acc":"required",
            "pwd": { 
				required: true 
			},
			"verify_code":"required"        
			
        },
        messages:{
            "acc":"請輸入會員帳號",
            "pwd":{ 
				required: "請輸入會員密碼" 
			},
			"verify_code":"請輸入驗證碼"      
			
        }

    });
});

// 填寫完會員登入表單 ------------------------------------------------------------------------------------------------
function fuu(){
	//alert("fuc...k");
  var contact_submit_msg='表單送出中';
  var acc = $('#acc').val();
  var pwd = $('#pwd').val();
  var options = { 
        url:'do/memberdo.php',
        type:"post",
		data : { acc : acc, pwd : pwd, actiontype : "memberLogin" },
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_member").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.result === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_member :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                //alert("會員登入成功"); 
                //location.reload();
				window.location.href='index.html';
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_member').ajaxSubmit(options);
}
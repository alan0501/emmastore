//驗證碼
function changeCaptcha(path){
	var _path = path ? '../' : '';
	$('#captcha').attr('src', _path + 'img.php?time='+ new Date().getTime());
}

$(document).ready(function(){
	//台灣地址
	$('#twzipcode').twzipcode({
		'zipcodeSel'  : zipcode,
		'countySel'   : county,
		'districtSel' : district
	});
	ckaddr();
	//訂郵遞區號 text 寬
	$('#zipcode').width(80);
	function ckaddr()
	{
		if($("input[name='addrtype']:checked").val() != '1')
		{
			$('#addr').val("");
			$('#addr').attr('disabled', true);
			$('#addr_tw').attr('disabled', false);
			$('#county').attr('disabled', false);
			$('#district').attr('disabled', false);
			$('#zipcode').attr('disabled', false);
			$('#addr').removeAttr('class');
			$('#addr-error').remove();
		}
		else
		{
			$('#addr_tw').val("");	
			$('#addr_tw').attr('disabled', true);
			$('#county').attr('disabled', true);
			$('#district').attr('disabled', true);
			$('#zipcode').attr('disabled', true);
			$('#addr').attr('disabled', false);
			
			$('#addr_tw').removeAttr('class');
			
			$('#addr_tw-error').remove();
			$('#county-error').remove();
			//重製台灣地址
			$('#twzipcode').twzipcode('reset');	
		}
	}
	
	//地址選擇按鈕
	$("input[name='addrtype']").click(function(){	
		ckaddr();				
	});
	
	$("#form_revise").validate({
        //debug:true,
        //onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
			"name":"required",
			/*"ymd_year":"required",
			"ymd_month":"required",
			"ymd_day":"required",*/
			"verify_code":"required",
			"telphone":{
				/*required: true,*/
				telphone: true
			},
            "phone":{
				required: true,
				phonecheck: true
			},
            /*"email":{
                required:true,
                email:true
            },*/
			"addr": { 
				required: '#addrtype:checked',
			},
			"county": { 
				required: '#addrtype:checked',
			},
			"addr_tw": { 
				required: '#addrtype:checked',
			}
            
			
        },
        messages:{
			"name":" 請輸入真實姓名",
			/*"ymd_year":" 請輸入年",
			"ymd_month":" 請輸入月",
			"ymd_day":" 請輸入日",*/
			"verify_code":" 請輸入驗證碼",
			"telphone":{
				/*required: " 請輸入電話號碼",*/
				telphone: " 請輸入XX-XXXXXXXX"
			},
            "phone":{
				required: " 請輸入手機號碼",
				phonecheck: " 請輸入正確手機號碼"
			},
            /*"email":{
				required: " 請輸入email",
				email: " email格式錯誤"
			},*/
			"addr":{
				required: " 請輸入海外地區地址",			
			},
			"county": { 
				required: " 請輸入縣市",
			},
			"addr_tw": { 
				required: " 請輸入台灣地區地址",
			} 
			
        }

    });
	
});

function reset_revise(){
	$("#form_revise")[0].reset();
	};
jQuery.validator.addMethod("phonecheck",
    function(cellphone, element) {
        cellphone = cellphone.replace(/\s+/g, "");
        return(this.optional(element) || /^09[0-9]{8}$/.test(cellphone));
    });

jQuery.validator.addMethod("telphone",
          function(cellphone, element) {

              cellphone = cellphone.replace(/\s+/g, "");

              return(
                  this.optional(element) || /^([0-9]{2}-[0-9]{6,8})|([0-9]{3}-[0-9]{6,8})$/.test(cellphone));

      });

// 填寫完修改會員表單 ------------------------------------------------------------------------------------------------
function finishContactForm(){
	
  var contact_submit_msg='表單送出中';

  var options = { 
        url:'do/memberdo.php',
        type:"post",
		data : { actiontype : "update" },
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_revise").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.result === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_revise :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                alert("會員修改成功"); 
                location.reload();
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_revise').ajaxSubmit(options);
}
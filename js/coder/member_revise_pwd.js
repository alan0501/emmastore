//驗證碼
function changeCaptcha(path){
	var _path = path ? '../' : '';
	$('#captcha').attr('src', _path + 'img.php?time='+ new Date().getTime());
}

$(function(){
    $("#form_revise_pwd").validate({
        //debug:true,
        onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
			"oldpwd": { 
				required: true
			},
            "pwd": { 
				required: true, 
				rangelength: [6, 8],
			},
			"c_pwd":{ 
				required: true, 
				equalTo: "#pwd" 
			},
			"verify_code":"required"
            
			
        },
        messages:{
			"oldpwd":{ 
				required: " 請輸入會員密碼"
			},
            "pwd":{ 
				required: " 請輸入會員密碼", 
				rangelength: " 長度必須介於{0}和{1}之間的字符"
			},
			"c_pwd":{ 
				required: " 請輸入確認密碼", 
				equalTo: " 兩次輸入密碼不一致不一致" 
			},
			"verify_code":" 請輸入驗證碼"        
			
        }

    });
});

function reset_revise_pwd(){
	$("#form_revise_pwd")[0].reset();
	};


// 填寫完修改密碼表單 ------------------------------------------------------------------------------------------------
function finishContactForm(){
	
  var contact_submit_msg='表單送出中';

  var options = { 
        url:'do/memberdo.php',
        type:"post",
		data : { actiontype : "pwd_update" },
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_revise_pwd").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.result === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_revise_pwd :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                alert("修改密碼成功"); 
                location.reload();
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_revise_pwd').ajaxSubmit(options);
}
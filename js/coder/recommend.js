$(function(){
    $("#form_recommend").validate({
        //debug:true,
        onclick: false,
        errorPlacement:function(error,element) {
            error.appendTo(element.next("span"));
        },
        rules:{
            "rname":"required",
            "rphone":"phonecheck required",
            "name":"required",
            "phone":"phonecheck required",
            "email":{
                required:true,
                email:true
            },
        },
        messages:{
            "rname":"請輸入講師姓名",
            "rphone":"請輸入講師的電話",
            "name":"請輸入姓名",
            "phone":"請輸入正確的電話",
            "email":"請輸入正確的E-mail",
        }
    });
});
jQuery.validator.addMethod("phonecheck",
    function(cellphone, element) {
        cellphone = cellphone.replace(/\s+/g, "");
        return(this.optional(element) || /[1-9]/.test(cellphone));
    },
"請輸入正確手機號碼");

// 填寫完聯絡我們表單 ------------------------------------------------------------------------------------------------
function finishForm(){
  var contact_submit_msg='表單送出中';

  var options = { 
        url:'do/recommend_save.php',
        type:"post",
        //target:'#output1',   // target element(s) to be updated with server response 
        beforeSubmit:showRequest,
        success:showResponse,
        dataType:"json"
  };
  function showRequest(formData, jqForm, options) {
     if($("#form_recommend").valid()){
       $.blockUI({
         message:contact_submit_msg+'，請稍後...',
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
             '-webkit-border-radius': '10px', 
             '-moz-border-radius': '10px', 
             opacity: 6,
             color: '#fff' 
         } 
       });
       return true;
     }else{
       return false;
     }
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    if(responseText.state === false){ 
      setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){alert(responseText.msg); } 
        }); 
      }, 1500); 
    }else{
      $("#form_recommend :input").val("").attr("checked",false);
        setTimeout(function() { 
          $.unblockUI({ 
              onUnblock: function(){
                alert("送出成功"); 
                location.reload();
              } 
          }); 
        }, 1500); 
    }
  }
  $('#form_recommend').ajaxSubmit(options);
}
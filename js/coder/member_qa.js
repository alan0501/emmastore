$(document).ready(function(){
	// 幫 #faq_box 的 ul 子元素加上 .accordionPart
	// 接著再找出 li 中的第一個 div 子元素加上 .faqTitle close
	// 同時把兄弟元素加上 .faqContent 並隱藏起來
	$('.faq_box ul').addClass('accordionPart').find('li div:nth-child(1)').click(function(){

		$(this).removeClass('close');
		$(this).addClass('open');
		
		// 當點到標題時，若答案是隱藏時則顯示它，同時隱藏其它已經展開的項目
		// 反之則隱藏
		var $faqContent = $(this).next('div.faqContent');
		if(!$faqContent.is(':visible')){
			$('#faq_box ul li div.faqContent:visible').slideUp();
			$(this).removeClass('open');
			$(this).addClass('close');
		}
		$faqContent.slideToggle();
	}).siblings().addClass('faqContent').hide().first().show(); //第一個展開 其他隱藏 
	//}).siblings().addClass('faqContent'); //全展開
	
});
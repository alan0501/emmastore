$(document).ready(function(){
	$(".btn-next").click(function() {
		//驗證是否勾選
		var pay = $("input[name='pay']:checked").val(); 
		if (pay == null) {
			alert("請選擇付款方式");
			return false;
		}
		$('.form').submit(); 
		return true;
	});
});

<?php
include_once("_config.php");

$rows_banner = class_banner::getList();
$rows_ad = class_ad::getList();
$rows_ptype1 = class_product::getType1();
$rows_top_product = class_product::getIndexTopProduct();
$rows_index_product = class_product::getIndexProduct();

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="js/slider.js"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>
<script src="scripts/jquery.infinitescroll.min.js"></script>
<script src="scripts/jquery.masonry.min.js"></script>
<script type="text/javascript">
$(function() {
	var yan = new SellerScroll({lButton: "left_scroll",rButton: "right_scroll",oList: "slideshow_ul",showSum: 1, itemCount:1, showPage: 1});
});
</script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix"> 
    <!--mobile menu star-->
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <!--mobile menu end-->
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.jpg"/></a></div>
            <div class="wrap-mame"><a href="index.html">Q點購物中心</a></div>
            <div class="m_login">
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
				<div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}else{
						$member_id = $_SESSION["session_925_id"];
				?>
				<div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
            </form>
        </div>
        <div id="top_right"> 
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
					$member_id = $_SESSION["session_925_id"];
			?>
            <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" >
    <div id="wrapper">
        <div id="wrap-menu">
            <?php include_once($inc_path."page/menu.php"); ?>
        </div>
        <div id="wrap-news">新開幕！所有產品不分類別，全面優惠中。</div>
        <div id="wrap-main">
            <div class="main_column1 clearfix">
                <div id="banner">
                    <div class="prev" id="left_scroll"><img src="images/prev.png" width="28" height="51" /></div>
                    <div class="next" id="right_scroll"><img src="images/next.png" width="28" height="51" /></div>
                    <ul class="banner-circle clearfix" id="tab">
                        <?php
							for($i = 0; $i < count($rows_banner); $i ++){
						?>
                        <li class='<?php echo ($i == 0) ? "selected" : ""; ?>'><a href='#'></a></li>
                        <?php
							}
						?>
                    </ul>
                    <ul id="slideshow_ul">
                        <?php
							foreach($rows_banner as $row_banner){
                                $row_banner[coderDBConf::$col_banner["link"]];
						?>
                        <li>
                            <a href="<?php echo (empty($row_banner[coderDBConf::$col_banner["link"]]))?'#':('http://'.$row_banner[coderDBConf::$col_banner["link"]]); ?>" target="_blank">
                                <img src="<?php echo $web_path_banner."m".$row_banner[coderDBConf::$col_banner["pic"]]; ?>" />
                            </a>
                        </li>
                        <?php
							}
						?>
                    </ul>
                </div>
                <div id="banner_small">
                    <ul class="clearfix">
                    <?php
						$j = 1;
						foreach($rows_ad as $row_ad){
					?>
                        <li><a href="<?php echo (empty($row_ad[coderDBConf::$col_ad["link"]]))?'#':('http://'.$row_ad[coderDBConf::$col_ad["link"]]); ?>" target="_blank"><img src="<?php echo $web_path_ad."m".$row_ad[coderDBConf::$col_ad["pic"]]; ?>" /></a> </li>
                    <?php
							if($j == 2){
								echo('</ul><ul class="clearfix">');	
							}
							$j ++;
						}
					?> 
                    </ul>
                </div>
            </div>
            <div class="main_column2">
                <div class="big_list">
                    <ul class="clearfix">
                    	<?php
							foreach($rows_top_product as $row_top_product){
						?>
                        <li><a href="prduct_info_<?php echo $row_top_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_top_product[coderDBConf::$col_ptype2["ptype1_id"]]; ?>.html"><img src="<?php echo $web_path_product."b".$row_top_product[coderDBConf::$col_product["pic"]]; ?>" border="0"  /></a>
                            <p><a href="prduct_info_<?php echo $row_top_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_top_product[coderDBConf::$col_ptype2["ptype1_id"]]; ?>.html"><?php echo $row_top_product[coderDBConf::$col_product["name"]]; ?></a></p>
                            <h3>NT$ <?php echo $row_top_product[coderDBConf::$col_product["price"]]; ?></h3>
                        </li>
                        <?php
							}
						?>
                    </ul>
                </div>
                <div class="smail_list">
                    <ul class="clearfix" id="pro_show">
                    	<?php
							foreach($rows_index_product as $row_index_product){
						?>
                        <li class="pro_show_box" style="display:inline-block;"><a href="prduct_info_<?php echo $row_index_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_index_product[coderDBConf::$col_ptype2["ptype1_id"]]; ?>.html"><img src="<?php echo $web_path_product."b".$row_index_product[coderDBConf::$col_product["pic"]]; ?>" border="0"  /></a>
                            <p><a href="prduct_info_<?php echo $row_index_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $row_index_product[coderDBConf::$col_ptype2["ptype1_id"]]; ?>.html"><?php echo $row_index_product[coderDBConf::$col_product["name"]]; ?></a></p>
                            <h3>NT$ <?php echo $row_index_product[coderDBConf::$col_product["price"]]; ?></h3>
                        </li>
                        <?php
							}
						?>
                    </ul>
                    <div id="page-nav">
                    	<!--<a href="do/index.php?page=2"></a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--pc footer star-->
    <?php include_once($inc_path."page/footer.php"); ?>
    <!--pc footer end--> 
</div>
<script>
carItemShow();
/*$(function(){
	var $container = $('#pro_show');
	
	$container.masonry({
		itemSelector : '.pro_show_box',
		isAnimated:true
	});
	
	$container.infinitescroll({
		navSelector  : '#page-nav',    
		nextSelector : '#page-nav a',  
		itemSelector : '.pro_show_box',     
		loading: {
			finishedMsg: '載入完成',
			img: 'images/loading.gif',
			msgText: '載入中.....'
		}
	},
		// trigger Masonry as a callback
		function(newElements){
			// hide new items while they are loading
			var $newElems = $( newElements ).css({ opacity: 0 });
			// ensure that images load before adding to masonry layout
			$newElems.imagesLoaded(function(){
				// show elems now they're ready				
				$newElems.animate({ opacity: 1 });
				$container.masonry( 'appended', $newElems, true ); 
			});
		}
	);
});*/
</script>
</body>
</html>
<?php
$db -> close();
?>
<?php
include_once("_config.php");

if(!isLogin()){
	script("您已經登出系統，請重新登入!!", "index.html");
}else{
	$member_id = $_SESSION["session_925_id"];
}

$row_member = class_member::getInfo($member_id);
if($row_member){
	$acc = $row_member[coderDBConf::$col_member["account"]];
	$name = $row_member[coderDBConf::$col_member["name"]];
	$fb_id = $row_member[coderDBConf::$col_member["fb_id"]];
	$phone = $row_member[coderDBConf::$col_member["phone"]];
	$email = $row_member[coderDBConf::$col_member["email"]];
	$address = $row_member[coderDBConf::$col_member["address"]];
	$pwd = $row_member[coderDBConf::$col_member["password"]];
}
$rows_ptype1 = class_product::getType1();

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix">
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">Q點購物中心</a></div>
            <div class="m_login">
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
				<div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}else{
				?>
				<div class="top_login"><a href="member.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
                
            </form>
        </div>
        <div id="top_right"> 
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
			?>
            <div class="top_login"><a href="member.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class="gray ">
    <div id="wrapper">
        <div id="member_menu">
            <dl class="clearfix" id="smbar">
                <dt><img src="images/member.jpg" align="absmiddle"/></dt>
                <dt class="email"><?php echo $acc; ?><br />
                    <span> <?php echo $email; ?> </span></dt>
                <dd ><a href="cart.html">我的購物車</a></dd>
                <dd ><a href="bought.html">已買到的商品</a></dd>
                <dd><a href="favorite_product.html">收藏的商品</a></dd>
                <dd><a href="favorite_store.html">收藏的店家</a></dd>
                <dd><a href="gold.html">購物金</a></dd>
                <dd class="selected"><a href="member.html">會員管理</a></dd>
            </dl>
        </div>
        <div id="wrap-main" class="clearfix">
            <div class="member_talble">
                <div class="tab_header">修改會員資料</div>
                <div class="member_modify">
                    <form action="" method="post" id="member_update_form" name="member_update_form">
                        <div class="address"> 收件人姓名：
                            <input type="text" name="member_name"  class="member_name" id="member_name" value="<?php echo $name; ?>" />
                            <P>郵局掛號若無人簽收，需憑身分證至郵局領取，請務必填寫<span>中文本名</span>，避免糾紛。</P>
                        </div>
                        <div class="address">電子郵件：
                            <input type="text" name="member_email" id="member_email"  class="member_email" value="<?php echo $email; ?>" />
                            <P>供寄送訂單或相關資料使用，請正確填寫。</P>
                        </div>
                        <div class="address">聯絡電話：
                            <input type="text" name="member_tel" id="member_tel"  class="member_tel" value="<?php echo $phone; ?>"/>
                            <P>市內電話或手機其中一隻即可。市內電話範例：02-1234xxxx、手機範例：0935-123xxx。</P>
                        </div>
                        <div class="address ">地址：
                            <input type="text" name="member_address" id="member_address"  class="member_address" value="<?php echo $address; ?>"/>
                            <P>預設收貨地址，請正確填寫。</P>
                        </div>
                        <div class="pass">
                            <input name="chk_pwd" type="checkbox" value="1" id="chk_pwd" />
                            我要變更密碼</div>
                        <div class="pass_change">
                            <div class="b12">先前密碼&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="password" name="old_pwd" id="old_pwd"  class="pass_box" value=""/>
                            </div>
                            <div class="b12">新的密碼&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="password" name="new_pwd" id="new_pwd"  class="pass_box" value=""/>
                            </div>
                            <div class="">重複輸入密碼&nbsp;&nbsp;
                                <input type="password" name="new_pwd2" id="new_pwd2"  class="pass_box" value=""/>
                            </div>
                        </div>
                        <div class="pass_btn update_btn"><img src="images/modify.png" /></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end-->
<script>
carItemShow();
</script>
</body>
</html>
<?php
$db -> close();
?>
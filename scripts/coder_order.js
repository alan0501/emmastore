function TestRgexp(str){ 
	var re = /^[0-9]*[1-9][0-9]*$/; 
    return re.test(str);
}

function orderSave(){
	var payment_type = $("#payment_type").val();				//付款方式
	
	var recipient_namechk = $("#namechk").val();         		//姓名
	var recipient_phonechk = $("#phonechk").val();				//手機
	var recipient_telphonechk = $("#telphonechk").val();		//電話
	var recipient_emailchk = $("#emailchk").val();				//信箱
	var recipient_addr = $("#addr").val();						//地址
	var recipient_time = $("input[name='time']:checked").val(); //收件時間
	
	var invoice_type = $("input[name='receipt']:checked").val(); //發票資訊
	var invoice_name = $("#invoice_name").val(); 				//發票 買受人
	var invoice_number = $("#invoice_number").val(); 			//發票資訊 統一編號
	var invoice_head = $("#invoice_head").val(); 				//發票資訊 發票抬頭
	//var md_vip = $("#md_vip").val(); 							//VIP
	
	/* 會員地址修改 ↓ */
	//台灣地址
	var zipcode = $("#zipcode").val();
	var county = $("#county").val();
	var district = $("#district").val();
	var addr_tw = $("#addr_tw").val();
	
	var addrtype = $("input[name='addrtype']:checked").val();
	
	var addr_old = $("#addr_old").val();
	
	/* 會員地址修改 ↑ */
	$.ajax({
		url : 'do/order_save.php',
		async : false,
		type : 'POST',
		dataType : "json",
		data : {
				payment_type : payment_type,
				recipient_namechk : recipient_namechk,
				recipient_phonechk : recipient_phonechk,
				recipient_telphonechk : recipient_telphonechk,
				recipient_emailchk : recipient_emailchk,
				recipient_addr : recipient_addr,
				recipient_time : recipient_time,
				invoice_type :invoice_type,
				invoice_name :invoice_name,
				invoice_number : invoice_number,
				invoice_head : invoice_head,
				//md_vip:md_vip,
				
				zipcode : zipcode,
				county : county,
				district : district,
				addr_tw : addr_tw,
				addrtype : addrtype,
				addr_old : addr_old
		},
		error : function(){
			alert('資料傳送發生錯誤!');
		},
		success : function(response){
			if(response.result){
				alert(response.msg);
				/*if(payment_type == 1){
					location.href = "do/payment.html";
				}else if(payment_type == 2){
					location.href = "do/payment_paypal.html";
				}*/
				//$(".btn_write").show();
				location.href = "member_order.html";
				
			}else{
				alert(response.msg);
			} 
		}
	});
}

$(document).ready(function(e) {
	
	$(".buy_btn").click(function(e) {
		var product_id = $(this).attr("pro_id");    //產品ID
		var product_sno = $(this).attr("pro_sno");	//產品sno
		var product_name = $(this).attr("pro_name");//產品名稱  
		var pic = $(this).attr("pro_pic");			//產品圖片路徑
		var sell_price = $(this).attr("pro_sell_price");//產品價格
		var type_name = $(this).attr("pro_type_name");//產品類別名稱
		var md_id = $(this).attr("md_id");//會員
		if(md_id != "1")
		{
			alert("請登入會員!");
			window.location.href = 'member.html';
			return false
		}
		
		var amount = 1;
		var sub_total = (sell_price * amount);
		//console.log("hahahah");
		addCar(product_id, product_sno, product_name, pic, sell_price, type_name, amount, sub_total);
    });
	
	$("input.amount").blur(function(e) {
	   var chang_amount = $(this).val();
	   if(chang_amount == ""){
		   alert("請輸入數量!");
		   location.reload();
	   }else if(!TestRgexp(chang_amount)){
		   alert("請輸入正整數!");
		   location.reload();
	   }else{
		  var cart_id = $(this).parents("tr.tbdoy").attr("cart_id");
		  var list = $(this).parents("tr.tbdoy").attr("list"); 
		  updateCar(cart_id, chang_amount, list);
		  
	   }
    });
	
	$("a.remove-btn").click(function(e){
        var r = confirm("您確定要刪除此像商品!")
		if(r){
			var chang_amount = 0;
			var cart_id = $(this).parents("tr.tbdoy").attr("cart_id");
		  	var list = $(this).parents("tr.tbdoy").attr("list"); 
		  	updateCar(cart_id, chang_amount, list);
		}
    });
	
	$("a#c1_next_btn").click(function(e) {
        //var payment_type = $("input[type='radio']:checked").val();
		//location.href = "checkoutstep2_"+payment_type+".html";
		$("form#step1_form").submit();
    });
	
	$("a.coupons-btn").click(function(e) {
		var coupon_sno = $("#coupon_sno").val();
		if(coupon_sno != ""){
			$.ajax({
				url : 'do/set_coupon.php',
				async : false,
				type : 'POST',
				data : {
						coupon_sno : coupon_sno
						},
				dataType : "json",
				error : function(){
					alert('資料傳送發生錯誤');
				},
				success : function(response){
					if(response.sResult){
						alert(response.msg);
						var all_total = $("span#all_box").text();
						var all_discount = $("span#all_discount").text();
						//var freight = $("span#freight").text();
						var coupon_discount = response.coupon_discount;
						var coupon_limit = response.coupon_limit;
						
						if(all_total > coupon_limit){
							//$(span"#coupon_box").val(coupon);
							$("span#coupon_box").text(coupon_discount);
							var all_total_price = parseInt(all_total-all_discount-coupon_discount);
							all_total_price = (all_total_price > 0) ? all_total_price : 0;
							
							var freight = "";
							if(all_total_price >= freight_limit)
							{
								freight = 0;
							}
							else
							{
								freight = freight_price;
							}
							  
							$("span#all_box2").html(all_total_price+freight);
							$("span#freight").html(freight);
						}else{
							alert("您需購物滿"+coupon_limit+"元,才能使用此coupon!!");
						}
						
						
					}else{
						alert(response.msg);
						
					} 
				}
			});
	   }else{
		  alert("請輸入coupon代碼!"); 
	   }
	});
	
});
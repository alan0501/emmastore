/**
 * cully create 20120926
 * 購物車相關JS
 */
$.ajaxSetup({ cache: false });
var serviceurl = "do/shoppingservice.php";
/**
 * 將商品相關資訊新增至購物車  
 * @param{productid} 產品ID
 * @param{productsno} 產品sno
 * @param{pro_name} 產品名稱
 * @param{pro_pic} 產品圖片路徑 
 * @param{pro_price} 產品價格
 * @param{pro_type_name} 產品類別名稱
 * @param{amount} 產品數量
 * @param{subtotal} 商品小計
 * @param{isshow} 是否顯示
 * @param{createtime} 建立時間
 */
		
function addCar(product_id, product_sno, product_name, sell_price, amount, sub_total, product_bonus, pic, shop_id){
	if(product_id > 0 && product_sno != "" && sell_price != "" && amount > 0){
		//alert("hi");
		$.ajax({
			url : serviceurl,
			async : false,
			type : "POST",
			data : {
					method : "add", 
					product_id : product_id, 
					product_sno : product_sno,
					product_name : product_name,
					sell_price : sell_price,
					amount : amount,
					sub_total : sub_total,
					product_bonus : product_bonus,
					pic : pic,
					shop_id : shop_id
			},
			dataType : "json",
			success : function(data){
						if(data.result){
							alert('商品已成功加入至購物車\n目前購物車總共有'+data.cart_item+'項商品'+data.total+'元');
							
							updateCarNum(data.cart_item);
							carItemShow();
							updateItemShow(data.cart_item);
						}else{
							alert(data.message);
						}
			}
		});
	}else{
		alert('參數資料傳送錯誤!');
	}
}

function updateCarNum(cart_item){
	$('span.cart_amount').html(cart_item);  //購物車 產品數量
	
}

function carItemShow(){
		$.ajax({
		url : 'do/caritem_show.php',
		async : false,
		type : 'POST',
		//dataType : "json",
		error : function(){
			alert('發生錯誤');
		},
		success : function(response){
			if(response){
				$("ul#cart_item_show").html(response);
				//$("div.topcart_pcs").html("共有"+cart_item+"件商品");
  			}else{
				$("ul#cart_item_show").html("尚未選擇商品!");
  			} 
		}
	});
}

function updateCar(cart_id, shop_id, num, list){  //list:第幾樣商品; s=0:移除本列商品
	//var list = list || "";
	if(cart_id != ""){
		$.ajax({
			url : serviceurl,
			async : false,
			type : "POST",
			data : {
					method : "update",
					cart_id : cart_id,
					shop_id : shop_id,
					num : num
			},
			dataType : "json",
			success : function(data){
				if(data.result){
					//alert('此商品已成功刪除\n目前購物車總共有'+data[0].message+'項商品');
					//updateCarState(data[0].message[0].amount,data[0].message[0].subtotal,data[0].total,data[0].message[0].freight,data[0].message[0].alltotal,data[0].message[0].discount,data[0].message[0].disbonus,n);
					if(num <= 0){
						removeCarLine(list);
						updateCarNum(data.cart_item);
						//carPriceShow();
						//updateItemShow(data.cart_item);
						location.reload();
						if(data.cart_item <= 0){
							//alert("ahah");
							clearCar();
							location.reload();
						}
					}else{
						//console.log("haha");
						//updateCarNum(data.cart_item);
						carItemShow(data.cart_item);
						location.reload();
					}
				}else{
					alert(data.message);
				}
			},
			error:function(jqXHR, textStatus, errorThrown){
				alert(errorThrown);
			}
		});		
	}else{
		alert('參數資料傳送錯誤!');
	}
}

function removeCarLine(n){
	$obj = $('li.item_list'+n);
	$obj.remove();
}
function updateItemShow(cart_item){
	//console.log(cart_item);
	$("div.topcart_pcs").html("共有"+cart_item+"件商品");
}
function clearCar(){
	$.ajax({
		url : serviceurl,
		async : false,
		type : "POST",
		data : "method=clear",
		dataType : "json",
		success : function(data){
			if(data){
				if(data.result){
						
					}else{
						alert(data.message);
					}
				}
			}
		});
}

function setCarry(val, shop_id){
	var success = false;
	$.ajax({
		url : serviceurl,
		async : false,
		type : "POST",
		data : {
				method : "carry",
				val : val,
				shop_id : shop_id
		},
		dataType : "json",
		success : function(data){
			if(data){
				if(data.result){
						success = true;
					}else{
						alert(data.message);
				}
			}
		},
		error:function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		}
	});
	return success;
}






function carPriceShow(){
		$.ajax({
		url : 'do/price_show.php',
		async : false,
		type : 'POST',
		dataType : "json",
		error : function(){
			alert('資料傳輸發生錯誤!');
		},
		success : function(response){
			if(response){
				$("span#show_total").html(response.total);
				$("span#show_freight").html(response.freight);
				$("span#show_coupon").html(response.coupon);
				$("span#show_all_total").html(response.all_total);
  			}else{
				alert("資料傳送錯誤!");
  			} 
		}
	});
}






function chkProductStock(product_id, num){ 
	if(product_id != ""){
		var res = false;
		$.ajax({
			url : serviceurl,
			async : false,
			type : "POST",
			data : {method : "chkstock",
					product_id : product_id,
					num : num
			},
			dataType:"json",
			success:function(data){
				if(data.result){
					res = true;
				}else{
					alert(data.message);
				}
			},
			error:function(jqXHR, textStatus, errorThrown){
				alert(errorThrown);
			}
		});	
		return res;	
	}else{
		alert('參數資料傳送錯誤!');
	}
}

function updateCarState(amount,subtotal,total,freight,alltotal,discount,disbonus,n){
	$('#subtotal'+n).html(subtotal);
	$("#total").text(total);
	$("#freightt").text(freight);
	$("#freight").val(freight);
	$("#alltotal").text(alltotal);
	$("#totalprice").val(alltotal);
	$("#bonusreward").text(disbonus);
	
	showDisComment('totaltable',discount);
}	

function showDisComment(objid,data){
	$obj=$('#'+objid);
//	$first=$obj.find("tr:first"); 
//	$last=$obj.find("tr:last"); 
	$first=$obj.find("tr.line1"); 
	$second=$obj.find("tr.line2");
	$last=$obj.find("tr.line3"); 
	$obj.empty();
	$obj.append($first);
	for(var i=0,len=data.length;i<len;i++){
		var units=data[i].type==2 ? '紅利' : '-NT$';
		$obj.append('<tr> <td height="40" colspan="2" align="right"><span id="discountcomment">'+data[i].title+'</span></td><td height="40" align="left">優惠折扣<span class="txt14c_09f" style="padding-left:10px;">'+units+'<span id="discount">'+data[i].discount+'</span></span></td></tr>');
	}  
	$obj.append($second);
	$obj.append($last);
}



/*function getBuy(span_id){ 
	var spanobj = $("#"+span_id);
	var pro_id = spanobj.attr("pro_id");
	var pro_sno = spanobj.attr("pro_sno");
	var pro_name = spanobj.attr("pro_name");
	var pro_sellprice = spanobj.attr("pro_sellprice");
	var pro_amount = $("#"+span_id+" "+".amount").val();
	if(pro_amount == ""){
		pro_amount = 0;
	}
	var pro_subtotal = pro_sellprice*pro_amount;
	//alert (pro_subtotal);
	addCar(pro_id, pro_sno, pro_name, pro_sellprice, pro_amount, pro_subtotal);
}*/


//會員是否登入
function isLogin(){
	var success = false;
	$.ajax({
		url: 'do/check_login.php',
		type: 'POST',
		dataType: "json",
		async:false,
		error: function(){
			alert('發生錯誤');
		},
		success: function(data){
			success = data.sResult;			
		}
	});		
	return success;
}
//收藏商品
function setFavoriteProduct(m_id, p_id){
	var has = false;
	$.ajax({
		url : 'do/set_fproduct.php',
		data : {
			m_id : m_id,
			p_id : p_id
		},
		type : 'POST',
		dataType : "json",
		async : false,
		error : function(){
			alert('資料傳送發生錯誤!');
		},
		success : function(data){
			if(data.sResult){
				alert(data.msg);
				has = data.has;
			}else{
				alert(data.msg);	
			}			
		}
	});		
	return has;
}
//收藏店家
function setFavoriteShop(m_id, s_id){
	var success = false;
	$.ajax({
		url : 'do/set_fshop.php',
		data : {
			m_id : m_id,
			s_id : s_id
		},
		type : 'POST',
		dataType : "json",
		async : false,
		error : function(){
			alert('資料傳送發生錯誤!');
		},
		success : function(data){
			if(data.sResult){
				alert(data.msg);
				success = true;
			}else{
				alert(data.msg);
			}			
		}
	});		
	return success;
}
//刪除收藏商品
function delFavoriteProduct(fp_id){
	$.ajax({
		url : 'do/del_fproduct.php',
		data : {
			fp_id : fp_id
		},
		type : 'POST',
		dataType : "json",
		async : false,
		error : function(){
			alert('資料傳送發生錯誤!');
		},
		success : function(data){
			if(data.sResult){
				alert(data.msg);
				location.reload();
			}else{
				alert(data.msg);	
			}			
		}
	});		
}
//刪除收藏店家
function delFavoriteShop(fs_id){
	$.ajax({
		url : 'do/del_fshop.php',
		data : {
			fs_id : fs_id
		},
		type : 'POST',
		dataType : "json",
		async : false,
		error : function(){
			alert('資料傳送發生錯誤!');
		},
		success : function(data){
			if(data.sResult){
				alert(data.msg);
				location.reload();
			}else{
				alert(data.msg);
			}			
		}
	});		
}
//轉換購物金
function changeBonus(all_bonus){
	$.ajax({
		url : 'do/change_bonus.php',
		data : {
			all_bonus : all_bonus
		},
		type : 'POST',
		dataType : "json",
		async : false,
		error : function(){
			alert('資料傳送發生錯誤!');
		},
		success : function(data){
			if(data.sResult){
				alert(data.msg);
				location.reload();
			}else{
				alert(data.msg);
			}			
		}
	});		
}

$(document).ready(function(e){
	var myMember = new coderMember();
	
	var check_pwd = function() {
        return $("#chk_pwd").is(':checked');
    };
	
	var check_commend = function() {
		var val = $("input[name=commend]:checked").val();
		return (val == 1) ? true : false;
    };
	
	var check_recipient = function() {
		var val = $("input[name=recipient]:checked").val();
		return (val == 2) ? true : false;
    };
	
	var showErrors = function(errorMap, errorList){
		// Clean up any tooltips for valid elements
		$.each(this.validElements(), function (index, element){
			var $element = $(element);
			$element.data("title", "") // Clear the title - there is no error associated anymore
					.removeClass("error")
					.tooltip("destroy");
		});
		// Create new tooltips for invalid elements
		$.each(errorList, function (index, error){
			var $element = $(error.element);
			
			$element.tooltip("destroy")// Destroy any pre-existing tooltip so we can repopulate with new tooltip content
					.data("title", error.message)
					.addClass("error")
					.tooltip(); // Create a new tooltip based on the error messsage we just set in the title
			
		});
	}
	$.validator.addMethod("chkAcc", function(value, element){
		var acc = $('#reg_userName').val();
		return this.optional(element) || myMember.chkAcc(acc);
	}, "會員帳號已存在,請重新輸入!");
	$.validator.addMethod("chkEmail", function(value, element){
		var email = ($('#input_email').length > 0) ? $('#input_email').val() : $('#member_email').val();
		//console.log(email);
		return this.optional(element) || myMember.chkEmail(email);
	}, "會員email已存在,請重新輸入!");
	$.validator.addMethod("chkEmail_reg", function(value, element){
		var email = ($('#input_email').length > 0) ? $('#input_email').val() : $('#member_email').val();
		//console.log(email);
		return this.optional(element) || myMember.chkEmail_reg(email);
	}, "會員email已存在,請重新輸入!");
	$.validator.addMethod("chinese", function(value, element) {
		var chinese = /^[\u4e00-\u9fa5]+$/;
		return this.optional(element) || (chinese.test(value));
	}, "請勿輸入英文、符號或特殊字元");
	$.validator.addMethod("chk_commend_self", function(value, element){
		//console.log(value);
		if(value == ""){
			return true;
		}else{
			//console.log("valid="+myMember.chkEmail_self(value));
			return (myMember.chkEmail_self(value)) ?  true : false;

		}
		//return this.optional(element) || myMember.chkEmail(email);
	}, "請勿推薦自己!");
	$.validator.addMethod("chk_commend_email", function(value, element){
		//console.log(value);
		if(value == ""){
			return true;
		}else{
			//console.log("valid="+myMember.chkEmail_reg(value));
			return (myMember.chkEmail_reg(value)) ? false : true;

		}
		//return this.optional(element) || myMember.chkEmail(email);
	}, "此email並非會員,請重新輸入!");
	$.validator.addMethod("isExistAcc", function(value, element){
		var acc = $('#forget_acc').val();
		return this.optional(element) || myMember.isExistAcc(acc);
	}, "會員帳號不存在,請重新輸入!");
	$.validator.addMethod("isTrueEmail", function(value, element){
		var acc = $('#forget_acc').val();
		var email = $('#forget_email').val();
		return this.optional(element) || myMember.isTrueEmail(acc, email);
	}, "Email不正確,請重新輸入!");
	
	$("#member_reg_form").validate({
		rules : {
			reg_userName : {
				required : true,
				chkAcc : true
			},
			reg_password : {
				maxlength : 20,
				required : true
			},
			reg_password2 : {
				maxlength : 20,
				equalTo : "#reg_password"
			},
			input_email : {
				required : true,
				email : true,
				chkEmail_reg : true
			}
		},
		messages : {
			reg_userName : { 
				required : "請輸入帳號"
			},
			reg_password : {
				maxlength : jQuery.format("最多不要超過{0}碼"),
				required : "請輸入密碼"
			},
			reg_password2 : {
				maxlength : jQuery.format("最多不要超過{0}碼"),
				equalTo : "二次密碼輸入不相同"
			},
			input_email : {
				required : "請輸入Email",
				email : "請填寫正確的Email格式"
			}
		},
		submitHandler : function(form){
			//var myMember = new coderMember();
            var _item = new Object();;
			_item.account = $('#reg_userName').val();
			_item.password = $('#reg_password').val();
			_item.email = $('#input_email').val();
			var path = $('#path').val();
			if(myMember.insert(_item)){
				//alert(myMember.memberid);
				var mid = myMember.member_id;
				//var bouns=20;
				//var bonustype = 0;
				//alert("會員加入完成");
				//alert(mid);
				if(mid > 0){
					alert("會員加入完成!");
					if(myMember.memberLogin(_item)){
						if(path == "i"){
							parent.$.fancybox.close();
							parent.window.location.reload();
						}else{
							window.location.href = "index.html";
						}
						
					}else{
						//open login_box
					}
				}else{
					alert("會員加入失敗!請聯絡客服人員!");
				}
			}else{
				alert("會員加入失敗!"+myMember.message);
			}
        },
		showErrors : showErrors
	});
	
	$("#member_login_form").validate({
		rules : {
			userName : {
				required : true
			},
			password : {
				required : true
			}
		},
		messages : {
			userName : { 
				required : "請輸入帳號"
			},
			password : {
				required : "請輸入密碼"
			}
		},
		submitHandler : function(form){
			//var myMember = new coderMember();
            var _item = new Object();;
			_item.account = $('#userName').val();
			_item.password = $('#password').val();
			var path = $('#path').val();
			if(myMember.memberLogin(_item)){
				if(path == "i"){
					parent.$.fancybox.close();
					parent.window.location.reload();
				}else{
					window.location.href = "index.html";
				}
				
			}else{
				alert(myMember.message);
			}
        },
		showErrors : showErrors
	});
	
	$("#member_update_form").validate({
		rules : {
			member_name : {
				required : true,
				chinese : true
			},
			member_email : {
				required : true,
				email : true,
				chkEmail : true
			},
			member_tel : {
				required : true
			},
			member_address : {
				required : true
			},
			old_pwd : {
				required : { depends: check_pwd }
				//required : true
			},
			new_pwd : {
				required : { depends: check_pwd },
				maxlength : 20
			},
			new_pwd2 : {
				equalTo : "#new_pwd"
			}
		},
		messages : {
			member_name : { 
				required : "請輸入姓名"
			},
			member_email : {
				required : "請輸入電子郵件",
				email : "請輸入正確格式的email信箱"
			},
			member_tel : {
				required : "請輸入連絡電話"
			},
			member_address : {
				required : "請輸入地址"
			},
			old_pwd : {
				required : "請輸入先前密碼"
			},
			new_pwd : {
				required : "請輸入新密碼",
				maxlength : jQuery.format("最多不要超過{0}碼")
			},
			new_pwd2 : {
				equalTo : "二次密碼輸入不相同"
			}
		},
		submitHandler : function(form){
			var chk_val = $("#chk_pwd:checked").val(); 
            var _item = new Object();
			_item.name = $('#member_name').val();
			_item.email = $('#member_email').val();
			_item.phone = $('#member_tel').val();
			_item.address = $('#member_address').val();
			_item.old_pwd = (chk_val == 1) ? $('#old_pwd').val() : "";
			_item.new_pwd = (chk_val == 1) ? $('#new_pwd').val() : "";
			_item.chk_val = chk_val;
			
			if(myMember.update(_item)){
				//parent.$.fancybox.close();
				alert(myMember.message);
				window.location.reload();
			}else{
				alert(myMember.message);
			}
        },
		showErrors : showErrors
	});
	
	$("#search_form").validate({
		rules : {
			keyword : {
				required : true
			}
		},
		messages : {
			keyword : { 
				required : "請輸入關鍵字"
			}
		},
		showErrors : showErrors
	});
	
	$("#commend_form").validate({
		rules : {
			commend_email : {
				required : { depends: check_commend },
				chk_commend_self : true,
				chk_commend_email : true
			}
		},
		messages : {
			commend_email : { 
				required : "請輸入推薦人信箱"
			}
		},
		showErrors : showErrors
	});
	
	$("#recipient_form2").validate({
		rules : {
			recipient_name : {
				required : { depends: check_recipient },
				chinese : true
			},
			recipient_phone : {
				required : { depends: check_recipient }
			},
			recipient_email : {
				required : { depends: check_recipient },
				email : true
			},
			recipient_zcode : {
				required : { depends: check_recipient }
			},
			recipient_address : {
				required : { depends: check_recipient }
			}
		},
		messages : {
			recipient_name : { 
				required : "請輸入收件人姓名!"
			},
			recipient_phone : { 
				required : "請輸入收件人手機或電話!"
			},
			recipient_email : { 
				required : "請輸入收件人email!",
				email : "請輸入正確格式的email信箱"
			},
			recipient_zcode : { 
				required : "請輸入收件人郵遞區號!"
			},
			recipient_address : { 
				required : "請輸入收件人地址!"
			}
		},
		showErrors : showErrors
	});
	
	$("#member_forget_form").validate({
		rules : {
			forget_acc : {
				required : true,
				isExistAcc : true
			},
			forget_email : {
				required : true,
				isTrueEmail : true
			}
		},
		messages : {
			forget_acc : { 
				required : "請輸入帳號"
			},
			forget_email : {
				required : "請輸入email"
			}
		},
		submitHandler : function(form){
            var _item = new Object();;
			_item.account = $('#forget_acc').val();
			_item.email = $('#forget_email').val();
			var path = $('#path').val();
			//alert("forget");
			if(myMember.passwordMail(_item)){
				if(path == "i"){
					parent.$.fancybox.close();
					alert(myMember.message);
					parent.window.location.reload();
				}else{
					alert(myMember.message);
					window.location.href = "index.html";
				}
				
			}else{
				alert(myMember.message);
			}
        },
		showErrors : showErrors
	});
	
	$("div.reg_btn").click(function(){
		$("form#member_reg_form").submit();
	});
	
	$("div.login_btn").click(function(){
		$("form#member_login_form").submit();
	});
	
	$("div.update_btn").click(function(){
		$("form#member_update_form").submit();
	});
	
	$("#search").click(function(){
		$("form#search_form").submit();
	});
	
	$("a#commend_next_btn").click(function(){
		$("form#commend_form").submit();
	});
	
	$("div.forget_send_btn").click(function(){
		$("form#member_forget_form").submit();
	});
	
	$("a#recipient_btn").click(function(){
		var recipient_select = $("input[name='recipient']:checked").val();
		var company_invoice = $("input[name='company_invoice']:checked").val();
		if(recipient_select == 1){
			$("#company_invoice1").val(company_invoice);
			$("form#recipient_form1").submit();
		}else{
			$("#company_invoice2").val(company_invoice);
			$("form#recipient_form2").submit();
		}
		
	});
	
	$(".logout_btn").click(function(e){
		myMember.memberLogout();
		location.reload();
    });
	
	$(".fb_login_btn").click(function(){
		//console.log("haha");
		fbLoginMember("i");
	});
	
	$(".reg_new_btn").click(function(){
		parent.$.fancybox.close();
		parent.$.fancybox.open({
			href : 'reg_lb.html',
			type : 'iframe',
			width: 480,
			padding : 5
		});
	});
	
	$(".forget_btn").click(function(){
		parent.$.fancybox.close();
		parent.$.fancybox.open({
			href : 'forget_lb.html',
			type : 'iframe',
			width: 480,
			padding : 5
		});
	});
	
	$(".go_login_btn").click(function(){
		parent.$.fancybox.close();
		parent.$.fancybox.open({
			href : 'login_lb.html',
			type : 'iframe',
			width: 480,
			padding : 5
		});
	});

	$("div.favorite_pbtn").click(function(e) {
        if(isLogin()){
			var m_id = $(this).attr("m_id");
			var p_id = $(this).attr("p_id");
			setFavoriteProduct(m_id, p_id);
		}else{
			alert("請先登入會員!");
		}
    });
	
	$("div.favorite_sbtn").click(function(e) {
        if(isLogin()){
			var m_id = $(this).attr("m_id");
			var s_id = $(this).attr("s_id");
			setFavoriteShop(m_id, s_id);
		}else{
			alert("請先登入會員!");
		}
    });
	
	$("span.fp_del_btn").click(function(e) {
        if(isLogin()){
			var r = confirm("您確定要刪除此項商品!");
			if(r){
				var fp_id = $(this).attr("fp_id");
				delFavoriteProduct(fp_id);
			}
		}else{
			alert("請先登入會員!");
		}
    });
	$("span.fs_del_btn").click(function(e) {
        if(isLogin()){
			var r = confirm("您確定要刪除此店家!");
			if(r){
				var fs_id = $(this).attr("fs_id");
				delFavoriteShop(fs_id);
			}
		}else{
			alert("請先登入會員!");
		}
    });
	
	$("a.change_bonus_btn").click(function(e) {
        if(isLogin()){
			var all_bonus = $(this).attr("all_bonus");
			if(all_bonus > 0){
				var r = confirm("您確定要將紅利轉換購物金!");
				if(r){
					changeBonus(all_bonus);
				}
			}else{
				alert("您目前累積的紅利尚不足已轉換購物金!!");	
			}
		}else{
			alert("您已登出,請先登入會員!");
		}
    });
});
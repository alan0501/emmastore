function FloatMul(arg1, arg2){
  var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
  try { m += s1.split(".")[1].length; } catch (e) { }
  try { m += s2.split(".")[1].length; } catch (e) { }
  return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
}



$(document).ready(function(e){
	
	$("a.order-detail-btn").click(function(e) {
        var sno = $(this).parents("tr.tbdoy").attr("sno");
		console.log(sno);
		$.ajax({
			url : 'do/get_order_detail.html',
			async : false,
			type : 'POST',
			data : {
					sno : sno
					},
			dataType : "json",
			error : function(){
				alert('資料傳送發生錯誤!');
			},
			success : function(response){
				if(response.result){
					$("div.order-detail").html(response.list);
					
				}else{
					alert(response.msg);
				} 
			}
		});
    });
	
	/*$("div.btn-minus,div.btn-plus").click(function(e) {
        var num = parseFloat($(this).siblings(".num-content").val());
		var sell_price = parseFloat($(this).siblings("button.buy_btn").attr("sell_price"));
		var count_price = FloatMul(num, sell_price);
		$(this).parents("#pro_count").find("span.count_price").text(count_price);
    });
	
	$(".num-content").blur(function(e) {
		var num = parseFloat($(this).val());
		var sell_price = parseFloat($(this).siblings("button.buy_btn").attr("sell_price"));
		var count_price = FloatMul(num, sell_price);
		$(this).parents("#pro_count").find("span.count_price").text(count_price);
        //console.log(num);
    });
    
	//$("#buy_btn").on("click", "#buy_btn", function(){
	$(".buy_btn").click(function(e){
		var product_id = $(this).attr("pro_id");
		var product_part_no = $(this).attr("pro_part_no");
		var product_name = $(this).attr("pro_name");
		var sell_price = $(this).attr("sell_price");
		var pic = $(this).attr("pic");
		var cbm = $(this).attr("cbm");
		var amount = $(this).siblings(".num-content").val();
		var total_price = (sell_price * amount);
		var total_cbm = (cbm * amount);
		console.log(total_cbm);
		//var member_id = $(this).attr("member_id");
		
		if(isLogin()){
			addCar(product_id, product_part_no, product_name, sell_price, amount, total_price, pic, cbm, total_cbm);
		}else{
			alert("請先登入會員!");
			location.href = "signup.html";
			//location.href = "signup.html?page="+page+"&pro_id="+product_id+"&type_id="+type_id;
		}
    });
	
	$(".update_cart_btn").click(function(e){
		var cart_id = $(this).parents("tr.cart_list_tr").find("td.cart_list_td").attr("cart_id");
		var product_id = $(this).parents("tr.cart_list_tr").find("td.cart_list_td").attr("pro_id");
		var num = $(this).parents("tr.cart_list_tr").find(".amount").val();
		var list = $(this).parents("tr.cart_list_tr").find("td.cart_list_td").attr("list");
		//console.log(cart_id+"/"+num+"/"+list);
		updateCar(cart_id, num, product_id, list);
		location.reload();
    });
	
	$(".delete_cart_btn").click(function(e){
		var cart_id = $(this).parents("tr.cart_list_tr").find("td.cart_list_td").attr("cart_id");
		var product_id = $(this).parents("tr.cart_list_tr").find("td.cart_list_td").attr("pro_id");
		var num = 0;
		var list = $(this).parents("tr.cart_list_tr").find("td.cart_list_td").attr("list");
		//console.log(cart_id+"/"+num+"/"+list);
		updateCar(cart_id, num, product_id, list);
		location.reload();
    });
	
	$("button.btn-save").click(function(e){
		$("tr.cart_list_tr").each(function(index, element) {
			var cart_id = $(this).find("td.cart_list_td").attr("cart_id");
			var product_id = $(this).find("td.cart_list_td").attr("pro_id");
			var num = $(this).find(".amount").val();
			var list = $(this).find("td.cart_list_td").attr("list");
			//console.log(cart_id+"/"+num+"/"+list);
			updateCar(cart_id, num, product_id, list);            
        });
		
		location.reload();
    });*/
});
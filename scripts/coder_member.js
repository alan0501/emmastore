function coderMember(){
}
coderMember.prototype = {
	message : '',
	member_id : 0,
	member_acc : '',
	
	//檢查帳號是否重覆
	chkAcc : function(value){ 
	//console.log(value);
		var parent = this,
		success = false;  
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
				account : value, 
				actiontype : "chkAcc" 
			},
			dataType : "json",
			success : function(data){
				if(data){
					if(data.result){
						success = true;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error:function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});	
		//console.log(success);
		return success;	 
	},
	//檢查帳號是否存在
	isExistAcc : function(value){ 
	//console.log(value);
		var parent = this,
		success = false;  
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
				account : value, 
				actiontype : "isExistAcc" 
			},
			dataType : "json",
			success : function(data){
				if(data){
					
					if(data.result){
						success = true;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error:function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});	
		//console.log(success);
		return success;	 
	},
	//檢查email是否正確
	isTrueEmail : function(acc, email){ 
	//console.log(value);
		var parent = this,
		success = false;  
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
				account : acc, 
				email : email,
				actiontype : "isTrueEmail" 
			},
			dataType : "json",
			success : function(data){
				if(data){
					if(data.result){
						success = true;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error:function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});	
		//console.log(success);
		return success;	 
	},
	//檢查email是否重覆
	chkEmail : function(value){ 
	//console.log(value);
		var parent = this,
		success = false;  
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
				email : value, 
				actiontype : "chkEmail" 
			},
			dataType : "json",
			success : function(data){
				if(data){
					if(data.result){
						success = true;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error:function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});	
		//console.log(success);
		return success;	 
	},
	chkEmail_reg : function(value){ 
	//console.log(value);
		var parent = this,
		success = false;  
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
				email : value, 
				actiontype : "chkEmail_reg" 
			},
			dataType : "json",
			success : function(data){
				if(data){
					if(data.result){
						success = true;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error:function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});	
		//console.log(success);
		return success;	 
	},
	chkEmail_self : function(value){ 
	//console.log(value);
		var parent = this,
		success = false;  
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
				email : value, 
				actiontype : "chkEmail_self" 
			},
			dataType : "json",
			success : function(data){
				//console.log(data);
				if(data){
					if(data.result){
						success = true;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error:function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});	
		//console.log(success);
		return success;	 
	},
	//會員新增 
	insert : function(member){ 
		var parent = this;
		//member.actiontype = type;
		success = false;
		$.ajaxSetup({ cache: false });
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data: { 
				account : member.account, 
				password : member.password, 
				email : member.email,   
				actiontype : "insert" 
			},
			dataType : "json",
			success : function(data){
				if(data){
					if(data.result){
						success = true;
						parent.member_id = data.member_id;
						//parent.acc = data.acc;
						//parent.pwd = data.pwd;
						//alert("hi");
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤!";
				}
			},
			error : function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試!"+thrownError;
			}
		});
		return success;
	},
	//會員登入
	memberLogin : function(member){ 
		var parent = this,
		success = false;
		//console.log(member);
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
				account : member.account, 
				password : member.password, 
				actiontype : "memberLogin" 
			},
			dataType : "json",
			success  : function(data){
				if(data){					
					if(data.result){
						success = true;
						parent.message = data.msg;
						//parent.member_name = data.member_name;
						parent.member_id = data.member_id;
						parent.member_acc = data.member_acc;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤!";
				}
			},
			error : function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試!"+thrownError;
			} 
		});	
		//console.log(parent.message);
		return success;	 
	},
	//會員登出
	memberLogout : function(){ 
		var parent = this,
		success = false;
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { actiontype : "memberLogout" },
			dataType  :"json",
			success : function(data){
				if(data){					
					if(data.result){
						success = true;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error : function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});	
		return success;	 
	},
	//FB登入取資料
	chkToken : function(member, path){
		var parent = this,
		success = false;
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
				accesstoken : member.accesstoken, 
				actiontype : "chkToken" 
			},
			dataType : "json",
			success : function(data){
				if(data){					
					if(data.result){
						
						success = true;
						parent.message = data.msg;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			}
			,error : function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});	
		return success;	 
	},
	//忘記密碼
	passwordMail : function(member){
		var parent = this,
		success = false;
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : {
				account : member.account,
				email : member.email, 
				actiontype : "passwordMail" 
			},
			dataType : "json",
			success : function(data){
				if(data){
					if(data.result){
						success = true;
						parent.message = data.msg;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error : function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});
		return success;		 
	},
	//會員修改
	update : function(member){ 
		var parent = this;
		success = false;
		$.ajaxSetup({ cache : false });
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
					name : member.name,
					email : member.email,
					phone : member.phone,
					address : member.address,
					old_pwd : member.old_pwd,
					new_pwd : member.new_pwd,
					new_pwd : member.new_pwd, 
					chk_val : member.chk_val,
					actiontype : "update" 
			},
			dataType : "json",
			success : function(data){
				if(data){
					if(data.result == true){
						success = true;
						parent.message = data.msg;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error : function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});
		return success;
	},
	//會員修改密碼
	changePassword : function(member){ 
		var parent = this;
		success = false;
		$.ajaxSetup({ cache : false });
		$.ajax({
			url : "do/memberdo.php",
			async : false,
			type : "POST",
			data : { 
					old_password : member.old_pwd,
					new_password : member.new_pwd, 
					actiontype : "changePassword" 
			},
			dataType : "json",
			success : function(data){
				if(data){
					if(data.result){
						success = true;
					}else{
						parent.message = data.msg;
					}
				}else{
					parent.message = "回傳資料錯誤";
				}
			},
			error : function(xhr, ajaxOptions, thrownError){
				parent.message = "讀取資料時發生錯誤,請梢候再試"+thrownError;
			}
		});
		return success;
	}
	
	 
}


function TestRgexp(str){ 
	var re = /^[0-9]*[1-9][0-9]*$/; 
    return re.test(str);
}

function orderSave(){
	var shop_id = $("#shop_id").val();
	var commend_email = $("commend_email").val();
	
	var recipient_name = $("#recipient_name").val();
	var recipient_phone = $("#recipient_phone").val();
	var recipient_email = $("#recipient_email").val();
	var recipient_zcode = $("#recipient_zcode").val();
	var recipient_address = $("#recipient_address").val();
	var company_invoice = $("#company_invoice").val();
	var payment_type = $("input[name='payment_type']:checked").val();
	
	$.ajax({
		url : 'do/order_save.php',
		async : false,
		type : 'POST',
		data : {
			shop_id : shop_id,
			commend_email : commend_email,
			recipient_name : recipient_name,
			recipient_phone : recipient_phone,
			recipient_email : recipient_email,
			recipient_zcode : recipient_zcode,
			recipient_address : recipient_address,
			company_invoice : company_invoice,
			payment_type : payment_type
		},
		dataType : "json",
		error : function(){
			alert('資料傳送發生錯誤!');
		},
		success : function(response){
			if(response.result){
				alert(response.msg);
				if(payment_type == 1){//信用卡
					location.href = "do/payment1.php";
				}else if(payment_type == 2){//7-11 IBON/全家FAMIPORT/萊爾富LIFE-ET/OK超商OK-GO
					location.href = "do/payment2.php";
				}else if(payment_type == 3){//超商代收
					location.href = "do/payment3.php";
				}else if(payment_type == 4){//貨到付款
					if(isLogin()){
						location.href = "bought.html";
					}else{
						location.href = "index.html";
					}
				}
			}else{
				alert(response.msg);
			} 
		}
	});
}
function chk_amount(amount){
	var result = false;
	if(amount != "" && TestRgexp(amount)){
		result = true;
	}
	return result;
}

//會員退貨
function returnOrder(order_sno, product_id, state){
	var success = false;
	$.ajax({
		url: 'do/return_order.php',
		type: 'POST',
		data : { 
			order_sno : order_sno,
			product_id : product_id,
			state : state
			},
		dataType: "json",
		async:false,
		error: function(){
			alert('資料傳送發生錯誤');
		},
		success: function(data){
			if(data.sResult){
				alert(data.msg);
				location.reload();
			}else{
				alert(data.msg);
			}			
		}
	});		
	//return success;
}

$(document).ready(function(e) {
	
	$(".cart_add_btn").click(function(e) {
		var product_id = $(this).attr("p_id");
		var product_sno = $(this).attr("p_sno");
		var product_name = $(this).attr("p_name");
		var sell_price = $(this).attr("p_price");
		var product_bonus = $(this).attr("p_bonus");
		var amount = $("#amount").val();
		var sub_total = (sell_price * amount);
		var pic = $(this).attr("pic");
		var shop_id = $(this).attr("shop_id");
		if(chk_amount(amount)){
			//console.log(amount);
			addCar(product_id, product_sno, product_name, sell_price, amount, sub_total, product_bonus, pic, shop_id);
		}else{
			alert("數量請輸入正整數!");
		}
    });
	
	$("input#amount").blur(function(e) {
        var amount = $(this).val();
		if(amount == ""){
			alert("請輸入數量!");
		}else if(!TestRgexp(amount)){
			alert("請輸入正整數!");
		}
    });
	
	//上方購物車刪除
	$("body").on("click", "a.remove-btn", function(e){
        var r = confirm("您確定要刪除此項商品!")
		if(r){
			var chang_amount = 0;
			var cart_id = $(this).attr("cart_id");
			var shop_id = $(this).attr("shop_id");
		  	var list = $(this).attr("list"); 
		  	updateCar(cart_id, shop_id, chang_amount, list);
		}
    });
	
	$("input[name='cart_unit_amount']").blur(function(e) {
	   var chang_amount = $(this).val();
	   var list = 0;
	   if(chang_amount == ""){
		   alert("請輸入數量!");
		   location.reload();
	   }else if(!TestRgexp(chang_amount)){
		   alert("請輸入正整數!");
		   location.reload();
	   }else{
		  var cart_id = $(this).parents("ul.shop_box").attr("cart_id");
		  var shop_id = $(this).parents("ul.shop_box").attr("shop_id");
		  updateCar(cart_id, shop_id, chang_amount, list);
	   }
    });
	
	$("span.shop_box_del").click(function(e) {
        var r = confirm("您確定要刪除此商品!")
		if(r){
			var chang_amount = 0;
			var cart_id = $(this).parents("ul.shop_box").attr("cart_id");
			var shop_id = $(this).parents("ul.shop_box").attr("shop_id");
		  	var list = 0; 
		  	updateCar(cart_id, shop_id, chang_amount, list);
		}
    });
	
	$("span.shop_box_favorite").click(function(e) {
        var r = confirm("您確定要移動此商品至收藏夾!")
		if(r){
			var chang_amount = 0;
			var cart_id = $(this).parents("ul.shop_box").attr("cart_id");
			var shop_id = $(this).parents("ul.shop_box").attr("shop_id");
		  	var list = 0; 
			var p_id = $(this).parents("ul.shop_box").attr("p_id");
			var m_id = $(this).parents("ul.shop_box").attr("m_id");
			if(!setFavoriteProduct(m_id, p_id)){
				updateCar(cart_id, shop_id, chang_amount, list);
			}
		}
    });
	
	
	$("input[name=commend]:radio").change(function(e) {
        var val = $(this).val();
		if(val == 1){
			$("input#commend_email").attr("readonly", false);
		}else{
			$("input#commend_email").val("").attr("readonly", true);
		}
    });

	$("a#cart_check_btn").click(function(e) {
        var shop = $(this).attr("shop_id");
		var carry = $("span.carry_show"+shop).text();
		if(carry == ""){
			alert("請選擇運送方式!");
		}else{
			if(isLogin()){
				window.location.href = "post_copy_"+shop+".html";
			}else{
				window.location.href = "post_copy_no_"+shop+".html";
			}
			
		}
    });
	
	$("span.top_cart_over, div.topcart_pcs").click(function(e){
        if(isLogin()){
			location.href = "cart.html";
		}else{
			location.href = "cart_no.html";
		}
    });
	
	$("a.check_out").click(function(e) {
        //$("form#pay_form").submit();
		orderSave();
    });
	
	$("a.order-return-btn").click(function(e) {
        var sno = $(this).attr("sno");
		var p_id = $(this).attr("p_id");
		var state = 1;
		returnOrder(sno, p_id, state);
		
    });
});
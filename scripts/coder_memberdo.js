var myMember = new coderMember();

//會員新增 
function member_insert(){
	var _item = new Object();
	_item.account = $('.join_acc').val();
	_item.password = $('.join_pwd').val();
	
	if(myMember.insert(_item, 'insert')) {
		alert("會員加入完成!");
		if(myMember.memberLogin(_item)){
			//window.location.href = "index.html";
			window.location.reload();
		}
	}else{
		alert("會員加入失敗！");
	}
}
//會員登出
function member_logout(){
	if(myMember.memberLogout()) {
		var _file = window.location.pathname;
		_file = _file.split(/(\\|\/)/g).pop();
		//console.log(_file);
		if(_file == "order.html"){
			window.location.href = "index.html";
		}else{
			window.location.reload();
		}
		
	}else{
		alert("會員登出失敗!請再試一次!");
	}
}
//會員登入
function member_login(){
	var _item = new Object();
	_item.account = $('.login_acc').val();
	_item.password = $('.login_pwd').val();
	
	if(myMember.memberLogin(_item)) {
		window.location.reload();
		/*var _file = window.location.pathname;
		_file = _file.split(/(\\|\/)/g).pop();
		window.location.href = _file;*/
	}else{
		alert(myMember.message);
	}
}
//會員登入購物車
function member_login2(){
	var _item = new Object();
	_item.account = $('.login_acc2').val();
	_item.password = $('.login_pwd2').val();
	
	if(myMember.memberLogin(_item)) {
		window.location.href = "checkoutstep1.html";
	}else{
		alert(myMember.message);
	}
}
//忘記密碼
function member_forget_pwd(){
	var forget_mail = $('.forget_acc').val();
	
	if(myMember.passwordMail(forget_mail)) {
		alert('新密碼己寄出,請至您所填寫的Email中收取!');
		$('#dialogForgetPassword').closest('.dialog').dialog('close');
		$('#dialogSigninMember').dialog('open');
	}else{
		alert(myMember.message);
	}
}
//會員FB登入
function member_fb_login(){
	//$('.dialog').dialog('close');
	fbLoginMember("i");
}
//會員修改
function member_modify(member){
	
	if(myMember.update(member)) {
		alert('您的資料己修改完成!');
		window.location.reload();
	}else{
		alert("修改會員資料失敗！ <br> " + myMember.message);
	}
}
//會員修改密碼
function member_change_password(member){
	
	if(myMember.changePassword(member)){
		alert('密碼修改完成!');
		window.location.reload();
	}else{
		alert("密碼修改失敗！\n" + myMember.message);
	}
}
//會員是否登入
function isLogin(){
	var success = false;
	$.ajax({
		url: 'do/check_login.php',
		type: 'POST',
		dataType: "json",
		async:false,
		error: function(){
			alert('發生錯誤');
		},
		success: function(data){
			success= data.sResult;			
		}
	});		
	return success;
}
//會員取消訂單
function cancelOrder(order_sno, state){
	var success = false;
	$.ajax({
		url: 'do/cancel_order.php',
		type: 'POST',
		data : { 
			order_sno : order_sno,
			state : state
			},
		dataType: "json",
		async:false,
		error: function(){
			alert('發生錯誤');
		},
		success: function(data){
			success = data.sResult;			
		}
	});		
	return success;
}
//會員退貨
function returnOrder(order_sno, state){
	var success = false;
	$.ajax({
		url: 'do/return_order.php',
		type: 'POST',
		data : { 
			order_sno : order_sno,
			state : state
			},
		dataType: "json",
		async:false,
		error: function(){
			alert('發生錯誤');
		},
		success: function(data){
			success = data.sResult;			
		}
	});		
	return success;
}

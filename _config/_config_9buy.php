<?php
/*****Initialization*****/
ini_set('display_errors', 1);   //0不顯示 1顯示
ini_set('error_reporting', E_ALL | E_STRICT);	//report all errors
ini_set("magic_quotes_runtime", 0);
date_default_timezone_set("Asia/Taipei");
mb_internal_encoding("UTF-8");
ob_start();
if(!isset($_SESSION)){
    session_start();
}
header("Content-type: text/html; charset=utf-8");


/*****Web Meta*****/
$web_name = "9好買購物中心";
$web_manage_name = "後台管理系統-Neptunus V1.0";
$description = "";
$keywords = "";
$author = "CODER 誠智數位";
$copyright = "9好買 © 2015";
$favicon = "images/favicon.ico";

$web_url = "http://9buy.tw/";
$web_url_manage = $web_url."Web_Manage/";
$web_email = "bill@coder.com.tw"; //網站管理者email (寄送後台忘記密碼email確認信)


/*****Database*****/
$HS = "localhost";
$ID = "buytw_925";
$PW = "11oR40Gxxq";
$DB = "buytw_925";


/*****SMTP Server*****/
$smtp_auth = false;
$smtp_host = "127.0.0.1";
$smtp_port = 25;
$smtp_id   = "";
$smtp_pw   = "";


/*****Upload path*****/
/*admin*/
$web_path_admin = "upload/admin/";
$admin_path_admin = "../../upload/admin/";

/*ad*/
$web_path_ad = "upload/ad/";
$admin_path_ad = "../../upload/ad/";

/*banner*/
$web_path_banner = "upload/banner/";
$admin_path_banner = "../../upload/banner/";

/*product*/
$web_path_product = "upload/product/";
$admin_path_product = "../../upload/product/";

/*ptype1*/
$web_path_ptype1 = "upload/ptype1/";
$admin_path_ptype1 = "../../upload/ptype1/";

/*temp*/
$admin_path_temp="../../upload/temp/";

//$template_path = 'template/';


/*****Image setup*****/
/*admin*/
$admin_bpic_w = 225;
$admin_bpic_h = 225;

$admin_spic_w = 226;
$admin_spic_h = 112;

/*ad*/
$ad_pic_w = 216;
$ad_pic_h = 78;

/*banner*/
$banner_pic_w = 705;
$banner_pic_h = 310;

/*ptype1*/
$ptype1_pic_w = 20;
$ptype1_pic_h = 20;

/*product*/
$product_bpic_w = 380;
$product_bpic_h = 380;

$product_spic_w = 80;
$product_spic_h = 80;


/*****Cache name*****/
$web_cache = array('ptype1' => 'ptype1', 'ptype2' => 'ptype2', 'shop' => 'shop', 'banner' => 'banner', 'ad' => 'ad', 'freight' => 'freight');


/*****Web Array*****/
$ary_yn = array("否", "是");
$ary_payment_type = array(1 => '信用卡線上刷卡', 2 => '7-11 IBON/全家FAMIPORT/萊爾富LIFE-ET/OK超商OK-GO', 3 => '超商代收', 4 => '貨到付款');  //付款方式
$ary_payment_state = array(0 => '未付款', 1 => '已付款');  //付款狀態
$ary_order_state = array(0 => '處理中', 1 => '可出貨', 2 => '己出貨', 3 => '交易完成', 11 => '交易取消', 12 => '交易失敗', 30 => '退貨', 31 => '退貨中', 32 => '退貨完成', 13 => '退款完成');  //訂單狀態

$ary_invoice = array("1" => "電子發票", "2" => "紙本發票", "9" => "捐贈發票");
$ary_carry = array("1" => "宅急便", "2" => "郵局");


/*****Common Variable*****/
$iCache_ExpireHour = 24;
$iCookMainExpireDay = 30;

$null_date = '-0001-11-30';
//$null_date='1999-11-30';

$slash = (strstr(dirname(__FILE__), '/')) ? "/" : "\\";
define("CONFIG_DIR", dirname(__FILE__).$slash);


/*ckeditor*/
$path_ckeditor = '/925/upload/ckeditor/';  //ckeditor中路徑
$admin_path_ckeditor = "../../upload/ckeditor/";  //上傳放置(以後台位置來看)
$db_path_ckeditor = 'upload/ckeditor/';  //存入資料庫時改為
$web_path_ckeditor = 'upload/ckeditor/';  //前台ck路徑

/*Cache path*/
$path_cache = 'upload/cache/';
$web_path_cache = $path_cache;
$admin_path_cache = '../../'.$path_cache;
$freight_path_cache = '../'.$path_cache;

/*System Email*/
$sys_email = "do_not_reply@service.com.tw";
$sys_name = "Web Shop";

/*FB APP*/
$fb_appid = "1695421070674792";  //9buy
$fb_appsecrect = "ebf4b89257109954474e8ee49f4bb939";  //9buy

/*Online Payment*/
$M_ID = ""; 
$MAC_KEY = ""; 


/*****Include*****/
require_once(CONFIG_DIR."_func.php");
require_once(CONFIG_DIR."_database.class.php");
require_once(CONFIG_DIR."_func_smtp.php");

/*****LIB Autoload*****/
function __autoload($classname){
	global $slash;
	if(strlen($classname) > 6 && mb_substr($classname, 0, 6) == 'class_'){
        $filename = CONFIG_DIR . "class" . $slash . strtolower($classname) . ".php";
    }else{
        $filename = CONFIG_DIR . "lib" . $slash . strtolower($classname) . ".php";
    }
	
	if(file_exists($filename)){
		include_once($filename);
	}else{
		echo 'notfound:'.$filename;
	}
}

/*****END PHP*****/
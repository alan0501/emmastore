﻿<?php
include_once('_config.php');
include_once($inc_path."lib/coderorder.php");
include_once($inc_path."lib/coderorderobj.php");
include_once($inc_path.'_func_smtp.php');

function getParameter($pname){
	return isset($_POST[$pname])?$_POST[$pname]:"";
}

$code               = "abcd1234";
$merchantnumber     = getParameter('merchantnumber');
$ordernumber        = getParameter('ordernumber');
$amount             = getParameter('amount');
$paymenttype        = getParameter('paymenttype');

$serialnumber       = getParameter('serialnumber');
$writeoffnumber     = getParameter('writeoffnumber');
$timepaid           = getParameter('timepaid');
$tel                = getParameter('tel');
$hash               = getParameter('hash');

$verify = md5("merchantnumber=".$merchantnumber.
		   "&ordernumber=".$ordernumber.
		   "&serialnumber=".$serialnumber.
		   "&writeoffnumber=".$writeoffnumber.
		   "&timepaid=".$timepaid.
		   "&paymenttype=".$paymenttype.
		   "&amount=".$amount.
		   "&tel=".$tel.
		   $code);

//print "verify=".$verify;
if(strtolower($hash)!=strtolower($verify)){
  //-- 驗證碼錯誤，資料可能遭到竄改，或是資料不是由ezPay簡單付發送
  /*print "驗證碼錯誤!".
		"\nhash=".hash.
		"\nmerchantnumber=".$merchantnumber.
		"\nordernumber=".$ordernumber.
		"\nserialnumber=".$serialnumber.
		"\nwriteoffnumber=".$writeoffnumber.
		"\ntimepaid=".$timepaid.
		"\npaymenttype=".$paymenttype.
		"\namount=".$amount.
		"\ntel=".$tel;*/
}else{
	$data["order_payment_state"] = 1;
	$data["order_pay_time"] = request_cd();
	$data["order_payment_back"] = serialize("merchantnumber=".$merchantnumber."/ordernumber=".$ordernumber."/amount=".$amount."/paymenttype=".$paymenttype."/serialnumber=".$serialnumber."/writeoffnumber=".$writeoffnumber."/timepaid=".$timepaid."/tel=".$tel."/hash=".$hash);
	
	$data_b["bonus_is_use"] = 1;
 
	$db = new Database($HS, $ID, $PW, $DB);
	$db -> connect();
	
	$db -> query_update(coderDBConf::$order, $data, "order_sno='$ordernumber'");
	$db -> query_update(coderDBConf::$bonus, $data_b, "bonus_order_sno='$ordernumber'");
	
	$coderorder = new CoderOrder();
	$coderorder -> paySuccessMail($ordernumber);
 
  //-- 驗證正確，請更新資料庫訂單狀態
  /*print "驗證碼正確!".
		"\nmerchantnumber=".$merchantnumber.
		"\nordernumber=".$ordernumber.
		"\nserialnumber=".$serialnumber.
		"\nwriteoffnumber=".$writeoffnumber.
		"\ntimepaid=".$timepaid.
		"\npaymenttype=".$paymenttype.
		"\namount=".$amount.
		"\ntel=".$tel;*/
}


<?php 
include_once('_config.php');
include_once($inc_path."lib/coderorder.php");
include_once($inc_path."lib/coderorderobj.php");
include_once($inc_path.'_func_smtp.php');

function getParameter($pname){
	return isset($_POST[$pname])?$_POST[$pname]:"";
}
$final_result = getParameter('final_result');
$P_MerchantNumber = getParameter('P_MerchantNumber');
$P_OrderNumber = getParameter('P_OrderNumber');
$P_Amount = getParameter('P_Amount');
$P_CheckSum = getParameter('P_CheckSum');
$final_return_PRC = getParameter('final_return_PRC');
$final_return_SRC = getParameter('final_return_SRC');
$final_return_ApproveCode = getParameter('final_return_ApproveCode');
$final_return_BankRC = getParameter('final_return_BankRC');
$final_return_BatchNumber = getParameter('final_return_BatchNumber');
/*$MerchantName="";
if($P_MerchantNumber != ""){
	if( $P_MerchantNumber == "715332"){
		$MerchantName="中國信託";
	}else if( $P_MerchantNumber == "715333"){
		$MerchantName="聯邦銀行";
	}else if( $P_MerchantNumber == "715322"){
		$MerchantName="國泰世華";
	}
}*/

$Code = "abcd1234";

$msg = "";
$turl = "";
$has_cart = $_SESSION["has_cart"];


$db = new Database($HS, $ID, $PW, $DB);
$db -> connect();

$coderorder = new CoderOrder();

if($final_result=="1"){
	//$_SESSION['order_sno'] = $P_OrderNumber;
	
	if(strlen($P_CheckSum)>0){
		$checkstr = md5($P_MerchantNumber.$P_OrderNumber.$final_result.$final_return_PRC.$Code.$final_return_SRC.$P_Amount);
		if(strtolower($checkstr) != strtolower($P_CheckSum)){
			$msg = "交易發生問題，驗證碼錯誤!!請重新購買!";
			$data["order_state"] = 12;
			$coderorder -> payFaileMail($P_OrderNumber);
		}else{
			$data["order_payment_state"] = 1;
			$data["order_pay_time"] = request_cd();
			$data_b["bonus_is_use"] = 1;
			$msg = "交易成功!!";
			$coderorder -> paySuccessMail($P_OrderNumber);
		}
	}else{
		$data["order_payment_state"] = 1;
		$data["order_pay_time"] = request_cd();
		$data_b["bonus_is_use"] = 1;
		$msg = "交易成功!!";
		$coderorder -> paySuccessMail($P_OrderNumber);
	}
	
	if(isLogin() && $has_cart == "YES"){
		$turl = "cart.html";
	}else if(!isLogin() && $has_cart == "YES"){
		$turl = $web_url."cart_no.html";
	}else{
		$turl = $web_url."index.html";
	}
	
}else{
	$data["order_state"] = 12;
	
	if($final_return_PRC=="8" && $final_return_SRC=="204"){
		$msgs= "交易失敗-->訂單編號重複!";
	}else if($final_return_PRC=="34" && $final_return_SRC=="171"){
		$msgs= "交易失敗-->金融上的失敗!";
		$msgs.= "  銀行回傳碼=[".$final_return_BankRC."]<br>";
	}else{
		$msgs= "交易失敗-->請與商家聯絡!";
	}
	
	$msg = "付款未成功!請重新購買!";
	if(isLogin() && $has_cart == "YES"){
		$turl = $web_url."cart.html";
	}else if(!isLogin() && $has_cart == "YES"){
		$turl = $web_url."cart_no.html";
	}else{
		$turl = $web_url."index.html";
	}
	
	$coderorder -> payFaileMail($P_OrderNumber);
}

$data["order_payment_back"] = serialize("final_result=".$final_result."/P_MerchantNumber=".$P_MerchantNumber."/P_OrderNumber=".$P_OrderNumber."/P_Amount=".$P_Amount."/P_CheckSum=".$P_CheckSum."/final_return_PRC=".$final_return_PRC."/final_return_SRC=".$final_return_SRC."/final_return_ApproveCode=".$final_return_ApproveCode."/final_return_BankRC=".$final_return_BankRC."/final_return_BatchNumber=".$final_return_BatchNumber);

$db -> query_update(coderDBConf::$order, $data, "order_sno='$P_OrderNumber'");
$db -> query_update(coderDBConf::$bonus, $data_b, "bonus_order_sno='$P_OrderNumber'");

$db -> close();

script($msg, $turl);


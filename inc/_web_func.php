<?php
function isLogin(){
	return (((isset($_SESSION["session_925_acc"]) && trim($_SESSION["session_925_acc"]) != "") || (isset($_SESSION["session_925_fb"]) && trim($_SESSION["session_925_fb"]) != "")) && isset($_SESSION["session_925_id"]) && trim($_SESSION["session_925_id"] != ""));
}

function getCartInfo(){
	global $cart_item, $cart_total;
	$car = array();
	$cart_item = 0;
	$cart_total = 0;
	//print_r($_SESSION["car"]);
	if(isset($_SESSION["car"])){
		$car = unserialize($_SESSION["car"]);
	}
	//var_dump($_SESSION["car"]);
	foreach($car as $item){
		$cart_item += $item -> num; //商品數
		$cart_total += $item -> total; //總金額
	}
}

function show_carry($key){
	global $ary_carry;
	foreach($ary_carry as $val => $name){
		if($key == $val){
			return $name;
		}
	}
}


function getQueryAry(){
	$q = post('q',1);

	if($q == ''){
		throw new Exception('傳輸參數錯誤');
	}
	$query = coderAES256::Decrypt($q);

	parse_str($query, $data);//將查询字符串解析到變量(ex.id=23=>$id=23)

	if(!$data || !is_array($data)){
		throw new Exception('解碼錯誤');
	}	
	return $data;
}


function getErrorMsg($msg, $reload){
	return '<?xml version="1.0" encoding="utf-8" ?><data> <sReturn sReturn="False" msg="'.$msg.'" exception="'.($reload ? 'true' : 'false').'"/></data>';

}


/*****END PHP*****/
<?php
function getsql($sql, $page_size, $page_querystring) {
    global $page, $sql, $count, $page_count, $pre, $next, $querystring, $HS, $ID, $PW, $DB, $db;
    $querystring = clearPageStr($page_querystring);

    $page = get("page")!='' && get("page")>0 ? get("page") : 1;

    $count = $db -> queryCount($sql);
    $page_count = ceil($count / $page_size);
    if ($page > $page_count) {
        $page = $page_count;
    }
    if ($page <= 0) {
        $page = 1;
    }
    $start = number_format(($page-1)*$page_size,0,'.','');

    $pre = $page - 1;
    $next = $page + 1;
    $first = 1;
    $last = $page_count;
    $sql.= " limit $start,$page_size";
    return $count;
}

function pagesql() {
    global $sql;
    return $sql;
}
function showpage() {
    global $page, $page_count, $count, $pre, $next, $querystring;
    if ($querystring != "") {
        $querystring = $querystring . "&";
    }
    echo $page . ' / ' . $page_count . '&nbsp;&nbsp;共' . $count . '筆資料&nbsp;&nbsp;';
    if ($page != 1) {
        echo '<a href=?' . $querystring . 'page=1>首頁</a>&nbsp;&nbsp;
           <a href=?' . $querystring . 'page=' . $pre . '>上一頁</a>&nbsp;&nbsp;';
    }
    $viewpage = 5;

    if ($page_count > $viewpage) {
        if ($page - $viewpage < 0) {
            $s = 1;
            $j = $viewpage;
        }else {
            $s = (int)($page-$viewpage+1);
            if($s===0){$s++;}
            $j = $s + 5;
            if ($j >= $page_count) {
                $j = $page_count;
            }
            //$j = $page_count;
        }
    }else {
        $s = 1;
        $j = $page_count;
    }
    for ($i = $s; $i <= $j; $i++) {
        $num = $i;
        if ($page == $num) {
            echo $num . "&nbsp;";
        }else {
            echo '<a href=?' . $querystring . 'page=' . $num . '>' . $num . '</a>&nbsp;&nbsp;';
        }
    }
    if ($page < $page_count) {
        echo '<a href=?' . $querystring . 'page=' . ($page + 1) . '>下一頁</a>&nbsp;&nbsp;';
        echo '<a href=?' . $querystring . 'page=' . $page_count . '>末頁</a>&nbsp;';
    }
}

function clearPageStr($querystring) {

    $pageind = strpos($querystring, '&page=');
    if ($pageind !== false) {

        $pageind2 = strpos(substr($querystring, $pageind + 6), '&');
        $querystring_ = substr($querystring, 0, $pageind);

        if ($pageind2 !== false) {
            $querystring_.= substr($querystring, $pageind + 6 + $pageind2);
        }
    }
    else {
        $querystring_ = $querystring;
    }
    return $querystring_;
}


function showpageForMom() {
    global $page, $page_count, $count, $pre, $next, $querystring;
    $showpage = "";
    if ($querystring != "") {
        $querystring = $querystring . "&";
    }
    $showpage.= '<span class="count">共' . $count . '件商品</span>';
    if ($page != 1) {
        $showpage.= '<a href=?' . $querystring . 'page=1 title="最前頁" class="number pre">最前頁</a>
        <a href=?' . $querystring . 'page=' . $pre . ' title="上頁" class="number pre">上頁</a>' . "\n";
    }
    $viewpage = 5;

    if ($page_count > $viewpage) {
        if ($page - $viewpage < 0) {
            $s = 1;
            $j = $viewpage;
        }
        else {
            $s = (int)($page-$viewpage+1);
            if($s===0){$s++;}
            $j = $s + 5;
            if ($j >= $page_count) {
                $j = $page_count;
            }

            //$j = $page_count;

        }
    }
    else {
        $s = 1;
        $j = $page_count;
    }
    for ($i = $s; $i <= $j; $i++) {
        $num = $i;
        if ($page == $num) {
            $showpage.= '<a href=?' . $querystring . 'page=' . $num . ' title="p' . $num . '" class="number down"><span>' . $num . '</span></a>' . "\n";
        }
        else {
            $showpage.= '<a href=?' . $querystring . 'page=' . $num . ' title="p' . $num . '" class="number"><span>' . $num . '</span></a>' . "\n";
        }
    }

    if ($page < $page_count) {
        $showpage.= '<a href=?' . $querystring . 'page=' . ($page + 1) . ' title="下頁" class="number next">下頁</a>
      <a href=?' . $querystring . 'page=' . $page_count . ' title="最後頁" class="number next">最後頁</a>';
    }
    return $showpage;
}




/*************************************************************************************************************************/
function showPage_publication_teacher(){//前端用顯示頁數
    global $page, $page_count, $count, $pre, $next, $querystring,$type;
    $pagedata = "";
    $viewpage = 5;//頁碼顯示數量
    if($querystring != ""){
        $querystring = $querystring."&";
    }
	if($page != 1){
      $pagedata .= '<li class="left_index"><a href="javascript:void(0)" onclick="get_teacherlist(1)" title="首頁"><< </a></li><li class="left"><a href="javascript:void(0)" onclick="get_teacherlist('.($page-1).')" title="上一頁"> < </a></li>';
    }
    if($page_count > $viewpage){
      if($page < ceil($viewpage/2)){
        $s = 1;$e = $viewpage;
      }else{
        $s = (int)($page-$viewpage+ceil($viewpage/2));
        if($s===0){$s++;}
        $e = $s+$viewpage-1;
        if($e >= $page_count){
          $e = $page_count;
        }
      }
    }else{
      $s = 1;
      $e = $page_count;
    }

    for($i = $s;$i <= $e;$i++){
      $num = $i;
      if($page == $num){
        $pagedata .= '<li><a href="javascript:void(0)" class="active">'.$num.'</a></li>';
      }else{
        $pagedata .= '<li><a href="javascript:void(0)" onclick="get_teacherlist('.$num.')">'.$num.'</a></li>';
      }
    }

	if($page < $page_count){
      $pagedata .= '<li class="right"><a href="javascript:void(0)" onclick="get_teacherlist('.($page+1).')" title="下一頁">  ></a></li><li class="right_last"><a href="javascript:void(0)" onclick="get_teacherlist('.$page_count.')" title="末頁"> >></a></li>';
    }
    return $pagedata;
}

function showpage_new($url) {
	global $page, $page_count, $count, $pre, $next, $querystring;
	
    $pagedata = "";
    $viewpage = 10;//頁碼顯示數量
    if($querystring != ""){
        $querystring = $querystring."&";
    }
    if($page != 1){
      $pagedata .= '<li class="left_index"><a href="'.$url["left_index"].'"><< 首頁</a></li>';
	  $pagedata .= '<li class="left"><a href="'.$url["left"].'"> < 上ㄧ頁</a></li>';
    }
    if($page_count > $viewpage){
      if($page < ceil($viewpage/2)){
        $s = 1;$e = $viewpage;
      }else{
        $s = (int)($page-$viewpage+ceil($viewpage/2));
        if($s===0){$s++;}
        $e = $s+$viewpage-1;
        if($e >= $page_count){
          $e = $page_count;
        }
      }
    }else{
      $s = 1;
      $e = $page_count;
    }
	
    for($i = $s;$i <= $e;$i++){
      $num = $i;
      if($page == $num){
        $pagedata .= '<li><a href="javascript:void(0)" class="active">'.$num.'</a></li>';
      }else{
        $pagedata .= '<li><a href="'.str_replace ('"0"',$num,$url["num"]).'">'.$num.'</a></li>';
      }
    }

    if($page < $page_count){
      $pagedata .= '<li class="right"><a href="'.$url["right"].'"> 下ㄧ頁 ></a></li>';
	  $pagedata .= '<li class="right_last"><a href="'.$url["right_last"].'">末頁 >></a></li>';
    }
    return $pagedata;
}

function showPage_search_products(){//前端用顯示頁數 產品(作品)搜尋
    global $page, $page_count, $count, $pre, $next, $querystring,$search_text,$mtype;
	$url["left_index"] = 'search_products_'.$search_text.'_1'.'_'.$mtype.'.html'; //首頁
	$url["left"] = 'search_products_'.$search_text.'_'.($page-1).'_'.$mtype.'.html';//上一頁
	$url["num"] = 'search_products_'.$search_text.'_"0"_'.$mtype.'.html'; //頁
	$url["right"] = 'search_products_'.$search_text.'_'.($page+1)."_".$mtype.'.html'; //下一頁
	$url["right_last"] = 'search_products_'.$search_text.'_'.$page_count."_".$mtype.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_aboutnew(){//前端用顯示頁數
    global $page, $page_count, $count, $pre, $next, $querystring,$type;
	$url["left_index"] = 'about_new_'.$type.'_1.html'; //首頁
	$url["left"] = 'about_new_'.$type.'_'.($page-1).'.html';//上一頁
	$url["num"] = 'about_new_'.$type.'_"0".html'; //頁
	$url["right"] = 'about_new_'.$type.'_'.($page+1).'.html'; //下一頁
	$url["right_last"] = 'about_new_'.$type.'_'.$page_count.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_sfc(){//前端用顯示頁數  特色課程
    global $page, $page_count, $count, $pre, $next, $querystring,$type_id,$sfct_id;
    $url["left_index"] = 'school_feature_class_'.$type_id.'_'.$sfct_id.'_1.html'; //首頁
	$url["left"] = '"school_feature_class_'.$type_id.'_'.$sfct_id.'_'.($page-1).'.html';//上一頁
	$url["num"] = 'school_feature_class_'.$type_id.'_'.$sfct_id.'_"0".html'; //頁
	$url["right"] = ''; //下一頁
	$url["right_last"] = ''; //末頁
	echo showpage_new($url);
}

function showPage_lv(){//前端用顯示頁數 清涼音映像
    global $page, $page_count, $count, $pre, $next, $querystring,$type_id,$lvt_id,$keyword;
    $url["left_index"] = 'lecture_video_'.$type_id.'_'.$lvt_id.'_1_'.$keyword.'.html'; //首頁
	$url["left"] = 'lecture_video_'.$type_id.'_'.$lvt_id.'_'.($page-1).'_'.$keyword.'.html';//上一頁
	$url["num"] = 'lecture_video_'.$type_id.'_'.$lvt_id.'_"0"_'.$keyword.'.html'; //頁
	$url["right"] = 'lecture_video_'.$type_id.'_'.$lvt_id.'_'.($page+1).'_'.$keyword.'.html'; //下一頁
	$url["right_last"] = 'lecture_video_'.$type_id.'_'.$lvt_id.'_'.$page_count.'_'.$keyword.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_lc(){//前端用顯示頁數 清涼音專欄
    global $page, $page_count, $count, $pre, $next, $querystring,$type_id,$lct_id;
    $url["left_index"] = 'lecture_column_'.$type_id.'_'.$lct_id.'_1.html'; //首頁
	$url["left"] = 'lecture_column_'.$type_id.'_'.$lct_id.'_'.($page-1).'.html';//上一頁
	$url["num"] = 'lecture_column_'.$type_id.'_'.$lct_id.'_"0".html'; //頁
	$url["right"] = 'lecture_column_'.$type_id.'_'.$lct_id.'_'.($page+1).'.html'; //下一頁
	$url["right_last"] = 'lecture_column_'.$type_id.'_'.$lct_id.'_'.$page_count.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_ln(){//前端用顯示頁數 清涼音電子報
    global $page, $page_count, $count, $pre, $next, $querystring,$type_id,$lnt_id;
    $url["left_index"] = 'lecture_newsletter_'.$type_id.'_'.$lnt_id.'_1.html'; //首頁
	$url["left"] = 'lecture_newsletter_'.$type_id.'_'.$lnt_id.'_'.($page-1).'.html';//上一頁
	$url["num"] = 'lecture_newsletter_'.$type_id.'_'.$lnt_id.'_"0".html'; //頁
	$url["right"] = 'lecture_newsletter_'.$type_id.'_'.$lnt_id.'_'.($page+1).'.html'; //下一頁
	$url["right_last"] = 'lecture_newsletter_'.$type_id.'_'.$lnt_id.'_'.$page_count.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_lcc(){//前端用顯示頁數 清涼音公開課程
    global $page, $page_count, $count, $pre, $next, $querystring,$type_id,$lc_id,$keyword;
    $url["left_index"] = 'lecture_cln_class_'.$type_id.'_'.$lc_id.'_1_'.$keyword.'.html'; //首頁
	$url["left"] = 'lecture_cln_class_'.$type_id.'_'.$lc_id.'_'.($page-1).'_'.$keyword.'.html';//上一頁
	$url["num"] = 'lecture_cln_class_'.$type_id.'_'.$lc_id.'_"0"_'.$keyword.'.html'; //頁
	$url["right"] = 'lecture_cln_class_'.$type_id.'_'.$lc_id.'_'.($page+1).'_'.$keyword.'.html'; //下一頁
	$url["right_last"] = 'lecture_cln_class_'.$type_id.'_'.$lc_id.'_'.$page_count.'_'.$keyword.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_lch(){//前端用顯示頁數 清涼音公益講座
    global $page, $page_count, $count, $pre, $next, $querystring,$type_id,$lc_id,$keyword;
    $url["left_index"] = 'lecture_charity_'.$type_id.'_'.$lc_id.'_1_'.$keyword.'.html'; //首頁
	$url["left"] = 'lecture_charity_'.$type_id.'_'.$lc_id.'_'.($page-1).'_'.$keyword.'.html';//上一頁
	$url["num"] = 'lecture_charity_'.$type_id.'_'.$lc_id.'_"0"_'.$keyword.'.html'; //頁
	$url["right"] = 'lecture_charity_'.$type_id.'_'.$lc_id.'_'.($page+1).'_'.$keyword.'.html'; //下一頁
	$url["right_last"] = 'lecture_charity_'.$type_id.'_'.$lc_id.'_'.$page_count.'_'.$keyword.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_laa(){//前端用顯示頁數 清涼音全國講座
    global $page, $page_count, $count, $pre, $next, $querystring,$type_id,$lc_id,$keyword;
    $url["left_index"] = 'lecture_all_area_'.$type_id.'_'.$lc_id.'_1_'.$keyword.'.html'; //首頁
	$url["left"] = 'lecture_all_area_'.$type_id.'_'.$lc_id.'_'.($page-1).'_'.$keyword.'.html';//上一頁
	$url["num"] = 'lecture_all_area_'.$type_id.'_'.$lc_id.'_"0"_'.$keyword.'.html'; //頁
	$url["right"] = 'lecture_all_area_'.$type_id.'_'.$lc_id.'_'.($page+1).'_'.$keyword.'.html'; //下一頁
	$url["right_last"] = 'lecture_all_area_'.$type_id.'_'.$lc_id.'_'.$page_count.'_'.$keyword.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_soc(){//前端用顯示頁數 教育學苑  公開課程
    global $page, $page_count, $count, $pre, $next, $querystring,$soct2_id,$type_id;
    $url["left_index"] = 'school_open_class_'.$soct2_id.'_'.$type_id.'_1.html'; //首頁
	$url["left"] = 'school_open_class_'.$soct2_id.'_'.$type_id.'_'.($page-1).'.html';//上一頁
	$url["num"] = 'school_open_class_'.$soct2_id.'_'.$type_id.'_"0".html'; //頁
	$url["right"] = 'school_open_class_'.$soct2_id.'_'.$type_id.'_'.($page+1).'.html'; //下一頁
	$url["right_last"] = 'school_open_class_'.$soct2_id.'_'.$type_id.'_'.$page_count.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_sr(){//前端用顯示頁數 教育學苑  課程回顧
    global $page, $page_count, $count, $pre, $next, $querystring,$srt2_id,$type_id;
    $url["left_index"] = 'school_review_'.$srt2_id.'_'.$type_id.'_1.html'; //首頁
	$url["left"] = 'school_review_'.$srt2_id.'_'.$type_id.'_'.($page-1).'.html';//上一頁
	$url["num"] = 'school_review_'.$srt2_id.'_'.$type_id.'_"0".html'; //頁
	$url["right"] = 'school_review_'.$srt2_id.'_'.$type_id.'_'.($page+1).'.html'; //下一頁
	$url["right_last"] = 'school_review_'.$srt2_id.'_'.$type_id.'_'.$page_count.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_pub_event(){//前端用顯示頁數 影音出版-促銷活動
    global $page, $page_count, $count, $pre, $next, $querystring;
    $url["left_index"] = 'publication-news_event_1.html'; //首頁
	$url["left"] = 'publication-news_event_'.($page-1).'.html';//上一頁
	$url["num"] = 'publication-news_event_"0".html'; //頁
	$url["right"] = 'publication-news_event_'.($page+1).'.html'; //下一頁
	$url["right_last"] = 'publication-news_event_'.$page_count.'.html'; //末頁
	echo showpage_new($url);
}

function showPage_search_teachers(){//前端用顯示頁數 講師搜尋
    global $page, $page_count, $count, $pre, $next, $querystring,$search_text;
    $url["left_index"] = 'search_teachers_'.$search_text.'_1'.'.html'; //首頁
	$url["left"] = 'search_teachers_'.$search_text.'_'.($page-1).'.html';//上一頁
	$url["num"] = 'search_teachers_'.$search_text.'_"0".html'; //頁
	$url["right"] = 'search_teachers_'.$search_text."_".($page+1).'.html'; //下一頁
	$url["right_last"] = 'search_teachers_'.$search_text."_".$page_count.'.html'; //末頁
	echo showpage_new($url);
}

?>

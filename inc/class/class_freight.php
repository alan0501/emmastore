<?php
class class_freight{
	private static $_ary1 = null;

	public static function getList(){
		global $web_cache;
		$colname_freight = coderDBConf::$col_freight;
		if(self::$_ary1 == null){
			self::$_ary1 = getWebCache("SELECT * 
										FROM ".coderDBConf::$freight." 
										ORDER BY {$colname_freight['id']} DESC", $web_cache['freight']);
		}
		return self::$_ary1;
	}

	public static function clearCache(){
		global $web_cache;
		self::$_ary1 = null;
		clearCache($web_cache['freight']);
	}
}

/*****END PHP*****/
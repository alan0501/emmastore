<?php
class class_ptype1{
	private static $_ary1 = null;

	public static function getList(){//取name,value及pid
		global $web_cache;
		$colname = coderDBConf::$col_ptype1;
		if(self::$_ary1 == null){
			self::$_ary1 = getWebCache("SELECT {$colname['name']} AS name, {$colname['id']} AS value 
										FROM ".coderDBConf::$ptype1." 
										WHERE {$colname['is_show']} = 1 
										ORDER BY {$colname['ind']} DESC", $web_cache['ptype1']);
		}
		return self::$_ary1;
	}
	
	
	/*public static function getPublicList(){
	    global $db;
	    $colname = coderDBConf::$col_market_1sttype;
	    $sql = "select {$colname['name']} ,{$colname['id']} from ".coderDBConf::$market_1sttype." WHERE `{$colname['ispublic']}`=1 ORDER BY `{$colname['ind']}` DESC";
	    return $db -> fetch_all_array($sql);
	}*/
	
	public static function getName($_val){
        $ary = self::getList();
        return coderHelp::getArrayPropertyVal($ary, 'value', $_val, 'name');
    }
	
	public static function clearCache(){
		global $web_cache;
		self::$_ary1 = null;
		clearCache($web_cache['ptype1']);
	}
}

/*****END PHP*****/
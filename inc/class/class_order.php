<?php
include(CONFIG_DIR."lib/Hashids/HashGenerator.php");
include(CONFIG_DIR."lib/Hashids/Hashids.php");

class class_order{
	public static function getMemberOrder($member_id){
        global $db;
		$colname = coderDBConf::$col_order;
		$colname_detail = coderDBConf::$col_order_detail;
		$colname_product = coderDBConf::$col_product;
		
        $sql = "SELECT *
				FROM ".coderDBConf::$order."	
				LEFT JOIN ".coderDBConf::$order_detail." ON {$colname_detail['sno']} = {$colname['sno']}
				LEFT JOIN ".coderDBConf::$product." ON {$colname_product['id']} = {$colname_detail['product_id']} AND {$colname_product['sno']} = {$colname_detail['product_sno']}
				WHERE {$colname['member_id']} = '$member_id' AND ({$colname['order_state']} < 10)
				ORDER BY `{$colname['sno']}` DESC, `{$colname['create_time']}` DESC";
		
        $row = $db -> fetch_all_array($sql);
		
		
        return $row;
    }
	
	public static function getsn($id){
        $hashids = new Hashids\Hashids("love", 8, "abcdefghijklmnpqrstuvwxyz123456789");
        return $hashids->encode($id);

    }

   public static function update_sno($order_sno, $order_sno_o){
        //新的訂單單號：$order_sno
        //舊的訂單單號：$order_sno_o

        global $db;

        //更新訂單(order)------
        $table = coderDBConf::$order;
        $colname = coderDBConf::$col_order;
        $data[$colname['sno']] = $order_sno;
        $where = " {$colname['sno']} = $order_sno_o";

        $db->query_update($table, $data, $where);

        //更新訂單詳細資訊(order_detail)------
        $table_od = coderDBConf::$order_detail;
        $colname_od = coderDBConf::$col_order_detail;
        $data_od[$colname_od['sno']] = $order_sno;
        $where_od = " {$colname_od['sno']} = $order_sno_o";

        $db->query_update($table_od, $data_od, $where_od);
    }
}

/*****END PHP*****/
<?php
class class_bonus{
	private static $_ary1 = null;

	public static function getAllBonusList($m_id){
		global $db;
		$colname_bonus = coderDBConf::$col_bonus;
		$colname_ubonus = coderDBConf::$col_used_bonus;
		
		$sql = "SELECT {$colname_bonus['member_id']} AS member_id, {$colname_bonus['bonus']} AS bonus, {$colname_bonus['type']} AS type, {$colname_bonus['comment']} AS comment, {$colname_bonus['create_time']} AS create_time 
				FROM ".coderDBConf::$bonus." 
				WHERE {$colname_bonus['member_id']} ='$m_id' AND {$colname_bonus['is_use']} = 1
				UNION ALL
				SELECT {$colname_ubonus['member_id']} AS member_id, {$colname_ubonus['bonus']} AS bonus, {$colname_ubonus['type']} AS type, {$colname_ubonus['comment']} AS comment, {$colname_ubonus['create_time']} AS create_time 
				FROM ".coderDBConf::$used_bonus." 
				WHERE {$colname_ubonus['member_id']} ='$m_id'
				ORDER BY create_time DESC";
				
		return $db -> fetch_all_array($sql);
	}
	
	public static function getTotalBonus($m_id){
        global $db;
        $colname_bonus = coderDBConf::$col_bonus;
		$colname_ubonus = coderDBConf::$col_used_bonus;
		$colname_member = coderDBConf::$col_member;
		
		$sql = "SELECT (SELECT SUM({$colname_bonus['bonus']}) FROM ".coderDBConf::$bonus." WHERE {$colname_bonus['member_id']} ='$m_id' AND {$colname_bonus['is_use']} = 1) - IFNULL((SELECT SUM({$colname_ubonus['bonus']}) FROM ".coderDBConf::$used_bonus." WHERE {$colname_ubonus['member_id']} ='$m_id'), 0) AS c FROM ".coderDBConf::$member." WHERE {$colname_member['id']} ='$m_id'";
		
		return $db -> query_first($sql);
    }
	
}

/*****END PHP*****/
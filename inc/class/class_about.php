<?php
class class_about{
	public static function getPublicList_history(){
		global $db;
		$colname = coderDBConf::$col_about_history;
		$sql = "select `{$colname['content']}` as content from ".coderDBConf::$about_history." WHERE `{$colname['id']}`=1";
		$row = $db->query_prepare_first($sql);
		return $row;
	}

    public static function getPublicList_new($type){
        $colname = coderDBConf::$col_about_new;
        $sql = "select `{$colname['id']}` as id , `{$colname['type']}` as type ,`{$colname['title']}` as title , `{$colname['updatetime']}` as updatetime from ".coderDBConf::$about_new." WHERE `{$colname['type']}`=$type AND `{$colname['ispublic']}`=1 ORDER BY `{$colname['ind']}` DESC";
        return $sql;
    }

    public static function getPublicDetail_new($id){
        global $db;
        $colname = coderDBConf::$col_about_new;
        $sql = "select an.`{$colname['type']}` as type ,an.`{$colname['title']}` as title ,an.`{$colname['content']}` as content , an.`{$colname['updatetime']}` as updatetime,(select can.`{$colname['id']}` from ".coderDBConf::$about_new." can WHERE can.`{$colname['ispublic']}`=1 AND can.`{$colname['type']}` = an.`{$colname['type']}` AND can.`{$colname['ind']}` < an.`{$colname['ind']}` ORDER BY can.`{$colname['ind']}` DESC LIMIT 1) as next_id from ".coderDBConf::$about_new." an WHERE `{$colname['id']}`=$id AND `{$colname['ispublic']}`=1";
        $rows = $db->query_prepare_first($sql);
        return $rows;
    }
	
	public static function getPublicList_declare(){
		global $db;
		$colname = coderDBConf::$col_about_declare;
		$sql = "select `{$colname['content']}` as content from ".coderDBConf::$about_declare." WHERE `{$colname['id']}`=1";
		$row = $db->query_prepare_first($sql);
		return $row;
	}
	public static function getPublicList_map(){
		global $db;
		$colname = coderDBConf::$col_about_map;
		$sql = "select `{$colname['content']}` as content from ".coderDBConf::$about_map." WHERE `{$colname['id']}`=1";
		$row = $db->query_prepare_first($sql);
		return $row;
	}
	public static function getPublicList_careers(){
		global $db;
		$colname = coderDBConf::$col_about_careers;
		$sql = "select `{$colname['content']}` as content from ".coderDBConf::$about_careers." WHERE `{$colname['id']}`=1";
		$row = $db->query_prepare_first($sql);
		return $row;
	}
}
?>
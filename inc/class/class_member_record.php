<?php
class class_member_record{
	//課程紀錄
	public static function getmember_record(){
	    global $db;	
	    $colname_record = coderDBConf::$col_member_record;
	    $sql = "select *,IFNULL(`lc_day`,`soc_day`) as tempday
				FROM ".coderDBConf::$member_record."
				LEFT JOIN ".coderDBConf::$lecture_class." ON CONCAT('l', lc_id) = {$colname_record['lc_soc_id']}
				LEFT JOIN ".coderDBConf::$school_open_class." ON CONCAT('s', soc_id) = {$colname_record['lc_soc_id']}
				LEFT JOIN ".coderDBConf::$teacher." ON t_id = lc_t_id or t_id = soc_t_id
				WHERE {$colname_record['md_id']}='".$_SESSION["session_cln_id"]."' 
				ORDER BY tempday DESC";
				
	    return $db -> fetch_all_array($sql);
	}

}
?>
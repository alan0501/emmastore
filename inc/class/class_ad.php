<?php
class class_ad{
	private static $_ary1 = null;

	public static function getList(){
		global $web_cache;
		$colname_ad = coderDBConf::$col_ad;
		if(self::$_ary1 == null){
			self::$_ary1 = getWebCache("SELECT * 
										FROM ".coderDBConf::$ad." 
										WHERE {$colname_ad['is_show']} = 1 
										ORDER BY {$colname_ad['ind']} DESC 
										LIMIT 4", $web_cache['ad']);
		}
		return self::$_ary1;
	}

	public static function clearCache(){
		global $web_cache;
		self::$_ary1 = null;
		clearCache($web_cache['ad']);
	}
}

/*****END PHP*****/
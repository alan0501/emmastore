<?php
include_once("_config.php");

$rows_ptype1 = class_product::getType1();


$product_id = get("p_id");
if($product_id < 0){
	script("資料傳送錯誤!");
}
$eq_id = get("eq_id"); //type1_id

$row_product_detail = class_product::getProductDetail($product_id);
if($row_product_detail){
	$type1_id = $row_product_detail[coderDBConf::$col_ptype1["id"]];
	$type1_name = $row_product_detail[coderDBConf::$col_ptype1["name"]];
	$type2_id = $row_product_detail[coderDBConf::$col_ptype2["id"]];
	$type2_name = $row_product_detail[coderDBConf::$col_ptype2["name"]];
	$product_id = $row_product_detail[coderDBConf::$col_product["id"]];
	$product_sno = $row_product_detail[coderDBConf::$col_product["sno"]];
	$product_name = $row_product_detail[coderDBConf::$col_product["name"]];
	$product_price = $row_product_detail[coderDBConf::$col_product["price"]];
	$product_bonus = $row_product_detail[coderDBConf::$col_product["bonus"]];
	$product_pic = $row_product_detail[coderDBConf::$col_product["pic"]];
	$product_specification = $row_product_detail[coderDBConf::$col_product["specification"]];
	$product_information = $row_product_detail[coderDBConf::$col_product["information"]];
	$product_notice = $row_product_detail[coderDBConf::$col_product["notice"]];
	$shop_id = $row_product_detail[coderDBConf::$col_store["id"]];
	$shop_name = $row_product_detail[coderDBConf::$col_store["name"]];
	$shop_logo = $row_product_detail[coderDBConf::$col_store["spic"]];
}

$rows_like = class_product::getLikeProduct($eq_id); 
$rows_hot = class_product::getHotProduct($shop_id);

getCartInfo();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix"> 
    <!--mobile menu star-->
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <!--mobile menu end-->
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">購物中心</a></div>
            <div class="m_login">
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
				<div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}else{
				?>
				<div class="top_login"><a href="member.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
                
            </form>
        </div>
        <div id="top_right"> 
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
					$member_id = $_SESSION["session_925_id"];
			?>
            <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class=" white">
    <div id="wrapper">
        <div id="wrap-menu">
            <?php include_once($inc_path."page/menu.php"); ?>
        </div>
        <div id="wrap-news"><a href="#">STORE</a> &gt; <a href="category_<?php echo $type1_id; ?>_.html"><?php echo $type1_name; ?></a> &gt; <a href="category_<?php echo $type1_id; ?>_<?php echo $type2_id; ?>.html"><?php echo $type2_name; ?></a> &gt; <a href="#"><?php echo $product_name; ?></a></div>
        <div id="wrap-main" class="clearfix">
            <div id="column_left">
                <div class="store_logo"><img src="<?php echo $web_path_store."s".$shop_logo; ?>" width="226" height="112" />
                    <div class="store_name"><?php echo $shop_name; ?></div>
                    <div class="store_b1"><a href="store_<?php echo $shop_id; ?>.html">&nbsp;</a></div>
                    <div class="store_b2 favorite_sbtn" s_id="<?php echo $shop_id; ?>" m_id="<?php echo isset($_SESSION['session_925_id']) ? $_SESSION['session_925_id'] : ""; ?>"><a href="javascript:void(0);" >&nbsp;</a></div>
                </div>
                <div class="clear"></div>
                <div class="store_hot">
                    <h3>你可能會喜歡</h3>
                    <?php
						foreach($rows_like as $row_like){
					?>
                    <dl class="clearfix">
                        <dt><a href="prduct_info_<?php echo $row_like[coderDBConf::$col_product["id"]]; ?>_<?php echo $eq_id; ?>.html"><img src="<?php echo $web_path_product."s".$row_like[coderDBConf::$col_product["pic"]]; ?>" /></a></dt>
                        <dd><a href="prduct_info_<?php echo $row_like[coderDBConf::$col_product["id"]]; ?>_<?php echo $eq_id; ?>.html"> <?php echo $row_like[coderDBConf::$col_product["name"]]; ?> </a>
                            <div class="price">NT$ <?php echo $row_like[coderDBConf::$col_product["price"]]; ?></div>
                        </dd>
                    </dl>
                    <?php
						}
					?>
                </div>
                <?php
				//if(count($rows_hot) > 0){
				?>
                <div class="store_hot">
                    <h3>本店舖熱門商品</h3>
                    <?php
						foreach($rows_hot as $row_hot){
					?>
                    <dl class="clearfix">
                        <dt><a href="prduct_info_<?php echo $row_hot[coderDBConf::$col_product["id"]]; ?>_<?php echo $eq_id; ?>.html"><img src="<?php echo $web_path_product."s".$row_hot[coderDBConf::$col_product["pic"]]; ?>" /></a></dt>
                        <dd><a href="prduct_info_<?php echo $row_hot[coderDBConf::$col_product["id"]]; ?>_<?php echo $eq_id; ?>.html"> <?php echo $row_hot[coderDBConf::$col_product["name"]]; ?> </a>
                            <div class="price">NT$ <?php echo $row_hot[coderDBConf::$col_product["price"]]; ?></div>
                        </dd>
                    </dl>
                    <?php
						}
					?>
                </div>
                <?php
				//}
				?>
            </div>
            <div id="column_right">
                <div class="product_info"> 
                    <!--   <div class="storename">露木文創(店家名稱)</div>-->
                    <div id="m_store_logo" class="clearfix"> <img src="<?php echo $web_path_admin."s".$shop_logo; ?>" class="m_logo" />
                        <p><?php echo $shop_name; ?></p>
                    </div>
                    <dl class="product_date clearfix">
                        <dt><img src="<?php echo $web_path_product."b".$product_pic; ?>" /></dt>
                        <dd>
                            <div class="product_name"><?php echo $product_name; ?></div>
                            <div class="product_price">NT$ <?php echo $product_price; ?></div>
                            <div class="product_pcs">數量&nbsp;&nbsp;&nbsp;
                                <input name="amount" type="text" class="pcs" value="1" id="amount" />
                            </div>
                            <div class="btn clearfix">
                                <div class="buy_btn1 cart_buy_btn"><a href="<?php echo (isLogin()) ? "cart.html" : "cart_no.html"; ?>">&nbsp;</a></div>
                                <div class="buy_btn2 cart_add_btn" p_id="<?php echo $product_id; ?>" p_sno="<?php echo $product_sno; ?>" p_name="<?php echo $product_name; ?>" p_price="<?php echo $product_price; ?>" p_bonus="<?php echo $product_bonus; ?>" shop_id="<?php echo $shop_id; ?>" pic="<?php echo $product_pic; ?>"><a href="javascript:void(0);">&nbsp;</a></div>
                            </div>
                            <div class="buy_btn3 favorite_pbtn" p_id="<?php echo $product_id; ?>" m_id="<?php echo isset($_SESSION['session_925_id']) ? $_SESSION['session_925_id'] : ""; ?>"><a href="javascript:void(0);">&nbsp;</a> </div>
                        </dd>
                    </dl>
                    <div class="descript">
                        <div class="title hide_title">商品描述</div>
                        <div class="descript_txt"> <?php echo $product_information; ?> </div>
                        <div class="title">商品規格</div>
                        <div class="product_txt l2px"> <?php echo $product_specification; ?> </div>
                        <div class="title t40px hide_title">購物須知</div>
                        <div class="t40px dsc_mtitle clearfix" id="btn_descript">
                            <div class="moretitle">更多詳細介紹</div>
                            <div class="spreadimg"><img src="images/spread2.png" /></div>
                            <div class="spread">展開&nbsp;&nbsp;</div>
                        </div>
                        <div class="descript_txt" id="more_descript" style="display:none;"> <?php echo $product_information; ?> </div>
                        <div class="t40px dsc_mtitle clearfix" id="btn_notice">
                            <div class="moretitle">購物須知</div>
                            <div class="spreadimg"><img src="images/spread2.png"  /></div>
                            <div class="spread">展開&nbsp;&nbsp;</div>
                        </div>
                    </div>
                    <div class="product_notice" id="product_notice"> <?php echo $product_notice; ?> </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end-->
</div>
<script>
carItemShow();
$(document).ready(function(e) {
	var eq_id = "<?php echo $eq_id; ?>";
	if(eq_id != ""){
		$("ul.menu_ul li").each(function(index, element) {
            var _eq_id = $(this).attr("eq_id");
			if(eq_id ==_eq_id){
				$(this).siblings().find("a").removeClass("current");
				$(this).find("a").addClass("current");
			}
        });	
	}
	
});
</script>
</body>
</html>
<?php
$db -> close();
?>
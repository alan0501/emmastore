<?php
class class_shop{
	private static $_ary1 = null;

	public static function getList(){ //取name,value及pid
		global $web_cache;
		$colname = coderDBConf::$col_store;
		if(self::$_ary1 == null){
			self::$_ary1 = getWebCache("SELECT {$colname['name']} AS name, {$colname['id']} AS value
										FROM ".coderDBConf::$store." 
										WHERE {$colname['is_show']} = 1 
										ORDER BY {$colname['ind']} DESC", $web_cache['shop']);
		}
		return self::$_ary1;
	}
	
	public static function getName($_val){
        $ary = self::getList();
        return coderHelp::getArrayPropertyVal($ary, 'value', $_val, 'name');
    }
	
	public static function clearCache(){
		global $web_cache;
		self::$_ary1 = null;
		clearCache($web_cache['shop']);
	}
	
	public static function getFShop($member_id){
	    global $db;
	    $colname = coderDBConf::$col_fshop;
		$colname_store = coderDBConf::$col_store;
	    $sql = "SELECT *
				FROM ".coderDBConf::$fshop."
				LEFT JOIN ".coderDBConf::$store." ON {$colname_store['id']} = {$colname['shop_id']}
				WHERE `{$colname['member_id']}` =".$member_id." AND `{$colname_store['is_show']}` = 1
				ORDER BY `{$colname['create_time']}` DESC";
				
	    return $db -> fetch_all_array($sql);
	}
}

/*****END PHP*****/
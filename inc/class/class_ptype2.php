<?php
class class_ptype2{
	private static $_ary1 = null;

	public static function getList(){//取name,value及pid
		global $web_cache;
		$colname = coderDBConf::$col_ptype2;
		if(self::$_ary1 == null){
			self::$_ary1 = getWebCache("SELECT {$colname['name']} AS name, {$colname['id']} AS value, {$colname['ptype1_id']} AS pid
										FROM ".coderDBConf::$ptype2." 
										WHERE {$colname['is_show']} = 1 
										ORDER BY {$colname['ind']} DESC", $web_cache['ptype2']);
		}
		return self::$_ary1;
	}
	
	/*public static function getSubList($m1t_id){
	    global $db;
	    $colname_m1t = coderDBConf::$col_market_1sttype;
		$colname_m2t = coderDBConf::$col_market_2ndtype;
	    $sql = "SELECT `{$colname_m2t['name']}` AS name, `{$colname_m2t['id']}` AS m2t_id, `{$colname_m1t['name']}` AS m1t_name
				FROM ".coderDBConf::$market_2ndtype."
				LEFT JOIN ".coderDBConf::$market_1sttype." ON `{$colname_m1t['id']}` = `{$colname_m2t['m1t_id']}`
				WHERE `{$colname_m2t['ispublic']}` = 1 AND `{$colname_m2t['m1t_id']}` =".$m1t_id."
				ORDER BY `{$colname_m2t['ind']}` DESC";
				
	    return $db -> fetch_all_array($sql);
	}*/
	
	public static function getName($_val){
        $ary = self::getList();
        return coderHelp::getArrayPropertyVal($ary, 'value', $_val, 'name');
    }
	
	public static function clearCache(){
		global $web_cache;
		self::$_ary1 = null;
		clearCache($web_cache['ptype2']);
	}
}

/*****END PHP*****/
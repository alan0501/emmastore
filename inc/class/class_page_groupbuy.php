<?php
class class_page_groupbuy{
	public static function getList_page_groupbuy(){
		global $db;
		$colname = coderDBConf::$col_page_groupbuy;
		$sql = "select `{$colname['content']}` as content from ".coderDBConf::$page_groupbuy." WHERE `{$colname['id']}`=1";
		$row = $db->query_prepare_first($sql);
		return $row;
	}
	
	public static function getList_page_groupbuy_faq(){
		global $db;
		$colname = coderDBConf::$col_page_groupbuy_faq;
		$sql = "select `{$colname['content']}` as content from ".coderDBConf::$page_groupbuy_faq." WHERE `{$colname['id']}`=1";
		$row = $db->query_prepare_first($sql);
		return $row;
	}
	
	public static function getList_page_groupbuy_contact(){
		global $db;
		$colname = coderDBConf::$col_page_groupbuy_contact;
		$sql = "select `{$colname['content']}` as content from ".coderDBConf::$page_groupbuy_contact." WHERE `{$colname['id']}`=1";
		$row = $db->query_prepare_first($sql);
		return $row;
	}
	public static function getList_page_pubplay(){
		global $db;
		$colname = coderDBConf::$col_page_pubplay;
		$sql = "select `{$colname['content']}` as content from ".coderDBConf::$page_pubplay." WHERE `{$colname['id']}`=1";
		$row = $db->query_prepare_first($sql);
		return $row;
	}

}
?>
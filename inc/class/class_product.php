<?php
class class_product{
	private static $_ary1 = null;
	
	public static function getType1(){
	    global $db;
		$colname_ptype1 = coderDBConf::$col_ptype1;
		$table_ptype2 = coderDBConf::$ptype2;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$table_product = coderDBConf::$product;
		$colname_product = coderDBConf::$col_product;
		
	    $sql = "SELECT t1.*
				FROM ".coderDBConf::$ptype1." AS t1 
				LEFT JOIN $table_ptype2 ON {$colname_ptype2['ptype1_id']} = {$colname_ptype1['id']}
				WHERE {$colname_ptype1['is_show']} = 1 AND ((SELECT COUNT({$colname_product['id']}) FROM $table_product WHERE {$colname_product['ptype2_id']} = {$colname_ptype2['id']} AND {$colname_product['is_show']} = 1) > 0)
				GROUP BY {$colname_ptype1['id']}
				ORDER BY {$colname_ptype1['ind']} DESC";
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getIndexProduct(){
	    global $db;
	    $colname_product = coderDBConf::$col_product;
		$colname_ptype2 = coderDBConf::$col_ptype2;
	    $sql = "SELECT *
				FROM ".coderDBConf::$product."
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				WHERE {$colname_product['is_show']} = 1 AND {$colname_product['is_index']} = 1
				ORDER BY {$colname_product['ind']} DESC
				LIMIT 8";
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getIndexTopProduct(){
	    global $db;
	    $colname_product = coderDBConf::$col_product;
		$colname_ptype2 = coderDBConf::$col_ptype2;
	    $sql = "SELECT *
				FROM ".coderDBConf::$product."
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				WHERE {$colname_product['is_show']} = 1 AND {$colname_product['is_index_top']} = 1
				ORDER BY {$colname_product['ind']} DESC
				LIMIT 3";
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getLikeProduct($eq_id){
	    global $db;
	    $colname_product = coderDBConf::$col_product;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		
	    $sql = "SELECT *
				FROM ".coderDBConf::$product."
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				WHERE {$colname_product['is_show']} = 1 AND {$colname_ptype2['ptype1_id']} = '$eq_id'
				ORDER BY RAND()
				LIMIT 3";
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getType2($type1_id){
	    global $db;
	    $colname_ptype1 = coderDBConf::$col_ptype1;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$table_product = coderDBConf::$product;
		$colname_product = coderDBConf::$col_product;
	    $sql = "SELECT *, (SELECT COUNT({$colname_product['id']}) FROM $table_product WHERE {$colname_product['ptype2_id']} = {$colname_ptype2['id']} AND {$colname_product['is_show']} = 1) AS total
				FROM ".coderDBConf::$ptype2."
				LEFT JOIN ".coderDBConf::$ptype1." ON {$colname_ptype1['id']} = {$colname_ptype2['ptype1_id']}
				WHERE {$colname_ptype2['is_show']} = 1 AND {$colname_ptype2['ptype1_id']} = '$type1_id'
				HAVING total > 0
				ORDER BY {$colname_ptype2['ind']} DESC";
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getTopProduct($type1_id, $type2_id = ""){
	    global $db;
	    $colname_ptype1 = coderDBConf::$col_ptype1;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$table_product = coderDBConf::$product;
		$colname_product = coderDBConf::$col_product;
		$where_str = "";
		if($type2_id != ""){
			$where_str .= " AND {$colname_product['ptype2_id']} = '$type2_id'";
		}
	    $sql = "SELECT *
				FROM ".coderDBConf::$product."
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				LEFT JOIN ".coderDBConf::$ptype1." ON {$colname_ptype1['id']} = {$colname_ptype2['ptype1_id']}
				WHERE {$colname_product['is_show']} = 1 AND {$colname_product['is_top']} = 1 AND {$colname_ptype1['id']} = '$type1_id' $where_str
				ORDER BY {$colname_product['ind']} DESC";
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getProduct($type1_id, $type2_id = ""){
	    global $db;
	    $colname_ptype1 = coderDBConf::$col_ptype1;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$table_product = coderDBConf::$product;
		$colname_product = coderDBConf::$col_product;
		$where_str = "";
		if($type2_id != ""){
			$where_str .= " AND {$colname_product['ptype2_id']} = '$type2_id'";
		}
	    $sql = "SELECT *
				FROM ".coderDBConf::$product."
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				LEFT JOIN ".coderDBConf::$ptype1." ON {$colname_ptype1['id']} = {$colname_ptype2['ptype1_id']}
				WHERE {$colname_product['is_show']} = 1 AND {$colname_ptype1['id']} = '$type1_id' $where_str
				ORDER BY {$colname_product['ind']} DESC";
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getProductDetail($p_id){
	    global $db;
	    $colname_product = coderDBConf::$col_product;
		$colname_ptype1 = coderDBConf::$col_ptype1;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$colname_shop = coderDBConf::$col_store;
		
	    $sql = "SELECT pro.*, {$colname_ptype2['id']}, {$colname_ptype2['name']}, {$colname_ptype1['id']}, {$colname_ptype1['name']}, {$colname_shop['id']}, {$colname_shop['name']}, {$colname_shop['spic']}
				FROM ".coderDBConf::$product." AS pro
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']} 
				LEFT JOIN ".coderDBConf::$ptype1." ON {$colname_ptype1['id']} = {$colname_ptype2['ptype1_id']} 
				LEFT JOIN ".coderDBConf::$store." ON {$colname_shop['id']} = {$colname_product['shop_id']}
				WHERE {$colname_product['id']} = '$p_id'
				LIMIT 2";
				
	    $data = $db -> query_first($sql);

        return $data;
	}
	
	public static function getShopProduct($shop_id){
	    global $db;
	    $colname_ptype1 = coderDBConf::$col_ptype1;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$colname_product = coderDBConf::$col_product;
		$colname_shop = coderDBConf::$col_store;
	    $sql = "SELECT pro.*, {$colname_ptype1['id']}, {$colname_shop['id']}, {$colname_shop['name']}, {$colname_shop['spic']}
				FROM ".coderDBConf::$product." AS pro
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				LEFT JOIN ".coderDBConf::$ptype1." ON {$colname_ptype1['id']} = {$colname_ptype2['ptype1_id']}
				LEFT JOIN ".coderDBConf::$store." ON {$colname_shop['id']} = {$colname_product['shop_id']}
				WHERE {$colname_product['is_show']} = 1 AND {$colname_product['shop_id']} = '$shop_id'
				ORDER BY {$colname_product['ind']} DESC";
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getHotProduct($shop_id){
	    global $db;
	    $colname_ptype1 = coderDBConf::$col_ptype1;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$colname_product = coderDBConf::$col_product;
		$colname_shop = coderDBConf::$col_store;
	    $sql = "SELECT pro.*, {$colname_ptype1['id']}, {$colname_shop['id']}, {$colname_shop['name']}, {$colname_shop['spic']}
				FROM ".coderDBConf::$product." AS pro
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				LEFT JOIN ".coderDBConf::$ptype1." ON {$colname_ptype1['id']} = {$colname_ptype2['ptype1_id']}
				LEFT JOIN ".coderDBConf::$store." ON {$colname_shop['id']} = {$colname_product['shop_id']}
				WHERE {$colname_product['is_show']} = 1 AND {$colname_product['is_hot']} = 1 AND {$colname_product['shop_id']} = '$shop_id' AND {$colname_product['is_hot']} = 1
				ORDER BY {$colname_product['ind']} DESC
				LIMIT 3";
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getFProduct($member_id, $ptype2_id = ""){
	    global $db;
	    $colname = coderDBConf::$col_fproduct;
		$table_fproduct = coderDBConf::$fproduct;
		$colname_product = coderDBConf::$col_product;
		$table_product = coderDBConf::$product;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$where_str = "";
		if($ptype2_id != ""){
			$where_str = " AND {$colname_product['ptype2_id']} = '$ptype2_id'";
		}
	    $sql = "SELECT *, (SELECT COUNT(*) FROM $table_fproduct LEFT JOIN $table_product ON {$colname_product['id']} = {$colname['product_id']} WHERE `{$colname['member_id']}` =".$member_id." AND {$colname_product['is_show']} = 1) as total
				FROM ".coderDBConf::$fproduct."
				LEFT JOIN ".coderDBConf::$product." ON {$colname_product['id']} = {$colname['product_id']}
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				WHERE `{$colname['member_id']}` =".$member_id." AND {$colname_product['is_show']} = 1 $where_str
				ORDER BY `{$colname['create_time']}` DESC";
				
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getFPtype2($member_id){
	    global $db;
	    $colname = coderDBConf::$col_fproduct;
		$table_fproduct = coderDBConf::$fproduct;
		$colname_product = coderDBConf::$col_product;
		$table_product = coderDBConf::$product;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$table_ptype2 = coderDBConf::$ptype2;
		
	    $sql = "SELECT *, (SELECT COUNT(*) FROM $table_fproduct LEFT JOIN $table_product ON {$colname_product['id']} = {$colname['product_id']} WHERE `{$colname['member_id']}` =".$member_id." AND {$colname_product['is_show']} = 1 AND {$colname_product['ptype2_id']} = $table_ptype2.{$colname_ptype2['id']}) AS t
				FROM ".coderDBConf::$fproduct."
				LEFT JOIN ".coderDBConf::$product." ON {$colname_product['id']} = {$colname['product_id']}
				LEFT JOIN $table_ptype2 ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				WHERE `{$colname['member_id']}` =".$member_id." AND {$colname_product['is_show']} = 1 
				GROUP BY {$colname_product['ptype2_id']}
				ORDER BY `{$colname['create_time']}` DESC";
				
	    return $db -> fetch_all_array($sql);
	}
	
	public static function getSearchProduct($keyword, $ptype2_id = ""){
        global $db;

		$colname_product = coderDBConf::$col_product;
		$table_product = coderDBConf::$product;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$table_ptype2 = coderDBConf::$ptype2;
		$where_str = "";
		if($ptype2_id != ""){
			$where_str = " AND {$colname_product['ptype2_id']} = '$ptype2_id'";
		}
		$sql = "SELECT *, (SELECT COUNT(*) FROM $table_product WHERE `{$colname_product['name']}` LIKE '%$keyword%' AND {$colname_product['is_show']} = 1) as total
				FROM ".coderDBConf::$product."
				LEFT JOIN ".coderDBConf::$ptype2." ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				WHERE {$colname_product['is_show']} = 1 AND {$colname_product['name']} LIKE '%$keyword%' $where_str
				ORDER BY `{$colname_product['create_time']}` DESC";

        $rows = $db -> fetch_all_array($sql);

        return $rows;
    }
	
	public static function getSearchType($keyword){
        global $db;

		$colname_product = coderDBConf::$col_product;
		$table_product = coderDBConf::$product;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$table_ptype2 = coderDBConf::$ptype2;

		$sql = "SELECT *, (SELECT COUNT(*) FROM $table_product WHERE `{$colname_product['name']}` LIKE '%$keyword%' AND {$colname_product['is_show']} = 1 AND {$colname_product['ptype2_id']} = $table_ptype2.{$colname_ptype2['id']}) AS t
				FROM ".coderDBConf::$product."
				LEFT JOIN $table_ptype2 ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				WHERE {$colname_product['name']} LIKE '%$keyword%' AND {$colname_product['is_show']} = 1 
				GROUP BY {$colname_product['ptype2_id']}
				ORDER BY `{$colname_product['create_time']}` DESC";

        $rows = $db -> fetch_all_array($sql);

        return $rows;
    }
	
	public static function getCartType1($p_id){
        global $db;

		$colname_product = coderDBConf::$col_product;
		$colname_ptype2 = coderDBConf::$col_ptype2;
		$table_ptype2 = coderDBConf::$ptype2;

		$sql = "SELECT {$colname_ptype2['ptype1_id']} AS type1
				FROM ".coderDBConf::$product."
				LEFT JOIN $table_ptype2 ON {$colname_ptype2['id']} = {$colname_product['ptype2_id']}
				WHERE {$colname_product['id']} = '$p_id'";

        $row = $db -> query_first($sql);

        return $row["type1"];
    }
	
	
	
	
	
	public static function setViewHistory($p_id){//設置瀏覽紀錄並返回結果
        global $iCookMainExpireDay;
        $num = 8;
        $idlist = self::getViewHistory();

        $fid = current($idlist);
        if($p_id != $fid){
            if(count($idlist) > $num){$idlist = array_slice($idlist,0,$num);}
            array_unshift($idlist, $p_id);
        }

        saveCookie('ViewHistory_m',serialize($idlist));
        return $idlist;
    }
    public static function getViewHistory(){//取得瀏覽紀錄cookie
        $idlist = unserialize(getCookie("ViewHistory_m"));
        return $idlist?$idlist:array();
    }

    
	
	public static function clearCache(){
		global $web_cache;
		self::$_ary1 = null;
		clearCache($web_cache['market_up_product']);
	}
	
	public static function getProductList_rank($m1t_id, $m2t_id = "", $mtype,$product_sno){
	    global $db;
	    $colname_product = coderDBConf::$col_market_product;
		$colname_m2t = coderDBConf::$col_market_2ndtype;
		$colname_pmedia = coderDBConf::$col_market_product_media;
		$where_str = "";
		if($m2t_id == ""){
			$where_str = "{$colname_m2t['m1t_id']} = ".$m1t_id." AND {$colname_product['ispublic']} = 1";
		}else{
			$where_str = "{$colname_m2t['id']} = ".$m2t_id." AND {$colname_product['ispublic']} = 1";
		}
		if($mtype!=''){ 
            $where_str .= " AND mpm.`{$colname_pmedia['media']}`= $mtype ";
        }
	    $sql = "SELECT mp.{$colname_product['id']} AS p_id, mp.{$colname_product['pic']} AS pic, mp.{$colname_product['name']} AS name, mp.{$colname_product['summary']} AS summary
				FROM ".coderDBConf::$market_2ndtype." as m2t
				LEFT JOIN ".coderDBConf::$market_product." as mp ON mp.{$colname_product['m2t_id']} = m2t.{$colname_m2t['id']}
				LEFT JOIN ".coderDBConf::$market_product_media." mpm ON mpm.{$colname_pmedia['mp_id']} = mp.{$colname_product['id']} AND mpm.{$colname_pmedia['ispublic']}=1
				WHERE ".$where_str."  and mpm.{$colname_pmedia['sno']} = $product_sno  
				GROUP BY mp.`{$colname_product['id']}`
				ORDER BY mp.{$colname_product['ind']} DESC";
				
	    return $db -> query_prepare_first($sql);
	}
}
/*****END PHP*****/
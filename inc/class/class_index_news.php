<?php
class class_index_news{
	private static $_ary1=null;

	public static function getPublicList(){
		global $web_cache;
		$colname_idxnew = coderDBConf::$col_about_new;
		if(self::$_ary1==null){
			self::$_ary1= getWebCache("select {$colname_idxnew['id']} as id ,{$colname_idxnew['type']} as type , {$colname_idxnew['title']} as title , {$colname_idxnew['updatetime']} as updatetime FROM ".coderDBConf::$about_new." WHERE {$colname_idxnew['ispublic']}=1 ORDER BY {$colname_idxnew['ind']} DESC LIMIT 6 ",$web_cache['index_news']);
		}
		return self::$_ary1;
	}

	public static function clearCache(){
		global $web_cache;
		self::$_ary1 = null;
		clearCache($web_cache['index_news']);
	}
}
?>
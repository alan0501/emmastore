<?php
class class_banner{
	private static $_ary1 = null;

	public static function getList(){
		global $web_cache;
		$colname_banner = coderDBConf::$col_banner;
		if(self::$_ary1 == null){
			self::$_ary1 = getWebCache("SELECT * 
										FROM ".coderDBConf::$banner." 
										WHERE {$colname_banner['is_show']} = 1 
										ORDER BY {$colname_banner['ind']} DESC", $web_cache['banner']);
		}
		return self::$_ary1;
	}

	public static function clearCache(){
		global $web_cache;
		self::$_ary1 = null;

		clearCache($web_cache['banner']);
	}
}

/*****END PHP*****/
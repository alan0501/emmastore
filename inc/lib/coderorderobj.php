<?php
class coderOrderObj {
	public $order_id = 0;
    public $order_no = "";
    public $order_member_id = 0;
	public $order_shop_id = 0;
	public $order_payment_type = 0;
    public $order_payment_state = 0;
	public $order_state = 0;
	public $order_express_type = 0; //物流方式
	public $order_comment = "";
	//public $coupon_discount = "";
	
	//public $discount = "";
	//public $discount_desc = "";
	public $order_total_price = 0;
	public $order_freight = 0;
	public $order_bonus = 0;
	public $order_purchaser_name = ""; 
	public $order_recipient_name = ""; 
    public $order_recipient_email = "";
    public $order_recipient_phone = "";
    public $order_recipient_zcode = "";
    public $order_recipient_address = "";
    //public $to_pickup_time = 0;
    public $order_invoice = 0;
	public $order_company_invoice = 0;
    //public $invoice_no = '';
    //public $invoice_name = '';
	public $order_express_sno = "";
	public $order_manager = "";
    public $order_create_time = "";
    public $order_update_time = "";
    public $order_pay_time = "";
    public $order_express_time = "";
    
	public $order_payment_back = "";
	public $order_detail_ary = array();
	/*public $coupon;
    public $bonus=0;
    public $money=0;
    
    public $discount=0;
    public $discount_desc='';
    public $disbonus=0;
    public $dismoney=0;*/
    
	public function insertDetail(coderOrderObj $detail_item ){
		$this -> order_detail_ary[] = $detail_item;
	}
	
    public function getDetailFromCar($car){
        $newcar = array();
        foreach($car as $item){
            $this -> _insertDiscountDesc($car, $item);
            $ex_item = $this -> _isItemExisit($newcar, $item);
            if($ex_item != null){
                $ex_item -> amount += $item -> amount;
                $ex_item -> subtotal = $ex_item -> amount*$ex_item -> price;
            }else{
                $newcar[] = $item;
            }
        }
        $this -> detailary = $newcar;
    }
	
    private function _isItemExisit(&$car , $_item){
        for($i = 0, $c = count($car); $i < $c; $i ++){
            if($car[$i] -> id == $_item -> id && $car[$i] -> price == $_item -> price){
                return $car[$i];
            }    
        }
        return null;
    }
	
    private function _getDetailName(&$car, $id, $buy_type){
        if($id > 0){
            for($i = 0, $c = count($car); $i < $c; $i++){
                if($car[$i] -> id == $id){
                    return '-by'.$car[$i] -> title;
                }    
            }
        }else if($id < 0){
            return '-活動贈品';
        }
        return '';       
    }
	
    private function _insertDiscountDesc(&$car, $item){
        if($item -> buy_type > 0){
            $tag = '';
            switch($item->buy_type){
                case 1:
                    $tag = '(加購)';
                    break;
                case 10:
                    $tag = '(贈品)';
                    break;
                case 11:
                    $tag = '(活動贈品)';
                    break;                    
            }
            $this -> discount_desc .= ';'.$item -> title.'x'.$item -> amount.$tag.$this -> _getDetailName($car, $item -> rel_id, $item -> rel_id);
        }
    }
}

/*****END PHP*****/

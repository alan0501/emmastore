<?php 
class CoderOrder{
	
	public function __construct(){
	}
	
	public function getOrderNO(){
		global $db, $table_ordersno;
		$sno = "";
		$today = date("Y-m-d", time());
		$orderno = explode("-", $today);
		$year = $orderno[0];
		$month = $orderno[1];
		$no = str_pad("1", 4, "0", STR_PAD_LEFT);
		
		$db -> begin();
		try{
			$sql = "SELECT * 
					FROM  $table_ordersno 
					WHERE os_year = $year AND os_month = $month 
					ORDER BY os_sno DESC 
					LIMIT 1 FOR UPDATE";
			$row = $db -> query_first($sql);
			if($row){
				$id = $row['os_id'];
				$sd = $row['os_sno']+1;
				$no = str_pad($sd, 4, "0", STR_PAD_LEFT);
			
				$data["os_year"] = $year;
				$data["os_month"] = $month;
				$data["os_sno"] = $no;
				$data["os_createtime"] = request_cd();
	
				$db -> query_update($table_ordersno, $data, "os_id = '$id'");
				
				$sno = $year.$month.$no;
			}else{
				$data["os_year"]=$year;
				$data["os_month"]=$month;
				$data["os_sno"]=$no;
				$data["os_createtime"] = request_cd();
	
				$db -> query_insert($table_ordersno, $data);
			 
				$sno = $year.$month.$no;
			}
			$db -> commit();
			return $sno;
		}catch(Exception $e){
			$db -> rollback();
			throw new Exception("訂單新增錯誤,請再操作一次.");
		}
	}
	
	public function orderDetailInsert($order_sno, $detail_ary){
		global $db, $table_orderdetail;
		//$db -> query("DELETE FROM $table_orderdetail WHERE orderdetail_ordersno = '$order_sno'");
		
		for($i = 0; $i < count($detail_ary); $i ++){
			/*get data from $detailary put into $dataDetail*/
			$data_detail = array();
			$data_detail["orderdetail_order_sno"] = $order_sno;
			$data_detail["orderdetail_product_id"] = $detail_ary[$i] -> orderdetail_product_id;
			$data_detail["orderdetail_product_sno"] = $detail_ary[$i] -> orderdetail_product_sno;
			$data_detail["orderdetail_selling_price"] = $detail_ary[$i] -> orderdetail_selling_price;
			$data_detail["orderdetail_amount"] = $detail_ary[$i] -> orderdetail_amount;
			$data_detail["orderdetail_total_price"] = $detail_ary[$i] -> orderdetail_total_price;
			$data_detail["orderdetail_create_time"] = $detail_ary[$i] -> orderdetail_create_time;
			
			$db -> query_insert($table_orderdetail, $data_detail);
			
		}
	}
	
	public function chkStock($detail_ary){
		global $db, $table_product;
		$result = array(true, "");
		for($i = 0; $i < count($detail_ary); $i ++){
			$product_id = $detail_ary[$i] -> product_id;
			$order_amount = $detail_ary[$i] -> amount;
			
			$row = $db -> query_first("SELECT * FROM $table_product WHERE product_id = '$product_id' AND product_stock < '$order_amount'");
			
			if($row){
				$result[0] = false;
				$result[1] = $row["product_name_tw"]."庫存不足";
				return $result;
			}
		}
		return $result;
	}
	
	public function orderInsert($myorder){
		global $db, $table_order;
		$sno = $this -> getOrderNO();
		
		$db -> begin();
			try{
				$myorder -> order_sno = $sno;
				//$myorder -> ind = $this -> getMaxInd($table_order, "order_ind", "");
				
				/*get data from $myorder put into $dataOrder*/
				$data_order = array();
				$data_order["order_sno"] = $myorder -> order_sno;
				$data_order["order_member_id"] = $myorder -> order_member_id;
				
				$data_order["order_payment_type"] = $myorder -> order_payment_type;
				$data_order["order_total_price"] = $myorder -> order_total_price;
				$data_order["order_recipient_name"] = $myorder -> order_recipient_name;
				$data_order["order_recipient_mobile"] = $myorder -> order_recipient_mobile;
				$data_order["order_recipient_phone"] = $myorder -> order_recipient_phone;
				$data_order["order_recipient_email"] = $myorder -> order_recipient_email;
				$data_order["order_recipient_address"] = $myorder -> order_recipient_address;
				$data_order["order_recipient_time"] = $myorder -> order_recipient_time;

				$data_order["order_invoice"] = $myorder -> order_invoice;
				$data_order["order_invoice_name"] = $myorder -> order_invoice_name;
				$data_order["order_invoice_number"] = $myorder -> order_invoice_number;
				
				$data_order["order_create_time"] = $myorder -> order_create_time;
				$data_order["order_update_time"] = $myorder -> order_update_time;
				//$data_order["order_payment_return"] = $myorder -> payment_return;



				
				$db -> query_insert($table_order, $data_order);
		
				$order_sno = $sno;
				$this -> orderDetailInsert($order_sno, $myorder -> detail_ary);
			
				$db -> commit();
			}catch(Exception $e){
				$db -> rollback();
				script($e -> getMessage());
			}
	}
	
	public function orderUpdate($myorder, $id){
		global $db, $table_order;
		
		/*get data from $myorder put into $dataOrder*/
		$data_order = array();
		$row_ordernumber = $db -> query_first("SELECT order_ordernumber FROM $table_order WHERE order_id='$id'");
		if(!$row_ordernumber){
			throw new Exception("查無此訂單資料!");
			return;
		}
		$ordernumber = $row_ordernumber["ordernumber"];
		$data_order["order_memberid"] = $myorder -> memberid;
		$data_order["order_payway"] = $myorder -> payway;
		$data_order["order_bank"] = $myorder -> bankname;
		$data_order["order_installment"] = $myorder -> installment;
		$data_order["order_paytype"] = $myorder -> paytype;
		$data_order["order_status"] = $myorder -> status;
		$data_order["order_comment"] = $myorder -> comment;
		$data_order["order_bankno"] = $myorder -> bankno;
		$data_order["order_totalprice"] = $myorder -> totalprice;
		$data_order["order_freight"] = $myorder -> freight;
		$data_order["order_manager"] = $myorder -> manager;
		$data_order["order_isshow"] = $myorder -> isshow;
		//$data_order["order_createtime"] = $myorder -> createtime;
		
		$db -> query_update($table_order, $data_order, "order_id = $id");
		
		$this -> orderDetailInsert($ordernumber, $myorder -> detail_ary);
	}
	
	function getMaxInd($table, $field, $where){
		global $db;
		$row = $db -> query_first("SELECT MAX($field) AS max FROM $table $where", "max");
		$maxind = intval($row["max"]);
		
		if($maxind == 0){
			$maxind = 1;
		}else{
			$maxind += 5;
		}
		return $maxind;
	}
	
	public function orderMail($myorder){
		global $db, $table_member, $ary_payment_type, $sys_email, $sys_name;
		
		$member_id = $myorder -> order_member_id;
		$sql = "SELECT * FROM $table_member WHERE member_id = '$member_id' ";
		$row = $db -> query_first($sql);
		
		$member_name = $row["member_name"];
		$member_email = $row["member_account"];
		
		$order_sno = $myorder -> order_sno;
		$create_time = $myorder -> order_create_time;
		$total_price = ($myorder -> order_total_price-$myorder -> order_coupon+$myorder -> order_freight);
		$freight = $myorder -> order_freight;
		$payment_type = $myorder -> order_payment_type;
		//$alltotal = $myorder -> totalprice;
		$detail_ary = $myorder -> detail_ary;
		//$allDiscount = ($disbonus+$discount);
		
		$cc_mail = "hello@otisan.com";
		
		$fr_em = $sys_email;
		$fr_na = $sys_name;
		$to_em = $member_email.",".$cc_mail;
		$to_na = $member_name;
		
		$subject = "【訂單成立】- 感謝您的購買!";
		
		$msg = "";
		$msg .= '親愛的顧客'.$member_name.'您好，感謝您的訂購，以下為您的訂單內容：<br /><br />';
		$msg .= '訂單編號：'.$order_sno.'<br />';
		$msg .= '付款方式：'.$ary_payment_type[$payment_type].'<br />';
		$msg .= '<br />';
		foreach($detail_ary as $item){
			$msg .= $item -> orderdetail_product_name.' &nbsp;&nbsp;* '.$item -> orderdetail_amount.'&nbsp;&nbsp;&nbsp;&nbsp;'.$item -> orderdetail_total_price.'<br /><br />';
		}
		$msg .= '總計：'.$total_price.'<br /><br />';
		$msg.='本系統已經收到您的訂購訊息，並供您再次自行核對之用，此通知函不代表交易已經完成。<br />';
		$msg.='感謝您的訂購，有任何問題請歡迎聯絡客服中心。<br /><br />';
			
			
		sendMail($fr_em, $fr_na, $to_em, $to_na, $subject, $msg);

	}	

}


class CoderOrderItem{
	public $order_sno, $order_member_id, $order_payment_type, $order_total_price, $order_freight, $order_recipient_name, $order_recipient_email, $order_recipient_mobile, $order_recipient_address, $order_invoice, $order_invoice_name, $order_invoice_number, $order_recipient_phone, $order_recipient_time, $order_create_time, $order_update_time, $detail_ary = array();
	
	public function _construct(){
	}
	
	public function insertDetail(CoderOrderDetailItem $detail_item ){
		$this -> detail_ary[] = $detail_item;
	}
	
}


class CoderOrderDetailItem{
	public $orderdetail_order_sno, $orderdetail_product_id, $orderdetail_product_sno, $orderdetail_product_name, $orderdetail_selling_price, $orderdetail_amount, $orderdetail_total_price, $orderdetail_create_time;
	
	public function _construct(){
	}
	
}

/*****END PHP*****/
<?php
class coderOrder {
    public static $_keyname = "orderSession";
	public static $payment_state = array(0 => '未付款', 1 => '已付款'); //付款狀態
	public static $payment_type = array(1 => '信用卡線上刷卡', 2 => '7-11 IBON/全家FAMIPORT/萊爾富LIFE-ET/OK超商OK-GO', 3 => '超商代收', 4 => '貨到付款');
	public static $invoice_type = array("1" => "電子發票", "2" => "紙本發票", "9" => "捐贈發票");
	public static $order_state = array(0 => '處理中', 1 => '可出貨', 2 => '己出貨', 3 => '交易完成', 11 => '交易取消', 12 => '交易失敗', 30 => '退貨', 31 => '退貨中', 32 => '退貨完成', 13 => '退款完成'); //訂單狀態
	 
    public static $express_type = array("1" => "宅急便", "2" => "郵局");
    //public static $pay_type = array('無', '貨到付款','郵局劃撥','信用卡');
    //public static $pay_state = array('未付款', '己付款');
	public static $payment_state2 = array(1 => '己付款');

    //public static $state_type = array(0=>'處理中', 1=>'可出貨', 2=>'己出貨', 3=>'交易完成', 10=>'退貨', 11=>'交易取消');
	public static $order_state_type2 = array(0 => '處理中', 11 => '交易取消');

    public function __construct(){
    }

	
	public function orderInsert($myorder){
		global $db;
		$sno = $this -> getOrderNO();
		
		$db -> begin();
			try{
				$myorder -> order_sno = $sno;
				//$myorder -> ind = $this -> getMaxInd($table_order, "order_ind", "");
				
				/*get data from $myorder put into $dataOrder*/
				$data_order = array();
				$data_order[coderDBConf::$col_order["sno"]] = $myorder -> order_sno;
				$data_order[coderDBConf::$col_order["member_id"]] = $myorder -> order_member_id;
				$data_order[coderDBConf::$col_order["shop_id"]] = $myorder -> order_shop_id;
				$data_order[coderDBConf::$col_order["payment_type"]] = $myorder -> order_payment_type;
				//$data_order[coderDBConf::$col_order["payment_state"]] = $myorder -> order_payment_state;
				
				$data_order[coderDBConf::$col_order["express_type"]] = $myorder -> order_express_type;
				//$data_order["order_coupon_sno"] = $myorder -> coupon_sno;
				//$data_order["order_discount"] = $myorder -> discount;
				//$data_order["order_discount_desc"] = $myorder -> discount_desc;
				
				$data_order[coderDBConf::$col_order["total_price"]] = $myorder -> order_total_price;
				$data_order[coderDBConf::$col_order["freight"]] = $myorder -> order_freight;
				$data_order[coderDBConf::$col_order["bonus"]] = $myorder -> order_bonus;
				$data_order[coderDBConf::$col_order["purchaser_name"]] = $myorder -> order_purchaser_name;
				$data_order[coderDBConf::$col_order["recipient_name"]] = $myorder -> order_recipient_name;
				$data_order[coderDBConf::$col_order["recipient_email"]] = $myorder -> order_recipient_email;
				$data_order[coderDBConf::$col_order["recipient_phone"]] = $myorder -> order_recipient_phone;
				$data_order[coderDBConf::$col_order["recipient_zcode"]] = $myorder -> order_recipient_zcode;
				
				$data_order[coderDBConf::$col_order["recipient_address"]] = $myorder -> order_recipient_address;
				//$data_order[coderDBConf::$col_order["recipient_address"]] = "1111122223333333";
				//$data_order["order_recipient_time"] = $myorder -> order_recipient_time;

				$data_order[coderDBConf::$col_order["invoice"]] = $myorder -> order_invoice;
				$data_order[coderDBConf::$col_order["company_invoice"]] = $myorder -> order_company_invoice;
				//$data_order["order_invoice_name"] = $myorder -> order_invoice_name;
				//$data_order["order_invoice_number"] = $myorder -> order_invoice_number;
				
				$data_order[coderDBConf::$col_order["create_time"]] = $myorder -> order_create_time;
				$data_order[coderDBConf::$col_order["update_time"]] = $myorder -> order_update_time;

				$data_order[coderDBConf::$col_order["payment_state"]] = $myorder -> order_payment_state;
				$data_order[coderDBConf::$col_order["order_state"]] = $myorder -> order_state;
				//$data_order["order_payment_return"] = $myorder -> payment_return;
				$db -> query_insert(coderDBConf::$order, $data_order);

				$order_sno = $sno;
				$this -> orderDetailInsert($order_sno, $myorder -> order_detail_ary);
				
				//購物金
				if($myorder -> order_bonus > 0){
					$data_bonus = array();
					$data_bonus[coderDBConf::$col_bonus["order_sno"]] = $sno;
					$data_bonus[coderDBConf::$col_bonus["member_id"]] = $myorder -> order_member_id;
					$data_bonus[coderDBConf::$col_bonus["bonus"]] = $myorder -> order_bonus;
					$data_bonus[coderDBConf::$col_bonus["type"]] = 1;
					$data_bonus[coderDBConf::$col_bonus["is_use"]] = 0;
					$data_bonus[coderDBConf::$col_bonus["comment"]] = "購物";
					$data_bonus[coderDBConf::$col_bonus["create_time"]] = request_cd();
					
					$db -> query_insert(coderDBConf::$bonus, $data_bonus);
				}
				
				$db -> commit();
			}catch(Exception $e){
				$db -> rollback();
				script($e -> getMessage());
			}
	}
	
	public function getOrderNO(){
		global $db;
		$sno = "";
		$today = date("Y-m-d", time());
		$orderno = explode("-", $today);
		$year = $orderno[0];
		$month = $orderno[1];
		$day = $orderno[2];
		$no = str_pad("1", 4, "0", STR_PAD_LEFT);
		
		$db -> begin();
		try{
			$sql = "SELECT * 
					FROM  ".coderDBConf::$order_sno." 
					WHERE ".coderDBConf::$col_order_sno["year"]." = $year AND ".coderDBConf::$col_order_sno["month"]." = $month AND ".coderDBConf::$col_order_sno["day"]." = $day 
					ORDER BY ".coderDBConf::$col_order_sno["sno"]." DESC 
					LIMIT 1 FOR UPDATE";
			$row = $db -> query_first($sql);
			if($row){
				$id = $row[coderDBConf::$col_order_sno["id"]];
				$sd = $row[coderDBConf::$col_order_sno["sno"]]+1;
				$no = str_pad($sd, 4, "0", STR_PAD_LEFT);
			
				$data[coderDBConf::$col_order_sno["year"]] = $year;
				$data[coderDBConf::$col_order_sno["month"]] = $month;
				$data[coderDBConf::$col_order_sno["day"]] = $day;
				$data[coderDBConf::$col_order_sno["sno"]] = $no;
				$data[coderDBConf::$col_order_sno["create_time"]] = request_cd();
	
				$db -> query_update(coderDBConf::$order_sno, $data, coderDBConf::$col_order_sno['id']." = '$id'");
				
				$sno = $year.$month.$day.$no; 
			}else{
				$data[coderDBConf::$col_order_sno["year"]] = $year;
				$data[coderDBConf::$col_order_sno["month"]] = $month;
				$data[coderDBConf::$col_order_sno["day"]] = $day;
				$data[coderDBConf::$col_order_sno["sno"]] = $no;
				$data[coderDBConf::$col_order_sno["create_time"]] = request_cd();
	
				$db -> query_insert(coderDBConf::$order_sno, $data);
			 
				$sno = $year.$month.$day.$no;
			}
			$db -> commit();
			return $sno;
		}catch(Exception $e){
			$db -> rollback();
			throw new Exception("訂單新增錯誤,請再操作一次.");
		}
	}
	
	public function orderDetailInsert($order_sno, $detail_ary){
		global $db;
		//$db -> query("DELETE FROM $table_orderdetail WHERE orderdetail_ordersno = '$order_sno'");

		for($i = 0; $i < count($detail_ary); $i ++){
			/*get data from $detailary put into $dataDetail*/
			$data_detail = array();
			$data_detail[coderDBConf::$col_order_detail["sno"]] = $order_sno;
			$data_detail[coderDBConf::$col_order_detail["product_id"]] = $detail_ary[$i] -> orderdetail_product_id;
			$data_detail[coderDBConf::$col_order_detail["product_sno"]] = $detail_ary[$i] -> orderdetail_product_sno;
			$data_detail[coderDBConf::$col_order_detail["shop_id"]] = $detail_ary[$i] -> orderdetail_shop_id;
			$data_detail[coderDBConf::$col_order_detail["price"]] = $detail_ary[$i] -> orderdetail_price;
			$data_detail[coderDBConf::$col_order_detail["amount"]] = $detail_ary[$i] -> orderdetail_amount;
			$data_detail[coderDBConf::$col_order_detail["total_price"]] = $detail_ary[$i] -> orderdetail_total_price;
			$data_detail[coderDBConf::$col_order_detail["create_time"]] = $detail_ary[$i] -> orderdetail_create_time;
			$data_detail[coderDBConf::$col_order_detail["update_time"]] = $detail_ary[$i] -> orderdetail_update_time;

			$data_detail[coderDBConf::$col_order_detail["state"]] = $detail_ary[$i] -> orderdetail_state;
			
			$db -> query_insert(coderDBConf::$order_detail, $data_detail);
			
		}
	}
	
	public static function getItem($ono, $member_id = -1){
        global $db;
        $sql = ' WHERE '.coderDBConf::$col_order["sno"].' = :ono';
        $sql_ary = array(':ono' => $ono);
        if($member_id > 0){
            $sql .= ' AND '.coderDBConf::$col_order["member_id"].' = :m_id';
            $sql_ary[':m_id'] = $member_id;
        }
        $row = $db -> query_first('SELECT * FROM '.coderDBConf::$order.' LEFT JOIN '.coderDBConf::$member.' ON '.coderDBConf::$col_member["id"].' = '.coderDBConf::$col_order["member_id"].$sql, $sql_ary);
        if(!$row){
            throw new Exception("查無訂單記錄!");
        }
        //var_dump($row);
        $order_info = new coderOrderObj();
		$order_info -> order_id = $row[coderDBConf::$col_order["id"]];
        $order_info -> order_no = $row[coderDBConf::$col_order["sno"]];
        $order_info -> order_member_id = $row[coderDBConf::$col_order["member_id"]];
		$order_info -> order_shop_id = $row[coderDBConf::$col_order["shop_id"]];
		$order_info -> order_payment_type = $row[coderDBConf::$col_order["payment_type"]];
		$order_info -> order_payment_state = $row[coderDBConf::$col_order["payment_state"]];
		$order_info -> order_state = $row[coderDBConf::$col_order["order_state"]];
		$order_info -> order_express_type = $row[coderDBConf::$col_order["express_type"]];
		$order_info -> order_comment = $row[coderDBConf::$col_order["comment"]];
		$order_info -> order_total_price = $row[coderDBConf::$col_order["total_price"]];
		$order_info -> order_freight = $row[coderDBConf::$col_order["freight"]];
		//$order_info -> coupon_discount = $row['order_coupon_discount'];
		//$order_info -> coupon_sno = $row['order_coupon_sno'];
		//$order_info -> discount = $row['order_discount'];
		//$order_info -> discount_desc = $row['order_discount_desc'];
		
		$order_info -> order_purchaser_name = $row[coderDBConf::$col_member["name"]];
		$order_info -> order_purchaser_phone = $row[coderDBConf::$col_member["phone"]];
		$order_info -> order_purchaser_email = $row[coderDBConf::$col_member["email"]];
		//$order_info -> order_purchaser_zcode = $row[coderDBConf::$col_order["purchaser_name"]];
		$order_info -> order_purchaser_address = $row[coderDBConf::$col_member["address"]];
        /*$order_info -> email = $row['md_email'];
        $order_info -> tel = $row['md_telphone'];
        $order_info -> mobile = $row['md_phone'];
		
		if($row['md_address']){
        	$order_info -> addr = $row['md_address'];
		}else{
			$order_info -> addr = $row['md_zipcode'].$row['md_county'].$row['md_district'].$row['md_addr_tw'];
		}*/
		
        $order_info -> order_recipient_name = $row[coderDBConf::$col_order["recipient_name"]];
        $order_info -> order_recipient_email = $row[coderDBConf::$col_order["recipient_email"]];
        $order_info -> order_recipient_phone = $row[coderDBConf::$col_order["recipient_phone"]];
        $order_info -> order_recipient_zcode = $row[coderDBConf::$col_order["recipient_zcode"]];
        $order_info -> order_recipient_address = $row[coderDBConf::$col_order["recipient_address"]];
        //$order_info -> to_pickup_time = $row['order_recipient_time'];
        $order_info -> order_invoice = $row[coderDBConf::$col_order["invoice"]];
        $order_info -> order_company_invoice = $row[coderDBConf::$col_order["company_invoice"]];
        $order_info -> order_express_sno = $row[coderDBConf::$col_order["express_sno"]];
		$order_info -> order_manager = $row[coderDBConf::$col_order["manager"]];
        $order_info -> order_create_time = $row[coderDBConf::$col_order["create_time"]];
        $order_info -> order_update_time = $row[coderDBConf::$col_order["update_time"]];
        $order_info -> order_pay_time = $row[coderDBConf::$col_order["pay_time"]];
        $order_info -> order_express_time = $row[coderDBConf::$col_order["express_time"]];
        $order_info -> order_payment_back = $row[coderDBConf::$col_order["payment_back"]];
		
		$order_info -> order_detail_ary = self::getDetails($ono);
		
        return $order_info;
    }
	
	public static function getDetails($ono){
        global $db;
        $rows = $db -> fetch_all_array('SELECT *
									    FROM '.coderDBConf::$order_detail.'
										LEFT JOIN '.coderDBConf::$product.' ON '.coderDBConf::$col_product["id"].' = '.coderDBConf::$col_order_detail["product_id"].'
									    WHERE '.coderDBConf::$col_order_detail["sno"].' = :ono', array(':ono' => $ono));
        $details = array();
		$item = array();
        foreach($rows as $row) {
            
            $item["id"] = $row[coderDBConf::$col_order_detail["id"]];
            $item["order_sno"] = $row[coderDBConf::$col_order_detail["sno"]];
            $item["product_id"] = $row[coderDBConf::$col_order_detail["product_id"]];
            $item["product_sno"] = $row[coderDBConf::$col_order_detail["product_sno"]];
			$item["product_name"] = $row[coderDBConf::$col_product["name"]];
			$item["shop_id"] = $row[coderDBConf::$col_order_detail["shop_id"]];
            $item["sell_price"] = $row[coderDBConf::$col_order_detail["price"]];
            $item["amount"] = $row[coderDBConf::$col_order_detail["amount"]];
            $item["sub_total"] = $row[coderDBConf::$col_order_detail["total_price"]];
			$item["state"] = $row[coderDBConf::$col_order_detail["state"]];
            $item["create_time"] = $row[coderDBConf::$col_order_detail["create_time"]];
			
            $details[] = $item;
        }
        return $details;
    }
	
	public static function orderMail($myorder){
		global $db, $sys_email, $sys_name, $ary_payment_type, $web_name;
		
		$member_id = $myorder -> order_member_id;
		if($member_id != ""){
			$row = $db -> query_first("SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["id"]." = ".$member_id);
			$member_name = $row[coderDBConf::$col_member["account"]];
			$member_email = $row[coderDBConf::$col_member["email"]];
		}else{
			$member_name = $myorder -> order_recipient_name;
			$member_email = $myorder -> order_recipient_email;
		}
		$to_array = array(0 => array("email" => $member_email, "name" => $member_name));
		
		$msg = '親愛的'.$member_name.'您好，感謝您的訂購，以下為您的訂單內容：<br /><br />';
		$msg .= '訂單編號:'.$myorder -> order_sno.'<br />';
		$msg .= '付款方式：'.$ary_payment_type[$myorder -> order_payment_type].'<br /><br />';
		
		$detail_ary = $myorder -> order_detail_ary;
		$msg_detail = '';
		for($i = 0; $i < count($detail_ary); $i ++){
			$msg_detail .= $detail_ary[$i] -> orderdetail_product_name.'     '.$detail_ary[$i] -> orderdetail_price.'*'.$detail_ary[$i] -> orderdetail_amount.'='.$detail_ary[$i] -> orderdetail_total_price.'<br />';
		}
		
		$msg .= '您的訂單資訊如下，請核對是否正確：<br />';
		$msg .= $msg_detail.'<br />';
		$msg .= '運費： NTD.'.$myorder -> order_freight.'<br />';
		$msg .= '總計： NTD.'.($myorder -> order_total_price+$myorder -> order_freight).'<br /><br />';
		$msg .= '本系統已經收到您的訂購訊息，此通知函提供您再次自行核對之用，不代表交易已經完成。<br />';
		$msg .= '感謝您的訂購，有任何問題請歡迎聯絡客服中心。<br />';
		$msg .= '*若您是使用超商代碼繳費，請在期限內付款到虛擬帳號(期限後虛擬帳號失效)，收到付款後我們即將為您出貨!<br /><br />';
		
		$subject = $web_name." 【訂單成立】- 感謝您的購買!";

        //sendMail($sys_email, $sys_name, $member_email, $member_name, $web_name." 【訂單成立】- 感謝您的購買!", $msg, $bcc="");
		send_smtp2($sys_email, $sys_name, $to_array, $subject, $msg);
        //0000000000000000
        
        //return true;
	}
	
	public static function paySuccessMail($sno){
		global $db, $sys_email, $sys_name, $ary_pay, $web_name;
		$rows = $db -> fetch_all_array("SELECT * FROM ".coderDBConf::$order." LEFT JOIN ".coderDBConf::$order_detail." ON ".coderDBConf::$col_order_detail["sno"]." = ".coderDBConf::$col_order["sno"]." WHERE ".coderDBConf::$col_order["sno"]." = '$sno'");
		
		$member_id = $rows[0][coderDBConf::$col_order["member_id"]];
		$order_sno = $rows[0][coderDBConf::$col_order["sno"]];
		$amount = $rows[0][coderDBConf::$col_order["sno"]];
		if($member_id != ""){
			$row = $db -> query_first("SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["id"]." = ".$member_id);
			$member_name = $row[coderDBConf::$col_member["name"]];
			$member_email = $row[coderDBConf::$col_member["email"]];
		}else{
			$member_name = $rows[0][coderDBConf::$col_order["recipient_name"]];
			$member_email = $rows[0][coderDBConf::$col_order["recipient_email"]];
		}
		
		$msg = '親愛的客戶'.$member_name.'您好<br />';
		$msg .= '您的訂單編號:'.$order_sno.'，已完成交易，!<br />';
		$msg .= '<br />';
		$msg .= '感謝您的訂購，有任何問題請歡迎聯絡客服中心。<br />';
		
        sendMail($sys_email, $sys_name, $member_email, $member_name, $web_name." - 訂單編號：".$order_sno." - 【已付款通知】!", $msg, $bcc="");
        
        //return true;
		
	}
	
	public static function payFaileMail($sno){
		global $db, $sys_email, $sys_name, $ary_pay, $web_name;
		$rows = $db -> fetch_all_array("SELECT * FROM ".coderDBConf::$order." LEFT JOIN ".coderDBConf::$order_detail." ON ".coderDBConf::$col_order_detail["sno"]." = ".coderDBConf::$col_order["sno"]." WHERE ".coderDBConf::$col_order["sno"]." = '$sno'");
		
		$member_id = $rows[0][coderDBConf::$col_order["member_id"]];
		$order_sno = $rows[0][coderDBConf::$col_order["sno"]];
		$amount = $rows[0][coderDBConf::$col_order["sno"]];
		if($member_id != ""){
			$row = $db -> query_first("SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["id"]." = ".$member_id);
			$member_name = $row[coderDBConf::$col_member["name"]];
			$member_email = $row[coderDBConf::$col_member["email"]];
		}else{
			$member_name = $rows[0][coderDBConf::$col_order["recipient_name"]];
			$member_email = $rows[0][coderDBConf::$col_order["recipient_email"]];
		}
		
		$msg = '親愛的客戶'.$member_name.'您好<br />';
		$msg .= '您的訂單編號:'.$order_sno.'，尚未付款，系統已經幫您把訂單取消囉!<br />';
		$msg .= '<br />';
		$msg .= '感謝您的訂購，有任何問題請歡迎聯絡客服中心。<br />';
		
		sendMail($sys_email, $sys_name, $member_email, $member_name, $web_name." - 訂單編號：".$order_sno." - 【訂單取消】!", $msg, $bcc="");
    
        //return true;
	
	}
	
	public function changeState($ono, $state, $pay_state=0, $pay_type=null, $note=null, $mail=false){
        global $db, $webname;
        $myorder = self::getItem($ono);
		//$row_member = $db -> query_first("select * from ".coderDBConf::$member." where m_id=".$myorder->member_id);
		//$myorder -> erp_id = $row_member["m_erp_id"];
        if($myorder -> order_state == 3 || $myorder -> order_state > 10){
            throw new Exception("此訂單無法修改狀態");
        }
        $db -> begin();
        try{
			/*if($pay_state == 1){
				$this->insert_erp_order($myorder);
			}*/
			
            //$_title = '訂單('.$ono.')狀態變更通知';
            //$_state = '你的訂單狀態變更為:'.self::$state_type[$state];
			/*if($myorder -> member_id > 0){
				if(($myorder->order_state<4 && $state==4)){
					if($myorder->bonus>0){
						coderMemberBonus::inserBonus($myorder->member_id,  'order' ,$myorder->bonus);
					}
					if($myorder->money>0){
						coderMemberMoney::inserMoney($myorder->member_id,  'order' ,$myorder->money);
					}
				}
				if(($myorder->order_state<10 &&  $state>9)){
					coderMemberBonus::removeByRef($ono);
					coderMemberMoney::removeByRef($ono);
				}
			}*/
			
            $_sql = coderDBConf::$col_order["order_state"].' = :state, '.coderDBConf::$col_order["payment_state"].' = :pay_state';
            $_sql_ary = array(':state' => $state, ':pay_state' => $pay_state, ':ono' => $ono);
            if($pay_type != null){
                $_sql .= ','.coderDBConf::$col_order["payment_type"].' = :pay_type';
                $_sql_ary[':pay_type'] = $pay_type;

            }
            if($note != null){
                $_sql .= ','.coderDBConf::$col_order["comment"].' = :note';
                $_sql_ary[':note'] = $note;
            }
			if($pay_state == 1){
				$_sql .= ','.coderDBConf::$col_order["pay_time"].' = :p_time';
				$_sql_ary[':p_time'] = request_cd();
				//開啟紅利
				$data_b["bonus_is_use"] = 1;
				$db -> query_update(coderDBConf::$bonus, $data_b, "bonus_order_sno='$ono'");
			}
			//關閉紅利
			if($state > 10){
				$data_b["bonus_is_use"] = 0;
				$db -> query_update(coderDBConf::$bonus, $data_b, "bonus_order_sno='$ono'");
			}
			//出貨時間
			if($state == 2){
				$_sql .= ','.coderDBConf::$col_order["express_time"].' = :express_time';
				$_sql_ary[':express_time'] = request_cd();
			}
			//送達時間
			if($state == 3){
				$_sql .= ','.coderDBConf::$col_order["get_time"].' = :get_time';
				$_sql_ary[':get_time'] = request_cd();
			}
			
            $db -> execute('UPDATE '.coderDBConf::$order.' SET '.$_sql.' WHERE '.coderDBConf::$col_order["sno"].' = :ono', $_sql_ary);
            $db -> commit();

        }catch(Exception $e){
            $db -> rollback();
            throw $e;
        }
		
        if($mail == true){
            $this -> _sendNotice($_title, $_state, $myorder);
        }

    }   
   
    public static function getQueryList($member_id,$ary=null){
        global $db;
        $sHelp = new coderSelectHelp($db);
        $sHelp->table = coderDBConf::$order;
        $sHelp->select = " * ";
        $sHelp->page_size = 20;
        $sHelp->page = get("page");
        $sHelp->orderby = ' o_createtime '; 
        $sHelp->orderdesc = 'desc';
        $sHelp->where = ' o_member_id=' . $member_id;
        if($ary!=null){
            if(coderHelp::getStr($ary['ono'])!==''){
                $sHelp->where.=' and '.coderSqlStr::equal('o_no', "%".hc($ary['ono'])."%", 'like');
            }
            $sdate=str_replace('/','-',coderHelp::getStr($ary['sdate']));
            $edate=str_replace('/','-',coderHelp::getStr($ary['edate']));
            if($sdate!=='' || $edate!==''){
                $sHelp->where.=' and '.coderSqlStr::getDateRangeSQL('o_createtime', $sdate, $edate);
            }
            if(coderHelp::getStr($ary['state'])!==''){
                $sHelp->where.=' and '.coderSqlStr::equal('o_state', $ary['state'] , '=');
            }
        }
        return array(
            'list' => $sHelp->getList() ,
            'page_info' => $sHelp->page_info
        );
    }
    public static function setInfo($obj){
        $_SESSION[self::$_keyname]=serialize($obj);
    }
    public static function getInfo(){

        if(!isset($_SESSION[self::$_keyname]) || $_SESSION[self::$_keyname]==null ){
            return null;
        }else{
            return unserialize($_SESSION[self::$_keyname]);
        }

    }
	
	
	
    public function insertDetail($ordernumber, $detailary) {
        global $db;
        $db->query("delete from ".coderDBConf::$order_details." where o_no='{$ordernumber}'");
        $createtime=request_cd();
        for ($i = 0; $i < count($detailary); $i++) {
            $dataDetail = array();
            $dataDetail['o_no'] = $ordernumber;
            $dataDetail['pd_pid'] = $detailary[$i]->id;
            $dataDetail['pd_pno'] = $detailary[$i]->no;
            $dataDetail['pd_title'] = $detailary[$i]->title;
            $dataDetail['pd_org_price'] = $detailary[$i]->org_price;
            $dataDetail['pd_price'] = $detailary[$i]->price;
            $dataDetail['pd_amount'] = $detailary[$i]->amount;
            $dataDetail['pd_total'] = $detailary[$i]->subtotal;
            $dataDetail['pd_type'] = $detailary[$i]->type;
            $dataDetail['pd_buy_type'] = $detailary[$i]->buy_type;
			$dataDetail['pd_isgroup'] = $detailary[$i]->is_group;
            $dataDetail['pd_createtime'] = $createtime;
            $db->query_insert(coderDBConf::$order_details, $dataDetail);
        }
    }
    public function insert($myorder) {
        global $db,$webname;
        $db->begin();
        try {
            $dataOrder = array();
            $dataOrder["o_no"] = $myorder->order_no;
            $dataOrder["o_state"] = 0;
            $dataOrder["o_member_id"] = $myorder->member_id;
            $dataOrder["o_name"] = $myorder->name;
            $dataOrder["o_email"] = $myorder->email;
            $dataOrder["o_tel"] = $myorder->tel;
            $dataOrder["o_mobile"] = $myorder->mobile;
            $dataOrder["o_zcode"] = $myorder->zcode;
            $dataOrder["o_city"] = $myorder->city;
            $dataOrder["o_area"] = $myorder->area;
            $dataOrder["o_addr"] = $myorder->addr;
            $dataOrder["o_pickup_time"] = $myorder->pickup_time;
            $dataOrder["o_to_name"] = $myorder->to_name;
            $dataOrder["o_to_email"] = $myorder->to_email;
            $dataOrder["o_to_tel"] = $myorder->to_tel;
            $dataOrder["o_to_mobile"] = $myorder->to_mobile;
            $dataOrder["o_to_zcode"] = $myorder->to_zcode;
            $dataOrder["o_to_city"] = $myorder->to_city;
            $dataOrder["o_to_area"] = $myorder->to_area;
            $dataOrder["o_to_addr"] = $myorder->to_addr;
            $dataOrder["o_to_pickup_time"] = $myorder->to_pickup_time;
            $dataOrder["o_invoice"] = $myorder->invoice;
            $dataOrder["o_invoice_no"] = $myorder->invoice_no;
            $dataOrder["o_invoice_title"] = $myorder->invoice_title;
            $dataOrder["o_pay_type"] = $myorder->pay_type;
            $dataOrder["o_pay_state"] = 0;
            $dataOrder["o_note"] = '';
            $dataOrder["o_bonus"] = $myorder->bonus;
            $dataOrder["o_money"] = $myorder->money;
            $dataOrder["o_subtotal"] = $myorder->subtotal;
            $dataOrder["o_discount"] = $myorder->discount;
            $dataOrder["o_discount_desc"] = $myorder->discount_desc;
            $dataOrder["o_freight"] = $myorder->freight;
            $dataOrder["o_disbonus"] = $myorder->disbonus;
            $dataOrder["o_dismoney"] = $myorder->dismoney;
            $dataOrder["o_total"] = $myorder->total;
            $dataOrder["o_createtime"] = $myorder->createtime;
            $dataOrder["o_updatetime"] = request_cd();
            $db->query_insert(coderDBConf::$order, $dataOrder);
            if($myorder->member_id>0){

                if($myorder->disbonus>0 ){
                    coderMemberBonus::inserUseBonus($myorder->member_id, 'order' ,$myorder->disbonus,$myorder->order_no);
                }
                if($myorder->dismoney>0 ){
                    coderMemberMoney::inserUseMoney($myorder->member_id, 'order' ,$myorder->dismoney,$myorder->order_no);
                }
				if($myorder->coupon != null){ 
					coderSales::updateCouponState($myorder->coupon, $myorder->order_no, $myorder->member_id);
				}
            }
            $this->insertDetail($dataOrder["o_no"], $myorder->detailary);
            
            $db->commit();
			self::getUserTotal(true);
        }
        catch(Exception $e) {
            $db->rollback();
            throw $e;
        }
        $this->_sendNotice($webname.'感謝您的訂購',$webname.'感謝您的訂購',$myorder);

    }
	
	
    public function orderUpdata($myorder, $id) {
        global $db;
        $dataOrder = array();
        $ordernumberrow = $db->query_first("select ordernumber from aaa_order where id='$id'");
        if (!$ordernumberrow) {
            throw new Exception("查無此訂單資料!");
            
            return;
        }
        $ordernumber = $ordernumberrow["ordernumber"];
        $dataOrder["menberid"] = $myorder->menberid;
        $dataOrder["typeid"] = $myorder->typeid;
        $dataOrder["payway"] = $myorder->payway;
        $dataOrder["paytype"] = $myorder->paytype;
        $dataOrder["status"] = $myorder->status;
        $dataOrder["comment"] = $myorder->comment;
        $dataOrder["discountcomment"] = $myorder->discountcomment;
        $dataOrder["discount"] = $myorder->discount;
        $dataOrder["totalprice"] = $myorder->totalprice;
        $dataOrder["freight"] = $myorder->freight;
        $dataOrder["recipientname"] = $myorder->recipientname;
        $dataOrder["recipientemail"] = $myorder->recipientemail;
        $dataOrder["recipienttel"] = $myorder->recipienttel;
        $dataOrder["recipientmobile"] = $myorder->recipientmobile;
        $dataOrder["recipientaddress"] = $myorder->recipientaddress;
        $dataOrder["companynumber"] = $myorder->companynumber;
        $dataOrder["companytitle"] = $myorder->companytitle;
        $dataOrder["manager"] = $myorder->manager;
        $dataOrder["isshow"] = $myorder->isshow;
        $db->query_update('aaa_order', $dataOrder, "id=$id");
        $this->orderDetailI($ordernumber, $myorder->detailary);
        $db->close();
    }
    
    
    
    public static function getPayInfo($ono){
        global $db;
        $ary=array('pay_type'=>'','pay_info'=>'');
        $row=$db->query_first('select * from '.coderDBConf::$order_payment.' where o_no=:ono',array(':ono'=>$ono));
        if($row){
            $ary['pay_type']=coderHelp::getStr($row['op_payment_type']);
            $ary['pay_info']=$row['op_code']==1 ? '付款完成' : '付款失敗';
            $ary['pay_info'].='於'.$row['op_payment_date'].'-'.$row['op_msg'];
        }
        else{
            $ary['pay_type']='';
            $ary['pay_info']='未付款';
        }
        return $ary;
    }
    private  function _sendNotice($title,$order_state,$myorder) {
        global $template_path, $sys_email, $webname;
        $content=self::_getNoticeContent($myorder);
        $body = coderContentManage::getMailContent('order', array(
            '{$name}',
            '{$order_state}',
            '{$content}'
        ) , array(
            $myorder->name,
            $order_state,
            $content
        ));
        if($myorder->member_id>0){
            coderMemberMessage::insert($myorder->member_id,'order',$title,$body);     
        }
        return sendmail($sys_email, $webname, $myorder->email, '', $title, $body);
    }
    private function _getNoticeContent($myorder){
        $detail=$myorder->detailary;
		$pay_state = ($myorder->pay_state == 0) ? "(尚未付款)" : "";
		$content='<p>您的訂單編號為:'.$myorder->order_no.$pay_state.'</p>';
        $content.='<table width="600" border="1"><tr><td>序號</td><td>訂購項目</td><td>單價</td><td>數量</td><td>金額</td></tr>';
        $i=1;
        foreach($detail as $item){
            $content.='<tr><td>'.$i.'</td><td>'.$item->title.'</td><td>'.$item->price.'</td><td>'.$item->amount.'</td><td>'.$item->subtotal.'</td></tr>';
            $i++;
        }
        $subtotal='小計:'.$myorder->subtotal.'<br>折扣:'.$myorder->discount.'<br>運費:'.$myorder->freight.'<br>折扺(紅利+購物金):'.($myorder->disbonus+$myorder->dismoney).'<br>應付金額:'.$myorder->total;
        $content.='<tr><td colspan="5" align="right">'.$subtotal.'</td></tr>';
        $content.='</table>';
        return $content;

    }
    public static function clear(){
        unset($_SESSION[self::$_keyname]);
    }
    public static function getUserTotal($refresh = false) {
        $user = coderMember::getLoginUser();
        if($user==null){
            return 0;
        }
        if ($refresh==false && isset($user['order_count'])) {
            
            return coderHelp::getNum($user['order_count']);
        }
        $bonus = self::getOrderTotal($user['id']);
        $user['order_count'] = $bonus;
        coderMember::setUser($user);

        return $bonus;
    } 
     public static function getOrderTotal($mid) {
         global $db;
         $row = $db->query_first('SELECT COUNT(*) as c FROM '.coderDBConf::$order.' where o_member_id=' . $mid);
         
         return coderHelp::getNum($row['c']);
     }
     public static function chkOrderState() {
         global $db;
         $row = $db->fetch_all_array('select * from '.coderDBConf::$order.' where o_pay_state=1 and o_state <>4 and o_state < 10');
         
         return $row;
     }

}

/*****END PHP*****/

<?php
class coderPromotion {
	//public static $rule_type=array(0=>'滿額',11=>'首購',12=>'生日當月');
	//public static $scope_type=array('一般','全館');
	//public static $event_type=array(0=>'現金折價',1=>'現金折扣',2=>'贈送紅利',3=>'贈送購物金',4=>'紅利加倍',5=>'贈品');
	public static $promotion_type = array(1 => '生日當月', 2 => '講師折扣');
	public static $coupon_state = array(0 => '無效', 1 => '未使用', 2 => '已使用');
	private static $_ary = null;
   // private static $bonus_price=0;
	
	public static function getList(){
		global $web_cache, $db;
		$_ary = array();
		//print_r($_ary);
		//die();
        if(self::$_ary == null) {
            $_ary = getWebCache('SELECT *  FROM ' . coderDBConf::$promotion. ' where ' . coderSqlStr::getOnOffSQL(coderDBConf::$col_promotion["sdate"], coderDBConf::$col_promotion["edate"]), $web_cache['promotion']);
			
            for($i = 0, $c = count($_ary); $i < $c; $i++){

            	$_ary[$i]['content_str'] = self::getEventContentTxt($_ary[$i]);
				//print_r($_ary[0]);
            	/*if($_ary[$i]['s_type']==5 && $_ary[$i]['s_val']>0){
            		$gift=self::getGift($_ary[$i]['s_val']);
            		if($gift!=null){
            			$gift->rel_id=$_ary[$i]['s_id'];
            			$_ary[$i]['gift']=$gift;
            		}
            	}*/
            }
            self::$_ary = $_ary;
        }
        return self::$_ary;
	}
	/*private static function getGift($id){
		global $db;
		$row=$db->query_first('select id,no,title,capacity,pic,type_id,price from '.coderDBConf::$product.' where id=\''.$id.'\'');
		if($row){
			$_item=new shoppingItem();
			$_item->id=$row['id'];
			$_item->amount=1;
			$_item->type=-1;
			$_item->buy_type=11;
			$_item->no=$row["no"];
			$_item->name=$row['title'];
			$_item->title=$row['title'].$row['capacity'];
			$_item->pic=$row['pic'];
			$_item->org_price=$row['price'];
			$_item->capacity=$row['capacity'];
			$_item->price= 0; 
			$_item->subtotal=0;
			$_item->cid=$row['type_id'];
			$_item->is_complete=true;
			return $_item;
		}
		return null;
	}*/
	public static function getEventContentTxt($row){
		$str = '';
		$promotion_type = $row['promotion_type'];
		$promotion_member_type = $row['promotion_member_type'];
		$promotion_rule = $row['promotion_rule'];
		$promotion_rule_value = $row['promotion_rule_value'];
		$promotion_title = $row['promotion_title'];
		$promotion_teacher_id = $row['promotion_teacher_id'];
		
		//$type = $row['s_type'];
		//$val = $row['s_val'];
		
		$str = $promotion_member_type > 0 ? "VIP會員" : "所有會員" ;
		//$str.= $m_is_vip ==0 ? '' : '(需要VIP資格)';
		
		if($promotion_type == 1){
			$str .= ','.self::$promotion_type["1"].',購物滿'.$promotion_rule.'元即可';
		}else if($promotion_type == 2){
			$str .= ',購買'.self::_getTeacher($promotion_teacher_id).'講師作品,即可';
			
			/*if($scope==1){
				$str.='全館';
			}
			else{
				if($cid!=''){
					$str.='在以下館別('.ProductType::getNamesByKeyAry(explode(',',$cid)).')';
				}
			}
			$str.='購物滿'.$rule_val.'元即';*/
		}
		$str .= '享'.$promotion_rule_value.'%優惠';
		//$str .= self::_getEventTypeTxt($type,$val);
		
		return $str;

	}
	
	private static function _getTeacher($id){
		global $db;
		$name = "";
		$row = $db -> query_first("SELECT * FROM ".coderDBConf::$teacher." WHERE t_id = $id");
		$name = $row["t_name"];
		return $name;
	}
	/*private static function _getEventTypeTxt($type,$val){
		$str='';
		switch ($type) {
			case 0:
				$str='折價'.$val.'元';
				break;
			case 1:
				$str='享'.$val.'%優惠';
				break;
			case 2:
				$str='贈送紅利'.$val.'點';
				break;
			case 3:
				$str='贈送購物金'.$val;
				break;					
			case 4:
				$str='紅利為'.$val;
				break;	
			case 5:
				$str='贈送贈品';
				break;
		}
		return $str;
	}*/

	public static function getDiscountTotalContent(& $ary_discount,$car){

		$discount_total=0;
		foreach ($ary_discount as $item) {
			$discount_total+=$item->discount;
		}

		$total=0;
		for($i=0;$i<count($car);$i++){
			$total+=$car[$i]->subtotal;
		}

		$chk_total=$total-$discount_total;

		if($chk_total>0){
			$s_list=self::getList();

			foreach ($s_list as $key => $sales) {
				
				if($sales['s_scope']==1 && $sales['s_rule']==0 && $chk_total>=$sales['s_rule_val']){

					$ary_discount[]=self::_countScopeContent($sales,$chk_total);
				}
			}

		}
		return $ary_discount;

	}
	
	public static function getDiscountContent(& $_ary, $promotion, $car){
		
		//$web_conf = WebConf::getList();
		//self::$bonus_price = $web_conf['bonus_price']['val'];
		if(self::isInSalesMember($promotion) && self::isInSalesRule($promotion, $car)){

			self::_insertDiscountContent($_ary, $promotion, $car);
		}
		
	}
	
	public static function isInSalesMember($promotion){
		$result = true;
		$login_user = coderMember::getLoginUser();

		if($promotion['promotion_member_type'] != $login_user['md_vip']){
			$result = false;
		}
		return $result;
	}
	
	public static function isInSalesRule($promotion, $car){
		switch($promotion['promotion_type']){
			case 1:
				return self::isBirthBuy();
				break;
			case 2:
				return self::isTeacherBuy($promotion, $car);
				break;
			default:
				break;				
		}
	}
	
	public static function isBirthBuy(){
		global $db, $null_date2;
		$login_user = coderMember::getLoginUser();
		if($login_user == null){
			return false;
		}
		if(isset($login_user['md_birthday']) && $login_user['md_birthday'] != $null_date2){
			$month = datetime('m', $login_user['md_birthday']);
			$thismonth = datetime('m');
			$login_user['md_birthday'] = ($month == $thismonth) ? 1 : 0 ;
			//coderMember::setUser($login_user);
		}
		return ($login_user['md_birthday'] == 1) ? true : false;

	}
	
	public static function isTeacherBuy($promotion, $car){
		//print_r($car);
		global $db;
		$result = false;
		foreach($car as $item){
			$product_id = $item -> product_id;
			$product_sno = $item-> product_sno;
			
			$sql = "SELECT t_id
					FROM ".coderDBConf::$publication_product_media."
					LEFT JOIN ".coderDBConf::$publication_product." ON ".coderDBConf::$publication_product_media.".pp_id = ".coderDBConf::$publication_product.".pp_id
					LEFT JOIN ".coderDBConf::$publication_series." ON ".coderDBConf::$publication_product.".ps_id = ".coderDBConf::$publication_series.".ps_id
					WHERE ppm_id = '$product_id' AND ppm_sno = '$product_sno'";
			$row = $db -> query_first($sql);
			if($row["t_id"] == $promotion["promotion_teacher_id"]){
				$result = true;
				break;
			}
			
		}
		return $result;

	}
	
	private static function _insertDiscountContent(& $_ary, $promotion, $car){
		switch($promotion['promotion_type']) {
			case 1:
				$total_price = 0;
				foreach($car as $item){
					$total_price += $item -> sub_total;
				}
				if($total_price >= $promotion['promotion_rule']){
					$_ary[] = self::_countScopeContent($promotion, $total_price);
				}
				break;
			case 2:
			$total_price = 0;
				foreach($car as $item){
					$total_price += $item -> sub_total;
				}
				$_ary[] = self::_countScopeContent($promotion, $total_price);
				break;			

		}
	}
	private static function _countScopeContent($promotion, $total_price){
		$item = new coderDiscountItem();
		$item -> event_id = $promotion['promotion_id'];
		$item -> title = $promotion['promotion_title'];
		//$item -> descript = $promotion['promotion_title'].'-'.$promotion['content_str'];
		$item -> descript = $promotion['promotion_title'];

		switch ($promotion['promotion_type']) {
			case 1:
				$money = ceil($total_price*round($promotion['promotion_rule_value']/100, 2));//應付
				//$item -> money = $money;
				$item -> discount = $total_price-$money;
				$item -> descript.='->折扣'.$item -> discount.'元';
				break;
			case 2:
				$money = ceil($total_price*round($promotion['promotion_rule_value']/100, 2));
				//$item -> money = $money;
				$item -> discount = $total_price-$money;
				$item -> descript.='->折扣'.$item -> discount.'元';
				break;
		}
		//print_r($item);
		return $item;
	}
	
	public static function reloadState(){
		$login_user=coderMember::getLoginUser();
		unset($login_user['firstbuy']);
		unset($login_user['birthbuy']);
		coderMember::setUser($login_user);
	}
	
		
    public static function clearCache() {
        global $web_cache;
        clearCache($web_cache['promotion']);
    }
	
    public static function chkCoupon($coupon) {
        global $db;
        $row = $db->query_first('select c_id, c_discount, c_state from '.coderDBConf::$coupon.' where (c_prefix + \'-\' + c_coupon)=:coupon and c_state > 0',
		array(
			':coupon'=>$coupon
		));
		$row && $row["is_used"] = $row["c_state"] == 1 ? 0 : 1; 
		return $row;
    }
	
	public static function getCouponDiscount(& $ary_discount){
		$order_info = coderOrder::getInfo();
		if($order_info != null && $order_info->coupon != null){
			$coupon = $order_info->coupon;
			$item=new coderDiscountItem();
			$item->type=1;
			$item->event_id=$coupon['id'];
			$item->title='Coupon折價優惠';
			$item->descript='Coupon折價優惠-序號：'.$coupon['no'].'->折扣'.$coupon['discount'].'元';
			$item->discount=$coupon['discount'];
			$ary_discount[] = $item;
		}
		return $ary_discount;
	}
	public static function updateCouponState($coupon, $order_no, $member_id){
		global $db;
		if(!$coupon || empty($coupon['id'])) { return false; }
		$db->execute('update '.coderDBConf::$coupon.' set c_state=2, c_order_no=:order_no, c_mid=:mid, c_updatetime=:updatetime where c_id=:id',
		array(
			':order_no'=>$order_no,
			':mid'=>$member_id,
			':id'=>$coupon['id'],
			':updatetime'=>request_cd()
		));
	}
}

<?php
/*
coder後台用List排序相關物件V1.0 2013/11/21 by cully
V1.1 2015/01/20 by Jane 修改成可自訂where的欄位 $oid_key(未給值時預設為id)
*/
class coderlistorderhelp{
	/*
	$order:方法up/down,
	$table:表格,
	$field:排序的欄位,
	$oid:篩選的id值
	$oid_key:篩選的id欄位,
	$where
	 */
	public static function  changeDescOrder($order,$table,$field,$oid,$oid_key='id',$where='')
	{ //上移下移的function
		$desc= ($order=="down") ? "down" : "up";
		self::changeOrder($desc,$table,$field,$oid,$oid_key,$where);
	}
	public static function  changeAscOrder($order,$table,$field,$oid,$oid_key='id',$where='')
	{ //上移下移的function
		$desc= ($order=="down") ? "up" : "down";
		self::changeOrder($desc,$table,$field,$oid,$oid_key,$where);
	}
	public static function  changeOrder($order,$table,$field,$oid,$oid_key='id',$where='')
	{ //上移下移的function
		global $db;
		$sql="";
		$row=$db->query_first("select $field from $table WHERE $oid_key='".$oid."'");
		$ind=$row[$field];

		if ($order=="down"){
			$sql="select $field as field,$oid_key as newid from $table WHERE $field < $ind $where order by $field desc limit 1";
		}else{
			$sql="select $field as field,$oid_key as newid from $table WHERE $field > $ind $where order by $field asc limit 1";
		}

		$row=$db->query_first($sql);
		if ($row)
		{
			$new_nid=$row["newid"];   //要換順序的id
			$new_ind=$row["field"];//新的順序
			
			$db->exec("update $table set $field=$new_ind where $oid_key=$oid"); //將指定id換到新順序
			$db->exec("update $table set $field=$ind where $oid_key=$new_nid"); //將要換順序的id換到舊順序
		}
	}

	public static function getMaxInd($table,$field,$where='',$add=5)
	{
		global $db;
		$row=$db->query_first("select max($field) as max from $table $where");
		$maxind=intval($row["max"]);

		if ($maxind==0){
			$maxind=1;
		}else{
			$maxind+=$add;
		}
		return $maxind;

	}

}?>
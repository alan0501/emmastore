<?php
class coderDBConf{
	/*****table*****/
    public static $admin = '925_admin';  //後台管理者
    public static $log = '925_log';  //後台管理者操作紀錄
	public static $ad = '925_ad';  //首頁廣告
    public static $banner = '925_banner';  //BANNER
	public static $meta = '925_meta'; //meta資料
	public static $footer = '925_footer'; //footer資料
	public static $ptype1 = '925_ptype1';  //商品一階分類
	public static $ptype2 = '925_ptype2';  //商品二階分類
	public static $product = '925_product';  //商品
	public static $member = '925_member';  //會員
	public static $fproduct = '925_fproduct';  //收藏商品
	public static $fshop = '925_fshop';  //收藏店家
	public static $order_sno = '925_ordersno';  //訂單編號查詢
	public static $order = '925_order';  //訂單
	public static $order_detail = '925_orderdetail';  //訂單詳細
	public static $freight = '925_freight';  //運費
	
	public static $bonus = '925_bonus';  //購物金
	public static $used_bonus = '925_used_bonus';  //購物金使用
	
	public static $store = '925_store';  //商店後台
    public static $store_log = '925_store_log';  //商店管理者操作紀錄

    public static $payment_detail = '925_payment_detail'; //綠界回傳的交易訊息
    
	/*****column*****/
	public static $col_admin = array('id' => 'admin_id', 'ind' => 'admin_ind', 'token_id' => 'admin_token_id', 'account' => 'admin_account', 'password' => 'admin_password', 'name' => 'admin_name', 'mobile' => 'admin_mobile', 'phone' => 'admin_phone', 'email' => 'admin_email', 'spic' => 'admin_spic', 'bpic' => 'admin_bpic', 'is_show' => 'admin_is_show', 'is_admin' => 'admin_is_admin', 'auth' => 'admin_auth', 'login_time' => 'admin_login_time', 'ip' => 'admin_ip', 'manager' => 'admin_manager', 'create_time' => 'admin_create_time', 'update_time' => 'admin_update_time', 'level' => 'admin_level');  //後台管理者
	public static $col_log = array('id' => 'log_id', 'name' => 'log_name', 'type' => 'log_type', 'comment' => 'log_comment', 'action' => 'log_action', 'ip' => 'log_ip', 'create_time'=>'log_create_time');  //後台管理者操作紀錄
	public static $col_banner = array('id' => 'banner_id', 'ind'=>'banner_ind', 'pic' => 'banner_pic', 'link' => 'banner_link', 'is_show'=>'banner_is_show', 'manager' => 'banner_manager', 'update_time' => 'banner_update_time', 'create_time' => 'banner_create_time');  //BANNER
	public static $col_ad = array('id' => 'ad_id', 'ind' => 'ad_ind', 'pic' => 'ad_pic', 'link' => 'ad_link', 'is_show' => 'ad_is_show', 'manager' => 'ad_manager', 'update_time' => 'ad_update_time', 'create_time' => 'ad_create_time');  //首頁廣告
    public static $col_meta = array('id' => 'meta_id', 'keyword' => 'meta_keyword', 'description' => 'meta_description', 'ga' => 'meta_ga', 'manager' => 'meta_manager', 'update_time' => 'meta_update_time', 'create_time' => 'meta_create_time');  //meta資料
    public static $col_footer = array('id'=>'ft_id', 'use'=>'ft_use', 'privacy'=>'ft_privacy', 'contact'=>'ft_contact', 'admin'=>'ft_admin', 'updatetime'=>'ft_updatetime', 'createtime'=>'ft_creatime','right'=>'ft_right');
	public static $col_ptype1 = array('id' => 'ptype1_id', 'ind'=>'ptype1_ind', 'name' => 'ptype1_name', 'pic' => 'ptype1_pic', 'is_show' => 'ptype1_is_show',  'manager' => 'ptype1_manager', 'update_time' => 'ptype1_update_time', 'create_time' => 'ptype1_create_time');  //商品一階分類
	public static $col_ptype2 = array('id' => 'ptype2_id', 'ind' => 'ptype2_ind', 'ptype1_id' => 'ptype2_ptype1_id', 'name' => 'ptype2_name', 'is_show' => 'ptype2_is_show', 'manager' => 'ptype2_manager', 'update_time' => 'ptype2_update_time', 'create_time' => 'ptype2_create_time');  //商品二階分類
	public static $col_product = array('id' => 'product_id', 'ind' => 'product_ind', 'ptype2_id' => 'product_ptype2_id', 'shop_id' => 'product_shop_id', 'sno' => 'product_sno', 'name' => 'product_name', 'price' => 'product_price', 'bonus' => 'product_bonus', 'pic' => 'product_pic', 'specification' => 'product_specification', 'information' => 'product_information', 'notice' => 'product_notice', 'is_hot' => 'product_is_hot', 'is_like' => 'product_is_like', 'is_index_top' => 'product_is_index_top', 'is_index' => 'product_is_index', 'is_top' => 'product_is_top', 'is_show' => 'product_is_show', 'manager'=>'product_manager', 'update_time' => 'product_update_time', 'create_time' => 'product_create_time');  //商品
	public static $col_member = array('id' => 'member_id', 'fb_id'=>'member_fb_id', 'account' => 'member_account', 'password' => 'member_password', 'name' => 'member_name', 'phone' => 'member_phone', 'email' => 'member_email', 'address' => 'member_address', 'is_show' => 'member_is_show', 'manager' => 'member_manager', 'update_time' => 'member_update_time', 'create_time' => 'member_create_time');  //會員	
	
	public static $col_fproduct = array('id' => 'fproduct_id', 'product_id' => 'fproduct_product_id', 'member_id' => 'fproduct_member_id', 'create_time' => 'fproduct_create_time'); //收藏商品
	public static $col_fshop = array('id' => 'fshop_id', 'shop_id' => 'fshop_shop_id', 'member_id' => 'fshop_member_id', 'create_time' => 'fshop_create_time'); //收藏店家
	public static $col_order_sno = array('id' => 'ordersno_id', 'year' => 'ordersno_year', 'month' => 'ordersno_month', 'day' => 'ordersno_day', 'sno' => 'ordersno_sno', 'create_time' => 'ordersno_create_time');  //訂單編號查詢
	public static $col_order = array('id' => 'order_id', 'sno' => 'order_sno', 'member_id' => 'order_member_id', 'shop_id' => 'order_shop_id', 'payment_type' => 'order_payment_type', 'payment_state' => 'order_payment_state', 'order_state' => 'order_state', 'express_type' => 'order_express_type', 'comment' => 'order_comment', 'total_price' => 'order_total_price', 'freight' => 'order_freight', 'bonus' => 'order_bonus', 'purchaser_name' => 'order_purchaser_name', 'recipient_name' => 'order_recipient_name',  'recipient_email' => 'order_recipient_email', 'recipient_phone' => 'order_recipient_phone', 'recipient_zcode' => 'order_recipient_zcode', 'recipient_address' => 'order_recipient_address', 'invoice' => 'order_invoice', 'company_invoice' => 'order_company_invoice', 'express_sno' => 'order_express_sno', 'manager' => 'order_manager', 'create_time' => 'order_create_time', 'update_time' => 'order_update_time', 'pay_time' => 'order_pay_time', 'express_time' => 'order_express_time', 'get_time' => 'order_get_time',  'payment_back'=>'order_payment_back');  //訂單

	public static $col_order_detail = array('id' => 'orderdetail_id', 'sno' => 'orderdetail_order_sno', 'product_id' => 'orderdetail_product_id', 'product_sno' => 'orderdetail_product_sno', 'shop_id' => 'orderdetail_shop_id', 'price' => 'orderdetail_price', 'amount' => 'orderdetail_amount', 'total_price' => 'orderdetail_total_price', 'state' => 'orderdetail_state', 'create_time' => 'orderdetail_create_time', 'update_time' => 'orderdetail_update_time');  //訂單詳細

	public static $col_freight = array('id' => 'freight_id', 'keyword' => 'freight_keyword', 'value' => 'freight_value', 'manager' => 'freight_manager', 'create_time' => 'freight_create_time', 'update_time' => 'freight_update_time');  //運費
	public static $col_bonus = array('id' => 'bonus_id', 'order_sno' => 'bonus_order_sno', 'member_id' => 'bonus_member_id', 'bonus' => 'bonus_bonus', 'comment' => 'bonus_comment', 'type' => 'bonus_type', 'is_use' => 'bonus_is_use', 'create_time' => 'bonus_create_time');  //購物金
	public static $col_used_bonus = array('id' => 'ubonus_id', 'member_id' => 'ubonus_member_id', 'money' => 'ubonus_money', 'bonus' => 'ubonus_bonus', 'comment' => 'ubonus_comment', 'type' => 'ubonus_type', 'create_time' => 'ubonus_create_time');  //購物金使用

	public static $col_store = array('id' => 'store_id', 'ind' => 'store_ind', 'token_id' => 'store_token_id', 'account' => 'store_account', 'password' => 'store_password', 'name' => 'store_name', 'mobile' => 'store_mobile', 'phone' => 'store_phone', 'email' => 'store_email', 'spic' => 'store_spic', 'bpic' => 'store_bpic', 'is_show' => 'store_is_show', 'is_admin' => 'store_is_admin', 'auth' => 'store_auth', 'login_time' => 'store_login_time', 'ip' => 'store_ip', 'manager' => 'store_manager', 'create_time' => 'store_create_time', 'update_time' => 'store_update_time', 'level' => 'store_level');  //後台管理者
	public static $col_store_log = array('id' => 'slog_id', 'name' => 'slog_name', 'type' => 'slog_type', 'comment' => 'slog_comment', 'action' => 'slog_action', 'ip' => 'slog_ip', 'create_time'=>'slog_create_time');  //後台管理者操作紀錄

	public static $col_payment_detail = array('id'=>'pd_id', 'sno'=>'pd_sno', 'trade_no'=>'pd_trade_no', 'payment_date'=>'pd_payment_date', 'payment_type'=>'pd_payment_type', 'trade_date'=>'pd_trade_date'); //綠界回傳的交易訊息
	
}
	
/*****END PHP*****/
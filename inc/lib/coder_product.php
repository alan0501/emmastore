<?php 
class CoderProduct{
	
	public function __construct(){
	}
	
	//取得sno
	public function getProductNO(){
		global $db, $table_psno, $colname_psno;
		$sno = "";
		$today = date("Y-m-d", time());
		$productno = explode("-", $today);
		$year = $productno[0];
		$month = $productno[1];
		$no = str_pad("1", 4, "0", STR_PAD_LEFT);
		
		$db -> begin();
		try{
			$sql = "SELECT * 
					FROM  $table_psno 
					WHERE {$colname_psno['year']} = $year AND {$colname_psno['month']}  = $month 
					ORDER BY {$colname_psno['sno']} DESC 
					LIMIT 1 FOR UPDATE";
			$row = $db -> query_first($sql);
			if($row){
				$id = $row[$colname_psno['id']];
				$sd = $row[$colname_psno['sno']]+1;
				$no = str_pad($sd, 4, "0", STR_PAD_LEFT);
			
				$data[$colname_psno['year']] = $year;
				$data[$colname_psno['month']] = $month;
				$data[$colname_psno['sno']] = $no;
				$data[$colname_psno['createtime']] = request_cd();
	
				$db -> query_update($table_psno, $data, "psno_id = '$id'");
				
				$sno = $year.$month.$no;
			}else{
				$data[$colname_psno['year']] = $year;
				$data[$colname_psno['month']] = $month;
				$data[$colname_psno['sno']] = $no;
				$data[$colname_psno['createtime']] = request_cd();
	
				$db -> query_insert($table_psno, $data);
			 
				$sno = $year.$month.$no;
			}
			$db -> commit();
			return $sno;
		}catch(Exception $e){
			$db -> rollback();
			throw new Exception("新增錯誤,請再操作一次.");
		}
	}
	
}
/*****END PHP*****/
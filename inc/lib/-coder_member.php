<?php
include_once("_config.php");
//***cln used {
	
class CoderMemberItem{
	public $acc = "";
	public $pwd = ""; 
	public $c_pwd = "";
	public $name = "";
	public $gender = "";
	public $email = "";
	public $telphone = "";
	public $phone = "";
	public $addr = "";
	public $verify_code = "";
	public $sub = "";
}


class CoderMember{
	static $table = "member_data";
	static $table_mailbox = 'member_mailbox'; //客服信箱

	public function __construct(){
	}
	
	//會員新增
	public function Insert(CoderMemberItem $member){ 
		global $db;
		if($this -> isExisit($member -> acc)){
			throw new Exception('會員帳號重覆,請重新輸入');
		}else{
			
			$data["md_account"] = $member -> acc;
			$data["md_password"] = md5($member -> pwd);
			$data["md_name"] = $member -> name;
			$data["md_gender"] = $member -> gender;
			$data["md_birthday"] = $member -> birthday;
			$data["md_email"] = $member -> email;
			$data["md_telphone"] = $member -> telphone;
			$data["md_phone"] = $member -> phone;
			$data["md_address"] = $member -> addr;
			$data["md_sub"] = $member -> sub;
			$data["md_ind"] = $member -> ind;
			$data["md_createtime"] = request_cd();
			$data["md_updatetime"] = request_cd();
			
			$memberid = $db -> query_insert(CoderMember::$table, $data);
		}
	}
	
	//會員修改
	public function Update(CoderMemberItem $member){ //修改會員資料
		global $db;
		if(!isset($_SESSION["session_cln_acc"]) || !isset($_SESSION["session_cln_name"]) || !isset($_SESSION["session_cln_id"])){
			throw new Exception('您已經登出系統，請重新登入');
		}else{
			$data["md_name"] = $member -> name;
			$data["md_gender"] = $member -> gender;
			$data["md_birthday"] = $member -> birthday;
			$data["md_email"] = $member -> email;
			$data["md_telphone"] = $member -> telphone;
			$data["md_phone"] = $member -> phone;
			$data["md_address"] = $member -> addr;
			$data["md_updatetime"] = request_cd();
			
			$db -> query_update(CoderMember::$table, $data, "md_id = ".$_SESSION["session_cln_id"]);
		}
	}
	
	//會員修改密碼
	public function pwd_Update(CoderMemberItem $member){ //修改會員資料
		global $db;
		if(!isset($_SESSION["session_cln_acc"]) || !isset($_SESSION["session_cln_name"]) || !isset($_SESSION["session_cln_id"])){
			throw new Exception('您已經登出系統，請重新登入');
		}else{
			$data["md_password"] = md5($member -> pwd);
			$data["md_updatetime"] = request_cd();
			
			$db -> query_update(CoderMember::$table, $data, "md_id = ".$_SESSION["session_cln_id"]);
		}
	}
	
	//忘記密碼
	public function forget_pwd_Update(CoderMemberItem $member){ //修改會員資料
		global $db;
		if(!$this -> isExisit($member -> acc)){
			throw new Exception('查無此帳號，請重新輸入!!');
		}else if(!$this -> isEmail($member -> acc, $member -> email)){
			throw new Exception('email跟此帳號註冊時不符，請重新輸入!!');
		}
		else{
			$sql = "select * from ".CoderMember::$table." where md_account ='".$member -> acc."'";
			$row = $db -> query_first($sql);
			
			$data["md_password"] = md5($member -> newpwd);
			$data["md_updatetime"] = request_cd();
			
			$db -> query_update(CoderMember::$table, $data, "md_id = ".$row["md_id"]);
		}
	}
	
	//客服信箱
	public function mailbox(CoderMemberItem $member){ 
		global $db;
		$data["mm_name"] = $member -> name;
		$data["mm_telphone"] = $member -> telphone;
		$data["mm_email"] = $member -> email;			
		$data["mm_content"] = $member -> content;
		$data["mm_updatetime"] = request_cd();
		$data["mm_createtime"] = request_cd();
		
		$memberid = $db -> query_insert(CoderMember::$table_mailbox, $data);
	}
	
	//檢查帳號是否存在
	static function isExisit($acc){
		global $db;
		if($db -> query_first("SELECT md_id FROM ".CoderMember::$table." WHERE md_account = '$acc'")){
			return true;
		}else{
			return false;
		}
	}
	
	//檢查帳號是否重覆
	public function chkAcc(CoderMemberItem $member){
		global $db;
		if($this -> isExisit($member -> acc)){
			throw new Exception('會員帳號重覆,請重新輸入');
		}
	}
	
	//檢查帳號的信箱是否正確
	static function isEmail($acc,$email){
		global $db;
		if($db -> query_first("SELECT * FROM ".CoderMember::$table." WHERE md_account = '$acc' and md_email = '$email'")){
			return true;
		}else{
			return false;
		}
	}
	
	//會員登入
	public function MemberLogin(CoderMemberItem $member){ 
		global $db;
		$sql = "select * from ".CoderMember::$table." where md_account ='".$member -> acc."' and md_password='".md5($member -> pwd)."'";
		$row = $db -> query_first($sql);
		if(!$this -> isAcc_Pwd($member -> acc, $member -> pwd)){
			throw new Exception('登入失敗,帳號或密碼不正確');
		}else if($this -> isAcc($member -> acc)){
			throw new Exception('帳號已停權，無法登入');
		}
		else{
			$_SESSION["session_cln_acc"] = $row["md_account"];
			$_SESSION["session_cln_name"] = $row["md_name"];
			$_SESSION["session_cln_id"] = $row["md_id"];
		}
	}
	
	//檢查帳號密碼是否錯誤
	static function isAcc_Pwd($acc, $pwd){
		global $db;
		if($db -> query_first("SELECT * FROM ".CoderMember::$table." WHERE md_account = '".$acc."' AND md_password = '".md5($pwd)."'")){
			return true;
		}else{
			return false;
		}
	}
	//檢查帳號是否停權
	static function isAcc($acc){
		global $db;
		if($db -> query_first("SELECT * FROM ".CoderMember::$table." WHERE md_ispublic = 1 AND md_account = '".$acc."'")){
			return true;
		}else{
			return false;
		}
	}
	
	
}


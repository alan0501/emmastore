<?php
//include_once("../_config.php");
	
class CoderMemberItem{
	public $account = "";
	public $password = ""; 
	public $name = "";
	public $email = "";
	public $phone = "";
	public $address = "";
	public $fb_id = "";
}


class CoderMember{
	
	public function __construct(){
	}
	
	//會員新增
	public function Insert(CoderMemberItem $member){ 
		global $db, $member_id;
		if($this -> isExisit($member -> account)){
			throw new Exception('會員帳號重覆,請重新輸入');
		}else{
			$data[coderDBConf::$col_member["account"]] = $member -> account;
			$data[coderDBConf::$col_member["password"]] = sha1($member -> password);
			$data[coderDBConf::$col_member["email"]] = $member -> email;
			$data[coderDBConf::$col_member["update_time"]] = request_cd();
			$data[coderDBConf::$col_member["create_time"]] = request_cd();
			
			$member_id = $db -> query_insert(coderDBConf::$member, $data);
		}
	}
	
	//會員登入
	public function memberLogin(CoderMemberItem $member){ 
		global $db;
		$sql = "SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["account"]." = '".$member -> account."' AND ".coderDBConf::$col_member["password"]." = '".sha1($member -> password)."'";
		$row = $db -> query_first($sql);

		if(!$this -> isAcc_Pwd($member -> account, $member -> password)){
			throw new Exception('登入失敗,帳號或密碼不正確!');
		}else if($this -> isAcc($member -> account)){
			throw new Exception('帳號已停權，無法登入!');
		}else{
			$_SESSION["session_925_acc"] = $row[coderDBConf::$col_member["account"]];
			//$_SESSION["session_cln_name"] = $row["md_name"];
			$_SESSION["session_925_id"] = $row[coderDBConf::$col_member["id"]];
		}
	}
	
	//檢查帳號是否存在
	static function isExisit($acc){
		global $db;
		if($db -> query_first("SELECT ".coderDBConf::$col_member["id"]." FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["account"]." = '$acc'")){
			return true;
		}else{
			return false;
		}
	}
	
	//檢查帳號是否重覆
	public function chkAcc(CoderMemberItem $member){
		global $db;
		if($this -> isExisit($member -> account)){
			throw new Exception('會員帳號重覆,請重新輸入');
		}
	}
	
	//檢查帳號是否存在
	public function isExistAcc($acc){
		global $db;
		if(!$this -> isExisit($acc)){
			throw new Exception('會員帳號不存在,請重新輸入');
		}
		/*if($db -> query_first("SELECT ".coderDBConf::$col_member["id"]." FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["account"]." = '$acc'")){
			return true; //有
		}else{
			return false; //無
		}*/
	}
	
	//檢查email是否正確
	public function isTrueEmail($acc, $email){
		global $db;
		if(!$this -> isEmail($acc, $email)){
			throw new Exception('Email不正確,請重新輸入!');
		}
	}
	
	//檢查email是否存在
	static function isExisitEmail($email){
		global $db;
		if(!(((isset($_SESSION["session_925_acc"]) && trim($_SESSION["session_925_acc"]) != "") || (isset($_SESSION["session_925_fb"]) && trim($_SESSION["session_925_fb"]) != "")) && isset($_SESSION["session_925_id"]) && trim($_SESSION["session_925_id"] != ""))){
			throw new Exception('您已經登出系統，請重新登入!');
		}else{
			$member_id = $_SESSION["session_925_id"];
		}

		if($db -> query_first("SELECT ".coderDBConf::$col_member["id"]." FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["email"]." = '$email' AND ".coderDBConf::$col_member["id"]."<>'$member_id'")){
			return true; //有
		}else{
			return false; //無
		}
	}
	
	
	//檢查email是否重覆
	public function chkEmail(CoderMemberItem $member){
		global $db;

		if($this -> isExisitEmail($member -> email)){
			throw new Exception('此email重覆,請重新輸入!');
		}
	}
	//檢查email是否重覆
	public function chkEmail_reg(CoderMemberItem $member){
		global $db;
		$email = $member -> email;

		if($db -> query_first("SELECT ".coderDBConf::$col_member["id"]." FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["email"]." = '$email'")){
			//throw new Exception('此email重覆,請重新輸入!');
			throw new Exception('此email重覆,請重新輸入!'); //有
		}else{
			return true; //無
		}
	}
	//檢查email是否自己
	public function chkEmail_self(CoderMemberItem $member){
		global $db;
		$email = $member -> email;
		if(isset($_SESSION["session_925_id"])){
			$m_id = $_SESSION["session_925_id"];
			
			$row = $db -> query_first("SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["id"]." = $m_id");
			
			$m_email = $row[coderDBConf::$col_member["email"]];

			if($m_email == $email){
				throw new Exception('請勿推薦自己!'); 
			}
		}else{
			return true;
		}
	}
	
	//檢查帳號密碼是否錯誤
	static function isAcc_Pwd($acc, $pwd){
		global $db;
		if($db -> query_first("SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["account"]." = '".$acc."' AND ".coderDBConf::$col_member["password"]." = '".sha1($pwd)."'")){
			return true;
		}else{
			return false;
		}
	}
	//檢查帳號是否停權
	static function isAcc($acc){
		global $db;
		if($db -> query_first("SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["is_show"]." = 1 AND ".coderDBConf::$col_member["account"]." = '".$acc."'")){
			return true;
		}else{
			return false;
		}
	}
	
	//會員修改
	public function Update(CoderMemberItem $member){
		global $db;
		if(!(((isset($_SESSION["session_925_acc"]) && trim($_SESSION["session_925_acc"]) != "") || (isset($_SESSION["session_925_fb"]) && trim($_SESSION["session_925_fb"]) != "")) && isset($_SESSION["session_925_id"]) && trim($_SESSION["session_925_id"] != ""))){
			throw new Exception('您已經登出系統，請重新登入!');
		}else{
			$data[coderDBConf::$col_member["name"]] = $member -> name;
			$data[coderDBConf::$col_member["email"]] = $member -> email;
			$data[coderDBConf::$col_member["phone"]] = $member -> phone;
			$data[coderDBConf::$col_member["address"]] = $member -> address;
			if($member -> new_pwd != ""){
				$data[coderDBConf::$col_member["password"]] = sha1($member -> new_pwd);
			}
			$data[coderDBConf::$col_member["manager"]] = isset($_SESSION["session_925_acc"]) ? $_SESSION["session_925_acc"] : $_SESSION["session_925_fb"];
			$data[coderDBConf::$col_member["update_time"]] = request_cd();
			
			$db -> query_update(coderDBConf::$member, $data, coderDBConf::$col_member["id"]." = ".$_SESSION["session_925_id"]);
		}
	}
	
	//忘記密碼
	public function forget_pwd_Update(CoderMemberItem $member){ //修改會員資料
		global $db, $sys_email, $sys_name;
		if(!$this -> isExisit($member -> account)){
			throw new Exception('查無此帳號，請重新輸入!!');
		}else if(!$this -> isEmail($member -> account, $member -> email)){
			throw new Exception('email跟此帳號註冊時不符，請重新輸入!!');
		}
		else{
			$sql = "SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["account"]." ='".$member -> account."'";
			$row = $db -> query_first($sql);
			$id = $row[coderDBConf::$col_member["id"]];
			
			$data[coderDBConf::$col_member["password"]] = sha1($member -> newpwd);
			$data[coderDBConf::$col_member["update_time"]] = request_cd();
			
			$db -> query_update(coderDBConf::$member, $data, coderDBConf::$col_member["id"]." = '$id'");
			
			
			$fr_em = $sys_email;
			$fr_na = $sys_name;
			$to_em = $row[coderDBConf::$col_member["email"]];
			$to_na = $row[coderDBConf::$col_member["name"]];
			$subject = "新密碼通知";
			$msg = "您的新密碼為:".$member -> newpwd.",登入後,請至會員中心設定您慣用的密碼!";
			$bcc = "";
			sendMail($fr_em, $fr_na, $to_em, $to_na, $subject, $msg, $bcc);
		}
	}
	
	//檢查帳號的信箱是否正確
	static function isEmail($acc, $email){
		global $db;
		if($db -> query_first("SELECT * FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["account"]." = '$acc' AND ".coderDBConf::$col_member["email"]." = '$email'")){
			return true;
		}else{
			return false;
		}
	}
	
	//推薦人紅利
	static function getCommendBonus($email){
		global $db;
		$row = $db -> query_first("SELECT ".coderDBConf::$col_member["id"]." FROM ".coderDBConf::$member." WHERE ".coderDBConf::$col_member["email"]." = '$email'");
		
		if($row){
			$data[coderDBConf::$col_bonus["member_id"]] = $row[coderDBConf::$col_member["id"]];
			$data[coderDBConf::$col_bonus["bonus"]] = 10;
			$data[coderDBConf::$col_bonus["comment"]] = "推薦";
			$data[coderDBConf::$col_bonus["type"]] = "1";
			$data[coderDBConf::$col_bonus["create_time"]] = request_cd();
			
			$db -> query_insert(coderDBConf::$bonus, $data);
		}
	}
	
	
	
	
	
	
	
	
	









	
	
	
	//會員修改密碼
	public function pwd_Update(CoderMemberItem $member){ //修改會員資料
		global $db;
		if(!isset($_SESSION["session_925_acc"]) || !isset($_SESSION["session_925_name"]) || !isset($_SESSION["session_925_id"])){
			throw new Exception('您已經登出系統，請重新登入');
		}else{
			$data["md_password"] = sha1($member -> pwd);
			$data["md_updatetime"] = request_cd();
			
			$db -> query_update(CoderMember::$table, $data, "md_id = ".$_SESSION["session_cln_id"]);
		}
	}
	
	
	
	//客服信箱
	public function mailbox(CoderMemberItem $member){ 
		global $db;
		$data["mm_name"] = $member -> name;
		$data["mm_telphone"] = $member -> telphone;
		$data["mm_email"] = $member -> email;			
		$data["mm_content"] = $member -> content;
		$data["mm_updatetime"] = request_cd();
		$data["mm_createtime"] = request_cd();
		
		$memberid = $db -> query_insert(CoderMember::$table_mailbox, $data);
	}
	
	
	
	public static function getLoginUser(){
		global $db;
        if(isset($_SESSION['session_cln_id'])){
			$m_id = $_SESSION['session_cln_id'];
            //throw new Exception('請先登入');
            //return self::$_user;
        return $db -> query_first("SELECT * FROM ".CoderMember::$table." WHERE md_id ='$m_id'");
    	}
	}
}


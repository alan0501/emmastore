<?php
class coderAdminLog{
	public static $type = array(
		'admin' => array('key' => 1, 'name' => '登入帳號'),
		'banner' => array('key' => 2, 'name' => 'banner'),
		'ad' => array('key' => 3, 'name' => '小廣告'),
		'meta' => array('key' => 4, 'name' => 'meta'),
		'footer' => array('key' => 12, 'name' => 'footer'),
		'ptype1' => array('key' => 5, 'name' => '商品一階分類'),
		'ptype2' => array('key' => 6, 'name' => '商品二階分類'),
		'product' => array('key' => 7, 'name' => '商品'),
		'member' => array('key' => 8, 'name' => '會員'),
		'order' => array('key' => 9, 'name' => '訂單'),
		'freight' => array('key' => 10, 'name' => '運費'),
		/* 'shop_order' => array('key' => 11, 'name' => '店家_訂單'),
		'shop_product' => array('key' => 12, 'name' => '店家_商品'),
		'shop_admin' => array('key' => 13, 'name' => '店家_管理'), */
		'store' => array('key' => 11, 'name' => '店家'),
		
		
	
	);
	
	public static $action = array(
		'login' => array('key'=>0, 'name' => '登入'),
		'view' => array('key' => 2, 'name' => '瀏覽'),
		'add' => array('key' => 3, 'name' => '新增'),
		'edit' => array('key' => 4, 'name' => '編輯'),
		'del' => array('key' => 5, 'name' => '刪除'),
		'copy' => array('key' => 6, 'name' => '複製')
	);
	
	private static $_type = NULL;
	private static $_action = NULL;
	
	public static function clearSession(){
		unset($_SESSION['loginfo']);
	}
	
	public static function insert($username, $type, $act, $descript = ""){
		global $db;
		if(!isset(self::$type[$type])){
			self::oops('記錄類型錯誤!');
		}
		if(!isset(self::$action[$act])){
			self::oops('記錄動作錯誤!');
		}			
		if(!isset($_SESSION['loginfo']) || $_SESSION['loginfo'] != $type.$descript){
			$data = array();
			$data[coderDBConf::$col_log['name']] = $username;
			$data[coderDBConf::$col_log['type']] = self::$type[$type]['key'];
			$data[coderDBConf::$col_log['action']] = self::$action[$act]['key'];
			$data[coderDBConf::$col_log['create_time']] = request_cd();
			$data[coderDBConf::$col_log['ip']] = request_ip();
			$data[coderDBConf::$col_log['comment']] = $descript;
			if($db -> query_insert(coderDBConf::$log, $data)){
				 $_SESSION['loginfo'] = $type.$descript;
			}
		}
	}
	
	public static function getLogByUser($username, $limit = 10){
		global $db;
		$rows = $db -> fetch_all_array('SELECT * FROM '.coderDBConf::$log.' WHERE '.coderDBConf::$col_log['name'].' = :username ORDER BY '.coderDBConf::$col_log['create_time'].' DESC LIMIT '.$limit, array(':username' => $username));
		$len = count($rows);
		for($i = 0; $i < $len; $i ++){
			$rows[$i][coderDBConf::$col_log['type']] = self::getTypeNameByKey($rows[$i][coderDBConf::$col_log['type']]);
			$rows[$i][coderDBConf::$col_log['action']] = self::getActionNameByKey($rows[$i][coderDBConf::$col_log['action']]);
		}
		return $rows;
	}
	
	public static function getTypeIndex($value){
		if(self::$_type == NULL){
			self::$_type = coderHelp::makeAryKeyValue(self::$type, 'key');
		}
		return self::$_type[$value];
	}
	
	public static function getActionIndex($value){
		if(self::$_action == NULL){
			self::$_action = coderHelp::makeAryKeyValue(self::$action, 'key');
		}
		return self::$_action[$value];
	}	
	
	public static function getTypeNameByKey($key){
		return self::$type[self::getTypeIndex($key)]['name'];
	}
	
	public static function getActionNameByKey($key){
		return self::$action[self::getActionIndex($key)]['name'];
	}	
	
	public static function getTypeName($type){
		return (isset(self::$type[$type])) ? self::$type[$type]['name'] : '' ;
	}
	
	public static function getActionName($act){
		return (isset(self::$action[$act])) ? self::$action[$act]['name'] : '' ;
	}
		
	private static function getItem($type){
		foreach(self::$type as $key => $item){
			if($key == $type){
				return $item;
			}
		}
		return false;
	}
	
	private static function oops($msg){
		echo('coderAdminLog:'.$msg);
	}
}
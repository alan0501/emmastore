<?php
class coderAdminStore{
	public static $Auth = array(
		'order' => array('key' => 2, 'name' => '訂單', 'icon' => 'icon-edit'),
		'product' => array('key' => 4, 'name' => '商品', 'icon' => 'icon-gift'),
		/* 'member' => array('key' => 8, 'name' => '會員', 'icon' => 'icon-group'),
		'freight' => array('key' => 32, 'name' => '運費', 'icon' => 'icon-truck'),
		'index' => array('key' => 2, 'name' => '首頁', 'icon' => 'icon-globe'),
		'store' => array('key' => 64, 'name' => '店家', 'icon' => 'icon-group'), */
		'admin' => array('key' => 1, 'name' => '登入帳號', 'icon' => 'icon-lock'),
		//'shop' => array('key' => 64, 'name' => '店家', 'icon' => 'icon-group')

		
		
	);

	public static function Auth($key){
		if(isset(self::$Auth[$key])){
			return self::$Auth[$key];
		}else{
			die('權限錯誤~');
		}
	}

	public static function vaild($key){
		if(!self::isAuth($key)){
			self::drawBody('授權失敗!', '您未擁有操作此項功能的權限,請聯絡系統管理員。');
		}
	}

	public static function isAuth($key){
		$user = self::getUser();
		$item = self::Auth($key);

		if($user['auth'] & $item['key']){
			return true;
		}
		return false;
	}

	public static function getAuthAry($level){
		$ary = array();
		$auth_ary = self::$Auth;
		if($level == 1){
			unset($auth_ary["shop"]);
		}
		foreach($auth_ary as $item){
			
			$ary[] = self::getReturnElement($item);
		}
		return $ary;
	}

	public static function getAuthListAryByInt($int){
		$ary = array();

		foreach(self::$Auth as $item){
			if($int & $item['key']){
				$ary[] = self::getReturnElement($item);
			}
		}
		return $ary;
	}

	private static function getReturnElement($item){
		return array('key' => $item['key'], 'name' => $item['name']);
	}

	public static function change_admin_data($username){
		global $db, $admin_path_store;
		$sql = "SELECT * FROM ".coderDBConf::$store." WHERE ".coderDBConf::$col_store['account']." = :username";
		$row = $db -> query_first($sql, array(':username' => $username));
		if($row){
			$user = array(
				'id' => $row[coderDBConf::$col_store['id']],
				'username' => $row[coderDBConf::$col_store['account']],
				'level' => $row[coderDBConf::$col_store['level']],
				'name' => $row[coderDBConf::$col_store['name']],
				'pic' => $admin_path_store.'s'.$row[coderDBConf::$col_store['spic']],
				'time' => datetime('A h:i'),
				'auth' => ($row[coderDBConf::$col_store['is_admin']]) ? 1099511627775 : $row[coderDBConf::$col_store['auth']]);
			
			self::setUser($user);
		}
	}

	public static function loginOut(){
		unCookie('mid');
		unset($_SESSION['store_manage_loginuser']);
	}

	public static function login($username, $password, $remember_me = ''){
		global $db, $admin_path_store;
		$password = sha1($password);

	  	/*$sql = "SELECT username, name, id, mid, ispublic, pic, auth, isadmin FROM ".coderDBConf::$admin." WHERE username=:username and password=:password";*/
		$sql = "SELECT * FROM ".coderDBConf::$store." WHERE ".coderDBConf::$col_store['account']." = :username and ".coderDBConf::$col_store['password']." = :password";

	  	$row = $db -> query_first($sql, array(':username' => $username, ':password' => $password));

	  	if(!$row){
			coderAdminStoreLog::insert($username, 'admin', 'login', '登入失敗-帳密不正確');
			throw new Exception('帳號或密碼不正確!');
		}else if($row[coderDBConf::$col_store['is_show']] != 1){
			coderAdminStoreLog::insert($username, 'admin', 'login', '登入失敗-己被停權');
			throw new Exception('此帳號己被停權!');
		}else{
			$user = array('id' => $row[coderDBConf::$col_store['id']], 'username' => $row[coderDBConf::$col_store['account']], 'level' => $row[coderDBConf::$col_store['level']], 'name' => $row[coderDBConf::$col_store['name']], 'pic' => $admin_path_store.'s'.$row[coderDBConf::$col_store['spic']], 'time' => datetime('A h:i'), 'auth' => ($row[coderDBConf::$col_store['is_admin']]) ? 1099511627775 : $row[coderDBConf::$col_store['auth']]);
			
			$mid_sessionid = substr(substr($row[coderDBConf::$col_store['token_id']], 0, 32).time().session_id(), 0, 65);
			
			$db -> execute("UPDATE ".coderDBConf::$store." SET ".coderDBConf::$col_store['login_time']." = :logintime, ".coderDBConf::$col_store['ip']." = :ip, ".coderDBConf::$col_store['token_id']." = :mid WHERE ".coderDBConf::$col_store['account']." = :username ", array(':logintime' => request_cd(), ':ip' => request_ip(), ':username' => $username, 'mid' => $mid_sessionid));

			if($remember_me === 1){
				saveCookieHour('mid', $mid_sessionid, 24*7);
			}else{
				unCookie('mid');
			}
			self::setUser($user);
			coderAdminStoreLog::insert($username, 'admin', 'login', '成功');
		}
	}


	public static function setUser($ary){
		if(!is_array($ary)){
			throw new Exception("USER格式不正確,儲存錯誤!");
		}else{
			$_SESSION['store_manage_loginuser'] = serialize($ary);
		}
	}
	
	public static function getUser_cookie(){
		global $db, $admin_path_store;
		$mid = getCookie('mid');
		if($mid != ''){
			$sql = "SELECT * FROM ".coderDBConf::$store." WHERE ".coderDBConf::$col_store['token_id']." = :mid";
			$row = $db -> query_first($sql, array(':mid' => $mid));
			if($row && $row[coderDBConf::$col_store['is_show']] == 1){
				$user = array('id' => $row[coderDBConf::$col_store['id']], 'username' => $row[coderDBConf::$col_store['account']], 'level' => $row[coderDBConf::$col_store['level']], 'name' => $row[coderDBConf::$col_store['name']], 'pic' => $admin_path_store.'b'.$row[coderDBConf::$col_store['bpic']], 'time' => datetime('A h:i'), 'auth' => ($row[coderDBConf::$col_store['is_admin']])? 1099511627775 : $row[coderDBConf::$col_store['auth']]);
				
				$db -> execute("UPDATE ".coderDBConf::$store." SET ".coderDBConf::$col_store['login_time']." = :logintime, ".coderDBConf::$col_store['ip']." = :ip WHERE ".coderDBConf::$col_store['account']." = :username ", array(':logintime' => request_cd(), ':ip' => request_ip(), ':username' => $row[coderDBConf::$col_store['account']]));
				
				self::setUser($user);
			}
		}
	}
	
	public static function getUser(){
		if(!isset($_SESSION['store_manage_loginuser'])){
			self::showLoginPage();
		}else{
			$user = unserialize($_SESSION['store_manage_loginuser']);

			if(!is_array($user)){
				self::showLoginPage();
			}else{
				return $user;
			}
		}
	}

	public static function sayHello(){
		$talktype = rand(0, 1);
		//一般問候
 		if($talktype == 0){
			$ary_talk = array('歡迎登入。', '感謝您使用本系統', 'Hello :)', ' 阿囉哈', '記得要微笑 : )', '每隔30分鐘記得喝水,出去活動一下。', '來杯咖啡嗎?', ' hihi!!');
		}else{
		//依時間問候
			$hour = datetime('H');
			if($hour > 5 && $hour < 9){ //早上5點到9點登入
				$ary_talk = array('早安!','早起的鳥兒有蟲吃!','您知道嗎?清晨的空氣特別新鮮','您今天真早','您早','來杯咖啡嗎?','記得吃早餐!','今天真是個美好的一天,不是嗎?','一日之計在於晨');
			}else if($hour > 9 && $hour < 11){
				$ary_talk = array('您今天加油了嗎?',' 每天告訴自己一次,我真的很不錯','抱最大的希望，為最大的努力，做最壞的打算','喝口水吧','每天都是一年中最美好的日子');
			}else if($hour > 10 && $hour < 14){
				$ary_talk = array('吃過飯了嗎?',' 記得多吃點蔬菜水果喔~ ','來根香蕉吧!','多吃香蕉有益健康');
			}else if($hour > 13 && $hour < 17){
				$ary_talk = array('來杯下午茶吧。','每一件事都要用多方面的角度來看它','美好的生命應該充滿期待、驚喜和感激。','天才是百分之一的靈感加上百分之九十九的努力','您累了嗎? 喝杯水吧休息一下吧。','肚子餓的話,吃些點心吧。');
			}else if($hour > 16 && $hour < 20){
				$ary_talk = array('今天沒什麼事就早點下班吧','記得吃晚餐','晚餐不要吃太多,身體才健康','想像力比知識更重要','晚餐請不要吃太多');
			}else if($hour > 19 && $hour < 23){
				$ary_talk = array('您辛苦了','別忙到太晚','加油加油!','如果你曾歌頌黎明，那麼也請你擁抱黑夜','吃晚餐了嗎?','沒什麼事就早點休息吧','研究指出,加班會降低工作效率','千萬別吃宵夜','睡前別喝太多水,會水腄');
			}else if($hour > 22 && $hour < 02){
				$ary_talk = array('請去休息吧!','研究指出,加班會降低工作效率','您睡不著嗎?','感謝每盞亮著的燈,沒留下你一個人','經驗是由痛苦中粹取出來的','天才是百分之一的靈感加上百分之九十九的努力');
			}else{
				$ary_talk = array('................','唔....','嗯.....','現在是下班時間吧?','天才是百分之一的靈感加上百分之九十九的努力','XD','囧');
			}
			echo $hour;
		}
		return '您好 '.$ary_talk[rand(0, count($ary_talk)-1)];
	}


	public static function showLoginPage(){
		self::drawBody('登入逾時', '您尚未登入或超過登入期限<br>為了確保安全性<br>請按下方連結重新登入。');
	}

	public static function drawMenu($submenu){
		$user = self::getUser();
		$auth = $user['auth'];
		$query_str = $_SERVER['QUERY_STRING'];
		$query_str = $query_str != '' ? '?'.$query_str : '';
		$pagename = realpath(request_basename()).$query_str;
		$auth_ary = self::$Auth;
		$adminuser = coderAdminStore::getUser();
		if($adminuser['level'] == 1){
			unset($auth_ary["shop"]);
		}
		
		foreach($auth_ary as $key => $item){
			if(!($user['auth'] & $item['key'])){
				continue;
			}
			$classname = '';

			$str = '<a href="javascript:void(0)" class="dropdown-toggle">
						<i class="'.$item['icon'].'"></i>
						<span>'.$item['name'].'管理</span>
						<b class="arrow icon-angle-right"></b>
					</a>';

			if(array_key_exists($key, $submenu)){
				$str .= '<ul class="submenu">';

				foreach($submenu[$key] as $subkey => $subitem){
					$index = strpos($subitem, '?');

					if($index > 0){
						$_subitem = realpath(substr($subitem, 0, $index)).substr($subitem, $index);
					}else{
						$_subitem = realpath($subitem);
					}

					//if($pagename==$_subitem)
					if(realpath(request_basename()) == $_subitem || $pagename==$_subitem){
						$classname = ' class="active" ';
						$str .= '<li '.$classname.'><a href="'.$subitem.'" >'.$subkey.'</a></li>';
					}else{
						$str .= '<li ><a href="'.$subitem.'" >'.$subkey.'</a></li>';
					}
				}
				$str .= '</ul>';
			}
			echo '<li '.$classname.'>'.$str.'</li>';
		}
	}

	public static function drawBody($title, $content){
		die('
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
				<meta name="description" content="">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">

				<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
				<link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">
				<link rel="stylesheet" href="../css/flaty.css">
				<link rel="stylesheet" href="../css/flaty-responsive.css">
			</head>
			<body class="error-page">
				<div class="error-wrapper">
					<div></div>
					<h5><img src="../images/logo.png"><span>OOPS</span></h5>
					<p><h5>'.$title.'</h5></p>
					<p>'.$content.'<hr>
						<p class="clearfix">
							<a href="javascript:void(0)" onclick="window.location.href = document.referrer" class="pull-left">← 回到前一頁</a>
							<a href="../login.php" class="pull-right"> 回到登入頁</a>
						</p>
					</p>
				</div>
				<!--basic scripts-->
				<script src="../assets/jquery/jquery-2.0.3.min.js"></script>
				<script src="assets/bootstrap/js/bootstrap.min.js"></script>
			</body>
			</html>
			');
	}

}

/*****END PHP*****/
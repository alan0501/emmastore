<?php
class coder_calendar{
    private $year;
    private $month;
    private $weeks  = array('日', '一', '二', '三', '四', '五', '六');//星期日開始
	private $today = "";
	private $today_year = "";
	private $today_month = "";

    function __construct($options = array()){
        $this -> year = date('Y');
        $this -> month = date('m');
		$this -> today = date('d');
		$this -> today_year = date('Y');
        $this -> today_month = date('m');
		
        $vars = get_class_vars(get_class($this)); //取類別中的預設的屬性,以陣列回傳

        foreach($options as $key => $value){	//取丟進來的值
            if(array_key_exists($key, $vars)){
                $this -> $key = $value;
            }
        }
    }
	
	
    function display($year, $month, $rows){
  		echo '<table class="calen_table"><tr><td><table>';
        	$this -> showChangeDate($year, $month);
        	$this -> showWeeks();
        	$this -> showDays($year, $month, $rows);
		echo '</table></td></tr></table>';
    }
	/*** 顯示星期幾 ***/ 
    private function showWeeks(){
        echo '<tr class="calen_title">';
        foreach($this -> weeks as $title){
            echo '<td><font size="3" face="Tahoma">'.$title.'</font></td>';
        }
        echo '</tr>';
    }
	/*** 顯示日期天數 ***/
    private function showDays($year, $month, $rows){
		global $db;
		
        //求當月總天數，每周起屹星期
  		$firstDay = mktime(0, 0, 0, $month, 1, $year);
        //$firstDay2 = mktime(0, 0, 0, $month, 0, $year);
        $starDay = date('w', $firstDay);//星期幾(0 for Sunday, 6 for Saturday)
        $days = date('t', $firstDay);//The number of days in the given month
		
        echo '<tr>';
        for($i = 0; $i < $starDay; $i ++){
            echo '<td>&nbsp;</td>';
        }
		
		//$y = "";
		//$m = "";
		
		$d = "";	$id = "";	$id_d = "";	$title = ""; $title_id = "";
		foreach($rows as $row)
		{

			//$y= substr($row["day"],0,4);
			//$m = substr($row["day"],5,2);
			$d []= sprintf("%2d",substr($row["day"],8,2));
			$id [] = $row["id"];
			$title [] = $row["title"];
			$id_d = array_combine($id, $d);
			$title_id = array_combine($id, $title);
			//print_r($title_id);
			//echo $row["day"]."<br>";
			//echo $row["id"]."<br>";
		}
		
		for($j = 1; $j <= $days; $j ++){
			if($i > 0 && $i%7 == 0){
				echo '</tr><tr>';
			}
			
			if(count($rows) != 0)
			{
				if(in_array(sprintf("%2d", $j), $d))
				{
					$all_id = "";	$ALLkey_id = "";	$all_title = ""; $key_id = "";
					$all_id = array_keys($id_d,sprintf("%2d", $j));	
								
					//print_r($all); 
					foreach($all_id as $key)
					{
						
						$href = "";
						//課程連結
						if(substr($key,0,1) == "s")
						{
							//判斷第一階和第二階 是否勾選顯示
							$rows_sid = class_index_calendar::getShoolList_sid(substr($key,1));
							if(count($rows_sid) != "0")
							{
								$href = "school_openclass_info_".substr($key,1)."_0.html";
								//$key 是主題 id
								$key_id [] = "s";
							}
							else
							{
								//$key 是主題 id
								$key_id [] = "l";
							}
						}
						else
						{
							
							$row = class_lecture::getList_info(substr($key,1));
							$type = "";
							$type = $row["lc_type"];
							switch ($type) {
								case '1':
									$href = "lecture_cln_class_info_".substr($key,1).".html";
									break;
								case '2':
									$href = "lecture_charity_info_".substr($key,1).".html";
									break;
								case '3':
									$href = "lecture_all_area_info_".substr($key,1).".html";
									break;
							}
							//$key 是主題 id
							$key_id [] = substr($key,0,1);
						}
						
						
						if($href != "")
						{
							$ALLkey_id .= '<a href="'.$href.'"><font size="2" face="Tahoma">'." 主題：".$title_id[$key].'</font></a>&nbsp;'."<br>";
						}
						
					}
					
					$val_id = "";
					//print_r(array_unique($key_id));					
					foreach(array_unique($key_id) as $k => $val)
					{
						$val_id .= $val;
					}
					
					$style = "";
					//區分課程 背景色
					if($val_id == "sl" || $val_id == "ls")
					{
						$style = 'style="background-color:#ffff00;color:black;"';
					}else if($val_id == "s")
					{
						$style = 'style="background-color:#fe0000;color:black;"';
					}else if($val_id == "l")
					{
						$style = 'style="background-color:#00ff01;color:black;"';
					}
					
					//日期課程
					if($ALLkey_id != "")
					{
						echo '<td valign=top><div class="calendar_day"'.$style.'>'.sprintf("%2d", $j).'<div class="calendar_text"><span class="arrow_t_int"></span>
    <span class="arrow_t_out"></span>'.$ALLkey_id.'</div></div></td>';
					}
					else
					{
						echo '<td valign=top><font size="2" face="Tahoma">'.sprintf("%2d", $j).'</font></td>';
					}
					
				}
				else
				{
					echo '<td valign=top><font size="2" face="Tahoma">'.sprintf("%2d", $j).'</font></td>';
				}
			}
			else
			{
				echo '<td valign=top><font size="2" face="Tahoma">'.sprintf("%2d", $j).'</font></td>';
			}
            $i ++;
        }
        echo '</tr>';
				
    }
	
	/*** 上下月控制 ***/   
    private function showChangeDate($year, $month){
       
       //$url = basename($_SERVER['PHP_SELF']);
		
		if($month == '01'){
			$thismo = '01';
		}else if($month == '02'){
			$thismo = '02';
		}else if($month == '03'){
			$thismo = '03';
		}else if($month == '04'){
			$thismo = '04';
		}else if($month == '05'){
			$thismo = '05';
		}else if($month == '06'){
			$thismo = '06';
		}else if($month == '07'){
			$thismo = '07';
		}else if($month == '08'){
			$thismo = '08';
		}else if($month == '09'){
			$thismo = '09 ';
		}else if($month == '10'){
			$thismo = '10';
		}else if($month == '11'){
			$thismo = '11';
		}else if($month == '12'){
			$thismo = '12';
		}

		echo '<tr class="calen_title">';

		echo '<td align=center><font size="3" face="Tahoma"><a href="javascript:void(0);" rel="nofollow" class="pre" year="'.$this -> preYear($year, $month).'" month="'.$this -> preMonth($year, $month).'"><</a></font></td>';
		
		echo '<td colspan=5 align=center><font size="3" face="Tahoma">'.$year.' / '.$thismo.'</td>';

		echo '<td align=center><font size="3" face="Tahoma"><a href="javascript:void(0);" rel="nofollow" class="next" year="'.$this -> nextYear($year, $month).'" month="'.$this -> nextMonth($year, $month).'">></a></font></td>';
	
		echo '</tr>';	
		
	}
	
	/*** 控制/設定開始年份 ***/
    private function preYearUrl($year, $month){
		$year = ($year <= 2000) ? 2000 : $year - 1 ;
        	return 'year='.$year.'&month='.$month;
    	}
	/*** 控制/設定結束年份 ***/
    private function nextYearUrl($year, $month){
		$year = ($year >= 2015)? 2015 : $year + 1;
        	return 'year='.$year.'&month='.$month;
    	}
	/*** 控制/設定開始月份 ***/
    private function preMonthUrl($year, $month){
        if($month == 1){
            $month = 12;
            $year = ($year <= 2000) ? 2000 : $year - 1 ;
        }else{
            $month--;
        }       
        return 'year='.$year.'&month='.$month;
    }
	/*** 控制/設定結束月份 ***/	
	private function nextMonthUrl($year, $month){
        if($month == 12){
            $month = 1;
            $year = ($year >= 2015) ? 2015 : $year + 1;
        }else{
            $month++;
        }
        return 'year='.$year.'&month='.$month;
    }
	/*** 前一年 ***/	
	private function preYear($year, $month){
        if($month == 1){
            $month = 12;
            $year = ($year <= 2000) ? 2000 : $year - 1 ;
        }else{
            $month--;
        }       
        return $year;
    }
	/*** 前一月 ***/
	private function preMonth($year, $month){
        if($month == 1){
            $month = 12;
            $year = ($year <= 2000) ? 2000 : $year - 1 ;
        }else{
            $month--;
        }       
        return $month;
    }
	/*** 下一年 ***/   
	private function nextYear($year, $month){
        if($month == 12){
            $month = 1;
            //$year = ($year >= 2015) ? 2016 : $year + 1;
			$year = $year + 1;
        }else{
            $month++;
        }
        return $year;
    }
	/*** 下一月 ***/ 
	private function nextMonth($year, $month){
        if($month == 12){
            $month = 1;
            //$year = ($year >= 2015) ? 2015 : $year + 1;
			$year = $year + 1;
        }else{
            $month++;
        }
        //return $month;
		return $month;
    }
}

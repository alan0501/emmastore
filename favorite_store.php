<?php
include_once("_config.php");

if(!isLogin()){
	script("您已經登出系統，請重新登入!!", "index.html");
}else{
	$member_id = $_SESSION["session_925_id"];
}

$row_member = class_member::getInfo($member_id);
if($row_member){
	$acc = $row_member[coderDBConf::$col_member["account"]];
	$name = $row_member[coderDBConf::$col_member["name"]];
	$fb_id = $row_member[coderDBConf::$col_member["fb_id"]];
	$phone = $row_member[coderDBConf::$col_member["phone"]];
	$email = $row_member[coderDBConf::$col_member["email"]];
	$address = $row_member[coderDBConf::$col_member["address"]];
	$pwd = $row_member[coderDBConf::$col_member["password"]];
}

$rows_fshop = class_shop::getFShop($member_id);
$total = count($rows_fshop);

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix">
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">Q點購物中心</a></div>
            <div class="m_login">
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
				<div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}else{
				?>
				<div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
                
            </form>
        </div>
        <div id="top_right"> 
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
			?>
            <div class="top_login"><a href="member.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class="gray ">
    <div id="wrapper">
        <div id="member_menu">
            <dl class="clearfix" id="smbar">
                <dt><img src="images/member.jpg" align="absmiddle"/></dt>
                <dt class="email"><?php echo $acc; ?><br />
                    <span> <?php echo $email; ?> </span></dt>
                <dd><a href="cart.html">我的購物車</a></dd>
                <dd ><a href="bought.html">已買到的商品</a></dd>
                <dd><a href="favorite_product.html">收藏的商品</a></dd>
                <dd class="selected"><a href="favorite_store.html">收藏的店家</a></dd>
                <dd><a href="gold.html">購物金</a></dd>
                <dd><a href="member.html">會員管理</a></dd>
            </dl>
        </div>
        <div id="wrap-main" class="clearfix">
            <div id="column_left">
                <ul class="category_list">
                    <h3>收藏的店家</h3>
                    <li>總共收藏<?php echo $total; ?>家店家</li>
                </ul>
            </div>
            <div id="column_right">
                <ul class="smail_p clearfix">
                	<?php
						if($total > 0){
							foreach($rows_fshop as $row_fshop){
					?>
                    <li>
                        <a href="store_<?php echo $row_fshop[coderDBConf::$col_fshop["shop_id"]]; ?>.html">
                            <img src="<?php echo $web_path_store."s".$row_fshop[coderDBConf::$col_store["spic"]]; ?>" width="200" height="200" />
                        </a>
                        <div class="prd_name">
                            <a href="store_<?php echo $row_fshop[coderDBConf::$col_fshop["shop_id"]]; ?>.html">
                                <?php echo $row_fshop[coderDBConf::$col_store["name"]]; ?>
                            </a>
                            <span class="fs_del_btn" fs_id="<?php echo $row_fshop[coderDBConf::$col_fshop["id"]]; ?>">刪除</span>
                        </div>
                    </li>
                    <?php
							}
						}else{
							echo("<li style='font-size:16px;'>您尚未收藏店家!</li>");	
						}
					?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end-->
<script>
carItemShow();
</script>
</body>
</html>
<?php
$db -> close();
?>
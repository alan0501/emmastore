<?php
include_once("_config.php");
include_once($inc_path.'lib/_shoppingcar.php');

if(!isLogin()){
	script("您已經登出系統，請重新登入!!", "index.html");
}else{
	$member_id = $_SESSION["session_925_id"];
}

if(isset($_SESSION["commend_email"])){
	unset($_SESSION["commend_email"]);
}

$shop_id = get("shop_id", 1);
if($shop_id <= 0){
	script("資料傳送錯誤!", "cart.html");
}
$row_member = class_member::getInfo($member_id);
if($row_member){
	$acc = $row_member[coderDBConf::$col_member["account"]];
	$name = $row_member[coderDBConf::$col_member["name"]];
	$fb_id = $row_member[coderDBConf::$col_member["fb_id"]];
	$phone = $row_member[coderDBConf::$col_member["phone"]];
	$email = $row_member[coderDBConf::$col_member["email"]];
	$address = $row_member[coderDBConf::$col_member["address"]];
	$pwd = $row_member[coderDBConf::$col_member["password"]];
}

if(isset($_SESSION["car"]) && is_array(unserialize($_SESSION["car"]))){
	$car = unserialize($_SESSION["car"]);
	foreach($car as $key => $item){
		$car[$key] -> getCarFromDB();
		$car[$key] -> calculate();
	}
	$_SESSION["car"] = serialize($car);
	$car = unserialize($_SESSION["car"]);
}else{
	$car = array();
}
if(count($car) <= 0){
	script("您尚未購買商品!", "index.html");
}


//print_r($car[$shop_id]);

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix"> 
    <!--mobile menu star-->
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <!--mobile menu end-->
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">購物中心</a></div>
            <div class="m_login"> 
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
                <div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
                <?php
					}else{
				?>
                <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
                <?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
            </form>
        </div>
        <div id="top_right"> 
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
			?>
            <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class="gray ">
    <div id="wrapper">
        <div id="member_menu">
            <dl class="clearfix" id="smbar">
                <dt><img src="images/member.jpg" align="absmiddle"/></dt>
                <dt class="email"><?php echo $acc; ?><br />
                    <span> <?php echo $email; ?> </span></dt>
                <dd class="selected"><a href="cart.html">我的購物車</a></dd>
                <dd ><a href="bought.html">已買到的商品</a></dd>
                <dd><a href="favorite_product.html">收藏的商品</a></dd>
                <dd><a href="favorite_store.html">收藏的店家</a></dd>
                <dd><a href="gold.html">購物金</a></dd>
                <dd><a href="member.html">會員管理</a></dd>
            </dl>
        </div>
        <div id="wrap-main" class="clearfix">
            <div id="bought_left">
                <div class="bought_table1">
                    <div class="tb_title">
                        <dl>
                            <dt>推薦人</dt>
                        </dl>
                    </div>
                    <div class="reference">是否想推薦本商品給別人？<br />
                        <form action="post.html" method="post" id="commend_form">
                        	<input name="shop_id" type="hidden" id="shop_id" value="<?php echo $shop_id; ?>" />
                            <input type="radio" name="commend" id="radio" value="0" checked="checked" />
                            殘忍說不<br />
                            <input type="radio" name="commend" id="radio" value="1" />
                            樂意之至，推薦人信箱
                            <input name="commend_email" type="text" class="re_box" id="commend_email" readonly="readonly"/>
                        </form>
                        <div class="re_btn"><a href="cart.html"><img src="images/last.png" width="96" height="34" /></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" id="commend_next_btn"><img src="images/step.png" width="96" height="34" /></a></div>
                    </div>
                </div>
                <div class="bought_table1">
                    <div class="tb_title"> <a href="javascript:void(0);">運送</a> </div>
                </div>
                <div class="bought_table1">
                    <div class="tb_title"> <a href="javascript:void(0);">付款</a> </div>
                </div>
            </div>
            <div id="bought_right">
                <div class="pay_table">
                    <div class="tab_title">店家 : <?php echo class_shop::getName($shop_id); ?></div>
                    <?php
						foreach($car[$shop_id] -> car as $item){
					?>
                    <dl class="sum clearfix">
                        <dt><a href="#"><img src="<?php echo $web_path_product."s".$item -> pic; ?>" /></a></dt>
                        <dd><a href="#"><?php echo $item -> product_name; ?></a><br />
                            單價：NT$ <?php echo $item -> sell_price; ?><br />
                            數量：<?php echo $item -> amount; ?></dd>
                    </dl>
                    <?php
						}
					?>
                    <!--<dl class="sum clearfix">
                        <dt><a href="#"><img src="images/p86.jpg" /></a></dt>
                        <dd><a href="#">創意能量手環/項鍊...</a><br />
                            單價：NT$ 299<br />
                            數量：2</dd>
                    </dl>-->
                    <dl class="type clearfix">
                        <dt>運送方式：<?php echo show_carry($car[$shop_id] -> carry); ?></dt>
                        <dd>NT$ <?php echo $car[$shop_id] -> freight; ?></dd>
                    </dl>
                    <div class="total">總計&nbsp;&nbsp;<span>NT&nbsp;&nbsp;$<?php echo ($car[$shop_id] -> total+$car[$shop_id] -> freight); ?></span></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end--> 
<script>
carItemShow();
</script>
</body>
</html>
<?php
$db -> close();
?>
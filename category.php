<?php
include_once("_config.php");

$rows_ptype1 = class_product::getType1();

$type1_id = get("type1") != "" ? get("type1") : $rows_ptype1[0][coderDBConf::$col_ptype1["id"]];
$eq_id = get("type1");

$rows_ptype2 = class_product::getType2($type1_id);
$type2_id = get("type2");
$type1_name = $rows_ptype2[0][coderDBConf::$col_ptype1["name"]];
$all_product = 0;

$rows_top_product = class_product::getTopProduct($type1_id, $type2_id);
$rows_product = class_product::getProduct($type1_id, $type2_id);

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script type="text/javascript" src="scripts/jquery.masonry.min.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix"> 
    <!--mobile menu star-->
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <!--mobile menu end-->
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">Q點購物中心</a></div>
            <div class="m_login"> 
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
                <div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
                <?php
					}else{
				?>
                <div class="top_login"><a href="member.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
                <?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
            </form>
        </div>
        <div id="top_right"> 
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
					$member_id = $_SESSION["session_925_id"];
			?>
            <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" >
    <div id="wrapper">
        <div id="wrap-menu">
            <?php include_once($inc_path."page/menu.php"); ?>
        </div>
        <div id="wrap-news"><a href="#">STORE</a> &gt; <a href="#"><?php echo $type1_name; ?></a></div>
        <div id="wrap-main" class="clearfix">
            <div id="column_left">
                <ul class="category_list type2_list" >
                    <h3><?php echo $type1_name; ?></h3>
                    <li class="selected"><a href="category_<?php echo $type1_id; ?>.html">全部商品(<span class="all_pro">11</span>)</a></li>
                    <?php
						foreach($rows_ptype2 as $row_ptype2){
					?>
                    <li id="<?php echo $row_ptype2[coderDBConf::$col_ptype2["id"]]; ?>"><a href="category_<?php echo $type1_id; ?>_<?php echo $row_ptype2[coderDBConf::$col_ptype2["id"]]; ?>.html"><?php echo $row_ptype2[coderDBConf::$col_ptype2["name"]]."(".$row_ptype2["total"].")"; ?></a></li>
                    <?php
							$all_product += $row_ptype2["total"];
						}
					?>
                </ul>
            </div>
            <div id="column_right">
                <div id="m_category_menu">
                    <div id="smbar" style="position:relative;">
                        <ul class="clearfix type2_list">
                            <li class="selected"><a href="category_<?php echo $type1_id; ?>.html">全部商品(<span class="all_pro">11</span>)</a></li>
                            <?php
								foreach($rows_ptype2 as $row_ptype2){
							?>
                            <li id="<?php echo $row_ptype2[coderDBConf::$col_ptype2["id"]]; ?>"><a href="category_<?php echo $type1_id; ?>_<?php echo $row_ptype2[coderDBConf::$col_ptype2["id"]]; ?>.html"><?php echo $row_ptype2[coderDBConf::$col_ptype2["name"]]."(".$row_ptype2["total"].")"; ?></a></li>
                            <?php
								}
							?>
                        </ul>
                        <div class="more"><a href="#"><img src="images/more.jpg"  /></a></div>
                    </div>
                </div>
                
                <ul class="big_p">
                	<?php 
					foreach($rows_top_product as $row_top_product){
					?>
                    <li><a href="prduct_info_<?php echo $row_top_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $eq_id; ?>.html"><img src="<?php echo $web_path_product."b".$row_top_product[coderDBConf::$col_product["pic"]]; ?>" /></a>
                        <div class="prd_name"><a href="prduct_info_<?php echo $row_top_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $eq_id; ?>.html"><?php echo $row_top_product[coderDBConf::$col_product["name"]]; ?></a></div>
                        <div class="price">NT$ <?php echo $row_top_product[coderDBConf::$col_product["price"]]; ?></div>
                    </li>
                    <?php
					}
					?>
                </ul>
                <div style="width:100%; height:1px; float:left;"></div>
                <ul class="smail_p">
                	<?php
					foreach($rows_product as $row_product){
					?>
                    <li><a href="prduct_info_<?php echo $row_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $eq_id; ?>.html"><img src="<?php echo $web_path_product."b".$row_product[coderDBConf::$col_product["pic"]]; ?>" width="226" height="226" /></a>
                        <div class="prd_name"><a href="prduct_info_<?php echo $row_product[coderDBConf::$col_product["id"]]; ?>_<?php echo $eq_id; ?>.html"><?php echo $row_product[coderDBConf::$col_product["name"]]; ?></a></div>
                        <div class="price">NT$ <?php echo $row_product[coderDBConf::$col_product["price"]]; ?></div>
                    </li>
                    <?php
					}
					?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end--> 
<script>
carItemShow();
$(document).ready(function(e) {
    var all_pro = "<?php echo $all_product; ?>";
	$("span.all_pro").html(all_pro);
	
	var type2_id = "<?php echo $type2_id; ?>";
	if(type2_id != ""){
		$("ul.type2_list li").each(function(index, element) {
            var _type2_id = $(this).attr("id");
			if(type2_id == _type2_id){
				$(this).siblings().removeClass("selected");
				$(this).addClass("selected");
			}
        });
	}else{
		$("ul.type2_list li:first").addClass("selected");
	}
	
	var eq_id = "<?php echo $eq_id; ?>";
	if(eq_id != ""){
		$("ul.menu_ul li").each(function(index, element) {
            var _eq_id = $(this).attr("eq_id");
			if(eq_id ==_eq_id){
				$(this).siblings().find("a").removeClass("current");
				$(this).find("a").addClass("current");
			}
        });	
	}
	
});
</script>
</body>
</html>
<?php
$db -> close();
?>
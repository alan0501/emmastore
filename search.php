<?php
include_once("_config.php");

$keyword = (post("keyword", 1) != "") ? post("keyword", 1) : $_SESSION["keyword"];
$ptype2_id = get("ptype2_id", 1);
$_SESSION["keyword"] = $keyword;

$rows = class_product::getSearchProduct($keyword, $ptype2_id);

$total_pro = (count($rows) > 0) ? $rows[0]["total"] : 0;

$rows_type = class_product::getSearchType($keyword);

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix">
    <div id="m_nemu" >
        <div class="mm_txt">
            <div class="mbm">
                <h3><a href="member.html"><img src="images/m.jpg" border="0" align="absmiddle" /></a><span><a href="#" class="m_close"><img src="images/header_close.jpg" border="0" /></a></span></h3>
                <ul class="clearfix">
                    <li class="m_menu1"><a href="index.html">&nbsp;</a></li>
                    <li class="m_menu2"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu3"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu4"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu5"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu6"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu7"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu8"><a href="category.html">&nbsp;</a></li>
                    <li class="m_menu9"><a href="category.html">&nbsp;</a></li>
                </ul>
            </div>
            <div id="m_footer"><a href="#">網站使用條款</a><br />
                <a href="#">隱私權政策</a><br />
                <a href="#">免責條款</a><br />
                © 2014-2015 kagin. All Rights Reserved </div>
        </div>
        <div class="mm_bg"></div>
    </div>
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">購物中心</a></div>
            <div class="m_login">
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
				<div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}else{
				?>
				<div class="top_login"><a href="member.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
                
            </form>
        </div>
        <div id="top_right">
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
			?>
            <div class="top_login"><a href="member.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class=" white">
    <div id="wrapper">
        <div id="wrap-menu">
            <ul class="clearfix">
                <li class="menu1"><a href="index.html" >&nbsp;</a></li>
                <li class="menu2"><a href="category.html">&nbsp;</a></li>
                <li class="menu3"><a href="category.html">&nbsp;</a></li>
                <li class="menu4"><a href="category.html">&nbsp;</a></li>
                <li class="menu5"><a href="category.html">&nbsp;</a></li>
                <li class="menu6"><a href="category.html">&nbsp;</a></li>
                <li class="menu7"><a href="category.html">&nbsp;</a></li>
                <li class="menu8"><a href="category.html">&nbsp;</a></li>
                <li class="menu9"><a href="category.html">&nbsp;</a></li>
            </ul>
        </div>
        <div id="search-news">你搜尋了「<?php echo $keyword; ?>」，共有<?php echo $total_pro; ?>商品。</div>
        <div id="wrap-main" class="clearfix">
            <div id="column_left">
            	<?php
					if($total_pro > 0){
				?>
                <ul class="category_list">
                    <h3>搜尋</h3>
                    <li class="selected"><a href="search.html">全部商品(<?php echo $total_pro; ?>)</a></li>
                    <?php
						foreach($rows_type as $row_type){
					?>
                    <li><a href="search_<?php echo $row_type[coderDBConf::$col_ptype2["id"]]; ?>.html"><?php echo $row_type[coderDBConf::$col_ptype2["name"]]; ?>(<?php echo $row_type["t"]; ?>)</a></li>
                    <?php
						}
					?>
                </ul>
                <?php
					}else{
						echo('<ul class="category_list"></ul>');	
					}
				?>
            </div>
            <div id="column_right">
                <ul class="smail_p">
                	<?php
						if($total_pro > 0){
							foreach($rows as $row){
					?>
                    <li><a href="prduct_info_<?php echo $row[coderDBConf::$col_product["id"]]; ?>_<?php echo $row[coderDBConf::$col_ptype2["ptype1_id"]]; ?>.html"><img src="<?php echo $web_path_product."b".$row[coderDBConf::$col_product["pic"]]; ?>" width="226" height="226" /></a>
                        <div class="prd_name"><a href="prduct_info_<?php echo $row[coderDBConf::$col_product["id"]]; ?>_<?php echo $row[coderDBConf::$col_ptype2["ptype1_id"]]; ?>.html"><?php echo $row[coderDBConf::$col_product["name"]]; ?></a></div>
                        <div class="price">NT$ <?php echo $row[coderDBConf::$col_product["price"]]; ?></div>
                    </li>
                    <?php
							}
						}else{
							echo("<li></li>");
						}
					?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end-->
<script>
carItemShow();
</script>
</body>
</html>
<?php
$db -> close();
?>
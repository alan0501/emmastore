<?php
include_once("_config.php");

if(!isLogin()){
	script("您已經登出系統，請重新登入!!", "index.html");
}else{
	$member_id = $_SESSION["session_925_id"];
}
$row_member = class_member::getInfo($member_id);
if($row_member){
	$acc = $row_member[coderDBConf::$col_member["account"]];
	$name = $row_member[coderDBConf::$col_member["name"]];
	$fb_id = $row_member[coderDBConf::$col_member["fb_id"]];
	$phone = $row_member[coderDBConf::$col_member["phone"]];
	$email = $row_member[coderDBConf::$col_member["email"]];
	$address = $row_member[coderDBConf::$col_member["address"]];
	$pwd = $row_member[coderDBConf::$col_member["password"]];
}

$rows_order = class_order::getMemberOrder($member_id);

getCartInfo();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once("model/meta.php");?>
<title><?php echo $web_name; ?></title>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/css_678.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="js/site.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/coder_member.js"></script>
<script src="scripts/member_comm.js"></script>
<script src="scripts/shoppingcar.js"></script>
<script src="scripts/order_comm.js"></script>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body>
<div id="header" class="clearfix"> 
    <!--mobile menu star-->
    <div id="m_nemu" >
        <?php include_once($inc_path."page/m_menu.php"); ?>
        <div class="mm_bg"></div>
    </div>
    <!--mobile menu end-->
    <div id="top">
        <div id="top_left" class="clearfix">
            <div  class="m_sub"></div>
            <div class="wrap-logo"><a href="index.html"><img src="images/logo.png"  /></a></div>
            <div class="wrap-mame"><a href="index.html">購物中心</a></div>
            <div class="m_login"> 
                <!--<div class="top_login"><a href="login_mobile.html" >登入</a> | <a href="register_mobile.html" >註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>-->
                <?php
				if(!isLogin()){
				?>
                <div class="top_login"><a href="login_mobile.html">登入</a> | <a href="register_mobile.html">註冊</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
                <?php
					}else{
				?>
                <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a><a href="cart.html"><img src="images/m_cart.png" border="0" align="absmiddle"/></a></div>
                <?php
					}
				?>
            </div>
        </div>
        <div class="top-search">
            <form name="search_form" id="search_form" action="search.html" method="post">
                <input name="keyword" type="text" class="search-keywords" id="keyword" placeholder="商品名稱"  autocomplete="off" value=""/>
                <input name="search" type="button" class="search_btn" id="search" value="送出"  />
            </form>
        </div>
        <div id="top_right"> 
            <!--top_cart star-->
            <?php include_once($inc_path."page/top_cart.php"); ?>
            <!--top_cart end-->
            <?php
				if(!isLogin()){
			?>
            <div class="top_login"><a href="javascript:void(0);" class="login_lb">登入</a> | <a href="javascript:void(0);" class="reg_lb">註冊</a></div>
            <?php
				}else{
			?>
            <div class="top_login"><a href="member_<?php echo $member_id; ?>.html" class="member_center">會員中心</a> | <a href="javascript:void(0);" class="logout_btn">登出</a></div>
            <?php
				}
			?>
        </div>
    </div>
</div>
<div id="wrap" class="gray ">
    <div id="wrapper">
        <div id="member_menu">
            <dl class="clearfix" id="smbar">
                <dt><img src="images/member.jpg" align="absmiddle"/></dt>
                <dt class="email"><?php echo $acc; ?><br />
                    <span> <?php echo $email; ?> </span></dt>
                <dd><a href="cart.html">我的購物車</a></dd>
                <dd class="selected"><a href="bought.html">已買到的商品</a></dd>
                <dd><a href="favorite_product.html">收藏的商品</a></dd>
                <dd><a href="favorite_store.html">收藏的店家</a></dd>
                <dd><a href="gold.html">購物金</a></dd>
                <dd><a href="member.html">會員管理</a></dd>
            </dl>
        </div>
        <div id="wrap-main" class="clearfix">
            <div id="bought_left">
                <?php
					$start_sno = "0000";
					foreach($rows_order as $row_order){
						$orderdetail_start_sno = $row_order[coderDBConf::$col_order["sno"]];
						if($row_order[coderDBConf::$col_order["sno"]] != $start_sno){
							if($start_sno != "0000"){
				?>				
								<ul class="process clearfix" >
                                    <li >訂購成功<br />
                                        11/25 13:45</li>
                                    <li>付款完成<br />
                                        11/25 14:45</li>
                                    <li class="selected">出貨中<br />
                                        11/25 14:45</li>
                                    <li class="not">送達時間<br />
                                        -- </li>
                                    <li class="logistics"><img src="images/wl.png" width="26" height="15" /><br />
                                        <a href="#">查看物流</a></li>
                                    <li class="cost"> NT$ 998 </li>
                                </ul>
                            </div>				
                <?php
							}
				?>
                <div class="bought_table1">
                    <div class="tb_title clearfix">
                        <dl class="clearfix">
                            <dt>店家 : <?php echo $row_order[coderDBConf::$col_order["shop_id"]]; ?></dt>
                            <dt>|&nbsp;&nbsp;&nbsp;訂單編號 ：<?php echo $row_order[coderDBConf::$col_order["sno"]]; ?></dt>
                            <dd>物流編號：<?php echo $row_order[coderDBConf::$col_order["express_sno"]]; ?></dd>
                        </dl>
                    </div>
                    <?php
						}
						if($row_order[coderDBConf::$col_order["sno"]] == $orderdetail_start_sno){
					?>
                    <ul class="bought_list clearfix"  >
                        <li class="bought_pic"><img src="images/p86.jpg"/></li>
                        <li class="bought_name"><a href="#"><?php echo $row_order[coderDBConf::$col_product["name"]]; ?></a><br />
                            <div class="w_price">單價 ：NT$ <?php echo $row_order[coderDBConf::$col_order_detail["price"]]; ?><br />
                                數量 ： <?php echo $row_order[coderDBConf::$col_order_detail["amount"]]; ?></div>
                            <!--<div class="refund"><a href="#">我要退換貨</a></div>-->
                        </li>
                        <li class="return">
                            <div class="w_re"><a href="#">我要退換貨</a></div>
                            <!--<div class="unitprice">單價：NT$ 299<br />
                                數量： 2</div>-->
                        </li>
                    </ul>
                    <?php
						}else{
					?>
                    <ul class="process clearfix" >
                        <li >訂購成功<br />
                            11/25 13:45</li>
                        <li>付款完成<br />
                            11/25 14:45</li>
                        <li class="selected">出貨中<br />
                            11/25 14:45</li>
                        <li class="not">送達時間<br />
                            -- </li>
                        <li class="logistics"><img src="images/wl.png" width="26" height="15" /><br />
                            <a href="#">查看物流</a></li>
                        <li class="cost"> NT$ 998 </li>
                    </ul>
                </div>
                <?php
						}
						$start_sno = $row_order[coderDBConf::$col_order["sno"]];
					}
				?>

            </div>
            
            <div id="bought_right">
                <div class="pay1 pay_type"> 購物安全、有保障，付款超方便
                    <h3> <img src="images/pay.png" width="237" height="95" /></h3>
                    我們採用最安全的線上刷卡服務，讓您付款快速又有保障。沒有手續費，還可以累積 Pinkoi 紅利點數喔！ </div>
                <div class="pay2 pay_type"> 找不到 ATM 嗎？超商繳費更便利
                    <h3> <img src="images/ibon.png" width="222" height="25" /> </h3>
                    只要在超商輸入代碼，就可以列印帳單直接繳費。我們還提供「7-ELEVEN 交貨便」及「全家店到店」物流服務，今天寄件後天取件，安心又放心。了解怎麼使用 </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--pc footer star-->
<?php include_once($inc_path."page/footer.php"); ?>
<!--pc footer end--> 
<script>
carItemShow();
</script>
</body>
</html>
